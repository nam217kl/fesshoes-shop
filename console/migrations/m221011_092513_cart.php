<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092513_cart extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%cart}}', [
			'id'                => Schema::TYPE_PK . '',
			'user_id'           => Schema::TYPE_INTEGER . ' NOT NULL',
			'product_detail_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'quantity'          => Schema::TYPE_INTEGER . ' NOT NULL',
			'created_at'        => Schema::TYPE_INTEGER . '',
		], $tableOptions);
		$this->createIndex('fk_user_id_cart', '{{%cart}}', 'user_id', 0);
		$this->createIndex('fk_product_id_cart', '{{%cart}}', 'product_detail_id', 0);
	}

	public function safeDown() {
		$this->dropIndex('fk_user_id_cart', '{{%cart}}');
		$this->dropIndex('fk_product_id_cart', '{{%cart}}');
		$this->dropTable('{{%cart}}');
	}
}

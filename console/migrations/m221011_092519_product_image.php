<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092519_product_image extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%product_image}}', [
			'id'         => Schema::TYPE_PK . '',
			'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'image'      => Schema::TYPE_TEXT . ' NOT NULL',
			'position'   => Schema::TYPE_INTEGER . ' NOT NULL',
		], $tableOptions);
		$this->createIndex('product_id', '{{%product_image}}', 'product_id', 0);
		$this->insert('{{%product_image}}', [
			'id'         => '20',
			'product_id' => '1',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657767659752_7b8744f08f9066d491d643a33f02cf5d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '21',
			'product_id' => '1',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657767978546_af0299883a6ce253fc00122c27e02b56.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '22',
			'product_id' => '1',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657768292118_35b5466aec4333e78db017a567d7a605.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '23',
			'product_id' => '1',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657768492153_f1af189c90c5fd42ce6dd4c35fa46852.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '24',
			'product_id' => '1',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657769927766_a08012ff35422a544f6c4b9e3fd1529d.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '25',
			'product_id' => '1',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657769959980_1c6fee249839bf47b193ead0dfd06d71.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '26',
			'product_id' => '1',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657770032400_b58e0936f40509695aa8e2eacae33b16.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '27',
			'product_id' => '1',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657770130055_a34391ba6e860aae056f47b49ab9c3a0.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '28',
			'product_id' => '1',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657770155248_c8fdf443e457431f77f727206db740ac.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '29',
			'product_id' => '1',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657770195815_f9d63b24ee754baa78327f278e26bdf9.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '30',
			'product_id' => '2',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3658054000434_549bc640ffd4b479828654ec6688c7f5.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '31',
			'product_id' => '2',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3658054063378_bee50ca6ed82494ec79aaee56c6d44a0.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '32',
			'product_id' => '2',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3658054082609_ba2906a1c6a82411d346d81eb1dec6f8.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '33',
			'product_id' => '2',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3658054113365_3fdf7d838ab823e388dde584c0fee2fb.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '34',
			'product_id' => '2',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3658054345006_6d44c97e499fc2e8886771d3a1fbb49d.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '35',
			'product_id' => '2',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3658054374304_92c7bd9fdc76c717a571c8c852f5f2ea.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '36',
			'product_id' => '2',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3658054420853_6cfae2679c114bb1158139ae44d4ef14.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '37',
			'product_id' => '2',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3658054459298_9ff65209748a324ebe2e515747f29b5d.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '38',
			'product_id' => '2',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3658054531271_4691f5c8b52a60f4666573a75111a826.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '39',
			'product_id' => '2',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3658054574439_68b6180871b62f7928ff7b79bc953ded.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '40',
			'product_id' => '3',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728941474867_a45b099a0c98cbbcc039c91bdcc5a20d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '41',
			'product_id' => '3',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728941477141_ebed11d7fef500d6048990cbeacd849d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '42',
			'product_id' => '3',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728941480610_0357de323c01eaa18e0e5908ddae80c2.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '43',
			'product_id' => '3',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728941484735_5f42a02dcd2590335b8ee3fbae994051.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '44',
			'product_id' => '3',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728941513837_e1f4584e94cb609ecb5f7eaeba519ccb.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '45',
			'product_id' => '3',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728941513864_8c89a8edab90ec2a5f46639b8716d82e.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '46',
			'product_id' => '3',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728941513877_c63b6421c2e493071165a59a25f2629d.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '47',
			'product_id' => '3',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728941516142_6ac757535314505b168bb4a1ca905289.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '48',
			'product_id' => '3',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728941518485_4f05f60ac3812d9aa926b99525d3f91b.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '49',
			'product_id' => '4',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3658023882870_74b4d8160f02e65723f2cb3dacde8555.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '50',
			'product_id' => '4',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3658023885603_e8c53e585aa4779acfda7c421a848be3.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '51',
			'product_id' => '4',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3658023889188_c99b8e60a6cff8a757326747d77eec85.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '52',
			'product_id' => '4',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3658023891972_ded47ff436b76fe607d563ffca45a132.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '53',
			'product_id' => '4',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3658024067680_5acbccf11e69d555e4eabe45390919fc.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '54',
			'product_id' => '4',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3658024088320_4c22714687bef6a21c08e8c5cdc75b4b.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '55',
			'product_id' => '4',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3658024125497_6df5cfcd373d36640126ba86fd22fc8c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '56',
			'product_id' => '4',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3658024157225_956317f6b4279928056a427f4e91e30f.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '57',
			'product_id' => '4',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3658024181413_143075efbb1d647c42cec1320759b47a.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '58',
			'product_id' => '4',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3658024212748_dcb727a6682292081b69d0707f7faaf4.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '59',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988336748_14ce8c3d5b7f44516d3e276a52d30cbe.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '60',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988341537_dad90213d89970af153644c1c3dddafc.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '61',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988346997_5e82e9927c52fb93d45b7f976962c197.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '62',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988398423_acee8b1858051ec867fd3ee41298c24b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '63',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988445460_513c5d13e8dab6ffd5b76532973558b4.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '64',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988715716_bdbe07d6f3e36669b3a712dea9009133.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '65',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988777151_35eb6596412f3e45e06b1e9b680f9e92.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '66',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988836366_f38e7edabb1fe0ecfd7333c59d5656f9.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '67',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988877211_fefbbbb722e0c19597db13803c087d36.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '68',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988919198_322c6eccb2cadf5cf0ac52f79f6dc781.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '69',
			'product_id' => '5',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3728988958742_e7164a685a12ea3157b3ce7f3dd58c47.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '70',
			'product_id' => '6',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728911930637_8a7936960b3383b33c69aac367b7e1cc.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '71',
			'product_id' => '6',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728911935575_d1ca45bb89bc9c97b13aadcadea1bc60.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '72',
			'product_id' => '6',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728911938924_eb56d35fcdae9871297886b90be8b10d.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '73',
			'product_id' => '6',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728911950311_2b70db70e682ffa2c00a22cce7187c8e.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '74',
			'product_id' => '6',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728912202781_42c3c4041554f2aa48fb3b1bf14fce26.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '75',
			'product_id' => '6',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728912247606_f60ab42e59c07e5b1342608fcd6d31b8.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '76',
			'product_id' => '6',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728912301473_1d9534dea54ee286951572b2ff781256.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '77',
			'product_id' => '6',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728912349666_4ff9b4b622bfe928f908167afa5c1c06.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '78',
			'product_id' => '6',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728912408732_841f586304a6f41bf1e9061817d5ecaa.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '79',
			'product_id' => '6',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3728912433287_646d6f050f2d0d610464a1cf54997643.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '80',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729007971241_faf05b5236e4fe070a9930b47bf71ecc.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '81',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729007974679_61dff303e30f695b979ab0ecf41202b9.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '82',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729007976315_07b2fc43ebce6a9bdc31a6bb794650c4.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '83',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729007980683_ab2c40c2079d8e36854fedc42d84ab5b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '84',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729008374340_4bb977478e5c1a8317ec94daa85c3279.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '85',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729008656745_80d485289bf7f64df5e61637be66ae3c.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '86',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729008731551_b773ecd630fe56812a5a31d227409c92.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '87',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729008761264_0230a048942abb1f4fc5c4addc0722f4.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '88',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729008825627_22613b1db3b4e3fa624daf4f166ede9e.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '89',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729008886164_fb153e25b48670e536167232c622fb27.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '90',
			'product_id' => '7',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729008913812_17d5af36cbe90e8f81da7ebad18eb05d.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '91',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665721937492_7085ca2aa13c38f2845b8138607468f4.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '92',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665721941390_43a986fb9d38cb00ef4e36fecd44d25d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '93',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665721946615_a7f7eef5da3177c2f145ce08864d00cf.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '94',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665721950080_e5d5f44a3761490e052306c85fcf21a7.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '95',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665721955536_93a5ad3e2e25b16a0543a0a8ac85c870.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '96',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665722157316_085f59812aaddf4d7b58fd0f17c2c713.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '97',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665722213601_a20d4017ff4640632f890e1f6b0bd475.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '98',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665722264711_c3839b3e3de2eccf116f8e8cbdd9cdbe.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '99',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665722292487_11a5ed194ad4be6e8c512b0e80bd41ab.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '100',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665722372496_621f01dc1978dade009028b05638f078.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '101',
			'product_id' => '8',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/07/z3665722403038_c48977f53b4713ba960825b120c4c351.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '102',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657913985424_6aca03ab77632b744ce42871fea54dac.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '103',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657913988116_91446df905c99f3982ec810a072094eb.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '104',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657913991624_f527913b7e7975b765026d24a5867c8a.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '105',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657913994680_3700b2bfa71320f50e369e26f1b368aa.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '106',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657914022600_648300370f02f9063fa4314cae44270f.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '107',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657914048337_dc10743f228aa3961b30d379aefbf0d7.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '108',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657914209055_aa2c21ceb792ee3a8029b6ade8a2a240.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '109',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657914267208_5bf1fe436d19c333a0de48e3a8b423ba.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '110',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657914311292_86319937523fe0ce8167af9e050b8c55.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '111',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657914323385_2efd1b33eaefad8fa19f8670a892d562.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '112',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657914327166_3c1a679cd5d3d9dc161a05e2f88a7edd.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '113',
			'product_id' => '9',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/08/z3657914333557_5962fae97219de958062f4a020d0ebe5.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '114',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670558702874_0a866457dce2575d00975823c3ddc52b.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '115',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670558706302_a74ea99323a11919b2fe8e4d982cb67b.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '116',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670558799055_55bf8b4e4ad5f4991ca2831ff140ccb2.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '117',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670558831570_65c68d5127b27b0b01a8e509f04f129c.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '118',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670558862734_ed11b66854f5f451794a362faebe2913.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '119',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670558894977_e45003737815f768b70b95cf644f8748.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '120',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670558928425_c3d7cd8ca93ad0e37ce3f6f9e0b8e4f5.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '121',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670558960630_f5f57bc7b4aad53eeb797e85e9cbe4aa.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '122',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670558967262_bcea90db48e55a344c3298a14c9a9e27.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '123',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670559026869_a5adacbae0810a3a94b782833c1618f0.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '124',
			'product_id' => '10',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3670559052422_89b80867fccb8eef2fb36ec0c5b29a4e.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '125',
			'product_id' => '11',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3656061608275_6aabcd14c3c861d1d453165068d4ea6f.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '126',
			'product_id' => '11',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3656061369978_5243082fddac5834d6f48275065d22f1.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '127',
			'product_id' => '11',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3656061365948_d0a7857714337aa753fcd81028f24787.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '128',
			'product_id' => '11',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3656061362167_c3930395eba3de69f3a4beda9fc5dc46.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '129',
			'product_id' => '11',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3656061358742_2051c1d20043accd998057950010c102.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '130',
			'product_id' => '11',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3656061353529_1dcf2009a2fca412c1ad520a0c3bf9fd.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '131',
			'product_id' => '11',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3656061350749_535254eae906598b76a5d1aadab8831f.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '132',
			'product_id' => '11',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3656061322194_54eb6c52b053a560a7578dcd42c4a5d3.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '133',
			'product_id' => '11',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3656061318130_0f40ba1713bc9c61b564737857847067.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '134',
			'product_id' => '12',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3656062369008_f4bb325a300b632325238d4339316d80.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '135',
			'product_id' => '12',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3656062360454_4b140b04f9b1d5a282862ab513c0accb.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '136',
			'product_id' => '12',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3656062357482_0bf27f2523e4b588caf9951754a3ae6e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '137',
			'product_id' => '12',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3656062353124_8942f3ff6d41d03d20c40b4d754d2d4a.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '138',
			'product_id' => '12',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3656062349307_7225448aa8c1abf5bdfc7f689242dafe.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '139',
			'product_id' => '12',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3656062345219_127cb9153ff8234fe64c7eca1681304e.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '140',
			'product_id' => '12',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3656062341383_c84698bbeff5a9cc06c7651e90804f28.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '141',
			'product_id' => '12',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3656062317117_8d1791937744a791f4ce5a968dcb02f7.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '142',
			'product_id' => '12',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3656062314030_7bc2db988c74b85e315c7e131b8492b0.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '143',
			'product_id' => '13',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/01/z3649755916732_a7f62622e53273afcd8d046a0e8ff4be.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '144',
			'product_id' => '13',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/01/z3649755920585_2c7b00bdb5d618aecc235a2c3f50a536.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '145',
			'product_id' => '13',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/01/z3649755927819_ddc7f765f4203f44c927bc334fd5de3a.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '146',
			'product_id' => '13',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/01/z3649756192663_96b854609c6e6e2774c18444ebb8d716.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '147',
			'product_id' => '13',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/01/z3649756245154_4c76d11a5235c7c6e5de775a5964c920.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '148',
			'product_id' => '13',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/01/z3649756302054_c47ce2964cab5b3720f674b9ee1b13b4.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '149',
			'product_id' => '13',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/01/z3649756361113_916e2eb3f3c09883a3900655ca52a364.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '150',
			'product_id' => '13',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/01/z3649756444443_5bfc598bb6e1e3a5bbe5b36acc5183d6-1.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '151',
			'product_id' => '14',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2868209230277_5bd7954ec1cb63f87287d9d9916dc7a6-scaled.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '152',
			'product_id' => '14',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2868209284067_4c57083ad3ebbfdbaa093e81c9b7be90-scaled.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '153',
			'product_id' => '14',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2868209292137_e84cf6ad49cc7ae13b5dfb5ddf208cdc-scaled.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '154',
			'product_id' => '14',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2868209345776_2393cc736acd9abfc859a0cce30ec2ad-scaled.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '155',
			'product_id' => '14',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2868209375282_705c28e24bd1665c156b283d5e4e24aa-scaled.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '156',
			'product_id' => '14',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2868209433924_44f962a83b12a7aa2d1df5d67fd20011-scaled.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '157',
			'product_id' => '14',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2868209453973_884e9caecf743dadb8a23ce099fbb845-scaled.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '158',
			'product_id' => '14',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2868209469779_2fffbd116de63bac14fc11dc82a11939-scaled.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '159',
			'product_id' => '15',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3668059142072_b57db0fad527eed79ccc97a151e82622.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '160',
			'product_id' => '15',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3668059148556_f896dfb839f48308bd77cd7d5504ea37.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '161',
			'product_id' => '15',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3668059151804_890c71b0c82c5ba2b4bf12c18af0b543.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '162',
			'product_id' => '15',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3668059156781_f8366060345516822acc1144373a3b9f.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '163',
			'product_id' => '15',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3668059163534_707f99fb7270a0d783307bd24fbb26f1.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '164',
			'product_id' => '15',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3668059191186_01d2173e8324c492b55ff8d7f0d9d7d5.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '165',
			'product_id' => '15',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3668059232936_c99f9ccd33b597641faf43e9d8a1a80b.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '166',
			'product_id' => '15',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3668059293064_33e45032680e6200ae64776c4ecfcdd2.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '167',
			'product_id' => '15',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3668059341889_777baf03be80eae198931c328ce03c70.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '168',
			'product_id' => '15',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3668059393105_9d9b038431fae5ed4fcc6fddf916b6bb.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '169',
			'product_id' => '16',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3665751943700_00d08bf68e5d81dccd09204f8e95ddc7.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '170',
			'product_id' => '16',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3665751948520_c8e92ba69799d71cd825f4eadc8d8c5d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '171',
			'product_id' => '16',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3665751954137_66dbd6ad6ebda851baca20dee1df6757.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '172',
			'product_id' => '16',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3665751996286_a61d95a2527dc421c179b130ecd4f210.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '173',
			'product_id' => '16',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3665752314464_678d4a32266d3ea299c047759f37e373.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '174',
			'product_id' => '16',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3665752360351_00efa3cdd9f022f30c11ed1bf8c991f5.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '175',
			'product_id' => '16',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3665752423474_aef44c096eefe3dc902bb74c5849c86f.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '176',
			'product_id' => '16',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3665752483695_fb49e23a6bd48a43065f63f3306ed366.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '177',
			'product_id' => '16',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3665752530980_04a5fa666955dfbc4cffdb7766a945e7.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '178',
			'product_id' => '16',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3665752559818_d63e43a3b33ae80b51f4822ab771b256.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '179',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670428710532_eb4c81e133cadd97a3d55bb7b98b8159.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '180',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670428719045_2da3720f4feb7f3f85d82f859c16e800.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '181',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670428723907_b36f23a7b1bebb28ff07ca0ccbe64a06.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '182',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670428732767_c9be8c673438f5d3bfa085ab7f0d37f9.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '183',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670428801759_adfd59a0e5b3ed53ed03fa4c4f310883.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '184',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670429033746_fce6c64aafdb40424e10f46f3ac74fce.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '185',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670429106149_7bb7721c6cf07529813affbd0f3bd300.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '186',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670429144161_d1edfc86ed89923a48ec5b177af15365.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '187',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670429185969_1e88bfb45c252e84134e304822f060c5.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '188',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670429240895_37fb0f06f3c1315426e939c762c3260a.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '189',
			'product_id' => '17',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3670429340478_107fc705a1b434f3e086798a9a4b4953.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '190',
			'product_id' => '18',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3665791058961_054406ec13ba77903f46e214589df60b.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '191',
			'product_id' => '18',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3665791063289_683d51f57393364d40c08db6608ae3ed.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '192',
			'product_id' => '18',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3665791068306_fa604e915ba33dea3cc2245e8122819c.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '193',
			'product_id' => '18',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3665791152270_7e127386459bff6fb23b70ed2e9e482a.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '194',
			'product_id' => '18',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3665791481443_337758369cbc404351fc3458177aab5e.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '195',
			'product_id' => '18',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3665791513687_80ba5bd3a4e15a52cce81be4280975ca.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '196',
			'product_id' => '18',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3665791565654_9f7149e3ebb4b3c36894bb481c931dc5.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '197',
			'product_id' => '18',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3665791634242_d4803d8873eedfa7a6a1169f86a8a371.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '198',
			'product_id' => '18',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3665791668488_32b4ed5e0f54aaecd8b76c16d83a658c.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '199',
			'product_id' => '18',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3665791704089_27abfe74d1626faf4c2c4bab462b6f62.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '200',
			'product_id' => '19',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668059964588_f6d8a3b8bf67b08c9b33e0ff332d7882.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '201',
			'product_id' => '19',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668059968913_7b819961b58774f77c5e9eea0e5a2dec.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '202',
			'product_id' => '19',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668059973843_c60a6997b469ab6c6ba7973f633ce672.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '203',
			'product_id' => '19',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668059977444_09a073e1baf33c4b5a5d44b69da42ce0.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '204',
			'product_id' => '19',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668059982161_f6c3c25f2720d32f301809666b760973.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '205',
			'product_id' => '19',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668060434336_eb0a73543046faa76eb1f8880ef7a89b.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '206',
			'product_id' => '19',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668060539890_d3135896882aaf959cd096e3d6098517.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '207',
			'product_id' => '19',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668060597257_6341c2a7476bc2935fda34703e1147a2.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '208',
			'product_id' => '19',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668060696071_20e9bedf34d420be971235d72b76b898.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '209',
			'product_id' => '19',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668060773739_767ba3f83c3f7347d6c066ff22bd0c75.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '210',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634969987199_179e8b78f929d60973ea6800699833ff.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '211',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634969997403_09f797d970b980df92e7079d642c0819.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '212',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634969999002_92dcaa3cae8513eb5b8cf67e6aa16f77.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '213',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634970043515_8df88b157d28af977cd1130dc1050d8c.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '214',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634970107281_f37e33478fd8fd198c12997be497eaee.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '215',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634970157709_52aa02f9c774f8ac537b9d0759793a3d.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '216',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634970202192_cc11e81eee052408396b9b4875075cbc.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '217',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634970242805_c63567e1fd31256641ed00719d76ca32.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '218',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634970308006_0b9a8b4e8ba730e5f9b090388ffe0a01.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '219',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634970375279_15a2071f3e40dd6bb05ff827b5d53eed.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '220',
			'product_id' => '20',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634970426815_ccaf3adb59d17784ebc18d0ad1b52b87.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '221',
			'product_id' => '21',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644835340046_e44d1aed85f0fcbafeac52e7a8899a61.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '222',
			'product_id' => '21',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644835343610_6354017b499282f34df14a9e1cbad14e.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '223',
			'product_id' => '21',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644835385400_f9d813846badac3055c880abcb80608d.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '224',
			'product_id' => '21',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644835501773_708e6528e329048c965e2786d5fe35d2.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '225',
			'product_id' => '21',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644835582590_027e4c15bab4734472a942ac5965b505.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '226',
			'product_id' => '21',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644835623748_5fac669127540df503d08938553dc570.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '227',
			'product_id' => '21',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644835651937_93be29ff3ca8c0f5d1e0affbbb50da55.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '228',
			'product_id' => '22',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644865800355_eb20a3f7dcf63e43203a9f8e600c701f.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '229',
			'product_id' => '22',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644865812067_ca010b7bdd22c120303bb3e6b01a8690.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '230',
			'product_id' => '22',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644866016567_3f7b08b7c077152a7b34bc4ca5f2fb2b.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '231',
			'product_id' => '22',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644866073048_481bfd1b06653bdb9f65e5a8c4b94821.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '232',
			'product_id' => '22',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644866113746_2df020877a4371e1929e1c7d72915391.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '233',
			'product_id' => '22',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644896733411_56b2ed34bc4d5c3050e4dbae018b1aa0.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '234',
			'product_id' => '23',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644896692068_631e4e5e3b28bc150c9885c7fdd79469.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '235',
			'product_id' => '23',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645147449877_429f008aca0bdb383994881ea80ac481.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '236',
			'product_id' => '23',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645147450999_34679f04b6b71bb131871b800480354a.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '237',
			'product_id' => '23',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645147470975_746113b91d756db166c093b88cfb77aa.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '238',
			'product_id' => '23',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645147897105_12b3422683bb9ba7ec8d3a4281302fa4.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '239',
			'product_id' => '23',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645147959914_a5a9661fd4fab1a3bc6fb6ddba392f88.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '240',
			'product_id' => '23',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645147961210_eaf5b54f0a66dcee2e4e6db5dc356e63.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '241',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668060909159_fcfaeb8c90730b41c81044148d60dcef.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '242',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668061014352_300d348834a98cb96606b124ec3ad82d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '243',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668061061833_67aff17478093e6dc42d6fc2963bea08.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '244',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668061172935_869cc55fd3f01dac6e50b3caddc2a00a.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '245',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668061248436_47fef66a917faa6429e7b6f11d188db9.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '246',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668061539445_a27f3245ecaef685d11cc3f6caac0505.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '247',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668061609703_7caafeeda071f7b7a49023784743f262.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '248',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668061661291_206c82f2aad5e47dec3a653917ff2622.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '249',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668061709335_b27a0f586ab407df3a1b49c078d6d86c.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '250',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668061790380_f20a264d5972ca2abc4d07fd4fe1f23a.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '251',
			'product_id' => '24',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3668061831677_5e49d405f3a86dc1ea2e5641f0fffba5.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '252',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655673261902_40410c86526bc7c758a36beba9ce9339.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '253',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655673321431_55bcb9215ad45ddc295cb7e132a61488.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '254',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655673376442_e89ebe426c0a80f887033c3a8415e4c1.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '255',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655673468499_f07aaeabbd99691a5943adca2f9304bd.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '256',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655673518444_e61549657114677f58d84fec40e3d9ae.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '257',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655673820623_a3292d4cd338c74cc6397d7929d42154.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '258',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655673883368_e21923a21bcea74c42c47365e827e4a1.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '259',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655673930879_c781b4b47bb4986e470c2ac11b8995aa.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '260',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655674017779_1b245603682bc4631aea3bae61d8b73e.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '261',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655674047224_4d2c5db30bc5b57639e38708180a9e30.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '262',
			'product_id' => '25',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3655674095595_2c79cbab2275b69e5351d6e896bb6145.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '263',
			'product_id' => '26',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655673216753_9f01636f83f6d4b038514d1cd28e76fa.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '264',
			'product_id' => '26',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655673159433_220f261b27b23a78863b91eec6d27fa9.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '265',
			'product_id' => '26',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655673102008_c32f52d35270be1da665b97e6f2b8a1f.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '266',
			'product_id' => '26',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655673047318_c4d88f1b2f4339ac6bffdf9d72504ded.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '267',
			'product_id' => '26',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655672998867_04614992b5d01bc947919ce0391b1077.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '268',
			'product_id' => '26',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655672930964_72d30cc4bac22016395548a85e17c517.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '269',
			'product_id' => '26',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655672626109_dc3fd30c1068f27b66436dd8c413a00c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '270',
			'product_id' => '26',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655672592071_d8430bd2dfd8d641d51b49c205060268.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '271',
			'product_id' => '26',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655672585948_6db55c85ad9d158c7c5bc75e7904fa68.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '272',
			'product_id' => '26',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655672580528_f387566160c1c146cc5c58479c21cc10.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '273',
			'product_id' => '27',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652942704499_1ba35702834786457ad9e23b982e21e5.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '274',
			'product_id' => '27',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652942658042_805a2264d2b9e0326e541f00bf3cb734.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '275',
			'product_id' => '27',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652942621946_67303ea7e31fb3d61c17de6bb272f1a5.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '276',
			'product_id' => '27',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652942573339_7418f90f3a514723829bc8d2d2005738.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '277',
			'product_id' => '27',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652942524031_3cef7e1644063f0ec8042f4ddd6c3d17.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '278',
			'product_id' => '27',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652942472550_f11de76ad7c518b46c1e62fbb73339c7.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '279',
			'product_id' => '27',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652942349298_eeac5b9bcba5b4e0e71278dc916a666d.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '280',
			'product_id' => '27',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652942349173_0d82571f18e242517c5263abad2e5584.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '281',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004059207_b72021433798436f8e3f4bec279eaf07.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '282',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004061423_4f1d4157147658ee4fa86e55e17eac91.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '283',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004064023_fbd02b0c69204fae60322d0d4d2f7621.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '284',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004068252_f834b84be08faab0b2a4bd76244289c9.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '285',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004071088_0e4f522b8221fb64092d78a293aed358.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '286',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004073972_f9650bed5f8d37faaab8142c32b61d6b.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '287',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004091245_6307419f33b091afa3c1963a5c246d4c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '288',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004093846_105019467b480024fbb0eb522e6f79e3.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '289',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004097256_1c3a84551a17f6ef6bf327e5b4992585.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '290',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004101067_88f11910731d83d66a95653b428ce443.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '291',
			'product_id' => '28',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3678004103533_27cce83741aaf928ff5a592b08718b58.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '292',
			'product_id' => '29',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3678002920720_2ea3e47cba5c9e724277c3bf672c8020.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '293',
			'product_id' => '29',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3678003128105_9b6fe7d2074165b62ad29a337a40a573.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '294',
			'product_id' => '29',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3678003143872_2ec5abc18f0beb3d5ca7a8342a6d3d9a.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '295',
			'product_id' => '29',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3678003187217_9d13396e87191c421c69d3cfcb36a3a4.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '296',
			'product_id' => '29',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3678003229112_b67b19c5f7efaba0edcf88ada801668f.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '297',
			'product_id' => '29',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3678003272782_65134edf727bebf889db02ee3ea0c920.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '298',
			'product_id' => '29',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3678003315730_b886b67d4bc70060d5528712ce1661de.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '299',
			'product_id' => '29',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3678002910892_8bd12fdd35acb53204c4e60a9753ef2f.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '300',
			'product_id' => '29',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3678002913658_3a2cd8687a8a0567405c7eb4830a0b37.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '301',
			'product_id' => '29',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3678002916930_ad05b307acab2c692b7e7620a6ab8af3.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '302',
			'product_id' => '30',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/11/z3652675369005_5dcb76b8f4ff615e889e8e79857f6842.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '303',
			'product_id' => '30',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/11/z3652675372952_f67471e458910f2235f3e210cccf8c06.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '304',
			'product_id' => '30',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/11/z3652675376550_d8f0a78814d3022c3f17c27a48425eed.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '305',
			'product_id' => '30',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/11/z3652675400007_320919bb5722e7a42547d6e738b53eb8.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '306',
			'product_id' => '30',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/11/z3652675437893_6ddadeb31dc96c4e3e6ce686b0534728.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '307',
			'product_id' => '30',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/11/z3652675460524_ee18923a3f5695ad0589c73bd00b41c8.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '308',
			'product_id' => '30',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/11/z3652675504002_e10bb0415c28faaeeef9c56d83527a11.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '309',
			'product_id' => '30',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/11/z3652675512996_dcc56b80f258a14e9e1c21636017712f.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '310',
			'product_id' => '30',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/11/z3652920134938_a7b77b8121737151ab198ac513d61306.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '311',
			'product_id' => '30',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/11/z3652920198116_ec584bf0a011cbfa9bf91a86687ace7b.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '312',
			'product_id' => '31',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657629985799_9b28e43d2699d92e8955046964642c8b.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '313',
			'product_id' => '31',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657630041985_b895bd1799787b035db1349f960e69c9.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '314',
			'product_id' => '31',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657630079590_b246c2310b70356a229bc58dddb06e6e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '315',
			'product_id' => '31',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657630122514_87dc5f7ede09115785722dbf65e43895.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '316',
			'product_id' => '31',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657630195366_e53ab2d9f347c1b33b5d6f7abe5fccc8.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '317',
			'product_id' => '31',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657630558338_f731192aefed9ee88f442b06cca0b2ed.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '318',
			'product_id' => '31',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657630613998_e49eda4467c5adff079a8e9843a0148e.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '319',
			'product_id' => '31',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657630693752_e800aa00bfdce7dcf672bd8e1f8d94c8.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '320',
			'product_id' => '31',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657630722086_797a4779fd6ddfbfdc2bf2af303693b1.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '321',
			'product_id' => '31',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657630777875_2b52bbd21dbf581f678ae8218c84c1ee.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '322',
			'product_id' => '32',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652925418832_7951f1ff55feefbc0423fbc78db60a34.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '323',
			'product_id' => '32',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652925422818_320d27a44236a68fa02ae07f3552b5c6.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '324',
			'product_id' => '32',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652925457548_97f9a797a4cd558f9d46742414c40215.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '325',
			'product_id' => '32',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652925492768_834fdcaff26cfee8ba56c6caef91221b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '326',
			'product_id' => '32',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652925787230_828edd14b6e8f09da6fa4cf5da71d211.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '327',
			'product_id' => '32',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652925828806_1ab7974c885120701127dfee9f5e8958.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '328',
			'product_id' => '32',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652925886760_0738d65c3bf1ef11c69fb99d9cedaf78.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '329',
			'product_id' => '32',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652925892696_af92121830d96cb2cc2da08c9137e3d8.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '330',
			'product_id' => '32',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652925900518_15ddfa00f7449cd2e65c820c679dc025.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '331',
			'product_id' => '32',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652925912198_21513f1b533c3a8dcd7a9699bb4f8da2.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '332',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652923423843_010498c1221de4318cd4c2951cd0b65e.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '333',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652923386105_1f599c0b0d86907b043908a4f3640a5d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '334',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652923307615_a8dbfa9af678b20b9413141de829f0f2.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '335',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652923296665_402dafe943a6c2611c2b9ce9be978e95.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '336',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652923196223_8de6f5720ffdb8ec03a46b715b52f6c1.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '337',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652923146997_e94c7860bf8bd8fe91b7f3251ff9c35c.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '338',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652922881689_aff10915c86375287279fe77469a7346.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '339',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652922877440_f7cb63848c2b4d43439c8d15983d7b92.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '340',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652922873177_2e0f78bf05e00a3f13a9db3965e1cd83.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '341',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652922868609_69fa14321aa671a3be9388f7cafb84c1.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '342',
			'product_id' => '33',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3652922865233_ca4d335ddc2bba20d0b4fad58eb8a961.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '343',
			'product_id' => '34',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3046233305146_2b5dd82b94adae54cba495e171e2448a-scaled.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '344',
			'product_id' => '34',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3046233308131_ff4be74f0684c307071dde1c0c996a7f-scaled.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '345',
			'product_id' => '34',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3046233318801_d436355f438dfbc12de67077c48102ce-scaled.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '346',
			'product_id' => '34',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3046233320110_463bcb275a472a93f54bf1d7f67856e9-scaled.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '347',
			'product_id' => '34',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3046233327051_00c3448be0e30f82084ff16001f4ffe5-scaled.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '348',
			'product_id' => '34',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3046233338616_bbc0ee4bd3df14360292943ba6bb43b9-scaled.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '349',
			'product_id' => '34',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3046233341763_7862f077f11d8a3ed908426d604f568f-scaled.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '350',
			'product_id' => '34',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3046233359905_ad91e2f939cfce9ed4f14c9c3991a19f-scaled.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '351',
			'product_id' => '34',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3046233360846_8b406bd7882905d9efbeecb04353f338-scaled.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '352',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652924816465_ec3c38770ce221b3b1e55a3a67281f76.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '353',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652924821326_501ce2ad25309dce6037aac0f315f86f.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '354',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652924824635_6f6846b8afce9c8c6482056ac1d31575.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '355',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652924829854_3b17fafea92ea7424b11b8834b0981e1.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '356',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652924835712_bd686c6618624f055e6d4b2e364afb3e.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '357',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652925022094_8326cb785415637fb7e22d88dd0f6d6c.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '358',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652925069249_7db841ff71c8d496fe6d27ba17951615.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '359',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652925125166_7d9b5721a2773c81ae1bc6532b1afc2d.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '360',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652925189907_523bb4fe42352ab2417efcedab06e2c7.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '361',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652925234473_0f915a9fd80c8c08802b583a7899efea.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '362',
			'product_id' => '35',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3652925325614_e8ceed15c1d8a0344ee1e826529dd476.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '363',
			'product_id' => '36',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644927791903_3f522a76e74f6bd52fb29657fe689068.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '364',
			'product_id' => '36',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644927792523_9c3a0ed1c2e80a72bc6fdf1e930bc8aa.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '365',
			'product_id' => '36',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644927795117_acc786cde7ace1f2909a32b289d740e0.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '366',
			'product_id' => '36',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644927798088_1149c4c13b1a5ad7f88cea855492d1cd.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '367',
			'product_id' => '36',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644927808707_8ff045ee28b535890d7ad31c0209c622.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '368',
			'product_id' => '36',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644927828571_8348e6402eddc1e5c4346de49af899a8.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '369',
			'product_id' => '36',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644927831738_78050da056715169091b755f7fbebd0b.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '370',
			'product_id' => '36',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644927891492_a7aef1c73b9dcb9b9e4d613fcdb3f2ba.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '371',
			'product_id' => '37',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3652924157810_8a6edbcd423c2001cc0974a66de9f27d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '372',
			'product_id' => '37',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3652924161711_1ac6ec933432701721bfdf3ab68ae561.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '373',
			'product_id' => '37',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3652924172232_46a72e4eec979e7f9c79bd2f4fca8840.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '374',
			'product_id' => '37',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3652924199508_61b39a1d7e904715cc4fe30a1bbda4ca.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '375',
			'product_id' => '37',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3652924202588_17b1fc7f16ca173bfe38968c5f6e45b8.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '376',
			'product_id' => '37',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3652924206906_4606661f40bf7f1b07d0056808763e57.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '377',
			'product_id' => '37',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3652924213173_446f33144a1230f9a591499566965a7e.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '378',
			'product_id' => '37',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3652924217168_e03755ea52f2af4a090ac8da8957ec29.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '379',
			'product_id' => '37',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3652924238848_79c197ef4950fef3afd9de5f909e015b.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '380',
			'product_id' => '38',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653019074954_b29324f6f3d585c2841e0c4ab04119bc.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '381',
			'product_id' => '38',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653019153362_8c7c4e0dde9fa85860ea93f8e2b499a7.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '382',
			'product_id' => '38',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653019206419_e22f32361d0cb96fef256c143a3e96ec.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '383',
			'product_id' => '38',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653019242584_04012f304fd25d39180a7179ab081368.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '384',
			'product_id' => '38',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653019307587_2ea8dfb56ed187019cd0fe645ea13977.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '385',
			'product_id' => '38',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653019639166_83f9cfe05b796aca4a8d147da866bad7.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '386',
			'product_id' => '38',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653019663388_04483a2fd45e781e9c485e5ce080cc8c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '387',
			'product_id' => '38',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653019713175_bcef8f1f95f85a622fcae82338358500.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '388',
			'product_id' => '39',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653018328542_cd15cc62bc5bf7155d972d0dc6fbafa1.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '389',
			'product_id' => '39',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653018339031_f75d8c73a5195eac819da8aab5489702.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '390',
			'product_id' => '39',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653018649725_2e09ec0d45bc1e1b802aad598b27d9c2.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '391',
			'product_id' => '39',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653018764694_363e36e3124a137ec74d552d5e249f86.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '392',
			'product_id' => '39',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653018836766_03733e2d017a05f3475a89f781daf00a.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '393',
			'product_id' => '39',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653018897726_a23faa458fba25c74f20931513019daf.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '394',
			'product_id' => '39',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653018944409_5bf4a09f8515ac6f56fc6e853bbbbbe3.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '395',
			'product_id' => '39',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653019008573_c74a94b4dead06fbac3877d3140880a5.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '396',
			'product_id' => '40',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653482187564_9932710b2b4ba3f02e23e9e878a48317.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '397',
			'product_id' => '40',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653482190523_53a7e4f86a17fce8c988aa527cca5235.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '398',
			'product_id' => '40',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653482194057_1aaf27fb5d76a576f266d2e89f0e8d5d.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '399',
			'product_id' => '40',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653482198241_caf231c2f9b8c7100148851e110df7f8.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '400',
			'product_id' => '40',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653482219509_fe9a746fb066f697edc337246dcadbd1.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '401',
			'product_id' => '40',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653482236493_04c3dd34de2a92cb3890b69502f5bd22.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '402',
			'product_id' => '40',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653482274152_9caa323d56cbef65ed68b909aaf8d1ef.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '403',
			'product_id' => '40',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653482279248_00fe33d348a4809a40e24b47a7d1813e.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '404',
			'product_id' => '40',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653482297149_c15939b3e67d076b58c44114b84b7896.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '405',
			'product_id' => '40',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653482332232_c2c941d545b10e92347f5027f74374e9.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '406',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653769669704_5e67d14760deaae3e2fdff3e23c8e352-scaled.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '407',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653769702545_3d5aeac94d9402075e821488132906e6-scaled.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '408',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653769716282_cde7d6faf7575a23671fd8895803237c-scaled.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '409',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653769756914_c11fe8070a2e2bca44ae4311eaca20ae-scaled.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '410',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653769797256_323cfa9b693357b935eedaeb4bb3cfff-scaled.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '411',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653769836806_4e7850aa1d0a123086dbd3b540d3608e-scaled.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '412',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653769859002_10112d8643a50ccc1b3793b1b49431a8-scaled.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '413',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653769971804_a5e382dcca164892ddd425689e8ce25e-scaled.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '414',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653769987072_bf77b0d2624495b4a2207f9db85df5e2-scaled.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '415',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653770008799_3bcf256563cd9ee678bc6249074e6f50-scaled.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '416',
			'product_id' => '41',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653770008830_307ab937dba5696324bd6769f18c708d-scaled.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '417',
			'product_id' => '42',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673797114287_e8583add59503e502cf0f32e72aee009.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '418',
			'product_id' => '42',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673797133077_bceb1088bfe2a3acf66efd2fdc718c40.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '419',
			'product_id' => '42',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673797173489_b7d1fdc1ac847ab60203ffaaff38dac3.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '420',
			'product_id' => '42',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673797466942_3d884c896b2341874d79a2c72c7f6bf8.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '421',
			'product_id' => '42',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673797734196_a57bdeed172fb20cce7a9966f6480822.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '422',
			'product_id' => '42',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673797923434_b48331658e0aa65468c990a64052dd5d.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '423',
			'product_id' => '42',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673798060352_35a9ffaa5556d3eb026b9082d234ee20.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '424',
			'product_id' => '42',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673798089772_e39970e22c60a1a83746fd655e34ade6.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '425',
			'product_id' => '42',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673798161752_818fd2b07890a8d1faf5a275a08c8994.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '426',
			'product_id' => '42',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673798232618_e18255871a9cf3eecf2d0c19837d857b.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '427',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3647649107546_a58e1c6c847fd122e317d73677fe49e5.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '428',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3647649111865_4210e8e8402141aca3c228be54103a9d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '429',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3647649115378_734794536707ddedee3cab2581bd7b9f.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '430',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3647649133401_7e4bcf9a8f4dfa506a08ac8271416c6e.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '431',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3647649138273_c231eca4662270d418f2edcf09fe0528.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '432',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3647649147111_30b517e827e05f08b3b18b8e79eb769c.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '433',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3647649151194_162a0fbf725abacf01549a3781c683aa.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '434',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3647649156383_4c9a8cc467e1668b7b825734ad1780b9.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '435',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3601495526406_359055c6ce890f41e504f4d9f1d77f6a.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '436',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3601495792780_23406227ff69cfe81d7175c6ce2e3fa2-scaled.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '437',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3601495797178_dc4b33f85c1c49abe6abaf066c9cd25f-scaled.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '438',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3601495841068_1cedb76a55d0ad0ba6d2d0bfbc435c8d-scaled.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '439',
			'product_id' => '43',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3601495895909_03ef27c38b3b02e31be69c39464d8cf7-scaled.jpg',
			'position'   => '24',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '440',
			'product_id' => '44',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670756881521_a01d397395b1c756e54e5cd949473a85.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '441',
			'product_id' => '44',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670756886445_03b59b8d0293663ce348e415e69cef2f.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '442',
			'product_id' => '44',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670756890992_b72722d9200077742edad22cfc553369.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '443',
			'product_id' => '44',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670756993032_362f94144e1deafb1700d32d30081ac2.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '444',
			'product_id' => '44',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670757391242_429f3c5d1d4e93741aadc99f5c8c81a4.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '445',
			'product_id' => '44',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670757412698_9047f7247928d14b2d2c60c2b05d6175.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '446',
			'product_id' => '44',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670757499697_bcf07cbc6506cd4e37a78ec5cd1a2533.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '447',
			'product_id' => '44',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670757570470_245572cfccb7b34847d0819412219b78.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '448',
			'product_id' => '44',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670757611348_3d1163fe738c1d62056dab1db8cc0ca1.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '449',
			'product_id' => '44',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670757666359_2e781ac6368772c0da64a2e73ddb69fb.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '450',
			'product_id' => '45',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3324933636342_540889f3da4b5ce0347ac390d30d627f.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '451',
			'product_id' => '45',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3324933640654_e7aafd4177f64ed08e18d3ccf48179c6.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '452',
			'product_id' => '45',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3324933645373_cc7970bfec0b2feaae4e9145cb741f11.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '453',
			'product_id' => '45',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3324933649481_8e530a28157aebcc11c5c93bbdf9ae86.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '454',
			'product_id' => '45',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3324933663489_45f9903be479ffc4fc438400d6789bd3.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '455',
			'product_id' => '45',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3324933846301_448567190fd63dacd44b8b3883ff62fb.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '456',
			'product_id' => '45',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3324933872981_639cffe1311ed587a29697351fcc1004-scaled.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '457',
			'product_id' => '46',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3683228679123_36314d0e5e20120d032ad1f428c4f4ca.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '458',
			'product_id' => '46',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3683228679152_87cdb218bb4b73d5bbfe4adf7458de00.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '459',
			'product_id' => '46',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3683228975965_e3522238a5b72d5a96c4d38e8dae23be.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '460',
			'product_id' => '46',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3683229020841_3e950497fe5c0ddaa853e2b7ac3a9843.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '461',
			'product_id' => '46',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3683229138130_7aa2ae4171952b9d5b964c22c7bea65d.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '462',
			'product_id' => '46',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3683229189461_29ed5e3173257bf2e1347faca353da14.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '463',
			'product_id' => '46',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3683229245634_b8d079c2c4bd954d6f865c9ca7e33efe.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '464',
			'product_id' => '46',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3683229480025_6f2dafadec1e514fdbcf34a0278bb410.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '465',
			'product_id' => '46',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3324933654182_b74755b0e28f365dc108a1c8679d902c.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '466',
			'product_id' => '46',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3324933658924_e5e78d0329e11f6959359410ef21f8c7.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '467',
			'product_id' => '47',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683212123281_3877fa6e443a8617ca6466b9cf5559b6.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '468',
			'product_id' => '47',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683212153060_32c836e18e2957e051ba60ba6ee65212.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '469',
			'product_id' => '47',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683212235431_4c8c2961482affb9968ffc3ecd947987.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '470',
			'product_id' => '47',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683212289488_55d6a7c3012583e8ce18bc544825d2ec.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '471',
			'product_id' => '47',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683212384554_9a680a2ccb865522ac1e5d8ae3ccbe2c.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '472',
			'product_id' => '47',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683212459690_490b15b703182cbb5049583f22eea53e.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '473',
			'product_id' => '47',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683212680049_a62e441a84d0c64cdb7eaea365da1eaa.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '474',
			'product_id' => '47',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683212727941_07837971c594cd1f3beb76b9b64de17c.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '475',
			'product_id' => '48',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683278136588_874794cf8d91553180f3a9eae9b8084d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '476',
			'product_id' => '48',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683278162625_1d06bcdb05f92b962a5ab22b9408f6ab.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '477',
			'product_id' => '48',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683278513905_628a6e87d8a2b16fe5b963afc876e690.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '478',
			'product_id' => '48',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683278727836_58fcdb309d1bc9be1cdd654bfe0f47d6.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '479',
			'product_id' => '48',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683278806746_fc0ae3a9b1f4221f7fa43588fd5163e1.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '480',
			'product_id' => '48',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683278965882_e76e4666ad5295d1fc515a19606b7444.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '481',
			'product_id' => '48',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683278975543_7c35a9adadcdffe718b1c2e3f7d577d1.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '482',
			'product_id' => '48',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3683279003401_64747a8a47c460afb0909951cd9e9edf.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '483',
			'product_id' => '49',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3664818243111_040f35c125f8b398e3232bfaf94ff372.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '484',
			'product_id' => '49',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3664818310056_398e0d92ae9c95fd6d8ce95bb0e28857.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '485',
			'product_id' => '49',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3664818367578_398355a2f245305010fcd776b6791e40.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '486',
			'product_id' => '49',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3664818680283_f0b380a8da31dac8eaaf73ce4640c5de.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '487',
			'product_id' => '49',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3664818767773_7c2148c23044346bf5ee2ae4b760a9a9.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '488',
			'product_id' => '49',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3664818815257_b25413e44fe5ba0abc285391dab28cc6.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '489',
			'product_id' => '49',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3664818909414_b462395c62e3d444f867384769517d12.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '490',
			'product_id' => '49',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3664818961667_1ee29073b1ab5c3206814206de9744e8.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '491',
			'product_id' => '49',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3664819024085_d5637d552f441a987ae3d393eccaf74a.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '492',
			'product_id' => '49',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3664819080375_fd4dca337c3401d403184983981ca0f5.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '493',
			'product_id' => '50',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3663010461144_287c394211350e211a79d7b731e02947.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '494',
			'product_id' => '50',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3663010466699_4a88376977aede98b37f8b912422ee4c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '495',
			'product_id' => '50',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3663010472602_ebe52fa18cb4f400f1e100c17d4e8f61.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '496',
			'product_id' => '50',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3663010477830_0467ed7d48b635b8e528d0bf23c78b6f.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '497',
			'product_id' => '50',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3663010511667_c164be3560d9337881bb2d09e019d0ef.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '498',
			'product_id' => '50',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3663010517607_b4b3c2fdd9f3baa05e05b8e94083ffe7.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '499',
			'product_id' => '50',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3663010522951_bec6ac1f577af70263b785f7b3404dd2.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '500',
			'product_id' => '50',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3663010534914_1d323b8f50eca9fc41ed3fbaec6f0088.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '501',
			'product_id' => '50',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3663010561257_7192c0b34aa46a21893acee1ace7da2d.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '502',
			'product_id' => '50',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3663010633270_05824dbb5751a5a42b834e140ac800f5.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '503',
			'product_id' => '51',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3089071474398_c516ff48a555b1b67aa16e95d3ff7874-scaled.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '504',
			'product_id' => '51',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3089071479728_a58318db8843f861f88091511bc9e41a-scaled.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '505',
			'product_id' => '51',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3089071498471_69f2c1f45894174fb250cb7ed4e8c44c-scaled.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '506',
			'product_id' => '51',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3089071498472_6c07405aea38c434be2eedb5bfd1868b-scaled.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '507',
			'product_id' => '51',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3089071498473_0bc290b59a5b9fae6a1a5bbd36fb1b7c-scaled.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '508',
			'product_id' => '51',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3089071506232_6f1c6b849aa8dccc038afd82364f55e2-scaled.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '509',
			'product_id' => '52',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3663121985430_d12705cbd9e97139561611e9c30d1ded.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '510',
			'product_id' => '52',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3663121990266_3cfdd479571d09b1b06b1881592d711c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '511',
			'product_id' => '52',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3663121996748_70fba7a60565e8fef63c83b72b7fa69c.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '512',
			'product_id' => '52',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3663122002266_ba2d7c8269b926d68698201afca76a36.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '513',
			'product_id' => '52',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3663122367140_11b77419fb63bf4ffda2a232c15b2a3a.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '514',
			'product_id' => '52',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3663122418454_9fb11013200733beff51045b1b4264a5.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '515',
			'product_id' => '52',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3663122492370_8a72f239d595a5e860e2f946bdb2f304.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '516',
			'product_id' => '52',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3663122564395_4f5db3a6a40eeaf954d5f9a208e248ac.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '517',
			'product_id' => '52',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3663122591351_06ce8218991651d030bd35ef8f3b975f.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '518',
			'product_id' => '52',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3663122657322_acec945e690de73c56a5f2a13d49140b.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '519',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194213303_91f15067186cccf65c8d4a5577780927.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '520',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194221687_2d4ef771eb785db85125db161ba57584.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '521',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194222948_d62ef0327f1786eb8382b854aa27422b.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '522',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194228695_6c530666bd18bfe148c682629c9721e6.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '523',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194373727_f04958d60f3664112daf7b941bd99ede.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '524',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194390710_75a3edb5c497802b31965cb3b3ec6945.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '525',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194695122_bf09d24dbdb12c2a47875913cdf6aa44.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '526',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194801907_75cb8f4c6a31831a8ae1b42f2860c1b6.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '527',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194882397_d4b1bd56f80affe63c8683d1f0f24641.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '528',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194942604_3a363697ae24a2568c9e8c4bb449a3db.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '529',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668194992837_cbdf7bf6b12ac2515c42eeedc25fb185.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '530',
			'product_id' => '53',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3668195007518_a02cbad9f36b65e6d24bbcfe85f3f454.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '531',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227073138_5602bc7202d6287d4bbd668d8ccc7450.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '532',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227078310_2d0b5d47dd0ace3e8b1646c421edbcd0.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '533',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227082849_7e2639525611f8a77372260f58ec6079.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '534',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227089732_1ff707bfcc7056b05a34eb3780eb609d.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '535',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227098719_830502294bb165d54753763743e0f3ed.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '536',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227142834_114b8d2ea05a6225cc88f4d065d78fb4.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '537',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227468808_f7f675c6947121e1880cf6d149a7fa7c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '538',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227553461_e334428cdda12fb6359dd046aaca6fbf.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '539',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227581742_5cf0bfb6d82f88ad14658d1759f985f3.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '540',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227629114_bc4cab16c19cf3c82fed3de06cc78898.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '541',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227751270_a169ad127f420296d76ec552adbc798f.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '542',
			'product_id' => '54',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/04/z3668227773964_79a77fc72092354a44041b3b395cf8bd.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '543',
			'product_id' => '55',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3660101265337_245f40b322294d6f245d23cabb709a34.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '544',
			'product_id' => '55',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3660101268230_3f4b5635ccaf4f7e19d3df6ff8da9242.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '545',
			'product_id' => '55',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3660101271464_8d673ac46b7e91d74784f9f25c777408.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '546',
			'product_id' => '55',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3660101273936_5e9945b897189aa7faac5f88cdc866a0.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '547',
			'product_id' => '55',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3660101424300_b099000558f7d3b1fac3e40ac8f6d8e7.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '548',
			'product_id' => '55',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3660101451518_ab3bdc6cefcbf03d6c40978df3bbca90.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '549',
			'product_id' => '55',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3660101482165_57e397ee7ac6d651147e1bdc221c4d49.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '550',
			'product_id' => '55',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3660101514444_a376f14378ac7e920282ed9602f1895b.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '551',
			'product_id' => '55',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3660101541980_adf73e36a0b62b70f98e8ea039075c67.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '552',
			'product_id' => '55',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3660101567451_70251ca0d1cd07e00cf6b295b5e28b41.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '553',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480251432_2da4e9055a93bcdfd0237442a1fc9438.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '554',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480256793_6237a5257b1e9f4a326ca34c4d9fb67c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '555',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480261326_ccc5e0d74bb7333e3659d4f8025a5e00.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '556',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480267467_34887adbdd7ca74595e28fb2d8cdbeeb.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '557',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480287569_a310f02b5a843c040ca5117569e20867.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '558',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480351566_d2f69f949a1fdcd43a21a64c7149358c.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '559',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480638895_0c81dd47cccdd173fc9a0029d4939bf6.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '560',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480665223_97e9591df69a62048770762271bcb738.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '561',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480728515_5fbc877cbd2e6c37fe470488a1c9d5c4.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '562',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480775942_aeb322d1279c786b33fcc80b22835bac.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '563',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480821298_133c3000869da72fade21ca95a453872.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '564',
			'product_id' => '56',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/09/z3673480879663_122d470a69e9dd30af44d073c7c3ca47.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '565',
			'product_id' => '57',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673432910184_516b31e08bf5e0df2459c7e349b1163c.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '566',
			'product_id' => '57',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673432924940_17aff9d1a01c1dc851fa41caa0d776c9.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '567',
			'product_id' => '57',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673432987205_535929876954d0bd84c0c15da755af32.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '568',
			'product_id' => '57',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673433031564_3ad91e7ed69b7cb37b39ecb75683cd5e.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '569',
			'product_id' => '57',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673433278839_07d31eccc80ba459c20b67a795d02f9e.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '570',
			'product_id' => '57',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673433381470_63058b9326422dd3746cf76082d75f12.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '571',
			'product_id' => '57',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673433390147_f1e39a5cb4456203b3d3db07f29d4d34.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '572',
			'product_id' => '57',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673433537706_7c77035598ee20f44a1f4bde36d5c51e.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '573',
			'product_id' => '57',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673433541936_9730defc18aaf34619eed9a7834c1e0b.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '574',
			'product_id' => '57',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673433568801_cc5224b50cc481963806ac12a288c152.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '575',
			'product_id' => '58',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660282538888_b518395ab745b9282cf29d8f728881fa.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '576',
			'product_id' => '58',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660282542595_28084bd8ecc25df831410924225b5319.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '577',
			'product_id' => '58',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660282544840_182dd465f1b3ed45ffc691ca9f5bcc21.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '578',
			'product_id' => '58',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660282572656_dc34443d13f8ec3fcd5af161a0a57bd8.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '579',
			'product_id' => '58',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660282747011_8fc3f287885670d04860a8efb23ee6d4.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '580',
			'product_id' => '58',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660282780141_bd523e286cd0849dfe5f8d23e3742d2a.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '581',
			'product_id' => '58',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660282814055_528fa8e531d7b1fb372dac9b56af60fd.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '582',
			'product_id' => '58',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660282843946_d2af8a71836395fd1967e9b344ceecdf.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '583',
			'product_id' => '58',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660282888158_c0799aa440a0a0792773c7602b8818e5.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '584',
			'product_id' => '58',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660282908518_da50c6136addae97b9429be8a17871a4.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '585',
			'product_id' => '59',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660239402148_d9e206d1e967cc0cffd7fd283b268da3.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '586',
			'product_id' => '59',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660239406249_3cd88c18a39a974134aaf30af282f29c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '587',
			'product_id' => '59',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660239408571_62ab85c43d6be6d5bcf0a8307f488e97.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '588',
			'product_id' => '59',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660239411012_ec9457c312ac49126b634cf39494ebfc.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '589',
			'product_id' => '59',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660239510420_b20fef2dd5aebfad3ac65df2b0122b98.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '590',
			'product_id' => '59',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660239551142_f0ae971947e8cfaa52f552291402aa86.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '591',
			'product_id' => '59',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660239571455_2034b5f4be4eee433defd70eef7f801b.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '592',
			'product_id' => '59',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660239600220_cc861aa1472776d6e562b0406bc5c644.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '593',
			'product_id' => '59',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660239630939_8a23e9c1a35ac13d43906c1eef576e99.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '594',
			'product_id' => '59',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3660239673307_225adec28275f16cb073af4c6e7e86ff.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '595',
			'product_id' => '60',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660087355165_2521e8847bf327c1899ff65fc96c5873.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '596',
			'product_id' => '60',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660087359852_cd530f8627f716ea234ab13ddf871585.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '597',
			'product_id' => '60',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660087360521_99ce2ef38cd4b16cbb273e493b0477e2.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '598',
			'product_id' => '60',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660087364101_541c747bfcb12f211bfc9da4e298a23b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '599',
			'product_id' => '60',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660087411863_4ca3e04840bb9eda13657df308e6bcf3.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '600',
			'product_id' => '60',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660087438310_702dffc90ce4ab9236703d1ae2af4969.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '601',
			'product_id' => '60',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660087481962_9d12a826107b2c8eb90902448c74fc8c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '602',
			'product_id' => '60',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660087495713_89d6b7a7f1ba6ecd96bd3b77e7bbc3e5.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '603',
			'product_id' => '60',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660087523028_6dcbc7a870f2861c6705c6e006a8e214.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '604',
			'product_id' => '60',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660087556391_3ce025a64fa4d7194450c7ec60d760e2.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '605',
			'product_id' => '61',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660217355311_02e4f7b35e9cc95e659bcfe92da1f8e2.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '606',
			'product_id' => '61',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660217355319_d623bf268f488470cfb1043bc7aaa97f.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '607',
			'product_id' => '61',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660217376195_73ff459c6188301707ab20a0d029b2a8.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '608',
			'product_id' => '61',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660217410057_adf6306c7da2d24efd4f7993af4bd41b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '609',
			'product_id' => '61',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660217485461_e5eacde1ff09d5a204228ce1ecf96c2b.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '610',
			'product_id' => '61',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660217494590_26b4fb233b04c128b89f4bb8911e75a0.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '611',
			'product_id' => '61',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660217497719_18604febdd5618063cf03a212ec2344f.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '612',
			'product_id' => '61',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3660217504835_1e4a934ba26de4fac907f8dfcefcc7c1.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '613',
			'product_id' => '62',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655781361689_467b06cc2469eacff44637082156286d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '614',
			'product_id' => '62',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655781282106_d4924d742c8ffc56a567c0c7472c8559.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '615',
			'product_id' => '62',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655781173468_b41ab6751a19615363696627e1ed633d.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '616',
			'product_id' => '62',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655781098986_11d3e92fe85c2efe61fe64a199925a90.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '617',
			'product_id' => '62',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655781036951_b91d96da149975bf3197e547bee77819.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '618',
			'product_id' => '62',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655781012987_2db8a37f18c49521acbbf638600fb6f9.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '619',
			'product_id' => '62',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655780631184_ba93651d45a84103b6c31f10dff7f0d6.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '620',
			'product_id' => '62',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655780558003_d816eaa0bed7b0ae5a34217749938ab0.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '621',
			'product_id' => '62',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655780534266_cc25189f184634088b987d342016bd2c.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '622',
			'product_id' => '62',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655780527256_46e0bc71202a7b98896c9d12a0785ade.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '623',
			'product_id' => '63',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3647689201396_1422719dfca4de0d44bd132a6fd2aa61.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '624',
			'product_id' => '63',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3647689206691_471c3ce9b7f719559b929dc08ec5e786.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '625',
			'product_id' => '63',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3647689209869_6dfc68277f4c3342dd6bceba63fb530c.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '626',
			'product_id' => '63',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3647689224053_b79fd3134f628ba206ecff4b83af7114.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '627',
			'product_id' => '63',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3647689229896_c20944afaba8d2bf1b70b791d20fc0c0.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '628',
			'product_id' => '63',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3647689276622_cd908a58fc376707716540fb5b4490dd.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '629',
			'product_id' => '63',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3647689305122_cbe52d486a8ef1012bb5bf214dd53300.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '630',
			'product_id' => '63',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3647689379322_06609ab6da3baf86284a7b5413fbbd1a.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '631',
			'product_id' => '64',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660354684959_dcf4cd238a5e2745791aa1eebfc1fe74.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '632',
			'product_id' => '64',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660354766684_30f108b73d0d7491082717851fc773df.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '633',
			'product_id' => '64',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660354777372_5c78dde7ae1a645226cdd1e86ff3e06e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '634',
			'product_id' => '64',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660355079205_c4160902dc58447487234cb5a87688c6.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '635',
			'product_id' => '64',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660355102766_41ca634f9a148bcf1404bf5f80673fce.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '636',
			'product_id' => '64',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660355157534_03aa0b0a186d7a2232cb963ef3231eb9.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '637',
			'product_id' => '64',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660355201638_31ff289af22e31f2f4005745a0f74ae8.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '638',
			'product_id' => '64',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660355260020_444ed909dde9081551103b4a96948f0e.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '639',
			'product_id' => '64',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660355327189_206fece7b1d598604f4ea9c2ba5f4651.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '640',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155473128_9f4a5f123e9059d4575561f556471c5d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '641',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155475200_c2c9683293d45de267a9bbe0b159f205.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '642',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155477371_9611b53e42c642003d54492f99c42417.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '643',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155513865_25c65d8704583df467405a36cab43dab.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '644',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155522151_f049c049763bf9d20bce8622ff87ac7d.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '645',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155661852_4f430097057bfe65c676d529f3ead351.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '646',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155686703_de2ff7a5df4c525d8a5536f3c7c9d539.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '647',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155726588_33fd386782500d7b0d6e719f630253d1.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '648',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155752997_fa305d2ceb5abbbe75a081e0c76280b6.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '649',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155789762_0fc4fe90dd3bb17a0181f209b3dba4c9.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '650',
			'product_id' => '65',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3660155817981_e712b2e8e7c7a7fa14443419e55d084a-1.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '651',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660155863434_e1e90af4f073fe1619f6f38b050112dc.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '652',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660155908107_49958baf6cfe569a2a500e2bbf42b87d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '653',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660155920719_cde916619b647b5927f56298b478ad49.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '654',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660155950991_96ff044beb281c5072ecf8938295733c.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '655',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660155975317_bace35f8b703a5bbbc19deb4a3278b2f.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '656',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660156155016_ceecd0c161582c9a0c76faf044cf89ff.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '657',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660156167897_694d5c67767ac870743b6f43b6ebdb32.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '658',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660156196372_59ffe50925bc4b6fab70410a6605aeb6.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '659',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660156235872_2fa9eec74fad2965a41b3978b821b7b4.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '660',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660156257586_8e71d61800323446d2fae886c365cab1.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '661',
			'product_id' => '66',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3660156285172_2e1df6d93f8a7f3f70fd237d1fcfaf61.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '662',
			'product_id' => '67',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458739095905_2e1a9aa4d636c4904ead10624a68f0be.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '663',
			'product_id' => '67',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458739190815_a2c76dadf0fdc2b18956734c5fe54819.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '664',
			'product_id' => '67',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458739528290_33f06ff6c00e9f98739b6c8fff5e1bbd.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '665',
			'product_id' => '67',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458739933726_d7caaf6f73ee9dfa5352325d6efc8f1d.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '666',
			'product_id' => '67',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458740277579_011a6e59aee7812b7429c2ad1642bd8b.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '667',
			'product_id' => '67',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458757589977_cf955cd913fb2287fb831a304b41d006.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '668',
			'product_id' => '67',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458758021054_2fe7f8f73e9d02d942a9d32602d51561.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '669',
			'product_id' => '68',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649723149200_9c3878c43817101c6d9a293c5a2613a8.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '670',
			'product_id' => '68',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649723089942_686964e20c6228899c2e1be16ad4729d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '671',
			'product_id' => '68',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649723068298_2eac4967d34a32bc47fc51f2088148d1.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '672',
			'product_id' => '68',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649722974709_f31ecf013fce9aa36b8b166f94be4c0b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '673',
			'product_id' => '68',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649722910792_eff684155177509c66830098ff4402a0.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '674',
			'product_id' => '68',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649722869528_8f88fca294cc8e4b859d7794898dce9d.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '675',
			'product_id' => '68',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649722629948_215df04181213b92e02758156ff30a07.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '676',
			'product_id' => '68',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649722624568_df730ad89a5c8fb99e0fddf9182c9d2f.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '677',
			'product_id' => '68',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649722620825_c64443e67ea19fa7c90de5d05a3eeb0e.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '678',
			'product_id' => '69',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657995263470_7d3a028693e841c537fa0436d2783922.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '679',
			'product_id' => '69',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657995266872_a9bbbf390e883281846f7056e034c8db.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '680',
			'product_id' => '69',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657995275996_1a12247877ec72a34bd04a382af207fc.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '681',
			'product_id' => '69',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657995279777_e4fd820fb31374d2a3f92f67a160792e.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '682',
			'product_id' => '69',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657995429617_654010ca0db298269cba5981fcc18ad4.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '683',
			'product_id' => '69',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657995476941_a76945ba768c7ac2292d11a332e83d2e.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '684',
			'product_id' => '69',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657995495182_8aede489887109fed1a18615314342d1.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '685',
			'product_id' => '69',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657995524521_160ddc640f0769a6ecd0de5cd3f69781.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '686',
			'product_id' => '69',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657995550151_5e1e02b1b3bb397c0530fe7aae607945.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '687',
			'product_id' => '69',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657995580713_b02f2755c5a55eca6d6b176f5e2f75bf.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '688',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195078737_b2eae316d76202d5eb954d2496535ef1.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '689',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195081086_a5c60ddc4f0cfa54983212137e746912.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '690',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195083728_491bb9fba0c92570f7d9f6f7a9afd9d2.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '691',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195086933_18fa0330eb1a2ab566dd5ba541a7c794.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '692',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195146722_20cbcf7b00c96bde29de92a2f47ded0b.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '693',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195155022_e3c74482ab26f4e5ce9a864305afa8c9.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '694',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195156953_d8abb69250aa59d58296f69e31091e08.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '695',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195160182_d9026c5f41e254d47c5190ee274890b6.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '696',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195170183_3c62aa6781296f327fd2a590d3fbc044.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '697',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195205196_8c3867a63bb72af9b117c77f97cba903.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '698',
			'product_id' => '70',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3660195214570_531940e7b39be0902fd4cbc81f9c2ca5.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '699',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145721674_c46e8bf151512848e7fb0e75c66a87d2.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '700',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145727100_55ae735ca46c570b232cc8f9c3f9a59f.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '701',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145732446_4a7cf7bb0a56068b4aa37e1280c0a7a7.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '702',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145739129_395a9a9ddbbb9101c807071bf04439fe.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '703',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145743146_1fe0601aae6bae4655b186f30bd22e41.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '704',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145749242_396c9919937479e9772361fc7cb96d44.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '705',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145781632_e3efb292a5b3d926eb2c0ae2f478fe2c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '706',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145787872_5bde7db610b2b5b2b5d955c0cd30c771.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '707',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145791741_bddb95d4c4676869bd9308e687404574.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '708',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145796899_4a81ac1b1b64b28a23c82aca5153a8bf.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '709',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145802356_50f83db8d0e83b89106606f69b00619d.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '710',
			'product_id' => '71',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3668145807772_a5fef324be8a3932220af4103b5e4e34.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '711',
			'product_id' => '72',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649936273467_02dcb06144cee4cccfd740ffb7cc20ef.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '712',
			'product_id' => '72',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649936217530_512b5f48b8a71cc3a248d2e4dff39faf.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '713',
			'product_id' => '72',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649936186580_9bd60116251e0764706d9c7aca087c64.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '714',
			'product_id' => '72',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649936126900_be811641d14fc7f541b0c59d6e3ff947.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '715',
			'product_id' => '72',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649936105897_6c146d24094dd6dafe87b029d884c70a.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '716',
			'product_id' => '72',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649935937274_17ac54726c8994990489768054d7bf2d.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '717',
			'product_id' => '72',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649935932863_d0f945f4a9289e231b635ec6205b4ddb.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '718',
			'product_id' => '72',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3649935928421_ab675778bec4d4f9c6bf1f5d0e317758.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '719',
			'product_id' => '73',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3647689997198_2aba1420c75b0bbd57c52d029bf2082f.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '720',
			'product_id' => '73',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3647690002061_f6e803521e7a1268bca13327c255ba7e.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '721',
			'product_id' => '73',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3647690008169_d07857a0494335608fba50d797e79ff2.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '722',
			'product_id' => '73',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3647690021164_5464eb3580eb60dac0ee70c7f44ed44b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '723',
			'product_id' => '73',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3647690030362_9412cd5839be7d4c11d7c83484f3c6f5.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '724',
			'product_id' => '73',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3647690034041_ce5768b628c5e5de4997b474f5585adf.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '725',
			'product_id' => '73',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3647690038825_942f634b77188e8952253f55ea086034.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '726',
			'product_id' => '73',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3647694845008_84e7735ab2abde2fa36ddc784cfbe438.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '727',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653442887009_42cf9323306fce95cd704011c7648758.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '728',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653442887035_95c957d8d00973187400338d4edf7014.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '729',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653442887039_4e6f5529528b1ea89fc677d4eca0d650.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '730',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653442889898_e57331cb608b523acea752952f58b96d.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '731',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653442976141_536536ced4a21becd81e70ea1bd4c9f4.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '732',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653443014362_c8a86f8996d7bbf2328259ab6ef12e3a.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '733',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653443074339_ecf935c7b75124c9f89f7b8579d77013.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '734',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653443096636_4e87a560231defe84a0b579b6da5af34.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '735',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653443136566_229bfd8351fd30cf7165af4c8d790cc1.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '736',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653443168899_1ce679d696cd2ec02a6cc9e4fa12b719.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '737',
			'product_id' => '74',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653443216901_949fb7762c30b4f46919d473a0607019.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '738',
			'product_id' => '75',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3720910692737_0e4bc9d1d8205cef254d9c6a374383ab.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '739',
			'product_id' => '75',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3720910703955_994db6aa23c1a2bae5c832f5821e6679.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '740',
			'product_id' => '75',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3720910706168_78d5f06aca69920e87326e78851056b9.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '741',
			'product_id' => '75',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3720910765043_552b3e1410ee420f2050a3b98298da34.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '742',
			'product_id' => '75',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3720911065561_47fed4d17442c18a1c8028cbb7f73424.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '743',
			'product_id' => '75',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3720911101035_75bf535114c225965da6ae549958b30f.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '744',
			'product_id' => '75',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3720911168467_6e4b4b27bf6800296212fd603596a37b.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '745',
			'product_id' => '75',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3720911247235_4df18aaf3120fbb9f08f466e1066f5a5.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '746',
			'product_id' => '75',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3720911318212_c9c4700f5f97dfa1e246469c6b431949.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '747',
			'product_id' => '75',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3720911367576_683f615cf3e080403bdcba98bfc1d6df.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '748',
			'product_id' => '76',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730957327271_2731f21dfadf72f3df29d432e72526fa.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '749',
			'product_id' => '76',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730957647546_e8da41232be5fb455cee2d8a551b05d2.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '750',
			'product_id' => '76',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730957721958_cd482b614eb9443e2df341e78784b59e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '751',
			'product_id' => '76',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730958262490_efb20fb1895677c6a4c455b2f617ff0e.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '752',
			'product_id' => '76',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730958360507_ecb9f961b3bc1bc74c271c775246b9fb.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '753',
			'product_id' => '76',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730958427450_3796b0381a190d600d5fc7e72970efb7.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '754',
			'product_id' => '76',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730958504038_01e4a8e646ce6e621b2c7de8857397c2.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '755',
			'product_id' => '76',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730958564371_db0f2c548eb03d5fea1449300912b4b9.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '756',
			'product_id' => '76',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730958674090_ad786c41edd85f101751d49510cd2858.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '757',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742044435921_5abfdd8d8a3baad4e4e2d33503b6695f.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '758',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742044594928_31368c3bc5b149a047cb3cfb9210b949.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '759',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742044584494_126123a67143442501989e086fd2283a.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '760',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742044567223_d7b30a5c70e24cf93b829bc454eb0a16.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '761',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742044485894_3b2ef53487e50dc954be57ecdb0d1e34.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '762',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742044445225_efff33c3cd61ad236af914ee11dabb91.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '763',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742043792112_c0f7ae82f8bac08655a92b80e2e515f6.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '764',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742043731734_28197701e745ca6e3023c219b69aae97.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '765',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742043612798_70e3e8688be1185c47b197566c4c4828.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '766',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742043569584_1bc5d1b0ac285d3f97fad4a886a54847.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '767',
			'product_id' => '77',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3742043461285_c9873202eb06766aaefdc98b36e85008.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '768',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930369844_25aa28ec9ec4a6809aafc6637cf35d65.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '769',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930372868_fc9b0a897313c59f1c4298ae4d99ca3e.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '770',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930377984_a1f8c6b763a69a389fd4640e2631e9ba.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '771',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930381765_6c75f5924d3161ca55d488fe157214ce.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '772',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930383356_54dd7e803b679f18a02c3333a4b00145.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '773',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930495592_fe4ab1e67beee3fb7966ae1fb83569f3.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '774',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930572748_2a4ee6eddac26fec2cf6eb7dde506362.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '775',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930614510_b40626cdfa240a2b13cea47bd843f2c6.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '776',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930667943_e3442e3d55d577454ef4b02a8a9fe9a9.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '777',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930750786_4afb044662814b1db63dee0ec02e23cc.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '778',
			'product_id' => '78',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3712930775247_98f3e5ecc63c65a129ce1c7bc947f26d.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '779',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712953073488_1c8b155fca98471b9b72c86bae037830.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '780',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712953140972_56feb8b3738f9cd082124440b759fcf1.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '781',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712953189334_c9f988606a612b10d54be43265795148.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '782',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712953286903_e21959c67620d183878b998c36da0fca.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '783',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712953332643_930f3797be471134654e531b53c17593.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '784',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712953628811_5f59df578183cc59f1ba89a1cf9b12ad.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '785',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712953687875_3bcc39cc00153dcdbe196026baa85f18.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '786',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712953759632_040bc5d99ac71ca2afd1b6dbe2385069.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '787',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712953866259_df5e8dd309cc4a5e582ec7eff709774c.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '788',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712953918563_1896ea471b0986501feace516dae6c12.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '789',
			'product_id' => '79',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3712954005727_001f9a5ba6eb5d5bef44342cce214380.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '790',
			'product_id' => '80',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662916247773_c19a618ad048b81890bdd9ad9357bddb.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '791',
			'product_id' => '80',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662916254422_9be319cdf9cad92e5badb0ee72e56c24.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '792',
			'product_id' => '80',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662916259501_e30a58d49750f7466e0f39d3afb76deb.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '793',
			'product_id' => '80',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662916265621_bb0bf10f6a9d8c26601402ffaeb5c512.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '794',
			'product_id' => '80',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662916293300_c130ff718ba503da1fd01cd47b28c13b.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '795',
			'product_id' => '80',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662916298719_ab1e5322bc30ed710d6940997095e268.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '796',
			'product_id' => '80',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662916304002_661c2c7c9ee8bea45429fbe464a94071.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '797',
			'product_id' => '80',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662916321178_4c32725f36ebe5bf482e76ccdf0a6052.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '798',
			'product_id' => '80',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662916325631_02b8c83f4889759602b1caf7535ee07c.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '799',
			'product_id' => '80',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662918076841_331d2b7515ec2ceadb4c841864b2f632.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '800',
			'product_id' => '81',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3658276509422_fc1b854c98e74a6286ab883864064fb8.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '801',
			'product_id' => '81',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3658276513576_81ea767894bc245b9e9048507a67dc8d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '802',
			'product_id' => '81',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3658276518854_5262ed97ebe526118b9f30cf50b7c90f.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '803',
			'product_id' => '81',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3658276522588_3616eb821a2f88eb0b349b420bf97cc3.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '804',
			'product_id' => '81',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3658276788535_7e3b27ac0d363591e0d9abc8e659d336.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '805',
			'product_id' => '81',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3658276841914_5d8bdbbe4e59cf1adbf6aab57c06b1a8.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '806',
			'product_id' => '81',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3658276896590_07d8b4ff06781637037f3c1cf5bcd8bc.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '807',
			'product_id' => '81',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3658276941415_a593621d3619570f5defe30f6b12debf.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '808',
			'product_id' => '81',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3658277002025_10142521d42308d31775d2339c997e2a.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '809',
			'product_id' => '81',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3658277017247_e97fc6e02d5e6c438f5f37f576501efe.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '810',
			'product_id' => '82',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3658314753436_eef6425509aae39a424d50d8c14460b6.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '811',
			'product_id' => '82',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3658314782833_8b879e38cf51cc365f5cbd9916eb98a9.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '812',
			'product_id' => '82',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3658314819782_fa1f8291fc484603f1bc57a92b37d7ea.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '813',
			'product_id' => '82',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3658315072307_927d9f6b414f7fa178a04e097cf5ebd4.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '814',
			'product_id' => '82',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3658315109478_806498f1bf422c4018f266c6e6eefca7.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '815',
			'product_id' => '82',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3658315161040_b4c9a0e9e68ad75617e8aeddfedf3cf8.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '816',
			'product_id' => '82',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3658315237487_b2f8085f14569005d53a6498c21de13e.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '817',
			'product_id' => '82',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3658315240775_0fe22248f6fa6f8c2c608b28042b77b6.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '818',
			'product_id' => '82',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3658315268546_64b9ad42f201af3559acd22e14c8d10b.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '819',
			'product_id' => '82',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3658315314088_594adbe58b69f891e814eb22dfc8cdd1.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '820',
			'product_id' => '83',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3644897010861_47f49887ba9383df92fd98425107fc81.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '821',
			'product_id' => '83',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3644897352344_c20771c53ba8aac8902c2d84238afba4.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '822',
			'product_id' => '83',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3644897357483_6025f2f0e383059806701449b57be492.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '823',
			'product_id' => '83',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3644897360251_9cba44f47628f7bfc2bc4660b962a270.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '824',
			'product_id' => '83',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3644897594867_846769ee11f6e8ea9096255f175095ce.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '825',
			'product_id' => '83',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/03/z3644897622143_515eddc33c81e3eef4ebb905e782e7b6.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '826',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502101257_ef308d353e38ecbf4ac9bd28a4b4e271-scaled.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '827',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502353980_7edd6b32446772c157f36566929733d0-scaled.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '828',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502453010_e866ea20c7449f98d486f75c600c6b3b-scaled.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '829',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502493832_26dbdbb3a24925d15e0678d8978ffb9e-scaled.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '830',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502591022_29826fa18aed525a0693705b97dd144b-scaled.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '831',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502601138_0ec5b2a97b6a7012c513e49c4dd0c5a0-scaled.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '832',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502601142_81e743aff0a6e8274eca31591b32d68e-scaled.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '833',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502621128_53700b5ad235de62062fc491cb703f41-scaled.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '834',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502652171_9e28d1c39fc2c2a9367d549f711d9954-scaled.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '835',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502679608_a9e195c7c465bb73824463ba4ff284d6-scaled.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '836',
			'product_id' => '84',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3660502695591_1766c6f5b6d59952cb1e1d5c47d941f0-scaled.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '837',
			'product_id' => '85',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3662952319055_6b0b5fba29e44b4b10d49988afd69fb3.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '838',
			'product_id' => '85',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3662952322950_efcd6e83e080d93718f1d4361798e741.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '839',
			'product_id' => '85',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3662952328954_6e6d49840f8bedaf282a33a528a4ca6e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '840',
			'product_id' => '85',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3662952339776_4c256a1d82f372c585673a10cd22fa8d.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '841',
			'product_id' => '85',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3662952357776_2816a0886985556d55333c4f0be1e382.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '842',
			'product_id' => '85',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3662952368645_f51f39a794f400a34c357273d2095aaa.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '843',
			'product_id' => '85',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3662952374589_454b34fdba1c09cbf49c652742c606be.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '844',
			'product_id' => '85',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3662952391341_4cda2feb06c8fddf37f40d14a30ad97d.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '845',
			'product_id' => '85',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3662952424321_283a90f8adc79096853514b373364691.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '846',
			'product_id' => '85',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3662952485081_45bb7418c39491b89065e24495423b37-1.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '847',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3647648327398_1e466548ff3a1b1ca68e208c8afb54ac.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '848',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3647648336577_5579f92b8ede83ae5fa43e50bece75fc.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '849',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3647648350207_bd114c8af7122dd9c470f0c65f074dfc.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '850',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3647648358587_09dcbde164bad73068fbdebcd4fd6048.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '851',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3647648363769_c6af23ab729dfcab0ea93d8e97d17918.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '852',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3647648373125_4e6264446df937ba6dfe3a106cac4c6a.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '853',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3619797774488_41d8490b9a1ce76e40f9a455d786d845.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '854',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3619797779034_0d794a9eff7c7131d618a13b2906f26a.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '855',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3619797779036_bd450ef02b815065daa6786c88e159f4.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '856',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3619797779644_091f783fc74589dfa751edd85805cbd5-1.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '857',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3619797795264_249e39c7e04c2daadc2f390341b1c77e.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '858',
			'product_id' => '86',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3619797795307_29b65dd0bf5879af562f6794580f8c4e.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '859',
			'product_id' => '87',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3658232359296_6b83e52ccfeb685850bc71df653256ee.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '860',
			'product_id' => '87',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3658232363512_f0b5e3614e31e5fe5fc149b8e30913bb.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '861',
			'product_id' => '87',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3658232369495_1ecd2bf39aa684c4213ed01b8aa06010.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '862',
			'product_id' => '87',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3658232372882_90160f85d57b23415f1917024cb6509b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '863',
			'product_id' => '87',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3658232403198_7a1eda79ee870db7a222af35ac08c33b.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '864',
			'product_id' => '87',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3658232445622_c8b69f6131a3a5b38f887faac28c5bc4.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '865',
			'product_id' => '87',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3658232594569_c4933b130f5f22f352c71c1e58f2bac2.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '866',
			'product_id' => '87',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3658232649708_f8020bfc038d4a2dfc0d140cc6d5dfc2.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '867',
			'product_id' => '87',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3658232703325_8acdecab4aec199b7fb56e574df3b379.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '868',
			'product_id' => '87',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/06/z3658232796447_895a746357eed3ef3ac93332df435ab6.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '869',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673513937462_c42e20823d6210cf28ecbc0234a2ef79.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '870',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673513942878_0fc9d30c10ffc1826af868d5ee16c275.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '871',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673513948297_7fec96e6777f75a6063aed85f59b847b.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '872',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673513984648_537e7e8f45397b18d288a3da1815e2ca.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '873',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673514018008_df87c254cd5cd0405527ce852a67bd95.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '874',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673514306380_11f989d9c7def9019aecb2d2d1f8d756.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '875',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673514318864_051419095247291bd244381db3c7f8cd.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '876',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673514385837_4c2c5b7c1c0f8448ff4555ac47d5ae4a.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '877',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673514440080_b058be262729837cc72d214cc34222ee.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '878',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673514494546_555585dc3e4d0b4a159b5fa94ce13467.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '879',
			'product_id' => '88',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3673514540202_520958bcabb80da9a4dc4709e8dbae21.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '880',
			'product_id' => '89',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3658330884403_7105d2febf4915e2c4ad8df86305e3c3.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '881',
			'product_id' => '89',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3658330887782_ebefb4f47a4a8ef63a396e8449c9242c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '882',
			'product_id' => '89',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3658330892185_1a991df5fd0608862a7140ffeb8b83e0.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '883',
			'product_id' => '89',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3658330894976_f320c45c5d2226ab218419dc33ff14a2.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '884',
			'product_id' => '89',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3658331128219_25ffef0c50dc43171a1ab44f80289b15.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '885',
			'product_id' => '89',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3658331171021_8df7423eec6af368c24f1e555ac58853.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '886',
			'product_id' => '89',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3658331215634_babdc41c4dd06c1ec334b26a99edd44f.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '887',
			'product_id' => '89',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3658331261765_d11d9b93dcb158a15541360713ff3c79.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '888',
			'product_id' => '89',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3658331303159_513468f9481db4b6a4167fb21563a13d.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '889',
			'product_id' => '89',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z3658331328486_fdf83c422588d25f6735c37863d6acbd.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '890',
			'product_id' => '90',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657631611810_4c2ec2aed566bfeb640a88325f5b082b.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '891',
			'product_id' => '90',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657631546192_9d623de502a39f3893430912a9a4c4aa.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '892',
			'product_id' => '90',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657631509940_7ed406b0c8b89c1cbde2ab735e94b68e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '893',
			'product_id' => '90',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657631483196_3f1c1c70227d317cc5ba0110ce293424.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '894',
			'product_id' => '90',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657631369609_8d45a51e53f8dfc92180091f4768bcf4.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '895',
			'product_id' => '90',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657631345081_80a5d34c2bc9486fd5d16764bc482f42.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '896',
			'product_id' => '90',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657631122377_678ceed619c2da678845e5984c15515c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '897',
			'product_id' => '90',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657631119429_20826d3c12d0b50b0a4b6695d1808f9d.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '898',
			'product_id' => '90',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657631114956_5a7d9e67e656285a06f61a4f46c41c66.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '899',
			'product_id' => '90',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3657631107658_098ef38125165100e8572b6d34d69087.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '900',
			'product_id' => '91',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647798633855_f8a093293e0161cf79bcc797749989a7.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '901',
			'product_id' => '91',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647798715830_1d6b5b500f08da71099be714d0f96f9c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '902',
			'product_id' => '91',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647798747946_12ee215e2370fdbac14e0618c4c120d1.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '903',
			'product_id' => '91',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647798988123_836babed319905c86337a12bdf098ede.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '904',
			'product_id' => '91',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647799037081_b82f25eff976e35dbf71c27a5cbf04a6.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '905',
			'product_id' => '91',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647799146642_9eb95a3d29d5a21a8184426794466d66.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '906',
			'product_id' => '92',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655990328476_7a110dd6dab1791356ae4f4c6e8e9ee0.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '907',
			'product_id' => '92',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655990332449_1827c5fbc778e8853530644df2f00879.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '908',
			'product_id' => '92',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655990337661_cece6161fc07c1f76251adaccddf148d.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '909',
			'product_id' => '92',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655990346233_94a67ab039b746e03b2bb8518e10104a.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '910',
			'product_id' => '92',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655990623014_b1d258370dd50428e6321ff2cf679168.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '911',
			'product_id' => '92',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655990693247_d370d3372dfa375c1ed3fd027d7d9a94.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '912',
			'product_id' => '92',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655990745945_afda6e9866ea77e3145f89c71939ceeb.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '913',
			'product_id' => '92',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655990851796_10309cf7e5b4273c716c1db9a972696d.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '914',
			'product_id' => '92',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655990856553_02413ba7d956485d835148c00cfe365a.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '915',
			'product_id' => '92',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655990898445_70e5a1e6330305aa904b99da3f987987.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '916',
			'product_id' => '93',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655896900609_5745f463c8fe8ec0f27383601caaccc1.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '917',
			'product_id' => '93',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655896996733_921f3f0d318f9c92c966079f2ff2c37a.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '918',
			'product_id' => '93',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655897122855_e93ad5d023fd6d0cd0a2451f6a8460e8.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '919',
			'product_id' => '93',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655897135765_d3f53264619a41b4e2199ed3b4208b00.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '920',
			'product_id' => '93',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655897487940_9cf9e1e91aca7882b4400f65ca99dda0.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '921',
			'product_id' => '93',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655897531022_67d79f498a4e602099fa87ab5f232e12.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '922',
			'product_id' => '93',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655897597773_cbf46ef7449b76ecfce3ed6cf9a1d9aa.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '923',
			'product_id' => '93',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655897733277_941110892e5d49f18818187482791e0f.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '924',
			'product_id' => '93',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655897742291_abed4baa9c30305e45371f4de9f8d05f.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '925',
			'product_id' => '93',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3655897747157_97c292b192421041238412efb61bb0f1.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '926',
			'product_id' => '94',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3649890795148_f207ffcde907322f77c97ba730ad1bad.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '927',
			'product_id' => '94',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3649890791212_69b21aef0df56ff6450f74dd31234e18.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '928',
			'product_id' => '94',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3649890786523_f7941b0bd65e566cff474e2d85c459e4.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '929',
			'product_id' => '94',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3649890783440_542983300acfdad825bb004efa873d97.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '930',
			'product_id' => '94',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3649890775741_2d40b8f7f7eeb6af82cccfd1095eb083.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '931',
			'product_id' => '94',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3649890750428_a0af3604a5224eb96b24ddfb7ed7465e.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '932',
			'product_id' => '94',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3649890745943_8ac157793057ab168ba6b6ea2ac06928.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '933',
			'product_id' => '94',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3649890741028_3a216957c6afd396a9d2cd9d18e150e9.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '934',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829133408_4aeb221c70340d275cfd49b0182404bf.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '935',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829138551_417aa9012f8f3c1994b0a571d179dce8.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '936',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829143767_3c8058efcf547676711fe577d6ec6201.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '937',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829149485_3ba338cb9b785f34e6f69a0cd2d9a1d6.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '938',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829155610_9b1b480203a2bf564ceeea730684a7ac.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '939',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829302811_0ecb38f9a702250aa4c98bf3984c0565.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '940',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829333590_327783d6b354326723b7351ff0cb34c7.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '941',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829369532_0075b99860c8c03c7efb92b5dae48e6c.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '942',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829431174_2bacc44352fd68222571cef8cf32a8a2.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '943',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829437587_fde326b6b7f302b5bf34012fd2c58e79.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '944',
			'product_id' => '95',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662829464861_f1661ab02694d7d07e1838edca46707b.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '945',
			'product_id' => '96',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3649873602412_6f68cd6cf110ab12fc93b75272173791.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '946',
			'product_id' => '96',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3649873607800_93244264d51601b29701d18089e05fe3.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '947',
			'product_id' => '96',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3649873612796_4358c834e8aeff0b5fbca537484fce64.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '948',
			'product_id' => '96',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3649873747223_03d4a79226a8605575dc3ee16dd7cc76.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '949',
			'product_id' => '96',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3649873793021_10c5881cf2f678a6e54b8f56ac8cd959.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '950',
			'product_id' => '96',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3649873839139_84ccb3efdaa839ce8f8ed6ce516e79be.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '951',
			'product_id' => '96',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3649873885607_6912b5ea81fd5aa33c98ee0c9f30f604.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '952',
			'product_id' => '96',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/01/z3649873980875_6b34784648f624df482fab4c674f406c.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '953',
			'product_id' => '97',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658314267946_fe2a0744a1aa1c4e7070f4743871ddb1.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '954',
			'product_id' => '97',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658314272594_8edfb655f4796945f23c1807c24f2b6c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '955',
			'product_id' => '97',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658314276121_059eba9375dc8b270113bcbd7fc6d37e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '956',
			'product_id' => '97',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658314281944_e73cbc575154a0d57e4f29d4a4addbfe.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '957',
			'product_id' => '97',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658314465788_34c7fbfb3a2a9d9b2ac1c380e9573729.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '958',
			'product_id' => '97',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658314515389_f7d19605c297d417d6491a47a33d3c40.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '959',
			'product_id' => '97',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658314574964_27a4615fe29c2e2c86b8cba87be0ca22.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '960',
			'product_id' => '97',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658314654312_c8cb4d719f11b727a0334e5085680bed.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '961',
			'product_id' => '97',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658314662017_a65d6643e820bdda72ab58cabef0569a.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '962',
			'product_id' => '97',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658314683619_cb6a6aa98df175eecc183bdcb88c29b3.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '963',
			'product_id' => '98',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658185286252_e1938c184ca4468a993c170e7192d6e6.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '964',
			'product_id' => '98',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658185307656_fcfbf85d053ca9c7faf9ffa79e925c73.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '965',
			'product_id' => '98',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658185352942_62914c711fba959c0aec76c3fcf38637.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '966',
			'product_id' => '98',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658185404782_4ddd945eb1595a0ddf8f559ecb2ad892.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '967',
			'product_id' => '98',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658185676230_4189af91c2cb12c390ed88ae990dcd4d.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '968',
			'product_id' => '98',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658185742360_5c87510735d8f84fa084a888f4a2c7f9.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '969',
			'product_id' => '98',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658185762508_95970b574b3d1d2bff4f7da206ffeb7d.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '970',
			'product_id' => '98',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658185803457_f5543f680e5776471e32b35b19eef002.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '971',
			'product_id' => '98',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658185879274_368fcd732720d6af61a48595d3f67d3e.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '972',
			'product_id' => '98',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3658185929221_09b0c94edb4c7134cb85384dc09fa6ec.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '973',
			'product_id' => '99',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2870585283922_a4f8d4b68b882a120c7a9bbcbdcea88b-scaled.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '974',
			'product_id' => '99',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2870585262189_2a86ff820dc41a41700d6836c259811d-scaled.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '975',
			'product_id' => '99',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2870585220521_c9d1e4865790024fb7900bcfa6ead27b-scaled.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '976',
			'product_id' => '99',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2870585213770_99619081290bc8afe3951d4a6e68c917-scaled.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '977',
			'product_id' => '99',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/10/z2870585192383_86124b33de6f3ab08ce7d8a7f529b020-scaled.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '978',
			'product_id' => '100',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653373993039_9b286a4a804adc61919a00af798d23c4.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '979',
			'product_id' => '100',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653373997310_04d4b57eb972fe8166de75a50fddc330.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '980',
			'product_id' => '100',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653374001801_72b0c640b6b914025d4046b11cd51dbd.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '981',
			'product_id' => '100',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653374005905_c295d3817e27b44613f39384a018bb7b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '982',
			'product_id' => '100',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653374038278_1ecea99f2789def8d5e4735e16c8dfa7.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '983',
			'product_id' => '100',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653374042034_09bc35c8cd699d8ca9a1bdf70d2b9a58.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '984',
			'product_id' => '100',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653374063959_c70408390d401f8e05d7d8a04acbd8df.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '985',
			'product_id' => '100',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653374071018_6e16a4706f9b51fe36c3f0bdf2671c95.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '986',
			'product_id' => '100',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653374092134_75ac9c02755f8ed7f81fae0175a07f99.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '987',
			'product_id' => '100',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653374112899_b1a5c8b431f6c86f34bbbeadbb1a5f64.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '988',
			'product_id' => '101',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652980012598_818818543828ef8ab311d6a03d1ca34d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '989',
			'product_id' => '101',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652979989466_60002fb83d6fb8d83c0efb11c4fa5f37.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '990',
			'product_id' => '101',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652979969899_f5adf4391dfb50bfe40c4f3c3bf39cba.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '991',
			'product_id' => '101',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652979963447_12aabd9604edbd446afdf371dc1bdb62.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '992',
			'product_id' => '101',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652979959191_8c6ed6cc2966d7b8ab91ab179b426b37.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '993',
			'product_id' => '101',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652979954946_3c0d41c576f23935ff2e1a540f94f70b.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '994',
			'product_id' => '101',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652979925654_846ddea13d31c13600dafc7788a0214e.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '995',
			'product_id' => '101',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652979922068_6d0aa720dd532df10774e6730313da93.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '996',
			'product_id' => '101',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652979916767_888348c73319bd9ae9e902d105dfa6b3.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '997',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965158241_13f532e6047048b71035d48d8c65b79a.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '998',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965159871_387be2b1925d913a0c3e76ad76454991.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '999',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965165690_e11da3def5aa6935ab87cd1434c4ba23.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1000',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965167064_999f7f233cadb1e2d566a52ab6fe91ce.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1001',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965169557_26f4172c4b0524c63a4427d577a48d78.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1002',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965191310_14cdf66eed332ea48ef8643d1e7e5b3c.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1003',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965196211_6e533287db453b72bd940f48578adf87.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1004',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965199381_d0bf842cb69411d513c0a8ca2261b3bc.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1005',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965219437_1b1a46a780fce837072aaff176291e27.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1006',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965234617_8f0dabe9e6827162528cf2ffe43c2cf4.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1007',
			'product_id' => '102',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657965258904_93c70700ed8703b1533f83200cdaadb2.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1008',
			'product_id' => '103',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655896198865_c9734a1e6ff48e7fcd60cbbf31a36c65.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1009',
			'product_id' => '103',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655896237939_a4c33c8d92f731c6123827c1c6a914ff.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1010',
			'product_id' => '103',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655896554126_5c3f2107566f0b549005d1e76a4160b5.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1011',
			'product_id' => '103',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655896585635_9dda587046576501de9eae11dd4a43c2.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1012',
			'product_id' => '103',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655896648689_845f9960817b9a4bf949f7623296e469.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1013',
			'product_id' => '103',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655896701990_60df01b95618be07aaa3213853286511.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1014',
			'product_id' => '103',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655896740390_973f939b21e444bf72504fce6118ec0b.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1015',
			'product_id' => '103',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655896786769_43542df59f37f44a3bdade39d1e9d369.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1016',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940375963_8f48813ee5095114567c7f2d6860c3fe.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1017',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940387714_9bb5f4c04503b69e83c0bfaba5539b09.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1018',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940388945_769f96783ba95af25eb820dc3925f425.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1019',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940392174_f87c395ca68fdb51113dde3dedca6f20.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1020',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940398953_2da1fdc19ca14df66694d662254037eb.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1021',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940407285_d0650cd1e2309fa31568fe1ce1bbed3c.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1022',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940413528_77821f499bd0d551b86548066ed01462.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1023',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940417313_039b042f9d40c6d5f8ba6875831a1bfe.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1024',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940418284_237354bc3819acc1f35a26e22218ae83.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1025',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940428123_ba46b7bb78560987ae6949b8029eed1e.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1026',
			'product_id' => '104',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634940428233_001d293ad43114134b8d74c316a5c182.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1027',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938650154_25f3e284a8f64707ec49052630c5fcf5.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1028',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938650611_7697941942738b901c41a5de64fd22a4.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1029',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938653954_95ed51887a802c376bdd8b9438753d26.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1030',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938663895_26acfa3c4f5cc04559896ee5e62bc297.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1031',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938665816_2cc6fcd441a79ca596f6f9d97d4fb90e.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1032',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938672277_2eddde914def000ad987b654f4c431c8.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1033',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938678132_d8bea4de2261ee19ca10051f7c58f6c5.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1034',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938678469_d9a9d3e18746ae9eda2cf1ad73ed4905.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1035',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938686577_3b5911f858af385085414735b71cd4cf.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1036',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938694522_03622d95b9322680483e1471ddb1bb4f.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1037',
			'product_id' => '105',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634938698781_7f7b53fd66b6294cd6acc6ea312ececd.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1038',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690632695791_7d4844df8788e2b4187e0aa781f2ece7.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1039',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690632699478_cc7d4dc86c69dddb2bb54f777d620686.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1040',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690632702155_ae0332ffa6a2d6d62d132e49b9777aea.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1041',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690632704848_398e6c363978a8d2c9871edf9ae6e4ea.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1042',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690632707832_db26e51d26b2c3af7aa1a1a77980a2db.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1043',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690632713349_f056b93692c1d0606d78bc6226c63a35.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1044',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690633071404_59039a2ba7ac28f63e1e84bb2b6e8964.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1045',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690633107173_b19a5ba614bf3eea1602333b3c42c891.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1046',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690633165518_544cb1307b02ee7d2790a23032ffd368.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1047',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690633231671_7c4642a0e958dec79b2bd89284f39e8a.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1048',
			'product_id' => '106',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3690633276576_e7683987396f173178046c1eb8542f0b.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1049',
			'product_id' => '107',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3657716043799_b00086719e04ebf83b3891224b06df82.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1050',
			'product_id' => '107',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3657716047058_6d8e9ac582bba985fe4c660dfde337eb.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1051',
			'product_id' => '107',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3657716051701_0fbed8afa1048d6cb078403eddf7ac08.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1052',
			'product_id' => '107',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3657716317314_8b7eab128474aa9e492c179caed31efc.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1053',
			'product_id' => '107',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3657716317314_8b7eab128474aa9e492c179caed31efc-1.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1054',
			'product_id' => '107',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3657716382810_d6aab85312008325abc0a11ab93e74da.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1055',
			'product_id' => '107',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3657716432326_a1c7e521fb23276a80cd5ff5e3b048e8.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1056',
			'product_id' => '107',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3657716483254_825d0ca26c21180932c1f615f91377d0.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1057',
			'product_id' => '107',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3657716585649_7085de3ac694c611f6ff4ec50fa32c97.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1058',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658255244670_f269153ef8db0982e2cc94ee2f0a9882.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1059',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658255244671_64ef2b1fcb57116702c826468508d830.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1060',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658255342329_a21c693a7a011d0fd8c0911ea5730e12.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1061',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658255379769_6c315a011dba26b53862899eb874cff7.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1062',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658255428187_7a51d48e817bd65accf9eef32abc318b.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1063',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658255867045_127c8ca539eb46a208569cfbc10b3f97.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1064',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658256023816_c9eeb7c32f34e30996226026daefcf97.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1065',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658256074665_63a3d6022f83c9bc08db2c1d1e023b21.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1066',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658256159579_1337198b7bfaec6e32a16c814a31a8ec.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1067',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658256209461_7ece159f6312acfef1a75c500d641a56.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1068',
			'product_id' => '108',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658256254360_d5a78cc78942c27148be052dc485d56f.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1069',
			'product_id' => '109',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658206737885_da31b690cbd9c6fa638392e0677d3cee.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1070',
			'product_id' => '109',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658206742994_9d8812e9d8e4184ea39c24d5d7e0fa50.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1071',
			'product_id' => '109',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658206746422_c4bd5bcf2fff6584d6195ca0a35d332e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1072',
			'product_id' => '109',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658206752017_f8cdf20d4dcd4ca9e18c1d3c6c1adf79.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1073',
			'product_id' => '109',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658207081970_b268aef105b208d91eee4a2922051d9a.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1074',
			'product_id' => '109',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658207192145_56e03d95682c997a39047d7d53c1840a.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1075',
			'product_id' => '109',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658207274576_cb1f5ba48fccd48b606828d6a6ad8944.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1076',
			'product_id' => '109',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658207326159_8263f6cebbb763fc12d80d2415ff7511.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1077',
			'product_id' => '109',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658207389094_7bb3dfe67ef9d0dee137477f258ef01c.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1078',
			'product_id' => '109',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3658207510010_2e7ca5d0e2becdc112d8382c18f71536.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1079',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657770830591_0fc1ee0313b4107afc5a4c71ad92278a.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1080',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657770884155_afc7397ba6a89b49d1f33aed3f9c1277.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1081',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657770937105_f066beb3fdefd49fa8c5fce55db138cd.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1082',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657770989263_21668fc123feca11481ecc002b3a1cbd.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1083',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657771113599_5e7737984827d65d04bf40dd2df4c130.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1084',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657771305579_67fa886715039350835338502e278a7a.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1085',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657771400780_4ec7c16a514059c07d1863040da3e1b7.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1086',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657771447094_adec1a3c83ca7084995b64d06ed73aeb.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1087',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657771497915_206f1b68e752ccfd66cacdf8e5a50e52.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1088',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657771542707_b4714684fac5ffca198835f5735826e1.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1089',
			'product_id' => '110',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3657771593931_fae552bbcfa77741c6b7c6b5459cad97.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1090',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656012753178_16f6864d89ec12fc3e7d91cfe089def7.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1091',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656012757642_8cc46d45c74a6430d7de563264054f94.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1092',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656012761697_69f5915c559c51e939bd9605e607f7a9.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1093',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656012768001_a91efa8b0feb9a401de8f8b81c7213d5.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1094',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656012769174_c5b929f9322f13b25aa4b3af7d707428.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1095',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656013076716_996c4feb053bdb34cb4ea3e851327a41.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1096',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656013173679_8980e4e771d20b29374233b17a85a5ea.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1097',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656013177998_7ca66518cce6134c96b86dec3fc1d985.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1098',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656013225085_e078e31cf2094b695860ac9e9447edfb.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1099',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656013264863_8ec7afc6f36d21f36a591e7daf049b4b.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1100',
			'product_id' => '111',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3656013363578_660914a0db9130213d5fa2f25ace8974.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1101',
			'product_id' => '112',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655990948059_82c753b3205dd88decc7e7ab4e73ff7b.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1102',
			'product_id' => '112',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655991003059_93ef69aff95569ed2ddbaab8ea77f602.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1103',
			'product_id' => '112',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655991050063_0cf96039a7463520b4a0d6f4c774fbe3.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1104',
			'product_id' => '112',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655991157596_c482ddc96b18971d618c3d5002308ea7.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1105',
			'product_id' => '112',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655991455598_342266b6d73672217bfb33f322403640.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1106',
			'product_id' => '112',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655991539290_6facd64a419efd1544c6f785c8896523.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1107',
			'product_id' => '112',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655991564894_b5c265b4986db3801c409aaed90ee7e4.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1108',
			'product_id' => '112',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655991615336_7b33fb58140dc91884dce7dd87999e58.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1109',
			'product_id' => '112',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655991667803_75905f685e944540baae1e85fa53e424.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1110',
			'product_id' => '112',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655991721837_bdc8446db1be8618f5edd16b466006c7.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1111',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653382051097_76474f822a11f420c4ab60c76db0108d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1112',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653382041576_1385a107d7c9cce17852676a6b3270a5.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1113',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653382037903_1835d18479829c0ee481f8b4bcef9a7a.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1114',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653382033179_14172ce47eb41ab0c022829390d04d6a.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1115',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653382029318_768d2b6becb8b659b1ba1fd9fc81eef5.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1116',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653382024388_b71b53e078a302ac959fbf71b3528f81.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1117',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653382020813_53cbbf51d52baacc70e8422d9e96cb92.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1118',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653382016113_375ca27f70fa86356cffb9324e5d027d.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1119',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653381991073_8e16a2858fe715ef1dc663c771637bdb.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1120',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653381986333_dddc5e68d850ffbd8fa0e083903245ef.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1121',
			'product_id' => '113',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653381982910_1de897a1b4bf6919c48dd477098042b6.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1122',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939190489_e451e89f3d6ae7469cf921007b3a9bef.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1123',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939193665_eba0fdf734632e71d1e7c34045cb90e2.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1124',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939196019_12462b55e133cf74fb491e53c7c8ce82.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1125',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939205246_c8b1a8a45d12823bbc9c3d6b7bd61b5b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1126',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939208896_9913fe978a416c3a6d39037d5e118627.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1127',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939222566_9b381c41b23d45c3e4b5e18ef78bd726.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1128',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939267684_16a9a6e432270d5a90da784f8905ab96.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1129',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939312729_16de9ed40d193be2d0e3f7a909c48cfc.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1130',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939409526_64be06f0e3ac14b38c4a0a8bf18e6ede.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1131',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939419420_825b38d8dcd86f7d896e102b6ff4ced6.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1132',
			'product_id' => '114',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634939463458_1ff99fb2bc48a56363fdf90fe3ad45c7.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1133',
			'product_id' => '115',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3742042228430_91fa4f1c786969e410be08dc38ac2fd1.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1134',
			'product_id' => '115',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3742042289680_25c142c0e4cbddf17fed9d846b98bc94.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1135',
			'product_id' => '115',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3742042795299_a11dcda25a06c9394a3ade117f041f7c.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1136',
			'product_id' => '115',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3742042915865_29b8dfc4287f716b95ad6e0f5164d7c5.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1137',
			'product_id' => '115',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3742043012578_ce778094ce54ba461dec96604b7d0094.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1138',
			'product_id' => '115',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3742043143708_8f291f86feef0d25d3c9b7c0f7f30635.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1139',
			'product_id' => '115',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3742043203786_fbdafd64f950d17879bdf6bb314fd24b.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1140',
			'product_id' => '115',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3742043283843_88ec2fd49aa30d48d9f11ac24b6d4089.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1141',
			'product_id' => '116',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3662700259992_b142417cd6a657be42fe3608a0d8a58d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1142',
			'product_id' => '116',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3662700270451_c2d0c1b040e9e0ca9297f90b53d93cc3.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1143',
			'product_id' => '116',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3662700274436_110a3eff095acf342b553d168f1c2ba2.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1144',
			'product_id' => '116',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3662700280994_550ba1602d8117012238624a3b283353.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1145',
			'product_id' => '116',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3662700308104_b77333f5110142999e144b4f8f3998d6.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1146',
			'product_id' => '116',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3662700313055_f3a4e69ea2df0456be6ec089f019179c.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1147',
			'product_id' => '116',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3662700347609_e677db3e869d91b3c574866aa421fcb0.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1148',
			'product_id' => '116',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3662700400714_a9d5ee5dd11049dce3bb61bd44aac3a9.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1149',
			'product_id' => '116',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3662700450426_acfcd30a444ccfe38abe96dbbc9e1e19.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1150',
			'product_id' => '116',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3662700525461_accc3055317740708a0c6f713e8630bf.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1151',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670471525008_da6327cea0aebf49da5181a459e577a2.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1152',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670471574104_f9f366257233e103e5b435fe66d9717d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1153',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670471618106_696dada3d0f49b1bb9dcaf9154ce55a4.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1154',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670471683205_61ffa6b6e3a439318588f69d2d8f9c6f.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1155',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670471738789_9e1630717069728665c9422e02ac996e.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1156',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670472065360_a26d33fff1658fa98fe8a383092baa86.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1157',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670472111340_308cd4599ca560028935c302f20f6da6.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1158',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670472173202_a3c9d8e83af4faa95f36adc45e7e0202.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1159',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670472218521_f1b4373e97f514b95792c2fdb8cda038.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1160',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670472265573_1a4c3accfa372c407ae4ba0938f8c9e0.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1161',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670472321297_112882c5aed416bb617d75305fdb768a.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1162',
			'product_id' => '117',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670472384562_4e0c0ae0179a40e5ac8fdb223b97559e.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1163',
			'product_id' => '118',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670472442482_9a1c46cc1c3441925ca133e665d0ebc3.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1164',
			'product_id' => '118',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670472556482_6e8a8c520ee94225f9aa3b11be9a18f3.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1165',
			'product_id' => '118',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670472595309_d3fa0455fe0c70f7289de163589c25be.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1166',
			'product_id' => '118',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670473009696_08700fb46bfa47c66832eff847a4e899.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1167',
			'product_id' => '118',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670473051270_783fec2b833811b6fbc7f4f2bbaa38b8.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1168',
			'product_id' => '118',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670473103867_748ac532a04a8a026b1a57c460f31309.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1169',
			'product_id' => '118',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670473157704_50e772c0682aba20641faff150f15678.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1170',
			'product_id' => '118',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670473206490_b5832f6cf9b317d13e205dc2ab12d673.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1171',
			'product_id' => '118',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670473235435_7510a2b1217bab4c043f7dbb3fd9a40d.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1172',
			'product_id' => '119',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3673687800822_d12a23f9e2edd23b26a41f690cee10e6.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1173',
			'product_id' => '119',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3673687805786_44689c7dad6ed2618f1f403019525614.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1174',
			'product_id' => '119',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3673687813011_88432ef62606539d939c3f520af11a58.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1175',
			'product_id' => '119',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3673687857407_770364c5b2297a84fa95ba720d169c64.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1176',
			'product_id' => '119',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3673688155462_3cdc8d027d36485700084f886e18c05c.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1177',
			'product_id' => '119',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3673688253591_22918bf903bc00cff091dc893b968916.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1178',
			'product_id' => '119',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3673688297023_ce1782c13e6fc92fb90edfa393473f71.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1179',
			'product_id' => '119',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3673688353732_5f2b14016bb898ed7137117b041b0771.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1180',
			'product_id' => '119',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3673688397186_2f12f7112f854c63a6f4a32e143ec2f7.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1181',
			'product_id' => '119',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/04/z3673688441425_4e180118c2b80eacf5eb7374da234f81.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1182',
			'product_id' => '120',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673556055980_a79b4373be07f2b2628ad7c0b40469d2.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1183',
			'product_id' => '120',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673556063522_ad8eb3c63dced947f2302a8260a3f393.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1184',
			'product_id' => '120',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673556068068_ca9bfd0a680a6fcdafd3435c6125ff5d.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1185',
			'product_id' => '120',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673556071760_459225ea5f67a16f96facf49845ae50e.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1186',
			'product_id' => '120',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673556102165_73f75bc397065d3bd8269dbc35ee32a2.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1187',
			'product_id' => '120',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673556463916_36c7f76997491e8c5af6a72b14348021.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1188',
			'product_id' => '120',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673556521325_8e7c28bd6e253851c4052d3f3b40e8a3.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1189',
			'product_id' => '120',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673556589887_17f00485b34dbff7f3652e7e436a2767.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1190',
			'product_id' => '120',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673556614786_0931e973d21eaa6afa4e4360ce6be872.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1191',
			'product_id' => '120',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673556671249_dcf6305edd5680ee56c2a1498506068b.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1192',
			'product_id' => '121',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673708618445_fa2702038e6b9be6a912d7f68802fe73.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1193',
			'product_id' => '121',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673708686881_70252142a13df4013f60d6f063ff4791.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1194',
			'product_id' => '121',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673708741570_1a876d2d16368885212ddd2e7ff65809.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1195',
			'product_id' => '121',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673708824799_86571d1275a5aab3c7a83d3f79105f35.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1196',
			'product_id' => '121',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673709154892_00f4ebb37989a008beb864a67d325c7e.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1197',
			'product_id' => '121',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673709192823_98cb4a08b3fc2f302f7d397d2ec0d93e.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1198',
			'product_id' => '121',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673709341730_a6a8e09a6bd7d506b54ee6189c69306c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1199',
			'product_id' => '121',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673709368899_9f3db87dca2f485b0741d8cd0354b31b.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1200',
			'product_id' => '121',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673709436554_98328cba9c44ec35b17ce8a0630144a8.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1201',
			'product_id' => '121',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673709494357_a2473a1245c022b87bc193a10e98af7e.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1202',
			'product_id' => '122',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673709671564_615c95aa02f10e445386a07a40092ea6.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1203',
			'product_id' => '122',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673709715888_5052804767f0dc2e47ac0b417af7c0c3.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1204',
			'product_id' => '122',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673709785258_1bd5d54f2fd25fcdf125b4b2c5341a66.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1205',
			'product_id' => '122',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673710160180_967de4a715c5ad14692c27dd38e033fb.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1206',
			'product_id' => '122',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673710214865_58a8d95b188853d049ecc6d085865787.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1207',
			'product_id' => '122',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673710280990_8f94b5b0bd7482c0eebc96935f9db867.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1208',
			'product_id' => '122',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673710350110_15591264743e8108b1aae40e94c56019.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1209',
			'product_id' => '122',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673710418478_0505c6216cd5378170f30b58473d3d24.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1210',
			'product_id' => '122',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673710483218_bc6811cbba7a14c1fb012660433696a5.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1211',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673708438589_578f4cc6e636df89c3da5a9c1fc3ee2d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1212',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673707708061_776e340afe0350e72748e6a4ad82e65b.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1213',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673707713259_26e5b862b8f5939a8714b3ca59b16a5d.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1214',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673707717553_ada0d0f024b44ba827330834535fa394.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1215',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673707725014_0b837ebe0a37004a25e14684b3ef4ce3.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1216',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673707807087_5f36a62d98af9fa8df1b361f53fadd9a.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1217',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673708133071_6483fd6586457a9a93d5d03ed07c0957.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1218',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673708187119_1d8f083f4e8a668d8b1b732282b82a6f.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1219',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673708268526_ce8efd9bc28790b338af97dc72bb7b1a.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1220',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673708324303_2a5007ff565dbc3a9ec487c649144d1e.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1221',
			'product_id' => '123',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673708374698_5f46b2db8f9d70694c06f30783556542.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1222',
			'product_id' => '124',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3635048646523_b2998450393d7ef2d561dbf0b9c76c67.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1223',
			'product_id' => '124',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3635048685636_4adf171514d2b3bc7cc78b00e0928fe4.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1224',
			'product_id' => '124',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3635048745358_52076a4c85409aa06d3705cf11a30fbf.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1225',
			'product_id' => '124',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3635048774259_108d40a529e9894e1d0121a99c3a9c8a.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1226',
			'product_id' => '124',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3635048782253_ef1f8b4db6bceca0815a4a2b11353881.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1227',
			'product_id' => '124',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3635048845245_eb780eb7c00aa49eace0fb82ccc691e7.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1228',
			'product_id' => '124',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3635048862326_9237e18bd1a55eeacf2007379fffb187.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1229',
			'product_id' => '124',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3635048906113_0a3e2e8a543ceec646526047e404cdaa.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1230',
			'product_id' => '124',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3635049003412_4fe50e8626d775cd61766ae48a703108.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1231',
			'product_id' => '124',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3635049030993_4468892bcaf3ae92de8efa4da50d6a65.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1232',
			'product_id' => '125',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3647647545757_930f1e83c13c8ab8b0f1c04cc07427a1.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1233',
			'product_id' => '125',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3647647524032_7bb9c11268772c14d50b575e0cbeb319.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1234',
			'product_id' => '125',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3647647515742_02256ad32431f4e4d84261e6c718639d.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1235',
			'product_id' => '125',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3647647510353_90ad49fe304dad10fb7f5d0e4c729a8c.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1236',
			'product_id' => '125',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3647647507040_77055a0846913d80e61dcb4962d2816e.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1237',
			'product_id' => '125',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3647647552088_ea4931dc4af745f7362b871722c4f49b.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1238',
			'product_id' => '126',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3649613963071_2f9409d1c89cf7e5b5c3a0530f013b24.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1239',
			'product_id' => '126',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3649613972615_c4713b27698fa9eff711979544062341.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1240',
			'product_id' => '126',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3649614095622_aeec904b626e9f0de4f2e755f0bc7ac7.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1241',
			'product_id' => '126',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3649614485570_a3404b5ee1e3300277e9b471b5fa040c.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1242',
			'product_id' => '126',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3649614503612_3c98f2135e9892d7f076ec669e5a339d.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1243',
			'product_id' => '126',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3649614560750_76079ae84f9fb6abd4335f59a2f5ea34.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1244',
			'product_id' => '126',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3649614664237_121ffdd655cd3744454beb7d9e52e0fb.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1245',
			'product_id' => '126',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3649614687978_ee9fa4deb781b15ebdb0d4097cb762e4.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1246',
			'product_id' => '127',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3721445505499_cd6077c118b6d07fcbef60fbac1a1974.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1247',
			'product_id' => '127',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3721445508974_79073e2e95883ceb689c26645b590729.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1248',
			'product_id' => '127',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3721445562494_f776bdc44a580e2b247ad03938884908.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1249',
			'product_id' => '127',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3721445924820_e233491b9c9c6e4a3dba03f3e462fc47.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1250',
			'product_id' => '127',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3721445988113_b2e813ea418b5cf82c231eafd850792f.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1251',
			'product_id' => '127',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3721446052892_fa8ffbdb9fa8bb14f7fa83d82f802a67.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1252',
			'product_id' => '127',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3721446140582_a7329c0a80f71710cbb225f92a5d24fa.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1253',
			'product_id' => '127',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3721446183717_6dd6d348bb270ff3e86ea5967ad4ac62.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1254',
			'product_id' => '127',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3721446321039_ed69e0afe9017b291c1e6c2a9885427d.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1255',
			'product_id' => '128',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3676105496750_a320d347845869fd2761c3f08bf9c649.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1256',
			'product_id' => '128',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3676105500160_2a3d8095b1036a97f0b96dc24597db68.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1257',
			'product_id' => '128',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3676105505080_b30650d0fd4ebbb1eaee80600f79dbf3.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1258',
			'product_id' => '128',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3676105523961_f9454a1204aca63cfe35741bd08e801b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1259',
			'product_id' => '128',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3676105569075_fd9ba7d7510b977371e920d034de2491.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1260',
			'product_id' => '128',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3676105807809_7cf03868f29242ee9d806b407ce44ba0.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1261',
			'product_id' => '128',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3676105863289_f0de3cd04acdc227b3b71907dea7a1f7.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1262',
			'product_id' => '128',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3676105940311_7048fbbccd401336a2510c5dff854b8a.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1263',
			'product_id' => '128',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3676105976177_a8632e1a8383e2466d3ba1407c963f25.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1264',
			'product_id' => '128',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3676106020097_8d5454e63a3ca2212afa9a6c9b1f3202.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1265',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008234050_026171e9e687bda4dd3e5f1d1ac125ba.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1266',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008240747_ebbe451e898d35a6eea07f3b54705cf1.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1267',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008242914_7625986de571c874d3a520041f66ef21.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1268',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008244832_0e971d4ca3d581d17ee621636d5f98d9.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1269',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008249099_4b4ef5c2b8b947397686ac8ffd32b8e5.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1270',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008414852_c4e6f1765bdca2fc4165df96974868ad.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1271',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008460196_ec4c6323902f7d10423399f4639bb5cc.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1272',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008502308_0448d68d49f450c3df879e2a7cd63bcd.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1273',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008550590_fe0ef0a09dd8a12f7bbeafb9ea94b675.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1274',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008589857_565deb2f39d386b86d0cb612dbf634b2.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1275',
			'product_id' => '129',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3676008627403_ca2ebb8556b7c0e3105f0a910754cb7d.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1276',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043200905_df5de2418995b73112277502ea75a380.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1277',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043205950_3155c5fcab8ce4025b0ffeefdb2c478a.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1278',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043206647_4da5f658e95bd5fe5c1bfd80b1c5d322.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1279',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043209613_53faeb4c35e3601a6130a533eedda288.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1280',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043213034_ef86c7a0b14148c159ea00784b58628d.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1281',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043233954_3f2702cbd8fe9b3de864cc6db4f0cd3a.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1282',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043238283_12bf792b5b692b916fe5b1e0453890c3.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1283',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043268475_0e5caae7362181a4962665cd7269e568.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1284',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043289701_54cd7c0f45c57b30375f6ab51b818727.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1285',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043322334_cf2beb3619ac6ca46c366233510257f5.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1286',
			'product_id' => '130',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3676043357735_2b02dcfc50a591dad8d96915de91171a.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1287',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108389144_8571faab7be699c6a2ed6b7e527a9264.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1288',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108392470_eab2a7fe3618a85cc39b6779e280892a.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1289',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108396105_3b3cab9c02c175ad11972a1e6a8331bb.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1290',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108400787_089618f1ad1d04ba44f16542598130ec.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1291',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108405458_c3700e508a35a5866be19d62fb7fc880.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1292',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108431290_ce3081100d93d05fb140fefc58234b35.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1293',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108435160_296a288aebdedeb45734da6710aece73.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1294',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108440080_3b6576237c919d9cd1a823be90c98e54.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1295',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108444313_fcc8f02445b0493737d6414853fd9b8f.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1296',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108448138_affdcb8f0ab390a4c1730920c9b65e9e.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1297',
			'product_id' => '131',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3676108452078_e3d2b58676998349c1c48aac18e70f71.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1298',
			'product_id' => '132',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729062422448_aa6d2c9d397636a6559d47878fb20700.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1299',
			'product_id' => '132',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729062428750_696b6beaee8ed7d52860de5347c832be.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1300',
			'product_id' => '132',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729062449157_ff5a2f25b27cc3e165a8802acd37c2a8.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1301',
			'product_id' => '132',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729062452378_50633e26597de076052380f822846b95.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1302',
			'product_id' => '132',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729062457205_c395db8a0896d66fc546a80520fd0954.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1303',
			'product_id' => '132',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729062459786_00c1a249bd2f80a2fdc96257cfea7058.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1304',
			'product_id' => '132',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729062463532_f9e62c89675127645d3cf03a50ef37fa.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1305',
			'product_id' => '132',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3729062466738_803fa6c75a70268005cc91ef34dfb0e2.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1306',
			'product_id' => '133',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3655835975733_56b442bf36db55dbc69445984937c95a.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1307',
			'product_id' => '133',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3655836002068_ea8e1f2eeb4e9563ab3825e5939be5c3.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1308',
			'product_id' => '133',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3655836100079_1dfa1f2f1ebd3edf4e9d000b2fe2fd5e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1309',
			'product_id' => '133',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3655836154253_969639d2d37344d60c86edae94d0e2e1.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1310',
			'product_id' => '133',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3655836392655_cb9bebd8724e7c32cf28c490b42d7ea2.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1311',
			'product_id' => '133',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3655836449377_2a913f16a70168e3aebf45055afda5f5.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1312',
			'product_id' => '133',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3655836631454_847fc19ab5130ee06cd2b3606db06504.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1313',
			'product_id' => '133',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3655836634474_4200004af6d0cea1ea9a949089e5f087.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1314',
			'product_id' => '133',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3655836743903_6c19d51a28f8f487593e1290976840ce.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1315',
			'product_id' => '133',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/02/z3655836798680_245adadd662e7366567836eb6d6c3372.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1316',
			'product_id' => '134',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655835149702_03cf30216e646aa61d4724135b949018.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1317',
			'product_id' => '134',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655835157050_9d91612da925b0d3222c1df410d09a51.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1318',
			'product_id' => '134',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655835167133_cdb93108c6c51b7b22f86f073fccba7b.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1319',
			'product_id' => '134',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655835224688_27325906e6edd8dbd9d76512f67698eb.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1320',
			'product_id' => '134',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655835535387_c94c7026d9a1e7b1554f40fcb759f7f6.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1321',
			'product_id' => '134',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655835635774_c0423c10bf1b1d38122afdd50d8d923f.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1322',
			'product_id' => '134',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655835663715_02207391b958b0bcd24ddafcd4c1ffe5.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1323',
			'product_id' => '134',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655835723569_1035d19469bf00fc00b20835b13c53fe.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1324',
			'product_id' => '134',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655835784118_f3ecffb89ae29cd99a3dabade43959d7.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1325',
			'product_id' => '134',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3655835893903_ae43a2197ad040e78f02b5f612f712eb.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1326',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655781438420_3d726cbf3cb204ba37902a083dacd2a4.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1327',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655781448943_41ede5a8768f3bfd3fbe35d2742fe153.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1328',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655781536520_ed724c0cf49fae5cd556455e1a6dd083.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1329',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655781618424_2361b8239f78d7f8a5b7c8831cd16301.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1330',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655781721676_0cbcfdfc84fd8a40f8dccac38e847882.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1331',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655782012867_9cb98c2c08f55b24a80b0d567a5fc553.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1332',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655782063814_051a7870d2f2a0d245b6c56910ba6c17.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1333',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655782130988_a541b66703452e3e4e3f958eb185b510.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1334',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655782218871_d544e207942288e22543b48074c3a4ae.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1335',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655782228208_e75b55672bf363c8f2573c7e5f1a458c.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1336',
			'product_id' => '135',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3655782283842_ba7c6268b017405bdd4c951af107831a.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1337',
			'product_id' => '136',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644821636709_258ffa723fa3b118ec6c92a1f48b4894.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1338',
			'product_id' => '136',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644821638021_7aee0c8c8c140b86863ea6d29007184c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1339',
			'product_id' => '136',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644821678815_bbf26ba6f2076cbf39ceeb9c7275ae73.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1340',
			'product_id' => '136',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644821899739_8d63d7073c59ee336058d367c60b0462.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1341',
			'product_id' => '136',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644821947790_5da07f234744c67604335d27e7701755.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1342',
			'product_id' => '136',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822005221_cbb54bd2a027a16643f2a49a2c293817.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1343',
			'product_id' => '137',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822282612_18972da389a47f2bb9fffb7f4c9e5559.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1344',
			'product_id' => '137',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822284448_8dbb3852e77a66863366c558b9797b4b.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1345',
			'product_id' => '137',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822291298_be71e2870158546a35d3c4a9296f68ce.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1346',
			'product_id' => '137',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822330632_0e341bce06cd377aada1aa6dec3c1512.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1347',
			'product_id' => '137',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822397423_7502abe5ae3345631f4d78e67af338ae.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1348',
			'product_id' => '137',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822421332_0a7b408c1ac1ef408d09ec67871e04ba.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1349',
			'product_id' => '137',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822461740_3d6ce3a78b0d99be32ee1b29dfb7bf0a.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1350',
			'product_id' => '137',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822497320_c8f2b74feb7f826275549dc9435f2cb0.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1351',
			'product_id' => '138',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822886523_bd6f4711e1fb01414d852a73b1067887.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1352',
			'product_id' => '138',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822894180_88d3b634f2353d4488893fd94b67b29b.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1353',
			'product_id' => '138',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644822967367_55522fb1af11533e24f047bfe0469120.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1354',
			'product_id' => '138',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644823000915_2f6c04599af8ab3f2de2f16b96619b01.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1355',
			'product_id' => '138',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644823091570_09f40f94377389ecc7b438cedbb3aa37.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1356',
			'product_id' => '138',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644823175058_cdbc7210161741acf72ddf8e7a34ae25.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1357',
			'product_id' => '138',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3644823197580_c5d1a6891d1bb2ea134da45b449e955a.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1358',
			'product_id' => '138',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3604072170945_63a21b014adab92a1435366de0415863.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1359',
			'product_id' => '138',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3604072417212_0fe3181f4b5e0e0230e2340c679e31e3.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1360',
			'product_id' => '139',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3651678943413_6e9d88df2221d887c47f3ed7c3b9f084-scaled.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1361',
			'product_id' => '139',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3651678985400_f5c2a3afb17825c7244e3a4698bb798c-scaled.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1362',
			'product_id' => '139',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3651679043531_8c52c9adcd9db5f47eedd639787b8e10-scaled.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1363',
			'product_id' => '139',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3651679062827_ccba515b355c419689336db4bceb3cbd-scaled.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1364',
			'product_id' => '139',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3651679083573_81357d64ffcb5358fdb96020038840f8-scaled.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1365',
			'product_id' => '139',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3651679100449_20dca1742647a1f7a9807d730add5726-scaled.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1366',
			'product_id' => '139',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3651679133084_3d3d9a94dc846fbc8d177b48ac8544af-scaled.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1367',
			'product_id' => '139',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3651679148516_1398443cf631ee271a4c9171a35c49f2-scaled.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1368',
			'product_id' => '139',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/01/z3651679166781_1c094d6d3d51bf72975608722aa64ef3-scaled.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1369',
			'product_id' => '140',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895247926_75c70ef0f5c8eba50ebf0df9c78d3d0d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1370',
			'product_id' => '140',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895191817_aebe39163cfbc1f6c16112f27d159109.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1371',
			'product_id' => '140',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895146374_cd9eecde33d1d542c295f5cf68f0c780.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1372',
			'product_id' => '140',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895092846_34765be06eac37f1de9f05b73cf4aca0.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1373',
			'product_id' => '140',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895037116_3e47b9b5ca13f91fa618856965cecda5.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1374',
			'product_id' => '140',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650894979617_9eb1569c0a3639d379e780a628cc4897.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1375',
			'product_id' => '140',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650894699942_2f166c4c539c7ac9e96c7b6dd9cea08f.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1376',
			'product_id' => '140',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650894608811_4650ccd81d2b8aac28e4737b6b829786.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1377',
			'product_id' => '141',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895302842_7d1e2d118556aab0b5a16477e93f84eb.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1378',
			'product_id' => '141',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895388565_29bd9cc9ff6f4e77932fa7b2c59f5bd2.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1379',
			'product_id' => '141',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895416302_e06e3038533f83de1f6e08b556195bd4.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1380',
			'product_id' => '141',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895825899_f55d473cfc361380cb83607de79a8532.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1381',
			'product_id' => '141',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895908620_f1e16413e09a5f66970c15de5f62ae57.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1382',
			'product_id' => '141',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650895956384_1bc393598f19ab58d6225530d58eff81.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1383',
			'product_id' => '141',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650896017264_39a4dcf8f1474c9345e23e5481afdd81.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1384',
			'product_id' => '141',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650896086444_adf0e1a8cba38c888ccbde90864577c9.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1385',
			'product_id' => '142',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652965590709_523443bc4e11460fdf0e7e05254e6796.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1386',
			'product_id' => '142',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652965595036_d7b1d0bb019d779078e70edfb4b5a776.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1387',
			'product_id' => '142',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652965600203_5812af552abacea81442b55c774fea54.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1388',
			'product_id' => '142',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652965810985_8ce415b7b50ec0bcac1e9785987da388.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1389',
			'product_id' => '142',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652965869732_4584f4dd94201c806f0546a3ad185744.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1390',
			'product_id' => '142',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652965918052_6e1cfd12018a0fd55c5d8c50f7dd3974.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1391',
			'product_id' => '142',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652965988532_747a93a1d01ab32c63775e98a7bc973f.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1392',
			'product_id' => '142',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652966025314_1b35eea59172e7d271bab82193db16ef.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1393',
			'product_id' => '142',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3652966067201_67b7bd3da275439c4d7b512d2a8acdc9.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1394',
			'product_id' => '143',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3650785199553_1145f14af248b10a031f175964a12320.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1395',
			'product_id' => '143',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3650785204175_63558698779396dad15161c70f782c48.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1396',
			'product_id' => '143',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3650785207675_800b8cd7c1606a58c343b6a59f37444d.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1397',
			'product_id' => '143',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3650785232902_76e4a8133eaa8da492ad43cec74c4228.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1398',
			'product_id' => '143',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3650785236408_05e9feb01c32e5a34cbc16a979044bca.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1399',
			'product_id' => '143',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3650785240882_2ac8ca86089819de44f11d712130611d.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1400',
			'product_id' => '143',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3650785259254_ea38724ca4699d11b6c6961fa20f614f.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1401',
			'product_id' => '143',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3650785286950_dd76cbf3c0771e975f6553e689e7a259.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1402',
			'product_id' => '143',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3650785309116_9a5079aa07e568bc6ec7ef4d1150328f.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1403',
			'product_id' => '144',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3650786666841_6e466b2a282cdb9f44d0ea9ceab0d2de-1.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1404',
			'product_id' => '144',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3650786671160_b7493511087688ce7e358aea9b8ce39f-1.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1405',
			'product_id' => '144',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3650786674491_27e78f57cfc150f90b51005e6a16d2cd-1.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1406',
			'product_id' => '144',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3650786678782_b32fa761360577b9c77dee7716c1d7a4-1.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1407',
			'product_id' => '144',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3650786703162_c24e92edd9a82e9a173211f47150e11e-1.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1408',
			'product_id' => '144',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3650786706977_e7dab045b29b4a66dcb7becec49c221f-1.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1409',
			'product_id' => '144',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3650786712325_263575b053ba68e272984efe4c22ffcb.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1410',
			'product_id' => '144',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3650786733363_7120c323a67922c7b69bfeb738a953b5.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1411',
			'product_id' => '144',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3650786754773_567fe331ba4abaf459012c908897df22.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1412',
			'product_id' => '145',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3650786010897_d92c6766be6eaaa743a1a98167de9336.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1413',
			'product_id' => '145',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3650786015960_662b213d2b532b9f596cb2767606a313.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1414',
			'product_id' => '145',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3650786019088_521c8fbbf25c8711c98a512653b2be6e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1415',
			'product_id' => '145',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3650786024391_2c84952cc858bdd42dda372f2a8db101.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1416',
			'product_id' => '145',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3650786079186_bb363d1204d1a1ba6e4a8d47e34179e3.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1417',
			'product_id' => '145',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3650786099645_ba013598b69ba652d53ae41fa374f77e.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1418',
			'product_id' => '145',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3650786109840_44973b51755d22a12bb015fee0324d6d.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1419',
			'product_id' => '145',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3650786132944_6f34803e116dbb119c7e753d4d4c2d72.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1420',
			'product_id' => '145',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3650786154998_a07769e6fa693653896c54df52d16791.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1421',
			'product_id' => '146',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650785696411_5323f5ff7167c983b2c05c97292e7b12.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1422',
			'product_id' => '146',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650785692904_0deee0142120d6b8bd670254120c1690.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1423',
			'product_id' => '146',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650785667837_37c6a8fb6f0db8fb402461326f8247c3.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1424',
			'product_id' => '146',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650785661211_55045b4e8dbda59d24a3d8a469f46e7a.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1425',
			'product_id' => '146',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650785657310_dd8b1d342c65db197ebabc68728074ad.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1426',
			'product_id' => '146',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650785631408_da410b9a25575befebe78818ea8e2d9c.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1427',
			'product_id' => '146',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650785626384_1b8122b8839dc17f093217005e54c097.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1428',
			'product_id' => '146',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650785622970_b076c0f1434aed71d26d6ed33d85bde6.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1429',
			'product_id' => '146',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650785618609_5817848cf184538e085fb7f4091a53ef.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1430',
			'product_id' => '147',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650786478711_df402c375e6ef609af2bf9bd25961c5d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1431',
			'product_id' => '147',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650786456273_c00f38aa9a2819fcdcf3b0105014d303.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1432',
			'product_id' => '147',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650786433351_0d8adba4edc0912a12ee91499d8e8245.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1433',
			'product_id' => '147',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650786424604_74ae97af87f18609ad8490aee151459d.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1434',
			'product_id' => '147',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650786378205_52b57c93935336c5da85ab7916ddd540.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1435',
			'product_id' => '147',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650786341236_e8cc24cab88fd135a85d7ad089f3f55f.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1436',
			'product_id' => '147',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650786336021_b819aea52830234488ff9def04e0b039.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1437',
			'product_id' => '147',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3650786332559_7e859bf827f1dff97c78ef3ae81a8aeb.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1438',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372112430_bc825a9202693d8f3ee64b7cccf7605d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1439',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372117036_7d1d769f42656f5966ae385b8b4254b3.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1440',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372120809_d4062f055d5344cf3765ea3b1b3df955.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1441',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372125505_2e8b5c9139996ead8c6a6baca1e52435.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1442',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372129727_4bee954ac84aa54df164d410136968c8.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1443',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372156237_c02d8deec72cba6d1c772f8a6fa15be3.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1444',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372160110_62d6e655a249a927bbbf55fc6ed89514.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1445',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372164694_bbb058d6efcec315356341e679d2f24a.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1446',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372168897_475e303d6c7905e482c7a18986120ea3.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1447',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372174019_cc79da534b4a55cc662d90f5acbf4086.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1448',
			'product_id' => '148',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/12/z3653372190088_99b9a24c70f5532e860953454f2818da.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1449',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425224322_fb3bb7d9afa28ff8d7bcd0c25c2df9dc.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1450',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425227980_6497ac896e96508b754c6254f2b1305d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1451',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425231983_1e382be512ac19f399c818fb26f414a8.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1452',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425237355_42852fa8c18f82ac7e4b7e9fcb4e2acb.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1453',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425240727_0ac697cad0bd79fbcd3b0bd44d59d333.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1454',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425243011_8a94594143838a975b196872189abbc8.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1455',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425251882_3ad351a92b28225aa196a3e392cbfb3c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1456',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425271000_c7633a9f820068a10e4d8f6835a39939.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1457',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425274615_c287fed0eca2e5f7d7f6c49ee2693b55.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1458',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425278541_3ed6933c9fd11aa17b0ba42ef54d65f0.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1459',
			'product_id' => '149',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3653425283497_1249c82f0027e99e4a06a2dec14d9a9d.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1460',
			'product_id' => '150',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3435940465734_63b3b79021e42e187d8edbcf2c0af574.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1461',
			'product_id' => '150',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3435940467456_3d4f81de90a1d417ff3f02ca5dc0f7ef.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1462',
			'product_id' => '150',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3435940476789_41fbe4e2829b57f119b5a3b8d89dd41d.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1463',
			'product_id' => '150',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3435940477101_61a27a586665bc9024852932250cacf9.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1464',
			'product_id' => '150',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3435940486184_445b594aaf6d852bd983086095b08223.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1465',
			'product_id' => '150',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3435940534713_f4127cf84ad9b844de2ecf14fe092a47.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1466',
			'product_id' => '150',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/05/z3435940643907_3d6d2b3d86206f5fa1c9530d7bb62295.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1467',
			'product_id' => '151',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653366642381_4bb31f2d0876320fd9008d975b9f230c.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1468',
			'product_id' => '151',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653366646116_73de03dea68fad077eff49090eb3d99b.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1469',
			'product_id' => '151',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653366650432_f343ab5ca56be81f31e56df4cf9e666b.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1470',
			'product_id' => '151',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653366655345_6dd0fdc28ad70e5b0d323396de120477.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1471',
			'product_id' => '151',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653366659519_5ba74140226c499ff4751a0135870e75.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1472',
			'product_id' => '151',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653366690170_2518185525a74c962d0fa2ccb73e8b63.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1473',
			'product_id' => '151',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653366694687_2cea8d4cc3fdb6d498c5d232aa4b843f.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1474',
			'product_id' => '151',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653366698609_668b916b3d7df9770cc178a49849c4b6.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1475',
			'product_id' => '151',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653366703669_38966b0bb49b3b7a42d89b92ba7d7a97.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1476',
			'product_id' => '151',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3653366738871_35e8123cc4dcf55975f7a3167b50d2b3.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1477',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364038599_9b044c90183b1d87a1ee222ec8327a98.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1478',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364042314_30af4cf5cddca8cd73c711978c1c667d.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1479',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364047024_f92da5a4444df237216602f2ddea2578.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1480',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364073641_fc1d92930004b851ceb921cf51aab95c.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1481',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364077435_c75b669836630842e1e21904e0ae85f1.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1482',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364082338_ec2ace7118710185788a3b898dc3b4c4.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1483',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364086524_793fb97764e45a0ef4b6a11992c6fae3.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1484',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364091454_b3dd1ad293e71661013d485496b0d2cf.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1485',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364098162_090d144982ce5a21ea3ecf77774848f7.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1486',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364116438_2f2da8e00bf6ddc9950b9a2ee2c8d2b6.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1487',
			'product_id' => '152',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3653364139005_dc48dff02f774bd3d24dbb4b7470e7ca.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1488',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367705976_e349ba0da5439cd9116b6bdbd85e933d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1489',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367709920_40afff4c7e1c35b8f404651be53dfd05.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1490',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367714890_491a7342f85d5b3935ef0c7ac9e111b0.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1491',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367718952_1ce5079c39132a37c84a43a89fe4e85b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1492',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367722614_1297714310b3f6e1a9ff60dcd62041b3.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1493',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367772895_8da6f88225a354cce108853b5a255cc9.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1494',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367803395_b95ea2093f36bc03d6b71c002bdba2a4.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1495',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367839979_53878b0526918948fa2156f4555d2596.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1496',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367863168_33d05821ffdd87d4adf49dcb89cb5900.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1497',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367889591_98c84f8078d33cef224d870b2e0814f2.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1498',
			'product_id' => '153',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653367940573_150eb665ed9cf3b270c75a9bf58ecf42.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1499',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420522576_fd58f6665e0fbd46c97a7f834c3dca01.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1500',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420525925_55eed3379d224cc39c29eb2fde4b1006.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1501',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420529972_89cde8e00cf5da2dbdb4a1ebb0b097b2.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1502',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420534872_063b4070a0b4a02d525092f8e962fedb.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1503',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420538233_73354491377d4da236c8ce0620fb3ebb.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1504',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420562132_f99c781635685254e2bf0786df022aa7.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1505',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420566044_f6b90dcc62de5e4ec1076d2475f64c7c.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1506',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420568666_89ccf2760d88a41ac2d484669ac2a74b.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1507',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420572958_c42d967dea227cb3ad49a0f1bdc09028.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1508',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420577442_eb04acb3a53019460a7aa8234ddb182d.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1509',
			'product_id' => '154',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3653420580902_751f6c6685856356fbfe71c58561100f.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1510',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673288656978_4ad47fe832d946dafef0c28f976006ae.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1511',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673288717396_ce05f5deb665e5b1ebb5b6f9d4d6ff5c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1512',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673288798251_5742f3a7ead0cc84aadb04b14f2547ea.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1513',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673288863763_25581ecc35a7b7ca1141bf7a92aba7d9.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1514',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673288906021_63e3c50317d0a5c87e1be45638885e0c.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1515',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673289256153_2e46518e02da00fa324eeaca6362f597.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1516',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673289317703_2e1f9d78e9454a69bd68a17c5e5cf9b9.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1517',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673289395588_b0f96037d92891aded13644105045c40.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1518',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673289463925_a4ca6944b2e697b682ddaa6be1f82cbf.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1519',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673289593203_3ab9e781a8a2705261591df9aa153e21.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1520',
			'product_id' => '155',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/10/z3673289648605_7fa854f5c7109520709158728b2c96af.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1521',
			'product_id' => '156',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673212073684_76a7a0a5e66b9ea5cdebfe5f3228647c.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1522',
			'product_id' => '156',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673212135989_5540b196af020935e177dbb5212efa3e.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1523',
			'product_id' => '156',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673212340810_c5ba611c7c0c8a1b16ab5594c03f612a.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1524',
			'product_id' => '156',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673212363377_35a7fca38d4522e8f0df68e7b9a5a10d.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1525',
			'product_id' => '156',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673212783408_faded05ce1eb9ac4ef23a38dea1b7c06.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1526',
			'product_id' => '156',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673212825211_648d7bce2c9999ac41bde8c30f2e1095.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1527',
			'product_id' => '156',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673212948430_50f5c46e222244ce24b1210e60c670d7.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1528',
			'product_id' => '156',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673213011665_e4024e255c55ea7dd2252908a3445ee8.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1529',
			'product_id' => '156',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673213074070_476904b61ddfe84379684b945f0e66ab.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1530',
			'product_id' => '156',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673213138407_b3c575e403aa56859f07b2cb3d763b4d.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1531',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673346318830_457ece113c9487f624cb86d909c181be.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1532',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673346322978_2c6bbdfc49f3134af13b1620f14c7211.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1533',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673346327114_a4fae9106f4ae323c21bf0148e4ba0e7.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1534',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673346338609_9e5ebb3ca19955c3c0bf0e62bea77383.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1535',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673346385301_27519e6bb0183bf1dd924441f272f2df.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1536',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673346704576_202a34c4eb913bd1d7bae758ddecdac2.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1537',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673346845977_416764c85b5687e9fa79667abc1d6e46.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1538',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673346921034_7ee613813636d4a1e2e57e9b768ed78b.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1539',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673346970747_e13bfed337809ae65d6a1091372f7823.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1540',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673347012844_ad80d604616083e8b83e04630a22faa9.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1541',
			'product_id' => '157',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673347065279_2b89f03ec503b1def1d9b1507285971a.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1542',
			'product_id' => '158',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673151901588_a18e086843ed448d576d804a9b79129d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1543',
			'product_id' => '158',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673151907873_dcd632394b969bd50cb749980b34da4f.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1544',
			'product_id' => '158',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673151912321_81fafaa26b75a6335ee0805a543e0733.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1545',
			'product_id' => '158',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673151917329_24e5adf48df728937c32361037e4f119.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1546',
			'product_id' => '158',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673151923694_b07f2d70c3df6e9941dbf9436eacd52e.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1547',
			'product_id' => '158',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673151963420_954d5038dde17a0267be7f01f294242a.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1548',
			'product_id' => '158',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673151965651_e636644d27617f140d15e1c100ced5d6.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1549',
			'product_id' => '158',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673151973815_23857c748861726f30227950d16341ea.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1550',
			'product_id' => '158',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673152017708_4c41c3680f6cd402bf22cee848ddae3d.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1551',
			'product_id' => '158',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673152069262_f006a6754726364f719cdc7d65a02358.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1552',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250161041_a113d411cc00d418204f9a91c6dc49bb.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1553',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250165304_8ff5f5197ce3a08414a16edeb56b06ca.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1554',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250168272_1a3543cf1787429a2835b7f960000923.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1555',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250172052_b7366fecc5a0c5fd1835fcc508d5e6ad.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1556',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250174722_bedc76144fa31f060854844a4a53ad2d.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1557',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250179525_1575af9c5ca93b963366a6d3b204f2b4.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1558',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250399210_c08dd8adb0ef6b8d0d53cb28b544d75b.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1559',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250458752_f3fdc07b8c80b11aa6e26ba0209e2266.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1560',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250518699_4107a9a1447bf285a6404aff30a02517.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1561',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250576954_2f001177d61064fd51cbf4870ef2d9b9.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1562',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250614466_19fd827cbcff1a214568b26c80b8137d.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1563',
			'product_id' => '159',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673250700927_44b81cd959c8c7363ffafc8b5de91ca0.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1564',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673314323879_3d31626b0a9680a2828e58ac322a1428.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1565',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673314328322_150cc592c230ad7008cfd2e595d3bccf.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1566',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673314339129_83cbde54588d938c77b78c56f5156daf.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1567',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673314392616_574e2218b8348b9b42affdf2bfeeecce.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1568',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673314467502_568d86a927bb221af1d884cddcb00586.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1569',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673314773174_bfe355e4a83f53dcae7c7d5ada10a1d0.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1570',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673314822775_33a1195bea77e8a5462881a5834b1ac4.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1571',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673314888501_386997d40cddddf2010fc9d3a0f991e3.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1572',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673314956689_8f8f681a9a342f5a462382bb3d588e02.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1573',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673314960330_1e2fdf832910713c79d1caad06135605.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1574',
			'product_id' => '160',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3673315021192_98be905607efa3fe67d0ffdfaa02aaa6.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1575',
			'product_id' => '161',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3670524739676_9cd2bf8651fcd8df4c6d9d78d95e92cd.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1576',
			'product_id' => '161',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3670524776823_61660e0a43af4ad8f520ff4350368828.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1577',
			'product_id' => '161',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3670524829310_fffa6531d096cb2ad6feeedc8b8e7c40.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1578',
			'product_id' => '161',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3670525048472_c470f1b374c6a2030c5ab823662d1c04.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1579',
			'product_id' => '161',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3670525114410_a2936bcb6c2340e1a12b71b00ce73af7.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1580',
			'product_id' => '161',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3670525158215_b50c76554562394e3fd91abcc1fa9a71.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1581',
			'product_id' => '161',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3670525214426_bd781c8f4a44823a8fad9c82532610cc.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1582',
			'product_id' => '161',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3670525249598_188f680ea87f596f7ff7fba6d8c01153.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1583',
			'product_id' => '161',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3670525299645_9e7a578c3eae4a244930be8797cd92e4.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1584',
			'product_id' => '161',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/09/z3670525345752_83d4194e97397992b9360e363b2bcd48.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1585',
			'product_id' => '162',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670525389518_38dcc27f7330cfa30cc1f2f67fb2eb0b.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1586',
			'product_id' => '162',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670525512379_b6fdee3af70976fe2547c301ee7dfd20.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1587',
			'product_id' => '162',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670525544797_2f5c8db13fba4ac8e856db13063ea187.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1588',
			'product_id' => '162',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670525740045_6d08695fab2b128421b7338cdb4b143d.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1589',
			'product_id' => '162',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670525772821_356b9bf46b6a5e44c1c95bcd906e7a60.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1590',
			'product_id' => '162',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670525816648_f2892359df4c289ffccc1d62fefb1bd1.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1591',
			'product_id' => '162',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670525828987_df54188637d47ca2a3bdcf5e7f05a2d2.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1592',
			'product_id' => '162',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670525835573_d8df71632c2127e36ad4a2279772727b.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1593',
			'product_id' => '162',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670525845846_d288468db5d191b4f11e91ba91eee9ec.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1594',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524015830_ead24d5c0f7a279f1d61ea6a7971021d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1595',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524019641_acff694b644295a930b87c2ea9119e32.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1596',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524046554_6c670d1fdc274d65383e38c6d9ba462e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1597',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524100070_b32cd73b5c86d33c9b0b87f188afd74d.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1598',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524139047_5730c5b9bb2d6ff9800ab9fe4ee3af8d.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1599',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524366509_6c71d4bd2406b1410415db0442c753b9.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1600',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524405351_4140e4dbd6922745d988d8ae314b275a.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1601',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524443263_2b9418463deb6c9e5c8a90f7d8211ab2.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1602',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524484920_4248e7e20e4c088203266cbf6fd18cd7.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1603',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524548451_164c45a47d467ce7957e23cff6badeb1.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1604',
			'product_id' => '163',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670524564017_360e0573d613f77688b47cc1898d71c7.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1605',
			'product_id' => '164',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3678117000566_1415c2708aed59a1571863ac15c53fdb.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1606',
			'product_id' => '164',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3678116959965_187f63b067749cb68ed2d0a80558a0c6.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1607',
			'product_id' => '164',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3678116936184_2a5a355c06bf044b67b4e2f6385c4e56.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1608',
			'product_id' => '164',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3678116895700_5151f2d3c908ec71eb486fcdd54666e0.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1609',
			'product_id' => '164',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3678116687636_a798444305c4cec22118202b353b5213.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1610',
			'product_id' => '164',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3678116657435_04e08116dbf6ef5b48a1a3a86410c47b.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1611',
			'product_id' => '164',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3678116626322_cd0cf63a05b25657d43a4819fae1958e.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1612',
			'product_id' => '164',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3678116616174_4f09c9f4b433d21c5bfaa7051a3bc614.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1613',
			'product_id' => '164',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3678116613239_409e6802b74d2ce2c7758e3fadde8302.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1614',
			'product_id' => '165',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662743894706_06a641cae2686a6e7fe68b350e29dedb.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1615',
			'product_id' => '165',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662743840890_6d4542599a9f8e9c8e8c92e7fa2a9d80.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1616',
			'product_id' => '165',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662743814496_ffa2a689221c893aa67e12bb7b730468.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1617',
			'product_id' => '165',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662743783954_c0e8ab78faaa2e8c3f61e45ca68e2216.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1618',
			'product_id' => '165',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662743778581_2ed5186a6a1c3c26b4dfdabd6d46d614.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1619',
			'product_id' => '165',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662743727367_44d6f5d3133c333db06be527610cbef3.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1620',
			'product_id' => '165',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662743693325_8e179f1941a2eab5dda45f6b95fb1802.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1621',
			'product_id' => '165',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662743688576_ce0930bc43466722fe419c917f470b66.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1622',
			'product_id' => '165',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662743683840_66f1171fa12b4009857c56cb1c01de9d.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1623',
			'product_id' => '165',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3662743679632_c497cb8fff3cdf557b1ff1d74186be2e.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1624',
			'product_id' => '166',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458775125517_6063168ced1812cb54d65433b7d4efbd.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1625',
			'product_id' => '166',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458775190700_c31553f7d7e49bfac97101e031c9ce10.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1626',
			'product_id' => '166',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458775297814_4b493bb42e037d7c7e439e8a1a85a7f0.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1627',
			'product_id' => '166',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3458775327370_f1df4f03d7682efbe8901ebbca4bc633.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1628',
			'product_id' => '167',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673611776471_d05bf2d621ca49e11ae9a9ee55db3daf.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1629',
			'product_id' => '167',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673611780778_908a57f4e4407eb58b5e5e51393e81f8.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1630',
			'product_id' => '167',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673611807438_a436a721a2cb5fc1ac7f57352aa63a62.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1631',
			'product_id' => '167',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673611858276_298abaf6d0076340c9db45c55ea03e4b.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1632',
			'product_id' => '167',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673611952194_f55a4712e68be5d91469e16b52250d87.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1633',
			'product_id' => '167',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673612283879_099fad1f0730b29d0281379a786df55b.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1634',
			'product_id' => '167',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673612340282_0b37680f1a0ffbedc394975fe493d492.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1635',
			'product_id' => '167',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673612421590_f0c93639417dda6d937a2d552d1caa2b.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1636',
			'product_id' => '167',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673612502274_066b00661a43a9650a63f7a12ab81792.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1637',
			'product_id' => '167',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673612576550_20cde0454b5ff0438841f3afabfcde61.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1638',
			'product_id' => '168',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673612771434_4946b09cdba226f59ff89efe336bcce6.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1639',
			'product_id' => '168',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673612856305_05fab1f634efafbdca02decc38250b34.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1640',
			'product_id' => '168',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673612915887_41eefc82b670cc3b001937640ec91af6.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1641',
			'product_id' => '168',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673612960908_76f7ad0916c440e59b3c24716b6540be.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1642',
			'product_id' => '168',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673613273050_00d22ba6c86cda81c65e63bb5d45e1e7.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1643',
			'product_id' => '168',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673613340278_fc9c0565f8077093103bbceef49047f1.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1644',
			'product_id' => '168',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673613392561_a1d43fbb00dde0e6f16ceb415a9ace0e.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1645',
			'product_id' => '168',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673613462032_68e38d550591fd6d4fc63109543da984.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1646',
			'product_id' => '168',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/06/z3673613553141_f90197eb8096658a2527bf3b1e2dd922.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1647',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673613639743_8672ce29f324ed2ab828043b92d69168.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1648',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673613716297_6974f7044eb146c1bc568ea332ad3c66.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1649',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673613769522_dc495bcb26c58e0144ecc826aa68be65.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1650',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673613832104_bfea238bb0921988ab3187f50b643a50.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1651',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673613877733_16dcc2ec9799aad29cefcc5a508e0e11.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1652',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673613924080_8aeae0bf3767e00936874968acdf4d89.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1653',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673614245674_b9196fb92bebff3c6199b774b9ba8ef7.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1654',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673614354787_cb6488b41f759ff1e6433069a50b3c1b.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1655',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673614386415_306b004e4534690bf490518bf93324c1.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1656',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673614415158_587932a01a8a80b74b44f8d3637934cd.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1657',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673614456424_c232906632a9cad47a03c8f4bade4e95.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1658',
			'product_id' => '169',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673614467097_6747aa080058ee32f01d975a0c988c79.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1659',
			'product_id' => '170',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713008832755_ee4767eb4bb1607f1d6f070fc3803b1f.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1660',
			'product_id' => '170',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713008892638_716584d51253b9dcad4b69e6a149afaa.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1661',
			'product_id' => '170',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713009314734_c482f2551d23482c763061efa335917a.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1662',
			'product_id' => '170',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713009383189_09b493ffbbe90e661c1a6c9058e3a371.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1663',
			'product_id' => '170',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713009459783_5efe4adf03ef08031401e4eea95f26df.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1664',
			'product_id' => '170',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713009528507_53bcfe28790431ae44ff7e59532c2836.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1665',
			'product_id' => '170',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713009608709_cc98433b2e876d1fe904abdba4f21f21.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1666',
			'product_id' => '170',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713009667459_819e9cd51ac05c5de7bba77dd1831326.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1667',
			'product_id' => '170',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713009723755_347e4af5cfe750df65a4cc505fda57f9.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1668',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713007110899_45b9ed6f42285060ca1d5e2a4a9b6d44.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1669',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713007209002_4e6c41493428ef530f5964d0b37c3516.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1670',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713007308456_9037ade8569e032ff8eb77123dd8ce8c.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1671',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713007460459_216da91aa76b6f8d0d6dc9a751395501.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1672',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713007662670_1c358447d5cb798cd1620617c2d1d841.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1673',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713007845554_9822ab8d2ab55e2385cf0265ad70941a.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1674',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713008266283_35c45301146dbedb41ef492a71913a12.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1675',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713008336574_8bcdbd4594a3258d036e4d34cc98b85e.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1676',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713008383256_333d9572febe91c84e30a91babe3646d.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1677',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713008448348_72809c8950ad563c88b3344592fde9ea.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1678',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713008517851_a544dfa21242f8b30daf3fbd122bf3f1.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1679',
			'product_id' => '171',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3713008674303_bec9b467e411a47cbf1ae9364948d5fe.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1680',
			'product_id' => '172',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657932009014_6c27655e2c52f765849ae39da54cb121.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1681',
			'product_id' => '172',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657932012829_4f14c2cbcbf2d92107d39d2ea04975d7.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1682',
			'product_id' => '172',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657932015628_d80388de7222897a7b2b0a9dd1ee00bc.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1683',
			'product_id' => '172',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657932018547_ca8699ef1ba5de1ede1fbe2c2d6277e0.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1684',
			'product_id' => '172',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657932112902_9b06d13dff5ca6a3c1e3bec2332d85c2.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1685',
			'product_id' => '172',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657932141751_78b5ffd31e192e53029438df59a82d69.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1686',
			'product_id' => '172',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657932176184_529e4068af0d218f6f123dc1daa96a8b.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1687',
			'product_id' => '172',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657932245752_4196b73020a3bdae19d19d3ede22232e.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1688',
			'product_id' => '172',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657932276979_b1b1300f8bef5209d42fc776bcb132db.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1689',
			'product_id' => '172',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3657932325485_3b4a300585857d2952dd59862c13daea.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1690',
			'product_id' => '173',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645318994339_4269d4802b91d5f82618f570eb08b439.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1691',
			'product_id' => '173',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645319003926_a0f5345d9dec76dcf954ceeaa360a4d1.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1692',
			'product_id' => '173',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645319007640_34ef017f7aa42ef15124c3150d37d493.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1693',
			'product_id' => '173',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645319310792_9ccb1a9209994e97de7ed68f91b162d0.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1694',
			'product_id' => '173',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645319400511_60b19a7810a81a8a06ed37febd378c95.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1695',
			'product_id' => '173',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645319537420_938e4efc1e00b80992fedb6da5f3578b.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1696',
			'product_id' => '173',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3645319616856_51e5c6f8a24cb90d6460fb31ee6fcdf8.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1697',
			'product_id' => '174',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644788126857_49ec49291b11353e81037405e6ce8ff2.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1698',
			'product_id' => '174',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644788128773_295db97b5364d17789cd8c7ff827f23b.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1699',
			'product_id' => '174',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644788130587_343efa7c2c0f08271e98a7eaf7b49cb2.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1700',
			'product_id' => '174',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644788140692_2e2c45040ee0ab1e277349b6dbbc0eed.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1701',
			'product_id' => '174',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644788336970_c0d0696e57f0c3b897fb7bb64d7cc737.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1702',
			'product_id' => '174',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3644788381133_6386ec0f4c65737268f12c2933892164.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1703',
			'product_id' => '175',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649502170271_5d7cda71de85cf3b5457a1d128d2b777.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1704',
			'product_id' => '175',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649502175802_254b2f27faeadb2b18d9a69572ca9d76.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1705',
			'product_id' => '175',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649502204283_772f2936009d8e7275a767122e4d6b0c.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1706',
			'product_id' => '175',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649502811104_5f326daa269fe81af4b39d6e5313a0e7.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1707',
			'product_id' => '175',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649502973646_80d222418d288b39da08c60f49093b2a.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1708',
			'product_id' => '175',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649503101057_add28c416b260213471381f844e24307.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1709',
			'product_id' => '175',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649503617478_9cb7754b67cb4bc9597e57f9e1f9f3d7.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1710',
			'product_id' => '175',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649503681532_567ac92361ba4dbdb2dc15ee3b1ba59b.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1711',
			'product_id' => '176',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649503819878_bea272b822664ab4b6d5c1dcb8c03c93.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1712',
			'product_id' => '176',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649503881364_a620786de64cfbb77f043fd48e6bfca8.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1713',
			'product_id' => '176',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649503949201_74b287b390cd6d366fd0e27e5f9c8728.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1714',
			'product_id' => '176',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649504303699_14cf90d86b9c2c4f5c2e4cae894bbec9.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1715',
			'product_id' => '176',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649504390804_0ed6a2774ad6b8934970c435a7688932.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1716',
			'product_id' => '176',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649504441928_efdcb10444703dcb631c87505f89ffc0.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1717',
			'product_id' => '176',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649504492816_4564bc4373f40ebf2d19e39f77145be7.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1718',
			'product_id' => '176',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649504572144_5266a1988e51e81a769a3611a8d60144.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1719',
			'product_id' => '177',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649504657967_3f05a9a66cfdd6faa86d11aec81f414e.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1720',
			'product_id' => '177',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649504693898_3590b7e54b1bcf0c07bcafb7849f262c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1721',
			'product_id' => '177',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649504766720_f5c07ec502cb60f9bfe59f304be13170.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1722',
			'product_id' => '177',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649505145460_9fd5b3184ab6c90155f76df64a531191.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1723',
			'product_id' => '177',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649505263518_a316e041ceb818dcabac7bc189792e77.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1724',
			'product_id' => '177',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649505347040_859085a9e95d7e9b4f468bd31c11b962.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1725',
			'product_id' => '177',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649505429864_e82df7e1ea685bed70d7418cb4d427e6.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1726',
			'product_id' => '177',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3649505485779_1b77688bc87352c02562a72e03e0f960.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1727',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_6131-600x400.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1728',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/welcome8-2.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1729',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/3a2048c7f3f508ab51e4.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1730',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_6703.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1731',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_6159.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1732',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/790c6fe3d4d12f8f76c0.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1733',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_7239.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1734',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_1447.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1735',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_8507-compressed.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1736',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_7227-compressed.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1737',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_7221.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1738',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_6151.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1739',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_1432.jpg',
			'position'   => '24',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1740',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_7220-compressed.jpg',
			'position'   => '26',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1741',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_7149.jpg',
			'position'   => '30',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1742',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_8452-compressed.jpg',
			'position'   => '32',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1743',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_7449-compressed.jpg',
			'position'   => '34',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1744',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_7142-compressed.jpg',
			'position'   => '36',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1745',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_6756.jpg',
			'position'   => '38',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1746',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/2D763B6A-F9BE-4921-97C5-1067C126D10F-450x600.jpeg',
			'position'   => '40',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1747',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/6E833911-F7F6-4A13-85E1-96FA1161AADB-549x600.jpeg',
			'position'   => '42',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1748',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/9E44A6AB-B95F-40BC-A96F-9FC9CB3B95CD-600x573.jpeg',
			'position'   => '44',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1749',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/10B086ED-704A-4969-AB7A-BD1F2E9C5528-600x450.jpeg',
			'position'   => '46',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1750',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/26763801-664F-4FE8-B260-9BFAAFB68F7B-546x600.jpeg',
			'position'   => '48',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1751',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/A1D89AA6-8DEB-4E91-B5AF-FC11B76FE2A0-600x450.jpeg',
			'position'   => '50',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1752',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/B3D7A8DB-FA2B-4444-BCE7-F0D8BC28340C-450x600.jpeg',
			'position'   => '52',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1753',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/B1450A0B-CBE8-471E-B692-F968BEF84FE7-450x600.jpeg',
			'position'   => '54',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1754',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/B20917DB-116C-4AA1-AFED-B0C1C5B06E88-553x600.jpeg',
			'position'   => '56',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1755',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/BA323D5F-9EC8-4029-B81E-34EB5A446752-546x600.jpeg',
			'position'   => '58',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1756',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/C153FCC6-481B-43FC-A16B-E760A19D0DE0-600x535.jpeg',
			'position'   => '60',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1757',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/C6953225-2E35-4AD0-84C4-003EA91077F7-548x600.jpeg',
			'position'   => '62',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1758',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/D7904132-C5A2-43E6-8257-327E810BEC0E-600x453.jpeg',
			'position'   => '64',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1759',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/DA725D7E-ECCB-4970-B003-6822C4C870D7-452x600.jpeg',
			'position'   => '66',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1760',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/E71D7FF3-6E9C-4530-B139-3536E8D7E47F-555x600.jpeg',
			'position'   => '68',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1761',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/FCFC5052-914E-47A8-9DA8-D06FD1B5492F-338x600.jpeg',
			'position'   => '70',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1762',
			'product_id' => '178',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/cach-do-size-giay-copy-1-1-1-1-1.jpg',
			'position'   => '72',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1763',
			'product_id' => '179',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7139.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1764',
			'product_id' => '179',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7140.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1765',
			'product_id' => '179',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7141.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1766',
			'product_id' => '179',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7142.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1767',
			'product_id' => '179',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7142-compressed.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1768',
			'product_id' => '179',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7143.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1769',
			'product_id' => '179',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7143-compressed.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1770',
			'product_id' => '179',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7144.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1771',
			'product_id' => '180',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7145.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1772',
			'product_id' => '180',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7146.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1773',
			'product_id' => '180',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7147.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1774',
			'product_id' => '180',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7148.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1775',
			'product_id' => '180',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7149.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1776',
			'product_id' => '180',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/MG_7150.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1777',
			'product_id' => '181',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6139.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1778',
			'product_id' => '181',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6134.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1779',
			'product_id' => '181',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6131.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1780',
			'product_id' => '181',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6126.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1781',
			'product_id' => '181',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6123.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1782',
			'product_id' => '181',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6121.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1783',
			'product_id' => '181',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6119.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1784',
			'product_id' => '181',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6118.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1785',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6791.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1786',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6790.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1787',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6789.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1788',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6788.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1789',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6787.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1790',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6761.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1791',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6760.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1792',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6759.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1793',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6758.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1794',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6757.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1795',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6756.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1796',
			'product_id' => '182',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/03/MG_6755.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1797',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8451.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1798',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8451-compressed.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1799',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8452.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1800',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8452-compressed.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1801',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8453.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1802',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8454.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1803',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8455.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1804',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8456.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1805',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8501.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1806',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8501-compressed.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1807',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8502.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1808',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8503.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1809',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8504.jpg',
			'position'   => '24',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1810',
			'product_id' => '183',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/MG_8505.jpg',
			'position'   => '26',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1811',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127040767_72a0b866c4cbbeb9b93b488142f879ca.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1812',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127046005_a58adabc8c52b22faa07e20f3a896891.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1813',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127054151_1c6d89012df78986547acf55ca9edf0c.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1814',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127096832_6f18a262c9979493766cec2126d10d51.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1815',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127153913_bcf9030fe72c6ac354c7be2d1f7cea8e.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1816',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127196169_73bf656737a82620b5cd7fb3b433cb8f.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1817',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127508407_d05224400878d73b2266363fb7cf9ef0.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1818',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127581574_76954b9a5dc4943e8af817b0f343c014.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1819',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127609413_c6afe1dffc7254e29b6962eed729a3df.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1820',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127649954_98b4420845b6bc6b51dc6249ae580c46.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1821',
			'product_id' => '184',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/10/z3676127685798_32f1b52d534bc1e41e1a01a688c09129.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1822',
			'product_id' => '185',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647648764386_063a04fe20a949051e5a523325e40188.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1823',
			'product_id' => '185',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647648761303_f3159c00051d42e5c91727e974402ad1.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1824',
			'product_id' => '185',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647648756856_895351b85f31ac1b2fef5ad1732e644b.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1825',
			'product_id' => '185',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647648751302_6917ca508a1c4a8466fdb5b60bf1e88f.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1826',
			'product_id' => '185',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647648729273_534653751e723943dca99b81f19aa338.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1827',
			'product_id' => '185',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647648724135_9d84ae025969e325e2f8ac92babe9234.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1828',
			'product_id' => '185',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2021/12/z3647648719642_8dc154f57dd07f1f7048cefd64c5774d.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1829',
			'product_id' => '186',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634953969928_2807eef5ed4518cff13f2252e42eefd7.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1830',
			'product_id' => '186',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634953978169_00c455fa9eba658c90ad8609d23c03f0.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1831',
			'product_id' => '186',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634953980831_c89e066fabd43ab067e588fcdd22f56f.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1832',
			'product_id' => '186',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634954005570_dc3fe68333f7bac6b777291ec8b23334.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1833',
			'product_id' => '186',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634954075585_9bf4a6ff43764e5b2eabed365e5db671.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1834',
			'product_id' => '186',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634954095259_31241093c9d30e634cb0b67d62691b1d.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1835',
			'product_id' => '186',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634954173779_0e321cb65bdc429b1a7f621d1f219a0e.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1836',
			'product_id' => '186',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634954184134_f438e396cf0eb42fe21d81f5ba9a0671.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1837',
			'product_id' => '186',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634954243725_4b85cbd3a4b000142d911098e146469d.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1838',
			'product_id' => '186',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3634954285390_4c3ad2a144f54e7c06d6185cb6f22baf.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1839',
			'product_id' => '187',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730956566864_f0d7103b2a19a247174c3b720566983d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1840',
			'product_id' => '187',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730956643216_a1a57e9cec1ebd9b9d85ed2a71de5957.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1841',
			'product_id' => '187',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730957011404_40db5a73d9402b05b3e19da25d42d9b7.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1842',
			'product_id' => '187',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730957070540_b587eb7e4ef24459778dc0d0ead7c90d.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1843',
			'product_id' => '187',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730957121886_c66c1fd8cd09db8860d740138b0dd919.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1844',
			'product_id' => '187',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730957202886_30d6b4c1c9a0251f6e9f3ad964cd60d7.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1845',
			'product_id' => '187',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/09/z3730957252530_9c74a7137f0c16f3c7ddfa4b6063715d.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1846',
			'product_id' => '188',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670684913509_5a94d717d340293d818fed145922d181.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1847',
			'product_id' => '188',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670684942628_7f57ae078579fb3a9c419b7ee3db011c.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1848',
			'product_id' => '188',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670684946767_7b6b3c50dda5e90061da6fd87118470e.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1849',
			'product_id' => '188',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670684952053_6c7811acf19ce80bbb546907d004c54c.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1850',
			'product_id' => '188',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670684955577_2785ec35ddfd0054dfa8a69b2c3ba687.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1851',
			'product_id' => '188',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670684959596_ca346c62df3adfb348b8ead5ef5e8750.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1852',
			'product_id' => '188',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670684966582_71ae2437c8e98b554ebc51a9bc63ddb4.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1853',
			'product_id' => '188',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670684896797_5e8f66acc3335f2925380057326b104c.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1854',
			'product_id' => '188',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670684902446_ad27e4388df7badf5c8f3d80f8fb36d9.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1855',
			'product_id' => '188',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2019/02/z3670684907062_3d6e831c30fa69f0a6873985259421ed.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1856',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670630704306_39a8ca1f7e19cff6fd1dea56657160d1.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1857',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670630704307_5516a8f172161d68d0a77b1e9f433e89.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1858',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670630704308_5500cafd7b121b1c250822180f7b6d91.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1859',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670630704309_db1b0108e7285b230861757162b78c60.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1860',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670630704310_200ce079ccdf9a69a26b2f38d111f8bf.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1861',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670630872212_dda413543cd0ea5924f030f256715fb5.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1862',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670630913819_c73f5008ef8c7d0a63e20dd216b47657.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1863',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670630960678_46caa394c8f5d1a272a7dfe765f8998f.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1864',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670631004136_396bae7ed1011c7f29f4615d50ff2aaf.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1865',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670631047530_12ee961624b5aa7e799c2cdc80682c91.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1866',
			'product_id' => '189',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670631075806_ea2be8241987fa556a05bf52a4f5ec5f.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1867',
			'product_id' => '190',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673912168609_dfbc0ffe38b6ffad815aacebdb3a9887.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1868',
			'product_id' => '190',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673912112677_cb5c823e32382669d2862e8f327f4c9e.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1869',
			'product_id' => '190',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673912099252_886838c29cd2b8090f082b1105ffd738.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1870',
			'product_id' => '190',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673912085713_9ff5d683fef58e83772bf15b50a28cea.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1871',
			'product_id' => '190',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673912055076_041c28431c7ade68b3e273f1046d1c7d.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1872',
			'product_id' => '190',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673912013278_582e1e92799903ae056a8a684f2592e8.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1873',
			'product_id' => '190',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673912009013_6a784d12a104e014ac0c3f54e695330b.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1874',
			'product_id' => '190',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673912003872_6b30d2328bd388ef455aca2098b5065d.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1875',
			'product_id' => '190',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3673911995065_9564f724cdbbbdbace4ff02301e8f3ee.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1876',
			'product_id' => '191',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670702344792_d4dc678fc062234ab24d6bc355cc0bb8.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1877',
			'product_id' => '191',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670702350007_786ba7f0eddc10943d3c1ad5a0c644f2.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1878',
			'product_id' => '191',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670702400773_9020944c9fd45911afdf1767f45ec4bf.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1879',
			'product_id' => '191',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670702457884_b6b859467d7b76f65e3ae8ee77b9db3c.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1880',
			'product_id' => '191',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670702596193_c73e214d18b2d61faccd1999e77f3be1.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1881',
			'product_id' => '191',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670703082638_12e3294535496820df15a9320231bfde.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1882',
			'product_id' => '191',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670703167991_5a9f906d4389c879cdd1c76951bf604a.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1883',
			'product_id' => '191',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670703227851_bd468583feb027be0e4ca30dae596b5f.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1884',
			'product_id' => '191',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670703282843_b1e321c0b184ad50036bcd440f11b3bf.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1885',
			'product_id' => '191',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670703354970_cb66b22c1c7a85280a05dfb5b1e6e008.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1886',
			'product_id' => '192',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670719038993_9c5b378c15d04f5828a52e1fcf0f245d.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1887',
			'product_id' => '192',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670719041327_8385ec4474971ab98a648156a436bd04.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1888',
			'product_id' => '192',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670719046883_d9a0acbc9699c73e4620840d96329b4f.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1889',
			'product_id' => '192',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670719050717_f8c5e4f4ad2600e802b834db20a1c7cc.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1890',
			'product_id' => '192',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670719129823_be0feb9cf71df4841a7ee41e17080602.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1891',
			'product_id' => '192',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670719168034_6009e15e3edbc343fbe64cb39e48c778.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1892',
			'product_id' => '192',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670719235195_d5c0dede3ceef331bf1cd951fa4252ff.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1893',
			'product_id' => '192',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670719294655_79fdaa3bcbf2475d3a3ffff0a84cf7a1.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1894',
			'product_id' => '192',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670719302651_3881389bb9405986ae9648128aecc400.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1895',
			'product_id' => '192',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/07/z3670719365905_5fcbd9c219cceb443c274723dd8bcc14.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1896',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630703997_9f3b0d9549b7d33fd13f2126e567d51f.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1897',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630704000_31659169df0d96cc0158707f03e641b9.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1898',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630704006_d0a6cc840768aed92f1b484f2e92ffa8.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1899',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630704010_7bd7c40b788c2aeab60f75917af18dda.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1900',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630704028_dd5a8710cc94064e555858048094d343.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1901',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630704269_0fb433ca148e944b41146ed868a50eda.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1902',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630704273_32707d90b2f70ff663434aca3a84f30e.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1903',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630704277_181d68fc7001f9e105bfc4ba7db67965.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1904',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630704297_7a6b5c81ec8806f8cb3df7ee239549a1.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1905',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630704300_4fd8806aa3f9e4d700be9bffe6b56dae.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1906',
			'product_id' => '193',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2022/08/z3670630704303_18fe1ccdcd06939a8ad397587aba5eb6.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1907',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_1459-1-600x400.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1908',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/48caf0294b1bb045e90a.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1909',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_6322-1.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1910',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/IMG_3546-compressed.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1911',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/3f7224779f45641b3d54-compressed-1.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1912',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_1460-1.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1913',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_1462-1.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1914',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/IMG_3526.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1915',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_8020.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1916',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/IMG_3537-e1578424775249.jpg',
			'position'   => '18',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1917',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/IMG_3551.jpg',
			'position'   => '20',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1918',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/IMG_3542-e1578424763406.jpg',
			'position'   => '22',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1919',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/IMG_3526-1.jpg',
			'position'   => '24',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1920',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_1466-1.jpg',
			'position'   => '28',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1921',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/MG_1458-1.jpg',
			'position'   => '30',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1922',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/5DCCA172-DC54-4568-BFB7-FFFE625AD95C-1-450x600.jpeg',
			'position'   => '32',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1923',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/8EB46200-297B-40B8-BE7B-9EE95BDA5716-1-548x600.jpeg',
			'position'   => '34',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1924',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/9E3D883F-77CD-4D6C-8B91-8C02E00ED49D-1-548x600.jpeg',
			'position'   => '36',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1925',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/89439D2C-2E8A-4D7B-A8F6-E3A706B71765-1-600x572.jpeg',
			'position'   => '38',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1926',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/71858223-504C-4530-9385-1E01BFF81AE5-1-544x600.jpeg',
			'position'   => '40',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1927',
			'product_id' => '194',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/03/FA3A8E45-ED40-407B-9D69-8110378468BF-1-547x600.jpeg',
			'position'   => '42',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1928',
			'product_id' => '195',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/11/z3678379973334_8ad4b838f575b6ba9133a517b87cfcf7.jpg',
			'position'   => '0',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1929',
			'product_id' => '195',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/11/z3678379975850_2f7999f103655ea2ad7dbab817126c0e.jpg',
			'position'   => '2',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1930',
			'product_id' => '195',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/11/z3678379980358_b8a4a4caf304434abb2322715ff7b902.jpg',
			'position'   => '4',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1931',
			'product_id' => '195',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/11/z3678379983154_ea0b9e3d05a9f971ab0eed16556ec0ff.jpg',
			'position'   => '6',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1932',
			'product_id' => '195',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/11/z3678379987225_2d91bc34e59d80e772c593a996550d52.jpg',
			'position'   => '8',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1933',
			'product_id' => '195',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/11/z3678379991279_a1cbeec35496700f94338f372ca72e44.jpg',
			'position'   => '10',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1934',
			'product_id' => '195',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/11/z3678380018575_19d2f868cc4cfb980b1f86cac0ef734e.jpg',
			'position'   => '12',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1935',
			'product_id' => '195',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/11/z3678380022929_4564dff0a59f5f53985c4299dccf93cc.jpg',
			'position'   => '14',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1936',
			'product_id' => '195',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/11/z3678380026311_59ae4d02bbaf34ca2cedc030e2f835b6.jpg',
			'position'   => '16',
		]);
		$this->insert('{{%product_image}}', [
			'id'         => '1937',
			'product_id' => '195',
			'image'      => 'https://giayxshop.vn/wp-content/uploads/2020/11/z3678380029301_1f27112e1655a7238e19c1e81744a535.jpg',
			'position'   => '18',
		]);
	}

	public function safeDown() {
		$this->dropIndex('product_id', '{{%product_image}}');
		$this->dropTable('{{%product_image}}');
	}
}

<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092516_order_detail extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%order_detail}}', [
			'id'         => Schema::TYPE_PK . '',
			'order_id'   => Schema::TYPE_INTEGER . ' NOT NULL',
			'product_id' => Schema::TYPE_INTEGER . '',
			'quantity'   => Schema::TYPE_INTEGER . ' NOT NULL',
			'amount'     => Schema::TYPE_INTEGER . ' NOT NULL',
		], $tableOptions);
		$this->createIndex('fk_oder_id', '{{%order_detail}}', 'order_id', 0);
		$this->createIndex('product_id', '{{%order_detail}}', 'product_id', 0);
	}

	public function safeDown() {
		$this->dropIndex('fk_oder_id', '{{%order_detail}}');
		$this->dropIndex('product_id', '{{%order_detail}}');
		$this->dropTable('{{%order_detail}}');
	}
}

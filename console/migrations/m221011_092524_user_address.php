<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092524_user_address extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%user_address}}', [
			'id'       => Schema::TYPE_PK . '',
			'user_id'  => Schema::TYPE_INTEGER . ' NOT NULL',
			'fullname' => Schema::TYPE_STRING . '(1000) NOT NULL',
			'phone'    => Schema::TYPE_TEXT . ' NOT NULL',
			'address'  => Schema::TYPE_STRING . '(1000) NOT NULL',
			'city'     => Schema::TYPE_STRING . '(500) NOT NULL',
			'province' => Schema::TYPE_STRING . '(500) NOT NULL',
			'country'  => Schema::TYPE_STRING . '(500) NOT NULL',
		], $tableOptions);
		$this->createIndex('fk_user_id', '{{%user_address}}', 'user_id', 0);
	}

	public function safeDown() {
		$this->dropIndex('fk_user_id', '{{%user_address}}');
		$this->dropTable('{{%user_address}}');
	}
}

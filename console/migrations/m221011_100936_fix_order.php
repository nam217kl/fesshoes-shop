<?php

use yii\db\Migration;

/**
 * Class m221011_100936_fix_order
 */
class m221011_100936_fix_order extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->execute('ALTER TABLE `order` DROP FOREIGN KEY `fk_order_user_address_id`; ALTER TABLE `order` ADD CONSTRAINT `fk_order_user_address_id` FOREIGN KEY (`user_address_id`) REFERENCES `user_address`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m221011_100936_fix_order cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m221011_100936_fix_order cannot be reverted.\n";

		return false;
	}
	*/
}

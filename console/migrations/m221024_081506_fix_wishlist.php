<?php

use yii\db\Migration;

/**
 * Class m221024_081506_fix_wishlist
 */
class m221024_081506_fix_wishlist extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->execute("ALTER TABLE `wishlist` ADD `updated_at` INT NULL AFTER `created_at`;");
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m221024_081506_fix_wishlist cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m221024_081506_fix_wishlist cannot be reverted.\n";

		return false;
	}
	*/
}

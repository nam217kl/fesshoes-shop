<?php

use yii\db\Migration;

/**
 * Class m221018_083255_fix_cart
 */
class m221018_083255_fix_cart extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->execute("ALTER TABLE `cart` ADD `status` ENUM('pending','complete','draft') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft' AFTER `product_detail_id`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m221018_083255_fix_cart cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221018_083255_fix_cart cannot be reverted.\n";

        return false;
    }
    */
}

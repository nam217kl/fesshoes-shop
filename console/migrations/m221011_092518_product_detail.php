<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092518_product_detail extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%product_detail}}', [
			'id'         => Schema::TYPE_PK . '',
			'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'size'       => Schema::TYPE_STRING . '(255) NOT NULL',
			'color'      => Schema::TYPE_STRING . '(255) NOT NULL',
			'quantity'   => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT "0"',
			'price'      => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT "0"',
		], $tableOptions);
		$this->createIndex('fk_product_id', '{{%product_detail}}', 'product_id', 0);
		$this->insert('{{%product_detail}}', [
			'id'         => '44',
			'product_id' => '1',
			'size'       => '36',
			'color'      => 'caro',
			'quantity'   => '15',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '45',
			'product_id' => '1',
			'size'       => '37',
			'color'      => 'caro',
			'quantity'   => '11',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '46',
			'product_id' => '1',
			'size'       => '38',
			'color'      => 'caro',
			'quantity'   => '10',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '47',
			'product_id' => '1',
			'size'       => '39',
			'color'      => 'caro',
			'quantity'   => '14',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '48',
			'product_id' => '1',
			'size'       => '40',
			'color'      => 'caro',
			'quantity'   => '11',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '49',
			'product_id' => '1',
			'size'       => '41',
			'color'      => 'caro',
			'quantity'   => '10',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '50',
			'product_id' => '1',
			'size'       => '42',
			'color'      => 'caro',
			'quantity'   => '6',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '51',
			'product_id' => '1',
			'size'       => '43',
			'color'      => 'caro',
			'quantity'   => '13',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '52',
			'product_id' => '2',
			'size'       => '36',
			'color'      => 'Đen trắng',
			'quantity'   => '13',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '53',
			'product_id' => '2',
			'size'       => '37',
			'color'      => 'Đen trắng',
			'quantity'   => '11',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '54',
			'product_id' => '2',
			'size'       => '38',
			'color'      => 'Đen trắng',
			'quantity'   => '16',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '55',
			'product_id' => '2',
			'size'       => '39',
			'color'      => 'Đen trắng',
			'quantity'   => '5',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '56',
			'product_id' => '2',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '13',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '57',
			'product_id' => '2',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '20',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '58',
			'product_id' => '2',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '5',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '59',
			'product_id' => '2',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '19',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '60',
			'product_id' => '3',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '10',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '61',
			'product_id' => '3',
			'size'       => '41',
			'color'      => 'Trắng đen',
			'quantity'   => '7',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '62',
			'product_id' => '3',
			'size'       => '42',
			'color'      => 'Trắng đen',
			'quantity'   => '20',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '63',
			'product_id' => '3',
			'size'       => '43',
			'color'      => 'Trắng đen',
			'quantity'   => '8',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '64',
			'product_id' => '4',
			'size'       => '36',
			'color'      => 'Đen',
			'quantity'   => '16',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '65',
			'product_id' => '4',
			'size'       => '37',
			'color'      => 'Đen',
			'quantity'   => '16',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '66',
			'product_id' => '4',
			'size'       => '38',
			'color'      => 'Đen',
			'quantity'   => '9',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '67',
			'product_id' => '4',
			'size'       => '39',
			'color'      => 'Đen',
			'quantity'   => '7',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '68',
			'product_id' => '4',
			'size'       => '40',
			'color'      => 'Đen',
			'quantity'   => '8',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '69',
			'product_id' => '4',
			'size'       => '41',
			'color'      => 'Đen',
			'quantity'   => '9',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '70',
			'product_id' => '4',
			'size'       => '42',
			'color'      => 'Đen',
			'quantity'   => '11',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '71',
			'product_id' => '4',
			'size'       => '43',
			'color'      => 'Đen',
			'quantity'   => '6',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '72',
			'product_id' => '5',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '19',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '73',
			'product_id' => '5',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '7',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '74',
			'product_id' => '5',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '19',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '75',
			'product_id' => '5',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '11',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '76',
			'product_id' => '6',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '7',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '77',
			'product_id' => '6',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '14',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '78',
			'product_id' => '6',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '7',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '79',
			'product_id' => '6',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '19',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '80',
			'product_id' => '7',
			'size'       => '40',
			'color'      => 'xanh lá',
			'quantity'   => '8',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '81',
			'product_id' => '7',
			'size'       => '41',
			'color'      => 'xanh lá',
			'quantity'   => '15',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '82',
			'product_id' => '7',
			'size'       => '42',
			'color'      => 'xanh lá',
			'quantity'   => '20',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '83',
			'product_id' => '7',
			'size'       => '43',
			'color'      => 'xanh lá',
			'quantity'   => '17',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '84',
			'product_id' => '8',
			'size'       => '35',
			'color'      => 'Đen',
			'quantity'   => '6',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '85',
			'product_id' => '8',
			'size'       => '36',
			'color'      => 'Đen',
			'quantity'   => '8',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '86',
			'product_id' => '8',
			'size'       => '37',
			'color'      => 'Đen',
			'quantity'   => '19',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '87',
			'product_id' => '8',
			'size'       => '38',
			'color'      => 'Đen',
			'quantity'   => '5',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '88',
			'product_id' => '8',
			'size'       => '39',
			'color'      => 'Đen',
			'quantity'   => '15',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '89',
			'product_id' => '8',
			'size'       => '40',
			'color'      => 'Đen',
			'quantity'   => '7',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '90',
			'product_id' => '8',
			'size'       => '41',
			'color'      => 'Đen',
			'quantity'   => '7',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '91',
			'product_id' => '8',
			'size'       => '42',
			'color'      => 'Đen',
			'quantity'   => '20',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '92',
			'product_id' => '8',
			'size'       => '43',
			'color'      => 'Đen',
			'quantity'   => '15',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '93',
			'product_id' => '9',
			'size'       => '36',
			'color'      => 'Đen',
			'quantity'   => '6',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '94',
			'product_id' => '9',
			'size'       => '37',
			'color'      => 'Đen',
			'quantity'   => '10',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '95',
			'product_id' => '9',
			'size'       => '38',
			'color'      => 'Đen',
			'quantity'   => '16',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '96',
			'product_id' => '9',
			'size'       => '39',
			'color'      => 'Đen',
			'quantity'   => '11',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '97',
			'product_id' => '9',
			'size'       => '40',
			'color'      => 'Đen',
			'quantity'   => '9',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '98',
			'product_id' => '9',
			'size'       => '41',
			'color'      => 'Đen',
			'quantity'   => '11',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '99',
			'product_id' => '9',
			'size'       => '42',
			'color'      => 'Đen',
			'quantity'   => '18',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '100',
			'product_id' => '9',
			'size'       => '43',
			'color'      => 'Đen',
			'quantity'   => '8',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '101',
			'product_id' => '10',
			'size'       => '36',
			'color'      => 'Đen trắng',
			'quantity'   => '18',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '102',
			'product_id' => '10',
			'size'       => '37',
			'color'      => 'Đen trắng',
			'quantity'   => '13',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '103',
			'product_id' => '10',
			'size'       => '38',
			'color'      => 'Đen trắng',
			'quantity'   => '10',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '104',
			'product_id' => '10',
			'size'       => '39',
			'color'      => 'Đen trắng',
			'quantity'   => '19',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '105',
			'product_id' => '10',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '20',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '106',
			'product_id' => '10',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '7',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '107',
			'product_id' => '10',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '15',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '108',
			'product_id' => '10',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '20',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '109',
			'product_id' => '11',
			'size'       => '36',
			'color'      => 'phản quang',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '110',
			'product_id' => '11',
			'size'       => '37',
			'color'      => 'phản quang',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '111',
			'product_id' => '11',
			'size'       => '38',
			'color'      => 'phản quang',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '112',
			'product_id' => '11',
			'size'       => '39',
			'color'      => 'phản quang',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '113',
			'product_id' => '11',
			'size'       => '40',
			'color'      => 'phản quang',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '114',
			'product_id' => '11',
			'size'       => '41',
			'color'      => 'phản quang',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '115',
			'product_id' => '11',
			'size'       => '42',
			'color'      => 'phản quang',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '116',
			'product_id' => '11',
			'size'       => '43',
			'color'      => 'phản quang',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '117',
			'product_id' => '12',
			'size'       => '36',
			'color'      => 'Trắng đen',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '118',
			'product_id' => '12',
			'size'       => '37',
			'color'      => 'Trắng đen',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '119',
			'product_id' => '12',
			'size'       => '38',
			'color'      => 'Trắng đen',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '120',
			'product_id' => '12',
			'size'       => '39',
			'color'      => 'Trắng đen',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '121',
			'product_id' => '12',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '122',
			'product_id' => '12',
			'size'       => '41',
			'color'      => 'Trắng đen',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '123',
			'product_id' => '12',
			'size'       => '42',
			'color'      => 'Trắng đen',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '124',
			'product_id' => '12',
			'size'       => '43',
			'color'      => 'Trắng đen',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '125',
			'product_id' => '13',
			'size'       => '36',
			'color'      => 'Kem',
			'quantity'   => '7',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '126',
			'product_id' => '13',
			'size'       => '37',
			'color'      => 'Kem',
			'quantity'   => '18',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '127',
			'product_id' => '13',
			'size'       => '38',
			'color'      => 'Kem',
			'quantity'   => '13',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '128',
			'product_id' => '13',
			'size'       => '39',
			'color'      => 'Kem',
			'quantity'   => '13',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '129',
			'product_id' => '13',
			'size'       => '40',
			'color'      => 'Kem',
			'quantity'   => '12',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '130',
			'product_id' => '13',
			'size'       => '41',
			'color'      => 'Kem',
			'quantity'   => '17',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '131',
			'product_id' => '13',
			'size'       => '42',
			'color'      => 'Kem',
			'quantity'   => '9',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '132',
			'product_id' => '13',
			'size'       => '43',
			'color'      => 'Kem',
			'quantity'   => '17',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '133',
			'product_id' => '14',
			'size'       => '36',
			'color'      => 'xám trắng',
			'quantity'   => '14',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '134',
			'product_id' => '14',
			'size'       => '37',
			'color'      => 'xám trắng',
			'quantity'   => '5',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '135',
			'product_id' => '14',
			'size'       => '38',
			'color'      => 'xám trắng',
			'quantity'   => '18',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '136',
			'product_id' => '14',
			'size'       => '39',
			'color'      => 'xám trắng',
			'quantity'   => '6',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '137',
			'product_id' => '14',
			'size'       => '40',
			'color'      => 'xám trắng',
			'quantity'   => '13',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '138',
			'product_id' => '14',
			'size'       => '41',
			'color'      => 'xám trắng',
			'quantity'   => '14',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '139',
			'product_id' => '14',
			'size'       => '42',
			'color'      => 'xám trắng',
			'quantity'   => '6',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '140',
			'product_id' => '14',
			'size'       => '43',
			'color'      => 'xám trắng',
			'quantity'   => '10',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '141',
			'product_id' => '15',
			'size'       => '36',
			'color'      => 'Xanh dương',
			'quantity'   => '6',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '142',
			'product_id' => '15',
			'size'       => '37',
			'color'      => 'Xanh dương',
			'quantity'   => '19',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '143',
			'product_id' => '15',
			'size'       => '38',
			'color'      => 'Xanh dương',
			'quantity'   => '6',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '144',
			'product_id' => '15',
			'size'       => '39',
			'color'      => 'Xanh dương',
			'quantity'   => '15',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '145',
			'product_id' => '16',
			'size'       => '36',
			'color'      => 'Kem',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '146',
			'product_id' => '16',
			'size'       => '37',
			'color'      => 'Kem',
			'quantity'   => '12',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '147',
			'product_id' => '16',
			'size'       => '38',
			'color'      => 'Kem',
			'quantity'   => '5',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '148',
			'product_id' => '16',
			'size'       => '39',
			'color'      => 'Kem',
			'quantity'   => '14',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '149',
			'product_id' => '16',
			'size'       => '40',
			'color'      => 'Kem',
			'quantity'   => '12',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '150',
			'product_id' => '16',
			'size'       => '41',
			'color'      => 'Kem',
			'quantity'   => '7',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '151',
			'product_id' => '16',
			'size'       => '42',
			'color'      => 'Kem',
			'quantity'   => '15',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '152',
			'product_id' => '16',
			'size'       => '43',
			'color'      => 'Kem',
			'quantity'   => '11',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '153',
			'product_id' => '17',
			'size'       => '36',
			'color'      => 'xám đỏ',
			'quantity'   => '13',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '154',
			'product_id' => '17',
			'size'       => '37',
			'color'      => 'xám đỏ',
			'quantity'   => '8',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '155',
			'product_id' => '17',
			'size'       => '38',
			'color'      => 'xám đỏ',
			'quantity'   => '6',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '156',
			'product_id' => '17',
			'size'       => '39',
			'color'      => 'xám đỏ',
			'quantity'   => '8',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '157',
			'product_id' => '18',
			'size'       => '36',
			'color'      => 'kem vàng',
			'quantity'   => '15',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '158',
			'product_id' => '18',
			'size'       => '37',
			'color'      => 'kem vàng',
			'quantity'   => '19',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '159',
			'product_id' => '18',
			'size'       => '38',
			'color'      => 'kem vàng',
			'quantity'   => '14',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '160',
			'product_id' => '18',
			'size'       => '39',
			'color'      => 'kem vàng',
			'quantity'   => '20',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '161',
			'product_id' => '18',
			'size'       => '40',
			'color'      => 'kem vàng',
			'quantity'   => '14',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '162',
			'product_id' => '18',
			'size'       => '41',
			'color'      => 'kem vàng',
			'quantity'   => '20',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '163',
			'product_id' => '18',
			'size'       => '42',
			'color'      => 'kem vàng',
			'quantity'   => '19',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '164',
			'product_id' => '18',
			'size'       => '43',
			'color'      => 'kem vàng',
			'quantity'   => '12',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '165',
			'product_id' => '19',
			'size'       => '36',
			'color'      => 'trắng nâu',
			'quantity'   => '5',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '166',
			'product_id' => '19',
			'size'       => '37',
			'color'      => 'trắng nâu',
			'quantity'   => '6',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '167',
			'product_id' => '19',
			'size'       => '38',
			'color'      => 'trắng nâu',
			'quantity'   => '7',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '168',
			'product_id' => '19',
			'size'       => '39',
			'color'      => 'trắng nâu',
			'quantity'   => '17',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '169',
			'product_id' => '19',
			'size'       => '40',
			'color'      => 'trắng nâu',
			'quantity'   => '18',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '170',
			'product_id' => '19',
			'size'       => '41',
			'color'      => 'trắng nâu',
			'quantity'   => '9',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '171',
			'product_id' => '19',
			'size'       => '42',
			'color'      => 'trắng nâu',
			'quantity'   => '17',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '172',
			'product_id' => '19',
			'size'       => '43',
			'color'      => 'trắng nâu',
			'quantity'   => '7',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '173',
			'product_id' => '20',
			'size'       => '40',
			'color'      => 'xám đỏ',
			'quantity'   => '15',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '174',
			'product_id' => '20',
			'size'       => '41',
			'color'      => 'xám đỏ',
			'quantity'   => '19',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '175',
			'product_id' => '20',
			'size'       => '42',
			'color'      => 'xám đỏ',
			'quantity'   => '19',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '176',
			'product_id' => '20',
			'size'       => '43',
			'color'      => 'xám đỏ',
			'quantity'   => '18',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '177',
			'product_id' => '21',
			'size'       => '36',
			'color'      => 'nâu hồng',
			'quantity'   => '11',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '178',
			'product_id' => '21',
			'size'       => '37',
			'color'      => 'nâu hồng',
			'quantity'   => '6',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '179',
			'product_id' => '21',
			'size'       => '38',
			'color'      => 'nâu hồng',
			'quantity'   => '5',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '180',
			'product_id' => '21',
			'size'       => '39',
			'color'      => 'nâu hồng',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '181',
			'product_id' => '22',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '182',
			'product_id' => '22',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '6',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '183',
			'product_id' => '22',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '10',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '184',
			'product_id' => '22',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '185',
			'product_id' => '23',
			'size'       => '36',
			'color'      => 'xanh xám',
			'quantity'   => '8',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '186',
			'product_id' => '23',
			'size'       => '37',
			'color'      => 'xanh xám',
			'quantity'   => '12',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '187',
			'product_id' => '23',
			'size'       => '38',
			'color'      => 'xanh xám',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '188',
			'product_id' => '23',
			'size'       => '39',
			'color'      => 'xanh xám',
			'quantity'   => '18',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '189',
			'product_id' => '23',
			'size'       => '40',
			'color'      => 'xanh xám',
			'quantity'   => '13',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '190',
			'product_id' => '23',
			'size'       => '41',
			'color'      => 'xanh xám',
			'quantity'   => '17',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '191',
			'product_id' => '23',
			'size'       => '42',
			'color'      => 'xanh xám',
			'quantity'   => '13',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '192',
			'product_id' => '23',
			'size'       => '43',
			'color'      => 'xanh xám',
			'quantity'   => '12',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '193',
			'product_id' => '24',
			'size'       => '36',
			'color'      => 'Hồng trắng',
			'quantity'   => '6',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '194',
			'product_id' => '24',
			'size'       => '37',
			'color'      => 'Hồng trắng',
			'quantity'   => '11',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '195',
			'product_id' => '24',
			'size'       => '38',
			'color'      => 'Hồng trắng',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '196',
			'product_id' => '24',
			'size'       => '39',
			'color'      => 'Hồng trắng',
			'quantity'   => '19',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '197',
			'product_id' => '25',
			'size'       => '36',
			'color'      => 'Trắng xanh',
			'quantity'   => '15',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '198',
			'product_id' => '25',
			'size'       => '37',
			'color'      => 'Trắng xanh',
			'quantity'   => '8',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '199',
			'product_id' => '25',
			'size'       => '38',
			'color'      => 'Trắng xanh',
			'quantity'   => '6',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '200',
			'product_id' => '25',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '19',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '201',
			'product_id' => '25',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '13',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '202',
			'product_id' => '25',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '19',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '203',
			'product_id' => '25',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '12',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '204',
			'product_id' => '25',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '19',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '205',
			'product_id' => '26',
			'size'       => '36',
			'color'      => 'trắng xanh than',
			'quantity'   => '7',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '206',
			'product_id' => '26',
			'size'       => '37',
			'color'      => 'trắng xanh than',
			'quantity'   => '20',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '207',
			'product_id' => '26',
			'size'       => '38',
			'color'      => 'trắng xanh than',
			'quantity'   => '16',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '208',
			'product_id' => '26',
			'size'       => '39',
			'color'      => 'trắng xanh than',
			'quantity'   => '11',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '209',
			'product_id' => '26',
			'size'       => '40',
			'color'      => 'trắng xanh than',
			'quantity'   => '16',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '210',
			'product_id' => '26',
			'size'       => '41',
			'color'      => 'trắng xanh than',
			'quantity'   => '5',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '211',
			'product_id' => '26',
			'size'       => '42',
			'color'      => 'trắng xanh than',
			'quantity'   => '5',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '212',
			'product_id' => '26',
			'size'       => '43',
			'color'      => 'trắng xanh than',
			'quantity'   => '18',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '213',
			'product_id' => '27',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '20',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '214',
			'product_id' => '28',
			'size'       => '36',
			'color'      => 'xám trắng',
			'quantity'   => '8',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '215',
			'product_id' => '28',
			'size'       => '37',
			'color'      => 'xám trắng',
			'quantity'   => '6',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '216',
			'product_id' => '28',
			'size'       => '38',
			'color'      => 'xám trắng',
			'quantity'   => '15',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '217',
			'product_id' => '28',
			'size'       => '39',
			'color'      => 'xám trắng',
			'quantity'   => '18',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '218',
			'product_id' => '29',
			'size'       => '36',
			'color'      => 'Đen trắng',
			'quantity'   => '7',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '219',
			'product_id' => '29',
			'size'       => '37',
			'color'      => 'Đen trắng',
			'quantity'   => '20',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '220',
			'product_id' => '29',
			'size'       => '38',
			'color'      => 'Đen trắng',
			'quantity'   => '11',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '221',
			'product_id' => '29',
			'size'       => '39',
			'color'      => 'Đen trắng',
			'quantity'   => '12',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '222',
			'product_id' => '30',
			'size'       => '36',
			'color'      => 'Đỏ',
			'quantity'   => '9',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '223',
			'product_id' => '30',
			'size'       => '37',
			'color'      => 'Đỏ',
			'quantity'   => '12',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '224',
			'product_id' => '30',
			'size'       => '38',
			'color'      => 'Đỏ',
			'quantity'   => '10',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '225',
			'product_id' => '30',
			'size'       => '39',
			'color'      => 'Đỏ',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '226',
			'product_id' => '30',
			'size'       => '40',
			'color'      => 'Đỏ',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '227',
			'product_id' => '30',
			'size'       => '41',
			'color'      => 'Đỏ',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '228',
			'product_id' => '30',
			'size'       => '42',
			'color'      => 'Đỏ',
			'quantity'   => '8',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '229',
			'product_id' => '30',
			'size'       => '43',
			'color'      => 'Đỏ',
			'quantity'   => '19',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '230',
			'product_id' => '31',
			'size'       => '36',
			'color'      => 'Trắng',
			'quantity'   => '19',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '231',
			'product_id' => '31',
			'size'       => '37',
			'color'      => 'Trắng',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '232',
			'product_id' => '31',
			'size'       => '38',
			'color'      => 'Trắng',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '233',
			'product_id' => '31',
			'size'       => '39',
			'color'      => 'Trắng',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '234',
			'product_id' => '31',
			'size'       => '40',
			'color'      => 'Trắng',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '235',
			'product_id' => '31',
			'size'       => '41',
			'color'      => 'Trắng',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '236',
			'product_id' => '31',
			'size'       => '42',
			'color'      => 'Trắng',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '237',
			'product_id' => '31',
			'size'       => '43',
			'color'      => 'Trắng',
			'quantity'   => '10',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '238',
			'product_id' => '32',
			'size'       => '36',
			'color'      => 'Trắng',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '239',
			'product_id' => '32',
			'size'       => '37',
			'color'      => 'Trắng',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '240',
			'product_id' => '32',
			'size'       => '38',
			'color'      => 'Trắng',
			'quantity'   => '19',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '241',
			'product_id' => '32',
			'size'       => '39',
			'color'      => 'Trắng',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '242',
			'product_id' => '32',
			'size'       => '40',
			'color'      => 'Trắng',
			'quantity'   => '12',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '243',
			'product_id' => '32',
			'size'       => '41',
			'color'      => 'Trắng',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '244',
			'product_id' => '32',
			'size'       => '42',
			'color'      => 'Trắng',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '245',
			'product_id' => '32',
			'size'       => '43',
			'color'      => 'Trắng',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '246',
			'product_id' => '33',
			'size'       => '36',
			'color'      => 'Trắng',
			'quantity'   => '12',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '247',
			'product_id' => '33',
			'size'       => '37',
			'color'      => 'Trắng',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '248',
			'product_id' => '33',
			'size'       => '38',
			'color'      => 'Trắng',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '249',
			'product_id' => '33',
			'size'       => '39',
			'color'      => 'Trắng',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '250',
			'product_id' => '33',
			'size'       => '40',
			'color'      => 'Trắng',
			'quantity'   => '18',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '251',
			'product_id' => '33',
			'size'       => '41',
			'color'      => 'Trắng',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '252',
			'product_id' => '33',
			'size'       => '42',
			'color'      => 'Trắng',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '253',
			'product_id' => '33',
			'size'       => '43',
			'color'      => 'Trắng',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '254',
			'product_id' => '34',
			'size'       => '36',
			'color'      => 'Trắng',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '255',
			'product_id' => '34',
			'size'       => '38',
			'color'      => 'Trắng',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '256',
			'product_id' => '34',
			'size'       => '39',
			'color'      => 'Trắng',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '257',
			'product_id' => '35',
			'size'       => '36',
			'color'      => 'trắng nâu',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '258',
			'product_id' => '35',
			'size'       => '37',
			'color'      => 'trắng nâu',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '259',
			'product_id' => '35',
			'size'       => '38',
			'color'      => 'trắng nâu',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '260',
			'product_id' => '35',
			'size'       => '39',
			'color'      => 'trắng nâu',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '261',
			'product_id' => '35',
			'size'       => '40',
			'color'      => 'trắng nâu',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '262',
			'product_id' => '35',
			'size'       => '41',
			'color'      => 'trắng nâu',
			'quantity'   => '8',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '263',
			'product_id' => '35',
			'size'       => '42',
			'color'      => 'trắng nâu',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '264',
			'product_id' => '35',
			'size'       => '43',
			'color'      => 'trắng nâu',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '265',
			'product_id' => '36',
			'size'       => '36',
			'color'      => 'trắng vàng',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '266',
			'product_id' => '36',
			'size'       => '37',
			'color'      => 'trắng vàng',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '267',
			'product_id' => '36',
			'size'       => '38',
			'color'      => 'trắng vàng',
			'quantity'   => '8',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '268',
			'product_id' => '36',
			'size'       => '39',
			'color'      => 'trắng vàng',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '269',
			'product_id' => '36',
			'size'       => '40',
			'color'      => 'trắng vàng',
			'quantity'   => '9',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '270',
			'product_id' => '36',
			'size'       => '41',
			'color'      => 'trắng vàng',
			'quantity'   => '18',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '271',
			'product_id' => '36',
			'size'       => '42',
			'color'      => 'trắng vàng',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '272',
			'product_id' => '36',
			'size'       => '43',
			'color'      => 'trắng vàng',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '273',
			'product_id' => '37',
			'size'       => '36',
			'color'      => 'trắng nâu',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '274',
			'product_id' => '37',
			'size'       => '37',
			'color'      => 'trắng nâu',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '275',
			'product_id' => '37',
			'size'       => '38',
			'color'      => 'trắng nâu',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '276',
			'product_id' => '37',
			'size'       => '39',
			'color'      => 'trắng nâu',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '277',
			'product_id' => '37',
			'size'       => '40',
			'color'      => 'trắng nâu',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '278',
			'product_id' => '37',
			'size'       => '41',
			'color'      => 'trắng nâu',
			'quantity'   => '8',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '279',
			'product_id' => '37',
			'size'       => '42',
			'color'      => 'trắng nâu',
			'quantity'   => '18',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '280',
			'product_id' => '37',
			'size'       => '43',
			'color'      => 'trắng nâu',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '281',
			'product_id' => '38',
			'size'       => '36',
			'color'      => 'Kem',
			'quantity'   => '9',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '282',
			'product_id' => '38',
			'size'       => '37',
			'color'      => 'Kem',
			'quantity'   => '8',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '283',
			'product_id' => '38',
			'size'       => '38',
			'color'      => 'Kem',
			'quantity'   => '12',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '284',
			'product_id' => '38',
			'size'       => '39',
			'color'      => 'Kem',
			'quantity'   => '20',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '285',
			'product_id' => '38',
			'size'       => '40',
			'color'      => 'Kem',
			'quantity'   => '7',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '286',
			'product_id' => '38',
			'size'       => '41',
			'color'      => 'Kem',
			'quantity'   => '20',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '287',
			'product_id' => '38',
			'size'       => '42',
			'color'      => 'Kem',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '288',
			'product_id' => '38',
			'size'       => '43',
			'color'      => 'Kem',
			'quantity'   => '8',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '289',
			'product_id' => '39',
			'size'       => '36',
			'color'      => 'Trắng xanh',
			'quantity'   => '6',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '290',
			'product_id' => '39',
			'size'       => '37',
			'color'      => 'Trắng xanh',
			'quantity'   => '11',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '291',
			'product_id' => '39',
			'size'       => '38',
			'color'      => 'Trắng xanh',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '292',
			'product_id' => '39',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '7',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '293',
			'product_id' => '39',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '8',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '294',
			'product_id' => '39',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '17',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '295',
			'product_id' => '39',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '7',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '296',
			'product_id' => '39',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '20',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '297',
			'product_id' => '40',
			'size'       => '36',
			'color'      => 'kem nâu',
			'quantity'   => '5',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '298',
			'product_id' => '40',
			'size'       => '37',
			'color'      => 'kem nâu',
			'quantity'   => '15',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '299',
			'product_id' => '40',
			'size'       => '38',
			'color'      => 'kem nâu',
			'quantity'   => '14',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '300',
			'product_id' => '40',
			'size'       => '39',
			'color'      => 'kem nâu',
			'quantity'   => '20',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '301',
			'product_id' => '40',
			'size'       => '40',
			'color'      => 'kem nâu',
			'quantity'   => '9',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '302',
			'product_id' => '40',
			'size'       => '41',
			'color'      => 'kem nâu',
			'quantity'   => '11',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '303',
			'product_id' => '40',
			'size'       => '42',
			'color'      => 'kem nâu',
			'quantity'   => '15',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '304',
			'product_id' => '40',
			'size'       => '43',
			'color'      => 'kem nâu',
			'quantity'   => '20',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '305',
			'product_id' => '41',
			'size'       => '36',
			'color'      => 'đen kem',
			'quantity'   => '5',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '306',
			'product_id' => '41',
			'size'       => '37',
			'color'      => 'đen kem',
			'quantity'   => '12',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '307',
			'product_id' => '41',
			'size'       => '38',
			'color'      => 'đen kem',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '308',
			'product_id' => '41',
			'size'       => '39',
			'color'      => 'đen kem',
			'quantity'   => '8',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '309',
			'product_id' => '41',
			'size'       => '40',
			'color'      => 'đen kem',
			'quantity'   => '19',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '310',
			'product_id' => '41',
			'size'       => '41',
			'color'      => 'đen kem',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '311',
			'product_id' => '41',
			'size'       => '42',
			'color'      => 'đen kem',
			'quantity'   => '19',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '312',
			'product_id' => '41',
			'size'       => '43',
			'color'      => 'đen kem',
			'quantity'   => '18',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '313',
			'product_id' => '42',
			'size'       => '36',
			'color'      => 'Đen trắng',
			'quantity'   => '18',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '314',
			'product_id' => '42',
			'size'       => '37',
			'color'      => 'Đen trắng',
			'quantity'   => '17',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '315',
			'product_id' => '42',
			'size'       => '38',
			'color'      => 'Đen trắng',
			'quantity'   => '19',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '316',
			'product_id' => '42',
			'size'       => '39',
			'color'      => 'Đen trắng',
			'quantity'   => '20',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '317',
			'product_id' => '42',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '16',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '318',
			'product_id' => '42',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '19',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '319',
			'product_id' => '42',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '7',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '320',
			'product_id' => '42',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '5',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '321',
			'product_id' => '43',
			'size'       => '36',
			'color'      => 'Đen trắng',
			'quantity'   => '10',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '322',
			'product_id' => '43',
			'size'       => '37',
			'color'      => 'Đen trắng',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '323',
			'product_id' => '43',
			'size'       => '38',
			'color'      => 'Đen trắng',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '324',
			'product_id' => '43',
			'size'       => '39',
			'color'      => 'Đen trắng',
			'quantity'   => '18',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '325',
			'product_id' => '43',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '326',
			'product_id' => '43',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '327',
			'product_id' => '43',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '328',
			'product_id' => '43',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '19',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '329',
			'product_id' => '44',
			'size'       => '36',
			'color'      => 'xanh lá',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '330',
			'product_id' => '44',
			'size'       => '37',
			'color'      => 'xanh lá',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '331',
			'product_id' => '44',
			'size'       => '38',
			'color'      => 'xanh lá',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '332',
			'product_id' => '44',
			'size'       => '39',
			'color'      => 'xanh lá',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '333',
			'product_id' => '44',
			'size'       => '40',
			'color'      => 'xanh lá',
			'quantity'   => '12',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '334',
			'product_id' => '44',
			'size'       => '41',
			'color'      => 'xanh lá',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '335',
			'product_id' => '44',
			'size'       => '42',
			'color'      => 'xanh lá',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '336',
			'product_id' => '44',
			'size'       => '43',
			'color'      => 'xanh lá',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '337',
			'product_id' => '45',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '19',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '338',
			'product_id' => '45',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '13',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '339',
			'product_id' => '45',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '5',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '340',
			'product_id' => '45',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '15',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '341',
			'product_id' => '46',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '10',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '342',
			'product_id' => '46',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '8',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '343',
			'product_id' => '46',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '11',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '344',
			'product_id' => '46',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '14',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '345',
			'product_id' => '47',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '18',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '346',
			'product_id' => '47',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '8',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '347',
			'product_id' => '47',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '20',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '348',
			'product_id' => '47',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '10',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '349',
			'product_id' => '48',
			'size'       => '39',
			'color'      => 'full đen',
			'quantity'   => '20',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '350',
			'product_id' => '48',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '11',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '351',
			'product_id' => '48',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '17',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '352',
			'product_id' => '48',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '6',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '353',
			'product_id' => '48',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '13',
			'price'      => '35',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '354',
			'product_id' => '49',
			'size'       => '36',
			'color'      => 'Xanh dương',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '355',
			'product_id' => '49',
			'size'       => '37',
			'color'      => 'Xanh dương',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '356',
			'product_id' => '49',
			'size'       => '38',
			'color'      => 'Xanh dương',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '357',
			'product_id' => '50',
			'size'       => '36',
			'color'      => 'thổ cẩm',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '358',
			'product_id' => '50',
			'size'       => '37',
			'color'      => 'thổ cẩm',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '359',
			'product_id' => '50',
			'size'       => '38',
			'color'      => 'thổ cẩm',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '360',
			'product_id' => '50',
			'size'       => '39',
			'color'      => 'thổ cẩm',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '361',
			'product_id' => '50',
			'size'       => '40',
			'color'      => 'thổ cẩm',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '362',
			'product_id' => '50',
			'size'       => '41',
			'color'      => 'thổ cẩm',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '363',
			'product_id' => '50',
			'size'       => '42',
			'color'      => 'thổ cẩm',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '364',
			'product_id' => '50',
			'size'       => '43',
			'color'      => 'thổ cẩm',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '365',
			'product_id' => '51',
			'size'       => '36',
			'color'      => 'xám trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '366',
			'product_id' => '51',
			'size'       => '38',
			'color'      => 'xám trắng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '367',
			'product_id' => '52',
			'size'       => '36',
			'color'      => 'xám camo',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '368',
			'product_id' => '52',
			'size'       => '38',
			'color'      => 'xám camo',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '369',
			'product_id' => '52',
			'size'       => '39',
			'color'      => 'xám camo',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '370',
			'product_id' => '53',
			'size'       => '36',
			'color'      => 'Đen trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '371',
			'product_id' => '53',
			'size'       => '37',
			'color'      => 'Đen trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '372',
			'product_id' => '53',
			'size'       => '38',
			'color'      => 'Đen trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '373',
			'product_id' => '53',
			'size'       => '39',
			'color'      => 'Đen trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '374',
			'product_id' => '53',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '375',
			'product_id' => '53',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '376',
			'product_id' => '53',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '377',
			'product_id' => '53',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '378',
			'product_id' => '54',
			'size'       => '36',
			'color'      => 'Hồng trắng',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '379',
			'product_id' => '54',
			'size'       => '37',
			'color'      => 'Hồng trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '380',
			'product_id' => '54',
			'size'       => '38',
			'color'      => 'Hồng trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '381',
			'product_id' => '54',
			'size'       => '39',
			'color'      => 'Hồng trắng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '382',
			'product_id' => '55',
			'size'       => '36',
			'color'      => 'XD trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '383',
			'product_id' => '55',
			'size'       => '37',
			'color'      => 'XD trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '384',
			'product_id' => '55',
			'size'       => '38',
			'color'      => 'XD trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '385',
			'product_id' => '55',
			'size'       => '39',
			'color'      => 'XD trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '386',
			'product_id' => '55',
			'size'       => '40',
			'color'      => 'XD trắng',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '387',
			'product_id' => '55',
			'size'       => '41',
			'color'      => 'XD trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '388',
			'product_id' => '55',
			'size'       => '42',
			'color'      => 'XD trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '389',
			'product_id' => '55',
			'size'       => '43',
			'color'      => 'XD trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '390',
			'product_id' => '56',
			'size'       => '40',
			'color'      => 'Đen xám',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '391',
			'product_id' => '56',
			'size'       => '41',
			'color'      => 'Đen xám',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '392',
			'product_id' => '56',
			'size'       => '42',
			'color'      => 'Đen xám',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '393',
			'product_id' => '56',
			'size'       => '43',
			'color'      => 'Đen xám',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '394',
			'product_id' => '57',
			'size'       => '36',
			'color'      => 'hồng tím',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '395',
			'product_id' => '57',
			'size'       => '37',
			'color'      => 'hồng tím',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '396',
			'product_id' => '57',
			'size'       => '38',
			'color'      => 'hồng tím',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '397',
			'product_id' => '57',
			'size'       => '39',
			'color'      => 'hồng tím',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '398',
			'product_id' => '58',
			'size'       => '36',
			'color'      => 'Trắng xanh',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '399',
			'product_id' => '58',
			'size'       => '37',
			'color'      => 'Trắng xanh',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '400',
			'product_id' => '58',
			'size'       => '38',
			'color'      => 'Trắng xanh',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '401',
			'product_id' => '58',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '402',
			'product_id' => '58',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '403',
			'product_id' => '58',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '404',
			'product_id' => '58',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '405',
			'product_id' => '58',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '406',
			'product_id' => '59',
			'size'       => '36',
			'color'      => 'Trắng xanh',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '407',
			'product_id' => '59',
			'size'       => '37',
			'color'      => 'Trắng xanh',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '408',
			'product_id' => '59',
			'size'       => '38',
			'color'      => 'Trắng xanh',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '409',
			'product_id' => '59',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '410',
			'product_id' => '59',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '411',
			'product_id' => '59',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '412',
			'product_id' => '59',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '413',
			'product_id' => '59',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '414',
			'product_id' => '60',
			'size'       => '36',
			'color'      => 'nâu hồng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '415',
			'product_id' => '60',
			'size'       => '37',
			'color'      => 'nâu hồng',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '416',
			'product_id' => '60',
			'size'       => '38',
			'color'      => 'nâu hồng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '417',
			'product_id' => '60',
			'size'       => '39',
			'color'      => 'nâu hồng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '418',
			'product_id' => '61',
			'size'       => '36',
			'color'      => 'Xanh dương',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '419',
			'product_id' => '61',
			'size'       => '37',
			'color'      => 'Xanh dương',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '420',
			'product_id' => '61',
			'size'       => '38',
			'color'      => 'Xanh dương',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '421',
			'product_id' => '61',
			'size'       => '39',
			'color'      => 'Xanh dương',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '422',
			'product_id' => '61',
			'size'       => '40',
			'color'      => 'Xanh dương',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '423',
			'product_id' => '61',
			'size'       => '41',
			'color'      => 'Xanh dương',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '424',
			'product_id' => '61',
			'size'       => '42',
			'color'      => 'Xanh dương',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '425',
			'product_id' => '61',
			'size'       => '43',
			'color'      => 'Xanh dương',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '426',
			'product_id' => '62',
			'size'       => '36',
			'color'      => 'Nâu',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '427',
			'product_id' => '62',
			'size'       => '37',
			'color'      => 'Nâu',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '428',
			'product_id' => '62',
			'size'       => '38',
			'color'      => 'Nâu',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '429',
			'product_id' => '62',
			'size'       => '39',
			'color'      => 'Nâu',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '430',
			'product_id' => '63',
			'size'       => '36',
			'color'      => 'Đen cam',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '431',
			'product_id' => '63',
			'size'       => '37',
			'color'      => 'Đen cam',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '432',
			'product_id' => '63',
			'size'       => '38',
			'color'      => 'Đen cam',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '433',
			'product_id' => '63',
			'size'       => '39',
			'color'      => 'Đen cam',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '434',
			'product_id' => '63',
			'size'       => '40',
			'color'      => 'Đen cam',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '435',
			'product_id' => '63',
			'size'       => '41',
			'color'      => 'Đen cam',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '436',
			'product_id' => '63',
			'size'       => '42',
			'color'      => 'Đen cam',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '437',
			'product_id' => '63',
			'size'       => '43',
			'color'      => 'Đen cam',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '438',
			'product_id' => '64',
			'size'       => '37',
			'color'      => 'xám xanh',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '439',
			'product_id' => '64',
			'size'       => '38',
			'color'      => 'xám xanh',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '440',
			'product_id' => '64',
			'size'       => '39',
			'color'      => 'xám xanh',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '441',
			'product_id' => '65',
			'size'       => '36',
			'color'      => 'Trắng đen',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '442',
			'product_id' => '65',
			'size'       => '37',
			'color'      => 'Trắng đen',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '443',
			'product_id' => '65',
			'size'       => '38',
			'color'      => 'Trắng đen',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '444',
			'product_id' => '65',
			'size'       => '39',
			'color'      => 'Trắng đen',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '445',
			'product_id' => '65',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '446',
			'product_id' => '65',
			'size'       => '41',
			'color'      => 'Trắng đen',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '447',
			'product_id' => '65',
			'size'       => '42',
			'color'      => 'Trắng đen',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '448',
			'product_id' => '65',
			'size'       => '43',
			'color'      => 'Trắng đen',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '449',
			'product_id' => '66',
			'size'       => '36',
			'color'      => 'Xám',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '450',
			'product_id' => '66',
			'size'       => '37',
			'color'      => 'Xám',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '451',
			'product_id' => '66',
			'size'       => '38',
			'color'      => 'Xám',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '452',
			'product_id' => '66',
			'size'       => '39',
			'color'      => 'Xám',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '453',
			'product_id' => '66',
			'size'       => '40',
			'color'      => 'Xám',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '454',
			'product_id' => '66',
			'size'       => '41',
			'color'      => 'Xám',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '455',
			'product_id' => '66',
			'size'       => '42',
			'color'      => 'Xám',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '456',
			'product_id' => '66',
			'size'       => '43',
			'color'      => 'Xám',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '457',
			'product_id' => '67',
			'size'       => '36',
			'color'      => 'Xanh dương',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '458',
			'product_id' => '67',
			'size'       => '37',
			'color'      => 'Xanh dương',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '459',
			'product_id' => '67',
			'size'       => '38',
			'color'      => 'Xanh dương',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '460',
			'product_id' => '67',
			'size'       => '39',
			'color'      => 'Xanh dương',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '461',
			'product_id' => '67',
			'size'       => '40',
			'color'      => 'Xanh dương',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '462',
			'product_id' => '67',
			'size'       => '41',
			'color'      => 'Xanh dương',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '463',
			'product_id' => '67',
			'size'       => '42',
			'color'      => 'Xanh dương',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '464',
			'product_id' => '67',
			'size'       => '43',
			'color'      => 'Xanh dương',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '465',
			'product_id' => '68',
			'size'       => '36',
			'color'      => 'Trắng xanh',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '466',
			'product_id' => '68',
			'size'       => '37',
			'color'      => 'Trắng xanh',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '467',
			'product_id' => '68',
			'size'       => '38',
			'color'      => 'Trắng xanh',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '468',
			'product_id' => '68',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '469',
			'product_id' => '68',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '470',
			'product_id' => '68',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '471',
			'product_id' => '68',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '472',
			'product_id' => '68',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '473',
			'product_id' => '69',
			'size'       => '36',
			'color'      => 'Trắng',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '474',
			'product_id' => '69',
			'size'       => '37',
			'color'      => 'Trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '475',
			'product_id' => '69',
			'size'       => '38',
			'color'      => 'Trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '476',
			'product_id' => '69',
			'size'       => '39',
			'color'      => 'Trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '477',
			'product_id' => '69',
			'size'       => '40',
			'color'      => 'Trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '478',
			'product_id' => '69',
			'size'       => '41',
			'color'      => 'Trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '479',
			'product_id' => '69',
			'size'       => '42',
			'color'      => 'Trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '480',
			'product_id' => '69',
			'size'       => '43',
			'color'      => 'Trắng',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '481',
			'product_id' => '70',
			'size'       => '36',
			'color'      => 'xanh than',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '482',
			'product_id' => '70',
			'size'       => '37',
			'color'      => 'xanh than',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '483',
			'product_id' => '70',
			'size'       => '38',
			'color'      => 'xanh than',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '484',
			'product_id' => '70',
			'size'       => '39',
			'color'      => 'xanh than',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '485',
			'product_id' => '70',
			'size'       => '40',
			'color'      => 'xanh than',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '486',
			'product_id' => '70',
			'size'       => '41',
			'color'      => 'xanh than',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '487',
			'product_id' => '70',
			'size'       => '42',
			'color'      => 'xanh than',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '488',
			'product_id' => '70',
			'size'       => '43',
			'color'      => 'xanh than',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '489',
			'product_id' => '71',
			'size'       => '39',
			'color'      => 'xám trắng',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '490',
			'product_id' => '71',
			'size'       => '40',
			'color'      => 'xám trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '491',
			'product_id' => '72',
			'size'       => '36',
			'color'      => 'xanh lá',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '492',
			'product_id' => '72',
			'size'       => '37',
			'color'      => 'xanh lá',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '493',
			'product_id' => '72',
			'size'       => '38',
			'color'      => 'xanh lá',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '494',
			'product_id' => '72',
			'size'       => '39',
			'color'      => 'xanh lá',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '495',
			'product_id' => '72',
			'size'       => '40',
			'color'      => 'xanh lá',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '496',
			'product_id' => '72',
			'size'       => '41',
			'color'      => 'xanh lá',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '497',
			'product_id' => '72',
			'size'       => '42',
			'color'      => 'xanh lá',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '498',
			'product_id' => '72',
			'size'       => '43',
			'color'      => 'xanh lá',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '499',
			'product_id' => '73',
			'size'       => '36',
			'color'      => 'Đen trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '500',
			'product_id' => '73',
			'size'       => '37',
			'color'      => 'Đen trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '501',
			'product_id' => '73',
			'size'       => '38',
			'color'      => 'Đen trắng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '502',
			'product_id' => '73',
			'size'       => '39',
			'color'      => 'Đen trắng',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '503',
			'product_id' => '73',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '504',
			'product_id' => '73',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '505',
			'product_id' => '73',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '506',
			'product_id' => '73',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '507',
			'product_id' => '74',
			'size'       => '36',
			'color'      => 'full trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '508',
			'product_id' => '74',
			'size'       => '37',
			'color'      => 'full trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '509',
			'product_id' => '74',
			'size'       => '38',
			'color'      => 'full trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '510',
			'product_id' => '74',
			'size'       => '39',
			'color'      => 'full trắng',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '511',
			'product_id' => '74',
			'size'       => '40',
			'color'      => 'full trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '512',
			'product_id' => '74',
			'size'       => '41',
			'color'      => 'full trắng',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '513',
			'product_id' => '74',
			'size'       => '42',
			'color'      => 'full trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '514',
			'product_id' => '74',
			'size'       => '43',
			'color'      => 'full trắng',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '515',
			'product_id' => '75',
			'size'       => '36',
			'color'      => 'xanh lá',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '516',
			'product_id' => '75',
			'size'       => '37',
			'color'      => 'xanh lá',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '517',
			'product_id' => '75',
			'size'       => '38',
			'color'      => 'xanh lá',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '518',
			'product_id' => '75',
			'size'       => '39',
			'color'      => 'xanh lá',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '519',
			'product_id' => '75',
			'size'       => '40',
			'color'      => 'xanh lá',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '520',
			'product_id' => '75',
			'size'       => '41',
			'color'      => 'xanh lá',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '521',
			'product_id' => '75',
			'size'       => '42',
			'color'      => 'xanh lá',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '522',
			'product_id' => '75',
			'size'       => '43',
			'color'      => 'xanh lá',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '523',
			'product_id' => '76',
			'size'       => '36',
			'color'      => 'xanh lá',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '524',
			'product_id' => '76',
			'size'       => '37',
			'color'      => 'xanh lá',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '525',
			'product_id' => '76',
			'size'       => '38',
			'color'      => 'xanh lá',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '526',
			'product_id' => '76',
			'size'       => '39',
			'color'      => 'xanh lá',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '527',
			'product_id' => '76',
			'size'       => '40',
			'color'      => 'xanh lá',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '528',
			'product_id' => '76',
			'size'       => '41',
			'color'      => 'xanh lá',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '529',
			'product_id' => '76',
			'size'       => '42',
			'color'      => 'xanh lá',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '530',
			'product_id' => '76',
			'size'       => '43',
			'color'      => 'xanh lá',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '531',
			'product_id' => '77',
			'size'       => '40',
			'color'      => 'Nâu',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '532',
			'product_id' => '77',
			'size'       => '41',
			'color'      => 'Nâu',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '533',
			'product_id' => '77',
			'size'       => '42',
			'color'      => 'Nâu',
			'quantity'   => '10',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '534',
			'product_id' => '77',
			'size'       => '43',
			'color'      => 'Nâu',
			'quantity'   => '19',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '535',
			'product_id' => '77',
			'size'       => '44',
			'color'      => 'Nâu',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '536',
			'product_id' => '78',
			'size'       => '36',
			'color'      => 'Kem',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '537',
			'product_id' => '78',
			'size'       => '37',
			'color'      => 'Kem',
			'quantity'   => '19',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '538',
			'product_id' => '78',
			'size'       => '38',
			'color'      => 'Kem',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '539',
			'product_id' => '78',
			'size'       => '39',
			'color'      => 'Kem',
			'quantity'   => '12',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '540',
			'product_id' => '78',
			'size'       => '40',
			'color'      => 'Kem',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '541',
			'product_id' => '78',
			'size'       => '41',
			'color'      => 'Kem',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '542',
			'product_id' => '78',
			'size'       => '42',
			'color'      => 'Kem',
			'quantity'   => '8',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '543',
			'product_id' => '78',
			'size'       => '43',
			'color'      => 'Kem',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '544',
			'product_id' => '79',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '18',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '545',
			'product_id' => '79',
			'size'       => '41',
			'color'      => 'Trắng đen',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '546',
			'product_id' => '79',
			'size'       => '42',
			'color'      => 'Trắng đen',
			'quantity'   => '18',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '547',
			'product_id' => '79',
			'size'       => '43',
			'color'      => 'Trắng đen',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '548',
			'product_id' => '80',
			'size'       => '36',
			'color'      => 'trắng nâu',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '549',
			'product_id' => '80',
			'size'       => '37',
			'color'      => 'trắng nâu',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '550',
			'product_id' => '80',
			'size'       => '38',
			'color'      => 'trắng nâu',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '551',
			'product_id' => '81',
			'size'       => '36',
			'color'      => 'kem xanh hồng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '552',
			'product_id' => '81',
			'size'       => '37',
			'color'      => 'kem xanh hồng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '553',
			'product_id' => '81',
			'size'       => '38',
			'color'      => 'kem xanh hồng',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '554',
			'product_id' => '81',
			'size'       => '39',
			'color'      => 'kem xanh hồng',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '555',
			'product_id' => '82',
			'size'       => '36',
			'color'      => 'full trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '556',
			'product_id' => '82',
			'size'       => '37',
			'color'      => 'full trắng',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '557',
			'product_id' => '82',
			'size'       => '38',
			'color'      => 'full trắng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '558',
			'product_id' => '82',
			'size'       => '39',
			'color'      => 'full trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '559',
			'product_id' => '82',
			'size'       => '40',
			'color'      => 'full trắng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '560',
			'product_id' => '82',
			'size'       => '41',
			'color'      => 'full trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '561',
			'product_id' => '82',
			'size'       => '42',
			'color'      => 'full trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '562',
			'product_id' => '82',
			'size'       => '43',
			'color'      => 'full trắng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '563',
			'product_id' => '83',
			'size'       => '36',
			'color'      => 'xanh ngọc',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '564',
			'product_id' => '83',
			'size'       => '37',
			'color'      => 'xanh ngọc',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '565',
			'product_id' => '83',
			'size'       => '38',
			'color'      => 'xanh ngọc',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '566',
			'product_id' => '83',
			'size'       => '39',
			'color'      => 'xanh ngọc',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '567',
			'product_id' => '83',
			'size'       => '40',
			'color'      => 'xanh ngọc',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '568',
			'product_id' => '83',
			'size'       => '41',
			'color'      => 'xanh ngọc',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '569',
			'product_id' => '83',
			'size'       => '42',
			'color'      => 'xanh ngọc',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '570',
			'product_id' => '83',
			'size'       => '43',
			'color'      => 'xanh ngọc',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '571',
			'product_id' => '84',
			'size'       => '36',
			'color'      => 'kem vàng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '572',
			'product_id' => '84',
			'size'       => '37',
			'color'      => 'kem vàng',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '573',
			'product_id' => '84',
			'size'       => '38',
			'color'      => 'kem vàng',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '574',
			'product_id' => '84',
			'size'       => '39',
			'color'      => 'kem vàng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '575',
			'product_id' => '85',
			'size'       => '36',
			'color'      => 'kim cương',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '576',
			'product_id' => '85',
			'size'       => '37',
			'color'      => 'kim cương',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '577',
			'product_id' => '85',
			'size'       => '38',
			'color'      => 'kim cương',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '578',
			'product_id' => '85',
			'size'       => '39',
			'color'      => 'kim cương',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '579',
			'product_id' => '86',
			'size'       => '36',
			'color'      => 'Trắng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '580',
			'product_id' => '86',
			'size'       => '37',
			'color'      => 'Trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '581',
			'product_id' => '86',
			'size'       => '38',
			'color'      => 'Trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '582',
			'product_id' => '86',
			'size'       => '39',
			'color'      => 'Trắng',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '583',
			'product_id' => '86',
			'size'       => '40',
			'color'      => 'Trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '584',
			'product_id' => '86',
			'size'       => '41',
			'color'      => 'Trắng',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '585',
			'product_id' => '86',
			'size'       => '42',
			'color'      => 'Trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '586',
			'product_id' => '86',
			'size'       => '43',
			'color'      => 'Trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '587',
			'product_id' => '86',
			'size'       => '44',
			'color'      => 'Trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '588',
			'product_id' => '87',
			'size'       => '36',
			'color'      => 'xanh hồng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '589',
			'product_id' => '87',
			'size'       => '37',
			'color'      => 'xanh hồng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '590',
			'product_id' => '87',
			'size'       => '38',
			'color'      => 'xanh hồng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '591',
			'product_id' => '87',
			'size'       => '39',
			'color'      => 'xanh hồng',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '592',
			'product_id' => '88',
			'size'       => '36',
			'color'      => 'Trắng đen',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '593',
			'product_id' => '88',
			'size'       => '37',
			'color'      => 'Trắng đen',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '594',
			'product_id' => '88',
			'size'       => '38',
			'color'      => 'Trắng đen',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '595',
			'product_id' => '88',
			'size'       => '39',
			'color'      => 'Trắng đen',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '596',
			'product_id' => '88',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '597',
			'product_id' => '88',
			'size'       => '41',
			'color'      => 'Trắng đen',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '598',
			'product_id' => '88',
			'size'       => '42',
			'color'      => 'Trắng đen',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '599',
			'product_id' => '88',
			'size'       => '43',
			'color'      => 'Trắng đen',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '600',
			'product_id' => '89',
			'size'       => '36',
			'color'      => 'Trắng xanh',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '601',
			'product_id' => '89',
			'size'       => '37',
			'color'      => 'Trắng xanh',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '602',
			'product_id' => '89',
			'size'       => '38',
			'color'      => 'Trắng xanh',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '603',
			'product_id' => '89',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '604',
			'product_id' => '90',
			'size'       => '36',
			'color'      => 'Trắng đen',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '605',
			'product_id' => '90',
			'size'       => '37',
			'color'      => 'Trắng đen',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '606',
			'product_id' => '90',
			'size'       => '38',
			'color'      => 'Trắng đen',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '607',
			'product_id' => '90',
			'size'       => '39',
			'color'      => 'Trắng đen',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '608',
			'product_id' => '90',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '609',
			'product_id' => '90',
			'size'       => '41',
			'color'      => 'Trắng đen',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '610',
			'product_id' => '90',
			'size'       => '42',
			'color'      => 'Trắng đen',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '611',
			'product_id' => '90',
			'size'       => '43',
			'color'      => 'Trắng đen',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '612',
			'product_id' => '91',
			'size'       => '36',
			'color'      => 'Trắng xanh',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '613',
			'product_id' => '91',
			'size'       => '37',
			'color'      => 'Trắng xanh',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '614',
			'product_id' => '91',
			'size'       => '38',
			'color'      => 'Trắng xanh',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '615',
			'product_id' => '91',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '616',
			'product_id' => '91',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '617',
			'product_id' => '91',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '618',
			'product_id' => '91',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '619',
			'product_id' => '91',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '620',
			'product_id' => '92',
			'size'       => '36',
			'color'      => 'Trắng xám',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '621',
			'product_id' => '92',
			'size'       => '37',
			'color'      => 'Trắng xám',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '622',
			'product_id' => '92',
			'size'       => '38',
			'color'      => 'Trắng xám',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '623',
			'product_id' => '92',
			'size'       => '39',
			'color'      => 'Trắng xám',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '624',
			'product_id' => '92',
			'size'       => '40',
			'color'      => 'Trắng xám',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '625',
			'product_id' => '92',
			'size'       => '41',
			'color'      => 'Trắng xám',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '626',
			'product_id' => '92',
			'size'       => '42',
			'color'      => 'Trắng xám',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '627',
			'product_id' => '92',
			'size'       => '43',
			'color'      => 'Trắng xám',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '628',
			'product_id' => '93',
			'size'       => '36',
			'color'      => 'Xám',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '629',
			'product_id' => '93',
			'size'       => '37',
			'color'      => 'Xám',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '630',
			'product_id' => '93',
			'size'       => '38',
			'color'      => 'Xám',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '631',
			'product_id' => '93',
			'size'       => '39',
			'color'      => 'Xám',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '632',
			'product_id' => '93',
			'size'       => '40',
			'color'      => 'Xám',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '633',
			'product_id' => '93',
			'size'       => '41',
			'color'      => 'Xám',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '634',
			'product_id' => '93',
			'size'       => '42',
			'color'      => 'Xám',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '635',
			'product_id' => '93',
			'size'       => '43',
			'color'      => 'Xám',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '636',
			'product_id' => '94',
			'size'       => '36',
			'color'      => 'Trắng đen',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '637',
			'product_id' => '94',
			'size'       => '37',
			'color'      => 'Trắng đen',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '638',
			'product_id' => '94',
			'size'       => '38',
			'color'      => 'Trắng đen',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '639',
			'product_id' => '94',
			'size'       => '39',
			'color'      => 'Trắng đen',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '640',
			'product_id' => '94',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '641',
			'product_id' => '94',
			'size'       => '41',
			'color'      => 'Trắng đen',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '642',
			'product_id' => '94',
			'size'       => '42',
			'color'      => 'Trắng đen',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '643',
			'product_id' => '94',
			'size'       => '43',
			'color'      => 'Trắng đen',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '644',
			'product_id' => '95',
			'size'       => '36',
			'color'      => 'Hồng trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '645',
			'product_id' => '95',
			'size'       => '37',
			'color'      => 'Hồng trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '646',
			'product_id' => '95',
			'size'       => '38',
			'color'      => 'Hồng trắng',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '647',
			'product_id' => '95',
			'size'       => '39',
			'color'      => 'Hồng trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '648',
			'product_id' => '96',
			'size'       => '36',
			'color'      => 'xám xanh',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '649',
			'product_id' => '96',
			'size'       => '37',
			'color'      => 'xám xanh',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '650',
			'product_id' => '96',
			'size'       => '38',
			'color'      => 'xám xanh',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '651',
			'product_id' => '96',
			'size'       => '39',
			'color'      => 'xám xanh',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '652',
			'product_id' => '96',
			'size'       => '40',
			'color'      => 'xám xanh',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '653',
			'product_id' => '96',
			'size'       => '41',
			'color'      => 'xám xanh',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '654',
			'product_id' => '96',
			'size'       => '42',
			'color'      => 'xám xanh',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '655',
			'product_id' => '96',
			'size'       => '43',
			'color'      => 'xám xanh',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '656',
			'product_id' => '97',
			'size'       => '40',
			'color'      => 'xám vàng',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '657',
			'product_id' => '97',
			'size'       => '41',
			'color'      => 'xám vàng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '658',
			'product_id' => '97',
			'size'       => '42',
			'color'      => 'xám vàng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '659',
			'product_id' => '97',
			'size'       => '43',
			'color'      => 'xám vàng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '660',
			'product_id' => '98',
			'size'       => '36',
			'color'      => 'Trắng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '661',
			'product_id' => '98',
			'size'       => '37',
			'color'      => 'Trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '662',
			'product_id' => '98',
			'size'       => '38',
			'color'      => 'Trắng',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '663',
			'product_id' => '98',
			'size'       => '39',
			'color'      => 'Trắng',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '664',
			'product_id' => '98',
			'size'       => '40',
			'color'      => 'Trắng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '665',
			'product_id' => '98',
			'size'       => '41',
			'color'      => 'Trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '666',
			'product_id' => '98',
			'size'       => '42',
			'color'      => 'Trắng',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '667',
			'product_id' => '98',
			'size'       => '43',
			'color'      => 'Trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '668',
			'product_id' => '99',
			'size'       => '36',
			'color'      => 'kem nâu',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '669',
			'product_id' => '99',
			'size'       => '37',
			'color'      => 'kem nâu',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '670',
			'product_id' => '99',
			'size'       => '38',
			'color'      => 'kem nâu',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '671',
			'product_id' => '99',
			'size'       => '39',
			'color'      => 'kem nâu',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '672',
			'product_id' => '99',
			'size'       => '40',
			'color'      => 'kem nâu',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '673',
			'product_id' => '99',
			'size'       => '41',
			'color'      => 'kem nâu',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '674',
			'product_id' => '99',
			'size'       => '42',
			'color'      => 'kem nâu',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '675',
			'product_id' => '99',
			'size'       => '43',
			'color'      => 'kem nâu',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '676',
			'product_id' => '100',
			'size'       => '40',
			'color'      => 'xám cam',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '677',
			'product_id' => '100',
			'size'       => '41',
			'color'      => 'xám cam',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '678',
			'product_id' => '100',
			'size'       => '42',
			'color'      => 'xám cam',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '679',
			'product_id' => '100',
			'size'       => '43',
			'color'      => 'xám cam',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '680',
			'product_id' => '101',
			'size'       => '40',
			'color'      => 'xám kem',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '681',
			'product_id' => '101',
			'size'       => '41',
			'color'      => 'xám kem',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '682',
			'product_id' => '101',
			'size'       => '42',
			'color'      => 'xám kem',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '683',
			'product_id' => '101',
			'size'       => '43',
			'color'      => 'xám kem',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '684',
			'product_id' => '102',
			'size'       => '36',
			'color'      => 'xám trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '685',
			'product_id' => '102',
			'size'       => '37',
			'color'      => 'xám trắng',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '686',
			'product_id' => '102',
			'size'       => '38',
			'color'      => 'xám trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '687',
			'product_id' => '102',
			'size'       => '39',
			'color'      => 'xám trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '688',
			'product_id' => '102',
			'size'       => '40',
			'color'      => 'xám trắng',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '689',
			'product_id' => '102',
			'size'       => '41',
			'color'      => 'xám trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '690',
			'product_id' => '102',
			'size'       => '42',
			'color'      => 'xám trắng',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '691',
			'product_id' => '102',
			'size'       => '43',
			'color'      => 'xám trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '692',
			'product_id' => '103',
			'size'       => '40',
			'color'      => 'Đen xám',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '693',
			'product_id' => '103',
			'size'       => '41',
			'color'      => 'Đen xám',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '694',
			'product_id' => '103',
			'size'       => '42',
			'color'      => 'Đen xám',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '695',
			'product_id' => '103',
			'size'       => '43',
			'color'      => 'Đen xám',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '696',
			'product_id' => '104',
			'size'       => '36',
			'color'      => 'trắng vàng',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '697',
			'product_id' => '104',
			'size'       => '37',
			'color'      => 'trắng vàng',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '698',
			'product_id' => '104',
			'size'       => '38',
			'color'      => 'trắng vàng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '699',
			'product_id' => '104',
			'size'       => '39',
			'color'      => 'trắng vàng',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '700',
			'product_id' => '104',
			'size'       => '40',
			'color'      => 'trắng vàng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '701',
			'product_id' => '104',
			'size'       => '41',
			'color'      => 'trắng vàng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '702',
			'product_id' => '104',
			'size'       => '42',
			'color'      => 'trắng vàng',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '703',
			'product_id' => '104',
			'size'       => '43',
			'color'      => 'trắng vàng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '704',
			'product_id' => '105',
			'size'       => '40',
			'color'      => 'xám kem',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '705',
			'product_id' => '105',
			'size'       => '41',
			'color'      => 'xám kem',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '706',
			'product_id' => '105',
			'size'       => '42',
			'color'      => 'xám kem',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '707',
			'product_id' => '105',
			'size'       => '43',
			'color'      => 'xám kem',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '708',
			'product_id' => '106',
			'size'       => '40',
			'color'      => 'full trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '709',
			'product_id' => '106',
			'size'       => '41',
			'color'      => 'full trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '710',
			'product_id' => '106',
			'size'       => '42',
			'color'      => 'full trắng',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '711',
			'product_id' => '106',
			'size'       => '43',
			'color'      => 'full trắng',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '712',
			'product_id' => '107',
			'size'       => '36',
			'color'      => 'Kem',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '713',
			'product_id' => '107',
			'size'       => '37',
			'color'      => 'Kem',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '714',
			'product_id' => '107',
			'size'       => '38',
			'color'      => 'Kem',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '715',
			'product_id' => '107',
			'size'       => '39',
			'color'      => 'Kem',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '716',
			'product_id' => '107',
			'size'       => '40',
			'color'      => 'Kem',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '717',
			'product_id' => '107',
			'size'       => '41',
			'color'      => 'Kem',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '718',
			'product_id' => '107',
			'size'       => '42',
			'color'      => 'Kem',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '719',
			'product_id' => '107',
			'size'       => '43',
			'color'      => 'Kem',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '720',
			'product_id' => '108',
			'size'       => '36',
			'color'      => 'Kem',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '721',
			'product_id' => '108',
			'size'       => '37',
			'color'      => 'Kem',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '722',
			'product_id' => '108',
			'size'       => '38',
			'color'      => 'Kem',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '723',
			'product_id' => '108',
			'size'       => '39',
			'color'      => 'Kem',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '724',
			'product_id' => '108',
			'size'       => '40',
			'color'      => 'Kem',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '725',
			'product_id' => '108',
			'size'       => '41',
			'color'      => 'Kem',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '726',
			'product_id' => '108',
			'size'       => '42',
			'color'      => 'Kem',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '727',
			'product_id' => '108',
			'size'       => '43',
			'color'      => 'Kem',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '728',
			'product_id' => '109',
			'size'       => '36',
			'color'      => 'Trắng',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '729',
			'product_id' => '109',
			'size'       => '37',
			'color'      => 'Trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '730',
			'product_id' => '109',
			'size'       => '38',
			'color'      => 'Trắng',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '731',
			'product_id' => '109',
			'size'       => '39',
			'color'      => 'Trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '732',
			'product_id' => '109',
			'size'       => '40',
			'color'      => 'Trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '733',
			'product_id' => '109',
			'size'       => '41',
			'color'      => 'Trắng',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '734',
			'product_id' => '109',
			'size'       => '42',
			'color'      => 'Trắng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '735',
			'product_id' => '109',
			'size'       => '43',
			'color'      => 'Trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '736',
			'product_id' => '109',
			'size'       => '44',
			'color'      => 'Trắng',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '737',
			'product_id' => '110',
			'size'       => '40',
			'color'      => 'xám cam',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '738',
			'product_id' => '110',
			'size'       => '41',
			'color'      => 'xám cam',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '739',
			'product_id' => '110',
			'size'       => '42',
			'color'      => 'xám cam',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '740',
			'product_id' => '110',
			'size'       => '43',
			'color'      => 'xám cam',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '741',
			'product_id' => '111',
			'size'       => '40',
			'color'      => 'Xám',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '742',
			'product_id' => '111',
			'size'       => '41',
			'color'      => 'Xám',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '743',
			'product_id' => '111',
			'size'       => '42',
			'color'      => 'Xám',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '744',
			'product_id' => '111',
			'size'       => '43',
			'color'      => 'Xám',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '745',
			'product_id' => '112',
			'size'       => '40',
			'color'      => 'Xám',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '746',
			'product_id' => '112',
			'size'       => '41',
			'color'      => 'Xám',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '747',
			'product_id' => '112',
			'size'       => '42',
			'color'      => 'Xám',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '748',
			'product_id' => '112',
			'size'       => '43',
			'color'      => 'Xám',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '749',
			'product_id' => '113',
			'size'       => '40',
			'color'      => 'trắng nâu',
			'quantity'   => '18',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '750',
			'product_id' => '113',
			'size'       => '41',
			'color'      => 'trắng nâu',
			'quantity'   => '16',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '751',
			'product_id' => '113',
			'size'       => '42',
			'color'      => 'trắng nâu',
			'quantity'   => '14',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '752',
			'product_id' => '113',
			'size'       => '43',
			'color'      => 'trắng nâu',
			'quantity'   => '13',
			'price'      => '90',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '753',
			'product_id' => '114',
			'size'       => '40',
			'color'      => 'xám xanh',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '754',
			'product_id' => '114',
			'size'       => '41',
			'color'      => 'xám xanh',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '755',
			'product_id' => '114',
			'size'       => '42',
			'color'      => 'xám xanh',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '756',
			'product_id' => '114',
			'size'       => '43',
			'color'      => 'xám xanh',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '757',
			'product_id' => '115',
			'size'       => '36',
			'color'      => 'Trắng xanh',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '758',
			'product_id' => '115',
			'size'       => '37',
			'color'      => 'Trắng xanh',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '759',
			'product_id' => '115',
			'size'       => '38',
			'color'      => 'Trắng xanh',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '760',
			'product_id' => '115',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '761',
			'product_id' => '116',
			'size'       => '40',
			'color'      => 'xám kem',
			'quantity'   => '20',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '762',
			'product_id' => '116',
			'size'       => '41',
			'color'      => 'xám kem',
			'quantity'   => '5',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '763',
			'product_id' => '116',
			'size'       => '42',
			'color'      => 'xám kem',
			'quantity'   => '7',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '764',
			'product_id' => '116',
			'size'       => '43',
			'color'      => 'xám kem',
			'quantity'   => '19',
			'price'      => '95',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '765',
			'product_id' => '117',
			'size'       => '36',
			'color'      => 'Ghi',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '766',
			'product_id' => '117',
			'size'       => '37',
			'color'      => 'Ghi',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '767',
			'product_id' => '117',
			'size'       => '38',
			'color'      => 'Ghi',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '768',
			'product_id' => '117',
			'size'       => '39',
			'color'      => 'Ghi',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '769',
			'product_id' => '117',
			'size'       => '40',
			'color'      => 'Ghi',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '770',
			'product_id' => '118',
			'size'       => '41',
			'color'      => 'full trắng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '771',
			'product_id' => '119',
			'size'       => '40',
			'color'      => 'thấp cổ',
			'quantity'   => '10',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '772',
			'product_id' => '119',
			'size'       => '41',
			'color'      => 'thấp cổ',
			'quantity'   => '6',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '773',
			'product_id' => '119',
			'size'       => '42',
			'color'      => 'thấp cổ',
			'quantity'   => '14',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '774',
			'product_id' => '119',
			'size'       => '43',
			'color'      => 'thấp cổ',
			'quantity'   => '14',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '775',
			'product_id' => '120',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '14',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '776',
			'product_id' => '121',
			'size'       => '40',
			'color'      => 'ghi xanh',
			'quantity'   => '17',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '777',
			'product_id' => '121',
			'size'       => '41',
			'color'      => 'ghi xanh',
			'quantity'   => '12',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '778',
			'product_id' => '121',
			'size'       => '42',
			'color'      => 'ghi xanh',
			'quantity'   => '9',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '779',
			'product_id' => '121',
			'size'       => '43',
			'color'      => 'ghi xanh',
			'quantity'   => '11',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '780',
			'product_id' => '121',
			'size'       => '44',
			'color'      => 'ghi xanh',
			'quantity'   => '6',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '781',
			'product_id' => '122',
			'size'       => '40',
			'color'      => 'Ghi',
			'quantity'   => '18',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '782',
			'product_id' => '122',
			'size'       => '41',
			'color'      => 'Ghi',
			'quantity'   => '15',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '783',
			'product_id' => '122',
			'size'       => '42',
			'color'      => 'Ghi',
			'quantity'   => '6',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '784',
			'product_id' => '122',
			'size'       => '43',
			'color'      => 'Ghi',
			'quantity'   => '11',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '785',
			'product_id' => '122',
			'size'       => '44',
			'color'      => 'Ghi',
			'quantity'   => '16',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '786',
			'product_id' => '123',
			'size'       => '40',
			'color'      => 'Ghi',
			'quantity'   => '19',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '787',
			'product_id' => '123',
			'size'       => '41',
			'color'      => 'Ghi',
			'quantity'   => '16',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '788',
			'product_id' => '123',
			'size'       => '42',
			'color'      => 'Ghi',
			'quantity'   => '11',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '789',
			'product_id' => '123',
			'size'       => '43',
			'color'      => 'Ghi',
			'quantity'   => '9',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '790',
			'product_id' => '123',
			'size'       => '44',
			'color'      => 'Ghi',
			'quantity'   => '9',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '791',
			'product_id' => '124',
			'size'       => '36',
			'color'      => 'Trắng đen',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '792',
			'product_id' => '124',
			'size'       => '37',
			'color'      => 'Trắng đen',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '793',
			'product_id' => '124',
			'size'       => '38',
			'color'      => 'Trắng đen',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '794',
			'product_id' => '124',
			'size'       => '39',
			'color'      => 'Trắng đen',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '795',
			'product_id' => '124',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '796',
			'product_id' => '124',
			'size'       => '43',
			'color'      => 'Trắng đen',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '797',
			'product_id' => '125',
			'size'       => '36',
			'color'      => 'Trắng gót lục',
			'quantity'   => '6',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '798',
			'product_id' => '125',
			'size'       => '37',
			'color'      => 'Trắng gót lục',
			'quantity'   => '19',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '799',
			'product_id' => '125',
			'size'       => '38',
			'color'      => 'Trắng gót lục',
			'quantity'   => '15',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '800',
			'product_id' => '125',
			'size'       => '39',
			'color'      => 'Trắng gót lục',
			'quantity'   => '19',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '801',
			'product_id' => '125',
			'size'       => '40',
			'color'      => 'Trắng gót lục',
			'quantity'   => '17',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '802',
			'product_id' => '125',
			'size'       => '41',
			'color'      => 'Trắng gót lục',
			'quantity'   => '5',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '803',
			'product_id' => '125',
			'size'       => '42',
			'color'      => 'Trắng gót lục',
			'quantity'   => '19',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '804',
			'product_id' => '125',
			'size'       => '43',
			'color'      => 'Trắng gót lục',
			'quantity'   => '7',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '805',
			'product_id' => '126',
			'size'       => '36',
			'color'      => 'Trắng gót đen',
			'quantity'   => '5',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '806',
			'product_id' => '126',
			'size'       => '37',
			'color'      => 'Trắng gót đen',
			'quantity'   => '5',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '807',
			'product_id' => '126',
			'size'       => '38',
			'color'      => 'Trắng gót đen',
			'quantity'   => '16',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '808',
			'product_id' => '126',
			'size'       => '39',
			'color'      => 'Trắng gót đen',
			'quantity'   => '14',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '809',
			'product_id' => '126',
			'size'       => '40',
			'color'      => 'Trắng gót đen',
			'quantity'   => '14',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '810',
			'product_id' => '126',
			'size'       => '41',
			'color'      => 'Trắng gót đen',
			'quantity'   => '6',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '811',
			'product_id' => '126',
			'size'       => '42',
			'color'      => 'Trắng gót đen',
			'quantity'   => '10',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '812',
			'product_id' => '126',
			'size'       => '43',
			'color'      => 'Trắng gót đen',
			'quantity'   => '6',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '813',
			'product_id' => '127',
			'size'       => '36',
			'color'      => 'Trắng gót navy',
			'quantity'   => '5',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '814',
			'product_id' => '127',
			'size'       => '37',
			'color'      => 'Trắng gót navy',
			'quantity'   => '15',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '815',
			'product_id' => '127',
			'size'       => '38',
			'color'      => 'Trắng gót navy',
			'quantity'   => '8',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '816',
			'product_id' => '127',
			'size'       => '39',
			'color'      => 'Trắng gót navy',
			'quantity'   => '14',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '817',
			'product_id' => '127',
			'size'       => '40',
			'color'      => 'Trắng gót navy',
			'quantity'   => '17',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '818',
			'product_id' => '127',
			'size'       => '41',
			'color'      => 'Trắng gót navy',
			'quantity'   => '12',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '819',
			'product_id' => '127',
			'size'       => '42',
			'color'      => 'Trắng gót navy',
			'quantity'   => '7',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '820',
			'product_id' => '127',
			'size'       => '43',
			'color'      => 'Trắng gót navy',
			'quantity'   => '7',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '821',
			'product_id' => '128',
			'size'       => '40',
			'color'      => 'Ghi',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '822',
			'product_id' => '128',
			'size'       => '41',
			'color'      => 'Ghi',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '823',
			'product_id' => '128',
			'size'       => '42',
			'color'      => 'Ghi',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '824',
			'product_id' => '128',
			'size'       => '43',
			'color'      => 'Ghi',
			'quantity'   => '19',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '825',
			'product_id' => '129',
			'size'       => '40',
			'color'      => 'Trắng đen',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '826',
			'product_id' => '129',
			'size'       => '41',
			'color'      => 'Trắng đen',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '827',
			'product_id' => '129',
			'size'       => '42',
			'color'      => 'Trắng đen',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '828',
			'product_id' => '129',
			'size'       => '43',
			'color'      => 'Trắng đen',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '829',
			'product_id' => '130',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '12',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '830',
			'product_id' => '130',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '831',
			'product_id' => '130',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '832',
			'product_id' => '130',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '833',
			'product_id' => '131',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '834',
			'product_id' => '131',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '835',
			'product_id' => '131',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '836',
			'product_id' => '131',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '18',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '837',
			'product_id' => '132',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '838',
			'product_id' => '132',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '839',
			'product_id' => '132',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '840',
			'product_id' => '132',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '841',
			'product_id' => '132',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '842',
			'product_id' => '133',
			'size'       => '40',
			'color'      => 'Trắng',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '843',
			'product_id' => '133',
			'size'       => '41',
			'color'      => 'Trắng',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '844',
			'product_id' => '133',
			'size'       => '42',
			'color'      => 'Trắng',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '845',
			'product_id' => '133',
			'size'       => '43',
			'color'      => 'Trắng',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '846',
			'product_id' => '134',
			'size'       => '40',
			'color'      => 'ghi xám',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '847',
			'product_id' => '134',
			'size'       => '41',
			'color'      => 'ghi xám',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '848',
			'product_id' => '134',
			'size'       => '42',
			'color'      => 'ghi xám',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '849',
			'product_id' => '134',
			'size'       => '43',
			'color'      => 'ghi xám',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '850',
			'product_id' => '135',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '851',
			'product_id' => '135',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '852',
			'product_id' => '135',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '853',
			'product_id' => '135',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '854',
			'product_id' => '136',
			'size'       => '40',
			'color'      => 'Đen xám',
			'quantity'   => '8',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '855',
			'product_id' => '136',
			'size'       => '41',
			'color'      => 'Đen xám',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '856',
			'product_id' => '136',
			'size'       => '42',
			'color'      => 'Đen xám',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '857',
			'product_id' => '136',
			'size'       => '43',
			'color'      => 'Đen xám',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '858',
			'product_id' => '137',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '19',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '859',
			'product_id' => '137',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '10',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '860',
			'product_id' => '137',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '861',
			'product_id' => '137',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '862',
			'product_id' => '138',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '863',
			'product_id' => '138',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '10',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '864',
			'product_id' => '138',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '865',
			'product_id' => '138',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '866',
			'product_id' => '139',
			'size'       => '36',
			'color'      => 'Đen',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '867',
			'product_id' => '139',
			'size'       => '37',
			'color'      => 'Đen',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '868',
			'product_id' => '139',
			'size'       => '38',
			'color'      => 'Đen',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '869',
			'product_id' => '139',
			'size'       => '39',
			'color'      => 'Đen',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '870',
			'product_id' => '139',
			'size'       => '40',
			'color'      => 'Đen',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '871',
			'product_id' => '139',
			'size'       => '41',
			'color'      => 'Đen',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '872',
			'product_id' => '139',
			'size'       => '42',
			'color'      => 'Đen',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '873',
			'product_id' => '139',
			'size'       => '43',
			'color'      => 'Đen',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '874',
			'product_id' => '139',
			'size'       => '44',
			'color'      => 'Đen',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '875',
			'product_id' => '140',
			'size'       => '36',
			'color'      => 'full trắng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '876',
			'product_id' => '140',
			'size'       => '37',
			'color'      => 'full trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '877',
			'product_id' => '140',
			'size'       => '38',
			'color'      => 'full trắng',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '878',
			'product_id' => '140',
			'size'       => '39',
			'color'      => 'full trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '879',
			'product_id' => '140',
			'size'       => '40',
			'color'      => 'full trắng',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '880',
			'product_id' => '140',
			'size'       => '41',
			'color'      => 'full trắng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '881',
			'product_id' => '140',
			'size'       => '42',
			'color'      => 'full trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '882',
			'product_id' => '140',
			'size'       => '43',
			'color'      => 'full trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '883',
			'product_id' => '141',
			'size'       => '36',
			'color'      => 'Hồng trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '884',
			'product_id' => '141',
			'size'       => '37',
			'color'      => 'Hồng trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '885',
			'product_id' => '141',
			'size'       => '38',
			'color'      => 'Hồng trắng',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '886',
			'product_id' => '141',
			'size'       => '39',
			'color'      => 'Hồng trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '887',
			'product_id' => '142',
			'size'       => '36',
			'color'      => 'ghi trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '888',
			'product_id' => '142',
			'size'       => '37',
			'color'      => 'ghi trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '889',
			'product_id' => '142',
			'size'       => '38',
			'color'      => 'ghi trắng',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '890',
			'product_id' => '142',
			'size'       => '39',
			'color'      => 'ghi trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '891',
			'product_id' => '142',
			'size'       => '40',
			'color'      => 'ghi trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '892',
			'product_id' => '142',
			'size'       => '41',
			'color'      => 'ghi trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '893',
			'product_id' => '142',
			'size'       => '42',
			'color'      => 'ghi trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '894',
			'product_id' => '142',
			'size'       => '43',
			'color'      => 'ghi trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '895',
			'product_id' => '142',
			'size'       => '44',
			'color'      => 'ghi trắng',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '896',
			'product_id' => '143',
			'size'       => '36',
			'color'      => 'Trắng xanh',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '897',
			'product_id' => '143',
			'size'       => '37',
			'color'      => 'Trắng xanh',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '898',
			'product_id' => '143',
			'size'       => '38',
			'color'      => 'Trắng xanh',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '899',
			'product_id' => '143',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '900',
			'product_id' => '143',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '901',
			'product_id' => '143',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '902',
			'product_id' => '143',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '903',
			'product_id' => '143',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '904',
			'product_id' => '144',
			'size'       => '40',
			'color'      => 'ghi xám',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '905',
			'product_id' => '144',
			'size'       => '41',
			'color'      => 'ghi xám',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '906',
			'product_id' => '144',
			'size'       => '42',
			'color'      => 'ghi xám',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '907',
			'product_id' => '144',
			'size'       => '43',
			'color'      => 'ghi xám',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '908',
			'product_id' => '145',
			'size'       => '36',
			'color'      => 'Hồng trắng',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '909',
			'product_id' => '145',
			'size'       => '37',
			'color'      => 'Hồng trắng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '910',
			'product_id' => '145',
			'size'       => '38',
			'color'      => 'Hồng trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '911',
			'product_id' => '145',
			'size'       => '39',
			'color'      => 'Hồng trắng',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '912',
			'product_id' => '146',
			'size'       => '36',
			'color'      => 'Đen trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '913',
			'product_id' => '146',
			'size'       => '37',
			'color'      => 'Đen trắng',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '914',
			'product_id' => '146',
			'size'       => '38',
			'color'      => 'Đen trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '915',
			'product_id' => '146',
			'size'       => '39',
			'color'      => 'Đen trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '916',
			'product_id' => '146',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '917',
			'product_id' => '146',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '918',
			'product_id' => '146',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '919',
			'product_id' => '146',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '920',
			'product_id' => '147',
			'size'       => '36',
			'color'      => 'ghi trắng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '921',
			'product_id' => '147',
			'size'       => '37',
			'color'      => 'ghi trắng',
			'quantity'   => '6',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '922',
			'product_id' => '147',
			'size'       => '38',
			'color'      => 'ghi trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '923',
			'product_id' => '147',
			'size'       => '39',
			'color'      => 'ghi trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '924',
			'product_id' => '147',
			'size'       => '40',
			'color'      => 'ghi trắng',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '925',
			'product_id' => '147',
			'size'       => '41',
			'color'      => 'ghi trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '926',
			'product_id' => '147',
			'size'       => '42',
			'color'      => 'ghi trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '927',
			'product_id' => '147',
			'size'       => '43',
			'color'      => 'ghi trắng',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '928',
			'product_id' => '148',
			'size'       => '40',
			'color'      => 'Nâu kem',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '929',
			'product_id' => '148',
			'size'       => '43',
			'color'      => 'Nâu kem',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '930',
			'product_id' => '148',
			'size'       => '44',
			'color'      => 'Nâu kem',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '931',
			'product_id' => '149',
			'size'       => '36',
			'color'      => 'Đen',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '932',
			'product_id' => '149',
			'size'       => '37',
			'color'      => 'Đen',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '933',
			'product_id' => '149',
			'size'       => '38',
			'color'      => 'Đen',
			'quantity'   => '10',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '934',
			'product_id' => '149',
			'size'       => '39',
			'color'      => 'Đen',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '935',
			'product_id' => '149',
			'size'       => '40',
			'color'      => 'Đen',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '936',
			'product_id' => '149',
			'size'       => '41',
			'color'      => 'Đen',
			'quantity'   => '20',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '937',
			'product_id' => '149',
			'size'       => '42',
			'color'      => 'Đen',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '938',
			'product_id' => '149',
			'size'       => '43',
			'color'      => 'Đen',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '939',
			'product_id' => '150',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '940',
			'product_id' => '150',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '9',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '941',
			'product_id' => '150',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '942',
			'product_id' => '150',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '943',
			'product_id' => '151',
			'size'       => '36',
			'color'      => 'kem tím',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '944',
			'product_id' => '151',
			'size'       => '37',
			'color'      => 'kem tím',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '945',
			'product_id' => '151',
			'size'       => '38',
			'color'      => 'kem tím',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '946',
			'product_id' => '151',
			'size'       => '39',
			'color'      => 'kem tím',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '947',
			'product_id' => '152',
			'size'       => '36',
			'color'      => 'full trắng',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '948',
			'product_id' => '152',
			'size'       => '37',
			'color'      => 'full trắng',
			'quantity'   => '9',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '949',
			'product_id' => '152',
			'size'       => '38',
			'color'      => 'full trắng',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '950',
			'product_id' => '152',
			'size'       => '39',
			'color'      => 'full trắng',
			'quantity'   => '16',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '951',
			'product_id' => '152',
			'size'       => '40',
			'color'      => 'full trắng',
			'quantity'   => '10',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '952',
			'product_id' => '152',
			'size'       => '41',
			'color'      => 'full trắng',
			'quantity'   => '18',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '953',
			'product_id' => '152',
			'size'       => '42',
			'color'      => 'full trắng',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '954',
			'product_id' => '152',
			'size'       => '43',
			'color'      => 'full trắng',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '955',
			'product_id' => '153',
			'size'       => '40',
			'color'      => 'đen kem',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '956',
			'product_id' => '153',
			'size'       => '41',
			'color'      => 'đen kem',
			'quantity'   => '9',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '957',
			'product_id' => '153',
			'size'       => '42',
			'color'      => 'đen kem',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '958',
			'product_id' => '153',
			'size'       => '43',
			'color'      => 'đen kem',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '959',
			'product_id' => '154',
			'size'       => '40',
			'color'      => 'ghi trắng',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '960',
			'product_id' => '154',
			'size'       => '41',
			'color'      => 'ghi trắng',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '961',
			'product_id' => '154',
			'size'       => '42',
			'color'      => 'ghi trắng',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '962',
			'product_id' => '154',
			'size'       => '43',
			'color'      => 'ghi trắng',
			'quantity'   => '17',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '963',
			'product_id' => '155',
			'size'       => '40',
			'color'      => 'xám trắng',
			'quantity'   => '18',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '964',
			'product_id' => '155',
			'size'       => '41',
			'color'      => 'xám trắng',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '965',
			'product_id' => '155',
			'size'       => '42',
			'color'      => 'xám trắng',
			'quantity'   => '9',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '966',
			'product_id' => '155',
			'size'       => '43',
			'color'      => 'xám trắng',
			'quantity'   => '14',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '967',
			'product_id' => '155',
			'size'       => '44',
			'color'      => 'xám trắng',
			'quantity'   => '9',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '968',
			'product_id' => '156',
			'size'       => '39',
			'color'      => 'Đen trắng',
			'quantity'   => '10',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '969',
			'product_id' => '156',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '970',
			'product_id' => '156',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '971',
			'product_id' => '156',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '972',
			'product_id' => '156',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '12',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '973',
			'product_id' => '157',
			'size'       => '40',
			'color'      => 'Trắng đỏ',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '974',
			'product_id' => '157',
			'size'       => '41',
			'color'      => 'Trắng đỏ',
			'quantity'   => '13',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '975',
			'product_id' => '157',
			'size'       => '42',
			'color'      => 'Trắng đỏ',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '976',
			'product_id' => '157',
			'size'       => '43',
			'color'      => 'Trắng đỏ',
			'quantity'   => '7',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '977',
			'product_id' => '158',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '8',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '978',
			'product_id' => '158',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '10',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '979',
			'product_id' => '158',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '12',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '980',
			'product_id' => '158',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '981',
			'product_id' => '159',
			'size'       => '40',
			'color'      => 'xanh ngọc',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '982',
			'product_id' => '159',
			'size'       => '41',
			'color'      => 'xanh ngọc',
			'quantity'   => '8',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '983',
			'product_id' => '159',
			'size'       => '42',
			'color'      => 'xanh ngọc',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '984',
			'product_id' => '159',
			'size'       => '43',
			'color'      => 'xanh ngọc',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '985',
			'product_id' => '160',
			'size'       => '39',
			'color'      => 'xám đỏ',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '986',
			'product_id' => '160',
			'size'       => '40',
			'color'      => 'xám đỏ',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '987',
			'product_id' => '160',
			'size'       => '41',
			'color'      => 'xám đỏ',
			'quantity'   => '11',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '988',
			'product_id' => '160',
			'size'       => '42',
			'color'      => 'xám đỏ',
			'quantity'   => '9',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '989',
			'product_id' => '160',
			'size'       => '43',
			'color'      => 'xám đỏ',
			'quantity'   => '12',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '990',
			'product_id' => '161',
			'size'       => '40',
			'color'      => 'xanh rêu',
			'quantity'   => '9',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '991',
			'product_id' => '161',
			'size'       => '41',
			'color'      => 'xanh rêu',
			'quantity'   => '5',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '992',
			'product_id' => '161',
			'size'       => '42',
			'color'      => 'xanh rêu',
			'quantity'   => '16',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '993',
			'product_id' => '161',
			'size'       => '43',
			'color'      => 'xanh rêu',
			'quantity'   => '8',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '994',
			'product_id' => '161',
			'size'       => '44',
			'color'      => 'xanh rêu',
			'quantity'   => '18',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '995',
			'product_id' => '162',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '7',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '996',
			'product_id' => '162',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '14',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '997',
			'product_id' => '162',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '19',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '998',
			'product_id' => '162',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '9',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '999',
			'product_id' => '162',
			'size'       => '44',
			'color'      => 'Đen trắng',
			'quantity'   => '10',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1000',
			'product_id' => '163',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '7',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1001',
			'product_id' => '163',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '8',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1002',
			'product_id' => '163',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '10',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1003',
			'product_id' => '163',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '13',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1004',
			'product_id' => '163',
			'size'       => '44',
			'color'      => 'full đen',
			'quantity'   => '13',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1005',
			'product_id' => '164',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '20',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1006',
			'product_id' => '164',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '17',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1007',
			'product_id' => '164',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '17',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1008',
			'product_id' => '164',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '13',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1009',
			'product_id' => '164',
			'size'       => '44',
			'color'      => 'full đen',
			'quantity'   => '17',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1010',
			'product_id' => '165',
			'size'       => '36',
			'color'      => 'xám kem',
			'quantity'   => '9',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1011',
			'product_id' => '165',
			'size'       => '37',
			'color'      => 'xám kem',
			'quantity'   => '5',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1012',
			'product_id' => '165',
			'size'       => '38',
			'color'      => 'xám kem',
			'quantity'   => '7',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1013',
			'product_id' => '165',
			'size'       => '39',
			'color'      => 'xám kem',
			'quantity'   => '9',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1014',
			'product_id' => '165',
			'size'       => '40',
			'color'      => 'xám kem',
			'quantity'   => '14',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1015',
			'product_id' => '165',
			'size'       => '41',
			'color'      => 'xám kem',
			'quantity'   => '17',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1016',
			'product_id' => '165',
			'size'       => '42',
			'color'      => 'xám kem',
			'quantity'   => '7',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1017',
			'product_id' => '165',
			'size'       => '43',
			'color'      => 'xám kem',
			'quantity'   => '8',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1018',
			'product_id' => '166',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '5',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1019',
			'product_id' => '166',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '11',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1020',
			'product_id' => '167',
			'size'       => '40',
			'color'      => 'Ghi',
			'quantity'   => '5',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1021',
			'product_id' => '167',
			'size'       => '41',
			'color'      => 'Ghi',
			'quantity'   => '12',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1022',
			'product_id' => '167',
			'size'       => '42',
			'color'      => 'Ghi',
			'quantity'   => '6',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1023',
			'product_id' => '167',
			'size'       => '43',
			'color'      => 'Ghi',
			'quantity'   => '13',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1024',
			'product_id' => '167',
			'size'       => '44',
			'color'      => 'Ghi',
			'quantity'   => '13',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1025',
			'product_id' => '168',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '20',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1026',
			'product_id' => '168',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '16',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1027',
			'product_id' => '168',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '13',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1028',
			'product_id' => '168',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '10',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1029',
			'product_id' => '168',
			'size'       => '44',
			'color'      => 'full đen',
			'quantity'   => '19',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1030',
			'product_id' => '169',
			'size'       => '40',
			'color'      => 'đen xanh',
			'quantity'   => '6',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1031',
			'product_id' => '169',
			'size'       => '41',
			'color'      => 'đen xanh',
			'quantity'   => '5',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1032',
			'product_id' => '169',
			'size'       => '42',
			'color'      => 'đen xanh',
			'quantity'   => '8',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1033',
			'product_id' => '169',
			'size'       => '43',
			'color'      => 'đen xanh',
			'quantity'   => '17',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1034',
			'product_id' => '169',
			'size'       => '44',
			'color'      => 'đen xanh',
			'quantity'   => '14',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1035',
			'product_id' => '170',
			'size'       => '40',
			'color'      => 'ghi xanh',
			'quantity'   => '17',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1036',
			'product_id' => '170',
			'size'       => '41',
			'color'      => 'ghi xanh',
			'quantity'   => '15',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1037',
			'product_id' => '170',
			'size'       => '42',
			'color'      => 'ghi xanh',
			'quantity'   => '16',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1038',
			'product_id' => '170',
			'size'       => '43',
			'color'      => 'ghi xanh',
			'quantity'   => '8',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1039',
			'product_id' => '170',
			'size'       => '44',
			'color'      => 'ghi xanh',
			'quantity'   => '5',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1040',
			'product_id' => '171',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '14',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1041',
			'product_id' => '171',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '19',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1042',
			'product_id' => '171',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '12',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1043',
			'product_id' => '171',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '12',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1044',
			'product_id' => '171',
			'size'       => '44',
			'color'      => 'full đen',
			'quantity'   => '15',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1045',
			'product_id' => '172',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '9',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1046',
			'product_id' => '172',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '15',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1047',
			'product_id' => '172',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '19',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1048',
			'product_id' => '172',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '20',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1049',
			'product_id' => '172',
			'size'       => '44',
			'color'      => 'Đen trắng',
			'quantity'   => '11',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1050',
			'product_id' => '173',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '18',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1051',
			'product_id' => '173',
			'size'       => '41',
			'color'      => 'Trắng xanh',
			'quantity'   => '6',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1052',
			'product_id' => '173',
			'size'       => '42',
			'color'      => 'Trắng xanh',
			'quantity'   => '9',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1053',
			'product_id' => '173',
			'size'       => '43',
			'color'      => 'Trắng xanh',
			'quantity'   => '10',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1054',
			'product_id' => '173',
			'size'       => '44',
			'color'      => 'Trắng xanh',
			'quantity'   => '16',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1055',
			'product_id' => '174',
			'size'       => '40',
			'color'      => 'ghi xám',
			'quantity'   => '6',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1056',
			'product_id' => '174',
			'size'       => '41',
			'color'      => 'ghi xám',
			'quantity'   => '11',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1057',
			'product_id' => '174',
			'size'       => '42',
			'color'      => 'ghi xám',
			'quantity'   => '20',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1058',
			'product_id' => '174',
			'size'       => '43',
			'color'      => 'ghi xám',
			'quantity'   => '6',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1059',
			'product_id' => '174',
			'size'       => '44',
			'color'      => 'ghi xám',
			'quantity'   => '11',
			'price'      => '60',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1060',
			'product_id' => '175',
			'size'       => '40',
			'color'      => 'full đen',
			'quantity'   => '6',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1061',
			'product_id' => '175',
			'size'       => '41',
			'color'      => 'full đen',
			'quantity'   => '20',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1062',
			'product_id' => '175',
			'size'       => '42',
			'color'      => 'full đen',
			'quantity'   => '16',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1063',
			'product_id' => '175',
			'size'       => '43',
			'color'      => 'full đen',
			'quantity'   => '20',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1064',
			'product_id' => '175',
			'size'       => '44',
			'color'      => 'full đen',
			'quantity'   => '17',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1065',
			'product_id' => '176',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '16',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1066',
			'product_id' => '176',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '18',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1067',
			'product_id' => '176',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '7',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1068',
			'product_id' => '176',
			'size'       => '43',
			'color'      => 'Đen trắng',
			'quantity'   => '5',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1069',
			'product_id' => '176',
			'size'       => '44',
			'color'      => 'Đen trắng',
			'quantity'   => '8',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1070',
			'product_id' => '177',
			'size'       => '40',
			'color'      => 'Xám',
			'quantity'   => '7',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1071',
			'product_id' => '177',
			'size'       => '41',
			'color'      => 'Xám',
			'quantity'   => '15',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1072',
			'product_id' => '177',
			'size'       => '42',
			'color'      => 'Xám',
			'quantity'   => '10',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1073',
			'product_id' => '177',
			'size'       => '43',
			'color'      => 'Xám',
			'quantity'   => '20',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1074',
			'product_id' => '177',
			'size'       => '44',
			'color'      => 'Xám',
			'quantity'   => '10',
			'price'      => '65',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1075',
			'product_id' => '178',
			'size'       => '36',
			'color'      => 'Trắng xanh',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1076',
			'product_id' => '178',
			'size'       => '38',
			'color'      => 'Trắng xanh',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1077',
			'product_id' => '178',
			'size'       => '39',
			'color'      => 'Trắng xanh',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1078',
			'product_id' => '178',
			'size'       => '40',
			'color'      => 'Trắng xanh',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1079',
			'product_id' => '179',
			'size'       => '36',
			'color'      => 'Xanh dương',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1080',
			'product_id' => '179',
			'size'       => '37',
			'color'      => 'Xanh dương',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1081',
			'product_id' => '180',
			'size'       => '36',
			'color'      => 'Hồng trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1082',
			'product_id' => '180',
			'size'       => '39',
			'color'      => 'Hồng trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1083',
			'product_id' => '181',
			'size'       => '36',
			'color'      => 'đen xanh',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1084',
			'product_id' => '181',
			'size'       => '37',
			'color'      => 'đen xanh',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1085',
			'product_id' => '181',
			'size'       => '38',
			'color'      => 'đen xanh',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1086',
			'product_id' => '181',
			'size'       => '39',
			'color'      => 'đen xanh',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1087',
			'product_id' => '181',
			'size'       => '40',
			'color'      => 'đen xanh',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1088',
			'product_id' => '182',
			'size'       => '36',
			'color'      => 'ghi trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1089',
			'product_id' => '182',
			'size'       => '37',
			'color'      => 'ghi trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1090',
			'product_id' => '182',
			'size'       => '38',
			'color'      => 'ghi trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1091',
			'product_id' => '182',
			'size'       => '39',
			'color'      => 'ghi trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1092',
			'product_id' => '182',
			'size'       => '40',
			'color'      => 'ghi trắng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1093',
			'product_id' => '182',
			'size'       => '41',
			'color'      => 'ghi trắng',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1094',
			'product_id' => '182',
			'size'       => '42',
			'color'      => 'ghi trắng',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1095',
			'product_id' => '182',
			'size'       => '43',
			'color'      => 'ghi trắng',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1096',
			'product_id' => '183',
			'size'       => '36',
			'color'      => 'Đen trắng',
			'quantity'   => '12',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1097',
			'product_id' => '183',
			'size'       => '37',
			'color'      => 'Đen trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1098',
			'product_id' => '183',
			'size'       => '40',
			'color'      => 'Đen trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1099',
			'product_id' => '183',
			'size'       => '41',
			'color'      => 'Đen trắng',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1100',
			'product_id' => '183',
			'size'       => '42',
			'color'      => 'Đen trắng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1101',
			'product_id' => '184',
			'size'       => '39',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '16',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1102',
			'product_id' => '184',
			'size'       => '40',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '18',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1103',
			'product_id' => '184',
			'size'       => '43',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '19',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1104',
			'product_id' => '184',
			'size'       => '44',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '15',
			'price'      => '55',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1105',
			'product_id' => '185',
			'size'       => '36',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '10',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1106',
			'product_id' => '185',
			'size'       => '37',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '5',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1107',
			'product_id' => '185',
			'size'       => '38',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '8',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1108',
			'product_id' => '185',
			'size'       => '39',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '14',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1109',
			'product_id' => '185',
			'size'       => '40',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '7',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1110',
			'product_id' => '185',
			'size'       => '41',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '17',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1111',
			'product_id' => '185',
			'size'       => '42',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '12',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1112',
			'product_id' => '185',
			'size'       => '43',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '14',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1113',
			'product_id' => '185',
			'size'       => '44',
			'color'      => 'trắng kẻ đen',
			'quantity'   => '19',
			'price'      => '75',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1114',
			'product_id' => '186',
			'size'       => '36',
			'color'      => 'Kem',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1115',
			'product_id' => '186',
			'size'       => '37',
			'color'      => 'Kem',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1116',
			'product_id' => '186',
			'size'       => '38',
			'color'      => 'Kem',
			'quantity'   => '14',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1117',
			'product_id' => '186',
			'size'       => '39',
			'color'      => 'Kem',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1118',
			'product_id' => '186',
			'size'       => '40',
			'color'      => 'Kem',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1119',
			'product_id' => '186',
			'size'       => '41',
			'color'      => 'Kem',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1120',
			'product_id' => '186',
			'size'       => '42',
			'color'      => 'Kem',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1121',
			'product_id' => '186',
			'size'       => '43',
			'color'      => 'Kem',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1122',
			'product_id' => '187',
			'size'       => '36',
			'color'      => 'Hồng trắng',
			'quantity'   => '13',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1123',
			'product_id' => '187',
			'size'       => '37',
			'color'      => 'Hồng trắng',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1124',
			'product_id' => '187',
			'size'       => '38',
			'color'      => 'Hồng trắng',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1125',
			'product_id' => '187',
			'size'       => '39',
			'color'      => 'Hồng trắng',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1126',
			'product_id' => '188',
			'size'       => '36',
			'color'      => 'Rêu',
			'quantity'   => '15',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1127',
			'product_id' => '188',
			'size'       => '37',
			'color'      => 'Rêu',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1128',
			'product_id' => '188',
			'size'       => '38',
			'color'      => 'Rêu',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1129',
			'product_id' => '188',
			'size'       => '39',
			'color'      => 'Rêu',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1130',
			'product_id' => '188',
			'size'       => '40',
			'color'      => 'Rêu',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1131',
			'product_id' => '188',
			'size'       => '41',
			'color'      => 'Rêu',
			'quantity'   => '18',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1132',
			'product_id' => '188',
			'size'       => '42',
			'color'      => 'Rêu',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1133',
			'product_id' => '188',
			'size'       => '43',
			'color'      => 'Rêu',
			'quantity'   => '11',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1134',
			'product_id' => '189',
			'size'       => '36',
			'color'      => 'nâu hồng',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1135',
			'product_id' => '189',
			'size'       => '37',
			'color'      => 'nâu hồng',
			'quantity'   => '9',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1136',
			'product_id' => '189',
			'size'       => '38',
			'color'      => 'nâu hồng',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1137',
			'product_id' => '189',
			'size'       => '39',
			'color'      => 'nâu hồng',
			'quantity'   => '5',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1138',
			'product_id' => '190',
			'size'       => '36',
			'color'      => 'Xanh dương',
			'quantity'   => '11',
			'price'      => '40',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1139',
			'product_id' => '190',
			'size'       => '38',
			'color'      => 'Xanh dương',
			'quantity'   => '14',
			'price'      => '40',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1140',
			'product_id' => '190',
			'size'       => '40',
			'color'      => 'Xanh dương',
			'quantity'   => '18',
			'price'      => '40',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1141',
			'product_id' => '190',
			'size'       => '42',
			'color'      => 'Xanh dương',
			'quantity'   => '10',
			'price'      => '40',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1142',
			'product_id' => '190',
			'size'       => '43',
			'color'      => 'Xanh dương',
			'quantity'   => '6',
			'price'      => '40',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1143',
			'product_id' => '191',
			'size'       => '36',
			'color'      => 'xám chấm cam',
			'quantity'   => '8',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1144',
			'product_id' => '191',
			'size'       => '38',
			'color'      => 'xám chấm cam',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1145',
			'product_id' => '191',
			'size'       => '39',
			'color'      => 'xám chấm cam',
			'quantity'   => '19',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1146',
			'product_id' => '191',
			'size'       => '40',
			'color'      => 'xám chấm cam',
			'quantity'   => '7',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1147',
			'product_id' => '192',
			'size'       => '36',
			'color'      => 'Ghi cam',
			'quantity'   => '17',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1148',
			'product_id' => '192',
			'size'       => '37',
			'color'      => 'Ghi cam',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1149',
			'product_id' => '192',
			'size'       => '38',
			'color'      => 'Ghi cam',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1150',
			'product_id' => '192',
			'size'       => '39',
			'color'      => 'Ghi cam',
			'quantity'   => '10',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1151',
			'product_id' => '193',
			'size'       => '36',
			'color'      => 'Trắng cam',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1152',
			'product_id' => '193',
			'size'       => '37',
			'color'      => 'Trắng cam',
			'quantity'   => '16',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1153',
			'product_id' => '193',
			'size'       => '38',
			'color'      => 'Trắng cam',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1154',
			'product_id' => '193',
			'size'       => '39',
			'color'      => 'Trắng cam',
			'quantity'   => '20',
			'price'      => '85',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1155',
			'product_id' => '194',
			'size'       => '37',
			'color'      => 'Đen trắng đỏ',
			'quantity'   => '16',
			'price'      => '50',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1156',
			'product_id' => '195',
			'size'       => '36',
			'color'      => 'Trắng',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1157',
			'product_id' => '195',
			'size'       => '37',
			'color'      => 'Trắng',
			'quantity'   => '9',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1158',
			'product_id' => '195',
			'size'       => '38',
			'color'      => 'Trắng',
			'quantity'   => '19',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1159',
			'product_id' => '195',
			'size'       => '39',
			'color'      => 'Trắng',
			'quantity'   => '5',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1160',
			'product_id' => '195',
			'size'       => '40',
			'color'      => 'Trắng',
			'quantity'   => '15',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1161',
			'product_id' => '195',
			'size'       => '41',
			'color'      => 'Trắng',
			'quantity'   => '18',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1162',
			'product_id' => '195',
			'size'       => '42',
			'color'      => 'Trắng',
			'quantity'   => '6',
			'price'      => '0',
		]);
		$this->insert('{{%product_detail}}', [
			'id'         => '1163',
			'product_id' => '195',
			'size'       => '43',
			'color'      => 'Trắng',
			'quantity'   => '7',
			'price'      => '0',
		]);
	}

	public function safeDown() {
		$this->dropIndex('fk_product_id', '{{%product_detail}}');
		$this->dropTable('{{%product_detail}}');
	}
}

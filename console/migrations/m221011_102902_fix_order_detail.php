<?php

use yii\db\Migration;

/**
 * Class m221011_102902_fix_order_detail
 */
class m221011_102902_fix_order_detail extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->execute('ALTER TABLE `order_detail` CHANGE `product_id` `product_detail_id` INT NULL DEFAULT NULL;');
		$this->execute('ALTER TABLE `order_detail` DROP FOREIGN KEY `fk_order_detail_product_id`; ALTER TABLE `order_detail` ADD CONSTRAINT `fk_order_detail_product_id` FOREIGN KEY (`product_detail_id`) REFERENCES `product_detail`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m221011_102902_fix_order_detail cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m221011_102902_fix_order_detail cannot be reverted.\n";

		return false;
	}
	*/
}

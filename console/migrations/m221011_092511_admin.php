<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092511_admin extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%admin}}', [
			'id'            => Schema::TYPE_PK . '',
			'username'      => Schema::TYPE_STRING . '(255) NOT NULL',
			'password_hash' => Schema::TYPE_STRING . '(255) NOT NULL',
			'status'        => Schema::TYPE_INTEGER . ' NOT NULL',
			'created_at'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'super_admin'   => Schema::TYPE_INTEGER . ' NOT NULL',
		], $tableOptions);
		$this->insert('{{%admin}}', [
			'id'            => '1',
			'username'      => 'admin',
			'password_hash' => '$2y$13$Y2G.rQUQ4fncrMtATsWMBeyl78Mq7vdArnqo/aDOK7VOpqBQ1vhlO',
			'status'        => '10',
			'created_at'    => '1664786357',
			'updated_at'    => '1664786357',
			'super_admin'   => '1',
		]);
	}

	public function safeDown() {
		$this->dropTable('{{%admin}}');
	}
}

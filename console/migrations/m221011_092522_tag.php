<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092522_tag extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%tag}}', [
			'id'   => Schema::TYPE_PK . '',
			'name' => Schema::TYPE_STRING . '(255) NOT NULL',
		], $tableOptions);
		$this->insert('{{%tag}}', [
			'id'   => '2',
			'name' => 'Vans',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '3',
			'name' => 'Vault',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '4',
			'name' => 'Slip',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '5',
			'name' => 'on',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '6',
			'name' => 'Caro',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '7',
			'name' => 'Đen',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '8',
			'name' => 'Trắng',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '9',
			'name' => 'Classic',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '10',
			'name' => 'X',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '11',
			'name' => 'Fear',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '12',
			'name' => 'OF',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '13',
			'name' => 'God',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '14',
			'name' => 'Old',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '15',
			'name' => 'Skool',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '16',
			'name' => 'Da',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '17',
			'name' => 'Lộn',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '18',
			'name' => 'Viền',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '19',
			'name' => 'Navy',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '20',
			'name' => 'Vải',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '21',
			'name' => 'Xanh',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '22',
			'name' => 'Lá',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '23',
			'name' => 'Converse',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '24',
			'name' => '1970s',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '25',
			'name' => 'Cao',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '26',
			'name' => 'Cổ',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '27',
			'name' => 'Thấp',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '28',
			'name' => 'JW',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '29',
			'name' => 'Anderson',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '30',
			'name' => 'New',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '31',
			'name' => 'Balance',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '32',
			'name' => '300',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '33',
			'name' => 'Phản',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '34',
			'name' => 'Quang',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '35',
			'name' => '574',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '36',
			'name' => 'Xám',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '37',
			'name' => 'Dương',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '38',
			'name' => 'Pro',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '39',
			'name' => 'Court',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '40',
			'name' => 'Kem',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '41',
			'name' => 'Đỏ',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '42',
			'name' => 'Vàng',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '43',
			'name' => 'Gót',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '44',
			'name' => '530',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '45',
			'name' => 'Encap',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '46',
			'name' => 'Hồng',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '47',
			'name' => '550',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '48',
			'name' => 'Than',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '49',
			'name' => 'Giày',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '50',
			'name' => 'MLB',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '51',
			'name' => 'NY',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '52',
			'name' => 'Chunky',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '53',
			'name' => 'Jogger',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '54',
			'name' => 'P',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '55',
			'name' => 'Boston',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '56',
			'name' => 'Yankees',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '57',
			'name' => 'Nỉ',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '58',
			'name' => 'Bigball',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '59',
			'name' => 'DODGERS',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '60',
			'name' => 'Check',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '61',
			'name' => 'B',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '62',
			'name' => 'Mono',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '63',
			'name' => 'Nâu',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '64',
			'name' => 'Đế',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '65',
			'name' => 'Liner',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '66',
			'name' => 'Mustard',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '67',
			'name' => 'Chữ',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '68',
			'name' => 'LV',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '69',
			'name' => 'Trainer',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '70',
			'name' => 'Dép',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '71',
			'name' => 'Alphabounce',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '72',
			'name' => 'Adidas',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '73',
			'name' => 'Cloudfoam',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '74',
			'name' => 'Like',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '75',
			'name' => 'Auth',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '76',
			'name' => 'FULL',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '77',
			'name' => 'Nike',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '78',
			'name' => 'Jordan',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '79',
			'name' => '1',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '80',
			'name' => 'Low',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '81',
			'name' => 'Thổ',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '82',
			'name' => 'Cẩm',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '83',
			'name' => 'Camo',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '84',
			'name' => 'Nhạt',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '85',
			'name' => 'High',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '86',
			'name' => 'Tím',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '87',
			'name' => 'Vạch',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '88',
			'name' => 'Ni.ke',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '89',
			'name' => 'Bạc',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '90',
			'name' => 'Air',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '91',
			'name' => 'Paris',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '92',
			'name' => 'Kẻ',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '93',
			'name' => 'Travis',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '94',
			'name' => 'Scott',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '95',
			'name' => '4',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '96',
			'name' => 'Retro',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '97',
			'name' => 'Off',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '98',
			'name' => 'White',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '99',
			'name' => 'Force',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '100',
			'name' => 'Shadow',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '101',
			'name' => 'Ngọc',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '102',
			'name' => 'Kim',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '103',
			'name' => 'Cương',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '104',
			'name' => '8',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '105',
			'name' => 'ID',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '106',
			'name' => 'Gucci',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '107',
			'name' => 'Rắn',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '108',
			'name' => 'Vệt',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '109',
			'name' => 'Thêu',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '110',
			'name' => 'Brooklyn',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '111',
			'name' => 'Athletic',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '112',
			'name' => 'Club',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '113',
			'name' => 'XD',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '114',
			'name' => 'EMB',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '115',
			'name' => 'Siêu',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '116',
			'name' => 'Cấp',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '117',
			'name' => 'Max',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '118',
			'name' => '97',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '119',
			'name' => 'Ghi',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '120',
			'name' => 'Blazer',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '121',
			'name' => 'Zoom',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '122',
			'name' => 'N979',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '123',
			'name' => 'SF',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '124',
			'name' => 'Đậm',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '125',
			'name' => 'M2K',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '126',
			'name' => 'Stan',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '127',
			'name' => 'Smith',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '128',
			'name' => 'Lục',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '129',
			'name' => 'EQ',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '130',
			'name' => '21',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '131',
			'name' => 'Run',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '132',
			'name' => 'AlphaMagma',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '133',
			'name' => '22',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '134',
			'name' => 'Ultra',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '135',
			'name' => 'Boost',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '136',
			'name' => '6.0',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '137',
			'name' => 'Beyond',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '138',
			'name' => 'Chấm',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '139',
			'name' => 'Instinct',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '140',
			'name' => 'M',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '141',
			'name' => 'Black',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '142',
			'name' => 'A102',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '143',
			'name' => 'Rêu',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '144',
			'name' => 'A30',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '145',
			'name' => 'Tubular',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '146',
			'name' => '2195',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '147',
			'name' => 'N977',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '148',
			'name' => 'N975',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '149',
			'name' => '8681',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '150',
			'name' => 'A153',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '151',
			'name' => 'EQT',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '152',
			'name' => 'Bask',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '153',
			'name' => 'Adv',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '154',
			'name' => 'Superstar',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '155',
			'name' => 'Mũi',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '156',
			'name' => 'Sò',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '157',
			'name' => 'Cappuccino',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '158',
			'name' => 'Prophere',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '159',
			'name' => 'Balenciaga',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '160',
			'name' => 'Triple',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '161',
			'name' => 'S',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '162',
			'name' => 'Alexander',
		]);
		$this->insert('{{%tag}}', [
			'id'   => '163',
			'name' => 'McQueen',
		]);
	}

	public function safeDown() {
		$this->dropTable('{{%tag}}');
	}
}

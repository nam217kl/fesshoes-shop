<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092515_order extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%order}}', [
			'id'              => Schema::TYPE_PK . '',
			'user_address_id' => Schema::TYPE_INTEGER . '',
			'total_amount'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'status'          => Schema::TYPE_INTEGER . ' NOT NULL',
		], $tableOptions);
		$this->createIndex('user_address_id', '{{%order}}', 'user_address_id', 0);
	}

	public function safeDown() {
		$this->dropIndex('user_address_id', '{{%order}}');
		$this->dropTable('{{%order}}');
	}
}

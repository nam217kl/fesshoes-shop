<?php

use yii\db\Migration;

/**
 * Class m221017_044754_color
 */
class m221017_044754_color extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->replace("caro", "đen trắng");
		$this->replace("full đen", "đen");
		$this->replace("full trắng", "trắng");
		$this->replace("full trắng", "trắng");
		$this->replace("Trắng cam", "cam trắng");
		$this->replace("xám camo", "xám");
		$this->replace("XD trắng", "trắng");
		$this->replace("Rêu", "xanh lá");
		$this->replace("thấp cổ", "trắng");
		$this->replace("Trắng xanh", "xanh");
		$this->replace("Đen trắng đỏ", "đỏ");
		$this->replace("Nâu kem", "nâu");
		$this->replace("cam trắng", "cam");
		$this->replace("Ghi cam", "cam");
		$this->replace("xám chấm cam", "cam");
		$this->replace("Đen cam", "cam");
		$this->replace("xám cam", "cam");
		$this->replace("Trắng đỏ", "đỏ");
		$this->replace("thổ cẩm", "đỏ");
		$this->replace("xám đỏ", "đỏ");
		$this->replace("kem tím", "tím");
		$this->replace("hồng tím", "tím");
		$this->replace("Hồng trắng", "hồng");
		$this->replace("kem xanh hồng", "hồng");
		$this->replace("xanh hồng", "hồng");
		$this->replace("nâu hồng", "hồng");
		$this->replace("Hồng trắng", "hồng");
		$this->replace("xám vàng", "vàng");
		$this->replace("phản quang", "vàng");
		$this->replace("kem vàng", "vàng");
		$this->replace("trắng vàng", "vàng");
		$this->replace("kim cương", "xám");
		$this->replace("Trắng đen", "đen");
		$this->replace("đen trắng", "trắng");
		$this->replace("Đen trắng", "trắng");
		$this->replace("xám trắng", "xám");
		$this->replace("trắng nâu", "nâu");
		$this->replace("ghi trắng", "xám");
		$this->replace("xám kem", "xám");
		$this->replace("xám xanh", "xám");
		$this->replace("ghi xám", "xám");
		$this->replace("Đen xám", "đen");
		$this->replace("xanh rêu", "xanh");
		$this->replace("xanh than", "xanh");
		$this->replace("trắng xanh than", "xanh");
		$this->replace("xanh xám", "xanh");
		$this->replace("đen xanh", "xanh");
		$this->replace("ghi xanh", "xanh");
		$this->replace("xanh ngọc", "xanh");
		$this->replace("Xanh dương", "xanh");
		$this->replace("xanh lá", "xanh");
		$this->replace("kem nâu", "nâu");
		$this->replace("trắng kẻ đen", "trắng");
		$this->replace("đen kem", "đen");
		$this->replace("trắng xanh", "xanh");
		$this->replace("Trắng xám", "xám");
		$this->replace("Trắng gót lục", "trắng");
		$this->replace("Trắng gót đen", "trắng");
		$this->replace("Trắng gót navy", "trắng");
		$this->replace("Kem", "xám");
		$this->replace("Ghi", "xám");
	}

	private function replace($from, $to) {
		$this->execute('update product set color = replace(color, "' . $from . '","' . $to . '");');
		$this->execute('update product_detail set color = replace(color, "' . $from . '","' . $to . '");');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m221017_044754_color cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m221017_044754_color cannot be reverted.\n";

		return false;
	}
	*/
}

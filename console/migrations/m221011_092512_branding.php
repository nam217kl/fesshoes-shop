<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092512_branding extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%branding}}', [
			'id'    => Schema::TYPE_PK . '',
			'image' => Schema::TYPE_TEXT . ' NOT NULL',
			'url'   => Schema::TYPE_TEXT . ' NOT NULL',
		], $tableOptions);
		$this->insert('{{%branding}}', [
			'id'    => '2',
			'image' => 'https://cdn.shopify.com/s/files/1/0456/5070/6581/files/Nike_2048x_c79ac479-de5e-4217-9a2d-192e40451ca0_180x.png?v=1661593328',
			'url'   => '#',
		]);
		$this->insert('{{%branding}}', [
			'id'    => '3',
			'image' => 'https://cdn.shopify.com/s/files/1/0456/5070/6581/files/Logo_Brand-20_1296x_a0b5e631-1c26-40d7-a018-35c9bac37a77_180x.png?v=1661593353',
			'url'   => '#',
		]);
		$this->insert('{{%branding}}', [
			'id'    => '4',
			'image' => 'https://cdn.shopify.com/s/files/1/0456/5070/6581/files/Columbia_f1938539-ab54-4c73-845e-819954ff831b_180x.png?v=1664163928',
			'url'   => '#',
		]);
		$this->insert('{{%branding}}', [
			'id'    => '5',
			'image' => 'https://cdn.shopify.com/s/files/1/0456/5070/6581/files/Converse_2048x_497dcfd4-eef2-46d1-8340-7ac6140d3b03_180x.png?v=1661593402',
			'url'   => '#',
		]);
		$this->insert('{{%branding}}', [
			'id'    => '6',
			'image' => 'https://cdn.shopify.com/s/files/1/0456/5070/6581/files/Garmin_2048x_536bfb95-ec76-4fab-8408-2adc43d33122_180x.png?v=1661593439',
			'url'   => '#',
		]);
		$this->insert('{{%branding}}', [
			'id'    => '7',
			'image' => 'https://cdn.shopify.com/s/files/1/0456/5070/6581/files/Adidas_1296x_1ab6ec69-4bd6-417b-a73e-8df2e18f2f9d_180x.png?v=1661593339',
			'url'   => '#',
		]);
		$this->insert('{{%branding}}', [
			'id'    => '8',
			'image' => 'https://cdn.shopify.com/s/files/1/0456/5070/6581/files/Logo_Brand-14_2048x_ecd38575-d5ed-4b9b-8eaf-fe6ffd010390_360x.png?v=1661593372',
			'url'   => '#',
		]);
		$this->insert('{{%branding}}', [
			'id'    => '9',
			'image' => 'https://cdn.shopify.com/s/files/1/0456/5070/6581/files/New-Balance_2048x_2c138603-9a97-4546-ad03-0353b03f6c0f_180x.png?v=1661593387',
			'url'   => '#',
		]);
		$this->insert('{{%branding}}', [
			'id'    => '10',
			'image' => 'https://cdn.shopify.com/s/files/1/0456/5070/6581/files/Logo_Brand-05_2048x_7452dcef-6ccb-4f32-a5dc-1edecdd913f8_180x.png?v=1661593419',
			'url'   => '#',
		]);
	}

	public function safeDown() {
		$this->dropTable('{{%branding}}');
	}
}

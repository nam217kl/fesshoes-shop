<?php

use yii\db\Migration;

/**
 * Class m221012_031702_fix_admin
 */
class m221012_031702_fix_admin extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->execute('ALTER TABLE `admin` ADD UNIQUE(`username`);');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m221012_031702_fix_admin cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m221012_031702_fix_admin cannot be reverted.\n";

		return false;
	}
	*/
}

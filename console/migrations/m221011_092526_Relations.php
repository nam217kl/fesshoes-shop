<?php

use yii\db\Migration;

class m221011_092526_Relations extends Migration {

	public function safeUp() {
		$this->addForeignKey('fk_category_parent_id', '{{%category}}', 'parent_id', 'category', 'id', null, 'CASCADE');
		$this->addForeignKey('fk_order_user_address_id', '{{%order}}', 'user_address_id', 'user_address', 'id', 'RESTRICT', 'RESTRICT');
		$this->addForeignKey('fk_order_detail_order_id', '{{%order_detail}}', 'order_id', 'order', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_order_detail_product_id', '{{%order_detail}}', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_product_detail_product_id', '{{%product_detail}}', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_product_image_product_id', '{{%product_image}}', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_product_tag_product_id', '{{%product_tag}}', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_product_tag_tag_id', '{{%product_tag}}', 'tag_id', 'tag', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_user_address_user_id', '{{%user_address}}', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_wishlist_product_id', '{{%wishlist}}', 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_wishlist_user_id', '{{%wishlist}}', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown() {

		$this->dropForeignKey('fk_category_parent_id', '{{%category}}');
		$this->dropForeignKey('fk_order_user_address_id', '{{%order}}');
		$this->dropForeignKey('fk_order_detail_order_id', '{{%order_detail}}');
		$this->dropForeignKey('fk_order_detail_product_id', '{{%order_detail}}');
		$this->dropForeignKey('fk_product_detail_product_id', '{{%product_detail}}');
		$this->dropForeignKey('fk_product_image_product_id', '{{%product_image}}');
		$this->dropForeignKey('fk_product_tag_product_id', '{{%product_tag}}');
		$this->dropForeignKey('fk_product_tag_tag_id', '{{%product_tag}}');
		$this->dropForeignKey('fk_user_address_user_id', '{{%user_address}}');
		$this->dropForeignKey('fk_wishlist_product_id', '{{%wishlist}}');
		$this->dropForeignKey('fk_wishlist_user_id', '{{%wishlist}}');
	}
}

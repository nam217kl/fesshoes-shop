<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092525_wishlist extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%wishlist}}', [
			'id'         => Schema::TYPE_PK . '',
			'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'user_id'    => Schema::TYPE_INTEGER . ' NOT NULL',
			'created_at' => Schema::TYPE_INTEGER . '',
		], $tableOptions);
		$this->createIndex('product_id', '{{%wishlist}}', 'product_id', 0);
		$this->createIndex('user_id', '{{%wishlist}}', 'user_id', 0);
	}

	public function safeDown() {
		$this->dropIndex('product_id', '{{%wishlist}}');
		$this->dropIndex('user_id', '{{%wishlist}}');
		$this->dropTable('{{%wishlist}}');
	}
}

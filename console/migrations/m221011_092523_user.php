<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092523_user extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%user}}', [
			'id'                   => Schema::TYPE_PK . '',
			'username'             => Schema::TYPE_STRING . '(255) NOT NULL',
			'password_hash'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'password_reset_token' => Schema::TYPE_STRING . '(255)',
			'verification_token'   => Schema::TYPE_STRING . '(255) NOT NULL',
			'email'                => Schema::TYPE_STRING . '(255) NOT NULL',
			'auth_key'             => Schema::TYPE_STRING . '(255) NOT NULL',
			'status'               => Schema::TYPE_INTEGER . ' NOT NULL',
			'created_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
			'updated_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
		], $tableOptions);
		$this->insert('{{%user}}', [
			'id'                   => '2',
			'username'             => 'nam217',
			'password_hash'        => '$2y$13$Y2G.rQUQ4fncrMtATsWMBeyl78Mq7vdArnqo/aDOK7VOpqBQ1vhlO',
			'password_reset_token' => '',
			'verification_token'   => 'DJsp4f79bQKNfO5ioQA1s3sdVIHvUNs5_1664786357',
			'email'                => 'thanhnam217@gmail.com',
			'auth_key'             => 'yeUswlUwUS8qlEUZdql9xl0EzpgCEdcU',
			'status'               => '10',
			'created_at'           => '1664786357',
			'updated_at'           => '1664786357',
		]);
	}

	public function safeDown() {
		$this->dropTable('{{%user}}');
	}
}

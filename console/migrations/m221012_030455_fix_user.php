<?php

use yii\db\Migration;

/**
 * Class m221012_030455_fix_user
 */
class m221012_030455_fix_user extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->execute('ALTER TABLE `user` CHANGE `verification_token` `verification_token` VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NULL;');
		$this->execute('ALTER TABLE `user` CHANGE `auth_key` `auth_key` VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NULL;');
		$this->execute('ALTER TABLE `user` ADD UNIQUE(`username`);');
		$this->execute('ALTER TABLE `user` ADD UNIQUE(`email`);');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m221012_030455_fix_user cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m221012_030455_fix_user cannot be reverted.\n";

		return false;
	}
	*/
}

<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092514_category extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%category}}', [
			'id'        => Schema::TYPE_PK . '',
			'name'      => Schema::TYPE_STRING . '(255) NOT NULL',
			'parent_id' => Schema::TYPE_INTEGER . '',
		], $tableOptions);
		$this->createIndex('fk_parent_id', '{{%category}}', 'parent_id', 0);
		$this->insert('{{%category}}', [
			'id'        => '1',
			'name'      => 'Nike',
			'parent_id' => '',
		]);
		$this->insert('{{%category}}', [
			'id'        => '2',
			'name'      => 'Adidas',
			'parent_id' => '',
		]);
		$this->insert('{{%category}}', [
			'id'        => '3',
			'name'      => 'Vans - Converse',
			'parent_id' => '',
		]);
		$this->insert('{{%category}}', [
			'id'        => '4',
			'name'      => 'New balance',
			'parent_id' => '',
		]);
		$this->insert('{{%category}}', [
			'id'        => '5',
			'name'      => 'MLB',
			'parent_id' => '',
		]);
		$this->insert('{{%category}}', [
			'id'        => '6',
			'name'      => 'Louis Vuitton',
			'parent_id' => '',
		]);
		$this->insert('{{%category}}', [
			'id'        => '7',
			'name'      => 'Giày thời trang',
			'parent_id' => '',
		]);
		$this->insert('{{%category}}', [
			'id'        => '8',
			'name'      => 'Dép thời trang',
			'parent_id' => '',
		]);
		$this->insert('{{%category}}', [
			'id'        => '9',
			'name'      => 'Jordan',
			'parent_id' => '1',
		]);
		$this->insert('{{%category}}', [
			'id'        => '10',
			'name'      => 'Air Force 1',
			'parent_id' => '1',
		]);
		$this->insert('{{%category}}', [
			'id'        => '11',
			'name'      => 'Air Max 97',
			'parent_id' => '1',
		]);
		$this->insert('{{%category}}', [
			'id'        => '12',
			'name'      => 'Fashion',
			'parent_id' => '1',
		]);
		$this->insert('{{%category}}', [
			'id'        => '13',
			'name'      => 'Stand Smith',
			'parent_id' => '2',
		]);
		$this->insert('{{%category}}', [
			'id'        => '14',
			'name'      => 'EQ 21',
			'parent_id' => '2',
		]);
		$this->insert('{{%category}}', [
			'id'        => '15',
			'name'      => 'Alpha Magma',
			'parent_id' => '2',
		]);
		$this->insert('{{%category}}', [
			'id'        => '16',
			'name'      => 'Ultra Boost',
			'parent_id' => '2',
		]);
		$this->insert('{{%category}}', [
			'id'        => '17',
			'name'      => 'Alpha bounce',
			'parent_id' => '2',
		]);
		$this->insert('{{%category}}', [
			'id'        => '18',
			'name'      => 'Fashion',
			'parent_id' => '2',
		]);
		$this->insert('{{%category}}', [
			'id'        => '19',
			'name'      => 'Yezzy boost',
			'parent_id' => '2',
		]);
		$this->insert('{{%category}}', [
			'id'        => '20',
			'name'      => 'EQT',
			'parent_id' => '2',
		]);
		$this->insert('{{%category}}', [
			'id'        => '21',
			'name'      => 'Super star',
			'parent_id' => '2',
		]);
		$this->insert('{{%category}}', [
			'id'        => '22',
			'name'      => 'Prophere',
			'parent_id' => '2',
		]);
		$this->insert('{{%category}}', [
			'id'        => '24',
			'name'      => 'BALENCIAGA',
			'parent_id' => '7',
		]);
		$this->insert('{{%category}}', [
			'id'        => '25',
			'name'      => 'MCQUEEN',
			'parent_id' => '7',
		]);
	}

	public function safeDown() {
		$this->dropIndex('fk_parent_id', '{{%category}}');
		$this->dropTable('{{%category}}');
	}
}

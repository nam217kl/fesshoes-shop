<?php

use yii\db\Migration;

/**
 * Class m221014_090736_fix_product
 */
class m221014_090736_fix_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->execute("ALTER TABLE `product` ADD `featured` INT NOT NULL DEFAULT '0' AFTER `color`;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m221014_090736_fix_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m221014_090736_fix_product cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use common\models\ProductImage;
use yii\db\Migration;

/**
 * Class m221014_030919_add_images
 */
class m221014_030919_add_images extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$params = Yii::$app->params;
		if (isset($params['uploadUrl'])) {
			$images = ProductImage::find()->andWhere([
				'NOT LIKE',
				'image',
				'fesshoes',
			])->all();
			foreach ($images as $image) {
				$image->updateAttributes(['image' => \Yii::$app->params['uploadUrl'] . '/uploads/product/' . $image->product_id . '/' . $image->id . '.jpg']);
			}
		} else {
			throw new \yii\base\InvalidConfigException('Please config `uploadUrl` before');
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m221014_030919_add_images cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m221014_030919_add_images cannot be reverted.\n";

		return false;
	}
	*/
}

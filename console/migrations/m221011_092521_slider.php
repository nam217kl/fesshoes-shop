<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092521_slider extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%slider}}', [
			'id'          => Schema::TYPE_PK . '',
			'name'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'image'       => Schema::TYPE_TEXT . ' NOT NULL',
			'description' => Schema::TYPE_TEXT . ' NOT NULL',
			'url'         => Schema::TYPE_TEXT . ' NOT NULL',
			'status'      => Schema::TYPE_INTEGER . ' NOT NULL',
		], $tableOptions);
	}

	public function safeDown() {
		$this->dropTable('{{%slider}}');
	}
}

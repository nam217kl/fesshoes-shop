<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092517_product extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%product}}', [
			'id'          => Schema::TYPE_PK . '',
			'name'        => Schema::TYPE_STRING . '(255) NOT NULL',
			'condition'   => Schema::TYPE_STRING . '(255) NOT NULL',
			'short_desc'  => Schema::TYPE_TEXT . ' NOT NULL',
			'long_desc'   => Schema::TYPE_TEXT . ' NOT NULL',
			'category_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'size'        => Schema::TYPE_TEXT . '',
			'color'       => Schema::TYPE_TEXT . '',
		], $tableOptions);
		$this->insert('{{%product}}', [
			'id'          => '1',
			'name'        => 'Vans Vault Slip on Caro Đen Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '3',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["caro"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '2',
			'name'        => 'Vans Classic Đen Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '3',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '3',
			'name'        => 'Vans Vault X Fear OF God Trắng Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '3',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '4',
			'name'        => 'Vans Vault Old Skool Đen Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '3',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '5',
			'name'        => 'Vans Vault Da Lộn Trắng Viền Navy  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '3',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '6',
			'name'        => 'Vans Vault Old Skool Vải Trắng Xanh Navy  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '3',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '7',
			'name'        => 'Vans Vault Da Lộn Trắng Viền Xanh Lá  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '3',
			'size'        => '["40","41","42","43"]',
			'color'       => '["xanh lá"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '8',
			'name'        => 'Converse 1970s Đen Cao Cổ  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '3',
			'size'        => '["35","36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '9',
			'name'        => 'Converse 1970s Đen Thấp Cổ  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '3',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '10',
			'name'        => 'Converse JW Anderson Đen Trắng Cao Cổ  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '3',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '11',
			'name'        => 'New Balance 300 Trắng Phản Quang  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["phản quang"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '12',
			'name'        => 'New Balance 300 Trắng Đen Phản Quang  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '13',
			'name'        => 'New Balance 574 Xám  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '14',
			'name'        => 'New Balance 574 Xám Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xám trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '15',
			'name'        => 'New Balance 574 Xanh Dương  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Xanh dương"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '16',
			'name'        => 'New Balance 300 Pro Court Kem navy  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '17',
			'name'        => 'New Balance 574 Xám Đỏ  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39"]',
			'color'       => '["xám đỏ"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '18',
			'name'        => 'New Balance 300 Pro Court Kem Vàng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["kem vàng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '19',
			'name'        => 'New Balance 574 Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["trắng nâu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '20',
			'name'        => 'New Balance 574 Xám Gót Đỏ  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["40","41","42","43"]',
			'color'       => '["xám đỏ"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '21',
			'name'        => 'New Balance 530 Encap Xám Hồng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39"]',
			'color'       => '["nâu hồng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '22',
			'name'        => 'New Balance 530 Encap Trắng Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '23',
			'name'        => 'New Balance 530 Encap Xanh Xám  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xanh xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '24',
			'name'        => 'New Balance 574 Kem Hồng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Hồng trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '25',
			'name'        => 'New Balance 550 Trắng Xanh  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '26',
			'name'        => 'New Balance 550 Trắng Xanh Than  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '4',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["trắng xanh than"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '27',
			'name'        => 'Giày MLB NY Trắng Đen',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["40"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '28',
			'name'        => 'Giày MLB Chunky Jogger NY Da Lộn Xám Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39"]',
			'color'       => '["xám trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '29',
			'name'        => 'Giày MLB Chunky Jogger NY Da Lộn Đen Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '30',
			'name'        => 'Giày MLB Chunky P Boston Đỏ ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đỏ"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '31',
			'name'        => 'Giày MLB NY Yankees Gót Nỉ ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '32',
			'name'        => 'MLB Bigball Chunky NY Trắng Đen ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '33',
			'name'        => 'Giày MLB LA DODGERS Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '34',
			'name'        => 'MLB Bigball Chunky Check Boston B ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","38","39"]',
			'color'       => '["Trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '35',
			'name'        => 'Giày MLB Bigball Chunky Mono B Nâu ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["trắng nâu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '36',
			'name'        => 'Giày MLB NY Yankees Trắng Vàng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["trắng vàng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '37',
			'name'        => 'Giày MLB NY Yankees Đế Nâu ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["trắng nâu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '38',
			'name'        => 'MLB Chunky Liner NY Trắng Gót Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '39',
			'name'        => 'MLB Chunky Liner NY Trắng Gót Xanh  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '40',
			'name'        => 'Giày MLB Mustard NY Da Lộn Kem  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["kem nâu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '41',
			'name'        => 'Giày MLB Mustard NY Kem Chữ Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["đen kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '42',
			'name'        => 'Giày MLB Chunky NY Đen Trắng Cao Cổ  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '5',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '43',
			'name'        => 'Giày LV Trainer Trắng Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '6',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '44',
			'name'        => 'Giày LV Trainer Trắng Xanh Lá  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '6',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xanh lá"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '45',
			'name'        => 'Dép Alphabounce  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '8',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '46',
			'name'        => 'Dép Adidas Cloudfoam Like Auth',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '8',
			'size'        => '["40","41","42","43"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '47',
			'name'        => 'Dép Adidas Cloudfoam Đen Trắng Like Auth',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '8',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '48',
			'name'        => 'Dép Alphabounce FULL ĐEN  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '8',
			'size'        => '["39","40","41","42","43"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '49',
			'name'        => 'Nike Jordan 1 Low Xanh Dương ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38"]',
			'color'       => '["Xanh dương"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '50',
			'name'        => 'Nike Jordan 1 Low Thổ Cẩm ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["thổ cẩm"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '51',
			'name'        => 'Nike Jordan 1 Low Camo Xám Nhạt ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","38"]',
			'color'       => '["xám trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '52',
			'name'        => 'Nike Jordan 1 Low Xám Camo ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","38","39"]',
			'color'       => '["xám camo"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '53',
			'name'        => 'Nike Jordan 1 High Đen Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '54',
			'name'        => 'Nike Jordan 1 High Hồng Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Hồng trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '55',
			'name'        => 'Nike Jordan 1 Low Đen Trắng Xanh ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["XD trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '56',
			'name'        => 'Nike Jordan 1 High Xám Đen ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Đen xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '57',
			'name'        => 'Nike Jordan 1 Low Hồng Tím ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39"]',
			'color'       => '["hồng tím"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '58',
			'name'        => 'Nike Jordan 1 High Xám Trắng Xanh ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '59',
			'name'        => 'Nike Jordan 1 Low Xám Đế Xanh ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '60',
			'name'        => 'Nike Jordan 1 Low Hồng Đế Nâu ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39"]',
			'color'       => '["nâu hồng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '61',
			'name'        => 'Nike Jordan 1 Low Xanh Dương Vạch Đen ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Xanh dương"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '62',
			'name'        => 'Nike Jordan 1 High Hồng Đế Nâu ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Nâu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '63',
			'name'        => 'Nike Jordan 1 Low Đen Cam ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen cam"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '64',
			'name'        => 'Ni.ke Jordan 1 High Xanh Viền Bạc ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["37","38","39"]',
			'color'       => '["xám xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '65',
			'name'        => 'Ni.ke Jordan 1 Low Trắng Đen ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '66',
			'name'        => 'Nike Jordan 1 Low Xám ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '67',
			'name'        => 'Nike Jordan 1 Low Kem Xanh Dương Da Lộn ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Xanh dương"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '68',
			'name'        => 'Nike Air Jordan 1 Low Paris ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '69',
			'name'        => 'Nike Jordan 1 Low Trắng Kẻ Đen ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '70',
			'name'        => 'Nike Jordan 1 Low Xanh Than ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xanh than"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '71',
			'name'        => 'Nike Jordan 1 High Xám ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["39","40"]',
			'color'       => '["xám trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '72',
			'name'        => 'Nike Jordan 1 Low Đen Trắng Xanh Lá ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xanh lá"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '73',
			'name'        => 'Nike Jordan 1 Low Đen Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '74',
			'name'        => 'Nike Jordan 1 Low Full Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["full trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '75',
			'name'        => 'Nike Jordan 1 Low Xanh Lá  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xanh lá"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '76',
			'name'        => 'Nike Jordan 1 High Xanh Lá  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xanh lá"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '77',
			'name'        => 'Jordan 1 High Travis Scott Nâu Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["Nâu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '78',
			'name'        => 'Nike Air Jordan 4 Retro Off White Kem  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '79',
			'name'        => 'Nike Air Jordan 4 Retro Trắng Xám Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '9',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '80',
			'name'        => 'Nike Air Force 1 LV Trắng Nâu  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38"]',
			'color'       => '["trắng nâu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '81',
			'name'        => 'Nike Air Force 1 Shadow Kem Xanh Hồng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39"]',
			'color'       => '["kem xanh hồng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '82',
			'name'        => 'Nike Air Force 1 Shadow Full Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["full trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '83',
			'name'        => 'Nike Air Force 1 Shadow Xanh Ngọc  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xanh ngọc"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '84',
			'name'        => 'Nike Air Force 1 Shadow Kem Vàng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39"]',
			'color'       => '["kem vàng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '85',
			'name'        => 'Nike Air Force 1 Shadow Kim Cương  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39"]',
			'color'       => '["kim cương"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '86',
			'name'        => 'Nike Air Force 1 Full Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43","44"]',
			'color'       => '["Trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '87',
			'name'        => 'Nike Air Force 1 Lv 8 Xanh Hồng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39"]',
			'color'       => '["xanh hồng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '88',
			'name'        => 'Nike Air Force 1 Full Trắng Viền Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '89',
			'name'        => 'Nike Air Force 1 Trắng Viền Xanh  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '90',
			'name'        => 'Nike Air Force 1 NY Trắng Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '91',
			'name'        => 'Nike Air Force 1 ID Gucci Trắng Xanh Đỏ  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '92',
			'name'        => 'Nike Air Force 1 Da Lộn Xám Vải Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '93',
			'name'        => 'Nike Air Force 1 Da Lộn Kem Xám  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '94',
			'name'        => 'Nike Air Force 1 Trắng Đen Đế Nâu  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '95',
			'name'        => 'Nike Air Force 1 Hồng Đỏ  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Hồng trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '96',
			'name'        => 'Ni.ke Air Force 1 Xám Viền Xanh Da Rắn  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xám xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '97',
			'name'        => 'Nike Air Force 1 Xám Trắng Vệt Vàng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["xám vàng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '98',
			'name'        => 'Nike Air Force 1 Da Kem Navy  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '99',
			'name'        => 'Nike Air Force 1 Kem Nâu  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["kem nâu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '100',
			'name'        => 'Nike Air Force 1 Trắng Xanh Gót Nâu  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["xám cam"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '101',
			'name'        => 'Nike Air Force 1 Xám Kem Đế Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["xám kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '102',
			'name'        => 'Nike Air Force 1 Da Xám Vạch Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xám trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '103',
			'name'        => 'Nike Air Force 1 Da Lộn Xám Đen Đế Xám  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Đen xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '104',
			'name'        => 'Nike Air Force 1 Full Trắng Viền Vàng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["trắng vàng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '105',
			'name'        => 'Nike Air Force 1 Da Kem Xám  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["xám kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '106',
			'name'        => 'Nike Air Force 1 Cao Cổ Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["full trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '107',
			'name'        => 'Nike Air Force 1 Vải Kem Viền Thêu  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '108',
			'name'        => 'Nike Air Force 1 Vải Kem Navy  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '109',
			'name'        => 'Nike Air Force 1 Brooklyn NY Kem  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39","40","41","42","43","44"]',
			'color'       => '["Trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '110',
			'name'        => 'Nike Air Force 1 Da Lộn Xám Navy Cam  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["xám cam"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '111',
			'name'        => 'Nike Air Force 1 Xám Da Lộn ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '112',
			'name'        => 'Nike Air Force 1 Da Lộn Xám Vạch Kem  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '113',
			'name'        => 'Nike Air Force 1 LV Nâu Kem  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["trắng nâu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '114',
			'name'        => 'Nike Air Force 1 Athletic Club Da Lộn Xám Gót XD  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["xám xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '115',
			'name'        => 'Nike Air Force 1 Lv 8 EMB Trắng Xanh Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '116',
			'name'        => 'Nike Air Force 1 Travis Scott Da Lộn Xám Siêu Cấp',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '10',
			'size'        => '["40","41","42","43"]',
			'color'       => '["xám kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '117',
			'name'        => 'Nike Air Max 97 Ghi Xám ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '11',
			'size'        => '["36","37","38","39","40"]',
			'color'       => '["Ghi"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '118',
			'name'        => 'Nike Air Max 97 Full Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '11',
			'size'        => '["41"]',
			'color'       => '["full trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '119',
			'name'        => 'Nike Blazer Low Trắng Đen ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '12',
			'size'        => '["40","41","42","43"]',
			'color'       => '["thấp cổ"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '120',
			'name'        => 'Nike Blazer High Trắng Đen ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '12',
			'size'        => '["40"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '121',
			'name'        => 'Nike Zoom N979 Ghi Xanh SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '12',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["ghi xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '122',
			'name'        => 'Nike Zoom N979 Đen Ghi Đậm SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '12',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["Ghi"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '123',
			'name'        => 'Nike Zoom N979 Đen Ghi Nhạt SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '12',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["Ghi"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '124',
			'name'        => 'Giày M2K Trắng Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '12',
			'size'        => '["36","37","38","39","40","43"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '125',
			'name'        => 'Adidas Stan Smith Trắng Gót Lục  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '13',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng gót lục"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '126',
			'name'        => 'Adidas Stan Smith Trắng Gót Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '13',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng gót đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '127',
			'name'        => 'Adidas Stan Smith Trắng Gót Xanh Navy  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '13',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng gót navy"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '128',
			'name'        => 'Adidas EQ 21 Run Ghi Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '14',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Ghi"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '129',
			'name'        => 'Adidas EQ 21 Run Trắng Viền Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '14',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Trắng đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '130',
			'name'        => 'Adidas EQ 21 Run Đen Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '14',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '131',
			'name'        => 'Adidas EQ 21 Run Full Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '14',
			'size'        => '["40","41","42","43"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '132',
			'name'        => 'Adidas EQ 21 Run Trắng Viền Xanh Dương  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '14',
			'size'        => '["39","40","41","42","43"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '133',
			'name'        => 'Adidas AlphaMagma Trắng Đế Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '15',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '134',
			'name'        => 'Adidas AlphaMagma Ghi Đen ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '15',
			'size'        => '["40","41","42","43"]',
			'color'       => '["ghi xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '135',
			'name'        => 'Adidas AlphaMagma Đen Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '15',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '136',
			'name'        => 'Adidas Alphabounce New 22 Xám  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '15',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Đen xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '137',
			'name'        => 'Adidas Alphabounce New 22 Đen Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '15',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '138',
			'name'        => 'Adidas Alphabounce New 22 Đen Đế Xám  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '15',
			'size'        => '["40","41","42","43"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '139',
			'name'        => 'Adidas Ultra Boost Đen Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '16',
			'size'        => '["36","37","38","39","40","41","42","43","44"]',
			'color'       => '["Đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '140',
			'name'        => 'Adidas Ultra Boost Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '16',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["full trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '141',
			'name'        => 'Adidas Ultra Boost Hồng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '16',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Hồng trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '142',
			'name'        => 'Adidas Ultra Boost Ghi Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '16',
			'size'        => '["36","37","38","39","40","41","42","43","44"]',
			'color'       => '["ghi trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '143',
			'name'        => 'Adidas Ultra Boost 6.0 Trắng Xanh ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '16',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '144',
			'name'        => 'Adidas Ultra Boost 6.0 Xám Tím ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '16',
			'size'        => '["40","41","42","43"]',
			'color'       => '["ghi xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '145',
			'name'        => 'Adidas Ultra Boost 6.0 Hồng Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '16',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Hồng trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '146',
			'name'        => 'Adidas Ultra Boost 6.0 Đen Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '16',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '147',
			'name'        => 'Adidas Ultra Boost 6.0 Ghi Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '16',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["ghi trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '148',
			'name'        => 'Adidas Alphabounce Beyond Nâu Kem ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["40","43","44"]',
			'color'       => '["Nâu kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '149',
			'name'        => 'Adidas Alphabounce Beyond  Đen',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '150',
			'name'        => 'Adidas Alphabounce Beyond  Đen Chấm',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["40","41","42","43"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '151',
			'name'        => 'Adidas Alphabounce Beyond  Kem Tím',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["36","37","38","39"]',
			'color'       => '["kem tím"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '152',
			'name'        => 'Adidas Alphabounce Beyond  Full Trắng',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["full trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '153',
			'name'        => 'Adidas Alphabounce Beyond  Đen Kem',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["40","41","42","43"]',
			'color'       => '["đen kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '154',
			'name'        => 'Adidas Alphabounce Beyond Ghi Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["40","41","42","43"]',
			'color'       => '["ghi trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '155',
			'name'        => 'Adidas Alphabounce Instinct M Xám Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["xám trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '156',
			'name'        => 'Adidas Alphabounce Instinct M Đen Trắng  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["39","40","41","42","43"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '157',
			'name'        => 'Adidas Alphabounce Instinct M Trắng Navy Đỏ ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["40","41","42","43"]',
			'color'       => '["Trắng đỏ"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '158',
			'name'        => 'Adidas Alphabounce Instinct M Full Black  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["40","41","42","43"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '159',
			'name'        => 'Adidas Alphabounce Instinct M Xám Xanh Ngọc  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["40","41","42","43"]',
			'color'       => '["xanh ngọc"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '160',
			'name'        => 'Adidas Alphabounce Instinct M Xám Đỏ  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '17',
			'size'        => '["39","40","41","42","43"]',
			'color'       => '["xám đỏ"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '161',
			'name'        => 'Adidas A102 Xanh Rêu SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["xanh rêu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '162',
			'name'        => 'Adidas A102 Đen Trắng SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '163',
			'name'        => 'Adidas A102 Full Black SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '164',
			'name'        => 'Adidas A30 Full Black SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '165',
			'name'        => 'Adidas Tubular Shadow Xám Kem ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["xám kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '166',
			'name'        => 'Adidas 2195 Full Đen SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["41","43"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '167',
			'name'        => 'Adidas N977 Ghi Đỏ SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["Ghi"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '168',
			'name'        => 'Adidas N977 Full Black SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '169',
			'name'        => 'Adidas N977 Đen Xanh SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["đen xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '170',
			'name'        => 'Adidas N975 Ghi Xanh SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["ghi xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '171',
			'name'        => 'Adidas N975 Full Black SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '172',
			'name'        => 'Adidas 8681 Đen Kẻ Trắng SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '173',
			'name'        => 'Adidas 8681 Xanh Kẻ Trắng SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '174',
			'name'        => 'Adidas 8681 Ghi Kẻ Trắng SF',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["ghi xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '175',
			'name'        => 'Adidas A153 Full Black ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["full đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '176',
			'name'        => 'Adidas A153 Đen Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '177',
			'name'        => 'Adidas A153 Xám ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '18',
			'size'        => '["40","41","42","43","44"]',
			'color'       => '["Xám"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '178',
			'name'        => 'Adidas EQT Bask Adv Trắng Xanh ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '20',
			'size'        => '["36","38","39","40"]',
			'color'       => '["Trắng xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '179',
			'name'        => 'Adidas EQT Bask Adv Xanh Dương ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '20',
			'size'        => '["36","37"]',
			'color'       => '["Xanh dương"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '180',
			'name'        => 'Adidas EQT Bask Adv Hồng Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '20',
			'size'        => '["36","39"]',
			'color'       => '["Hồng trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '181',
			'name'        => 'Adidas EQT Bask Adv Đen Xanh ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '20',
			'size'        => '["36","37","38","39","40"]',
			'color'       => '["đen xanh"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '182',
			'name'        => 'Adidas EQT Bask Adv Ghi ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '20',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["ghi trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '183',
			'name'        => 'Adidas EQT Bask Adv Đen Trắng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '20',
			'size'        => '["36","37","40","41","42"]',
			'color'       => '["Đen trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '184',
			'name'        => 'Adidas Superstar Trắng Kẻ Đen Mũi Sò ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '21',
			'size'        => '["39","40","43","44"]',
			'color'       => '["trắng kẻ đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '185',
			'name'        => 'Adidas Superstar Trắng Kẻ Đen Mũi Sò  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '21',
			'size'        => '["36","37","38","39","40","41","42","43","44"]',
			'color'       => '["trắng kẻ đen"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '186',
			'name'        => 'Adidas Superstar Cappuccino Kem Mũi Sò  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '21',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Kem"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '187',
			'name'        => 'Adidas Superstar Hồng Kem Mũi Sò  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '21',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Hồng trắng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '188',
			'name'        => 'Adidas Prophere Rêu Gót Cam ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '22',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Rêu"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '189',
			'name'        => 'Adidas Prophere Nâu Gót Hồng ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '22',
			'size'        => '["36","37","38","39"]',
			'color'       => '["nâu hồng"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '190',
			'name'        => 'Adidas Prophere Xanh Dương ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '22',
			'size'        => '["36","38","40","42","43"]',
			'color'       => '["Xanh dương"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '191',
			'name'        => 'Adidas Prophere Xám Chấm Cam ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '22',
			'size'        => '["36","38","39","40"]',
			'color'       => '["xám chấm cam"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '192',
			'name'        => 'Adidas Prophere Ghi Gót Cam ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '22',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Ghi cam"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '193',
			'name'        => 'Adidas Prophere Trắng Xám Cam ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '22',
			'size'        => '["36","37","38","39"]',
			'color'       => '["Trắng cam"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '194',
			'name'        => 'Balenciaga Triple S Đen Trắng Đỏ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '24',
			'size'        => '["37"]',
			'color'       => '["Đen trắng đỏ"]',
		]);
		$this->insert('{{%product}}', [
			'id'          => '195',
			'name'        => 'Alexander McQueen Da Trắng Gót Đen  ',
			'condition'   => '1',
			'short_desc'  => 'crawling',
			'long_desc'   => 'Chất liệu cao cấp, bền đẹp theo thời gian. Thiết kế thời trang. Kiểu dáng phong cách. Độ bền cao. Dễ phối đồ. ',
			'category_id' => '25',
			'size'        => '["36","37","38","39","40","41","42","43"]',
			'color'       => '["Trắng"]',
		]);
	}

	public function safeDown() {
		$this->dropTable('{{%product}}');
	}
}

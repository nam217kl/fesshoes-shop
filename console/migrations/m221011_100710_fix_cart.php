<?php

use yii\db\Migration;

/**
 * Class m221011_100710_fix_cart
 */
class m221011_100710_fix_cart extends Migration {

	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->execute('ALTER TABLE `cart` ADD FOREIGN KEY (`product_detail_id`) REFERENCES `product_detail`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
		$this->execute('ALTER TABLE `cart` ADD FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		echo "m221011_100710_fix_cart cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m221011_100710_fix_cart cannot be reverted.\n";

		return false;
	}
	*/
}

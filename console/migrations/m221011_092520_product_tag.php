<?php

use yii\db\Migration;
use yii\db\Schema;

class m221011_092520_product_tag extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%product_tag}}', [
			'id'         => Schema::TYPE_PK . '',
			'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
			'tag_id'     => Schema::TYPE_INTEGER . ' NOT NULL',
		], $tableOptions);
		$this->createIndex('product_id', '{{%product_tag}}', 'product_id', 0);
		$this->createIndex('tag_id', '{{%product_tag}}', 'tag_id', 0);
		$this->insert('{{%product_tag}}', [
			'id'         => '1',
			'product_id' => '1',
			'tag_id'     => '2',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '2',
			'product_id' => '1',
			'tag_id'     => '3',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '3',
			'product_id' => '1',
			'tag_id'     => '4',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '4',
			'product_id' => '1',
			'tag_id'     => '5',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '5',
			'product_id' => '1',
			'tag_id'     => '6',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '6',
			'product_id' => '1',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '7',
			'product_id' => '1',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '8',
			'product_id' => '2',
			'tag_id'     => '2',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '9',
			'product_id' => '2',
			'tag_id'     => '9',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '10',
			'product_id' => '2',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '11',
			'product_id' => '2',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '12',
			'product_id' => '3',
			'tag_id'     => '2',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '13',
			'product_id' => '3',
			'tag_id'     => '3',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '14',
			'product_id' => '3',
			'tag_id'     => '10',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '15',
			'product_id' => '3',
			'tag_id'     => '11',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '16',
			'product_id' => '3',
			'tag_id'     => '12',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '17',
			'product_id' => '3',
			'tag_id'     => '13',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '18',
			'product_id' => '3',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '19',
			'product_id' => '3',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '20',
			'product_id' => '4',
			'tag_id'     => '2',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '21',
			'product_id' => '4',
			'tag_id'     => '3',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '22',
			'product_id' => '4',
			'tag_id'     => '14',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '23',
			'product_id' => '4',
			'tag_id'     => '15',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '24',
			'product_id' => '4',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '25',
			'product_id' => '4',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '26',
			'product_id' => '5',
			'tag_id'     => '2',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '27',
			'product_id' => '5',
			'tag_id'     => '3',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '28',
			'product_id' => '5',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '29',
			'product_id' => '5',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '30',
			'product_id' => '5',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '31',
			'product_id' => '5',
			'tag_id'     => '18',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '32',
			'product_id' => '5',
			'tag_id'     => '19',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '33',
			'product_id' => '6',
			'tag_id'     => '2',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '34',
			'product_id' => '6',
			'tag_id'     => '3',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '35',
			'product_id' => '6',
			'tag_id'     => '14',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '36',
			'product_id' => '6',
			'tag_id'     => '15',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '37',
			'product_id' => '6',
			'tag_id'     => '20',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '38',
			'product_id' => '6',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '39',
			'product_id' => '6',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '40',
			'product_id' => '6',
			'tag_id'     => '19',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '41',
			'product_id' => '7',
			'tag_id'     => '2',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '42',
			'product_id' => '7',
			'tag_id'     => '3',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '43',
			'product_id' => '7',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '44',
			'product_id' => '7',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '45',
			'product_id' => '7',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '46',
			'product_id' => '7',
			'tag_id'     => '18',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '47',
			'product_id' => '7',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '48',
			'product_id' => '7',
			'tag_id'     => '22',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '49',
			'product_id' => '8',
			'tag_id'     => '23',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '50',
			'product_id' => '8',
			'tag_id'     => '24',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '51',
			'product_id' => '8',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '52',
			'product_id' => '8',
			'tag_id'     => '25',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '53',
			'product_id' => '8',
			'tag_id'     => '26',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '54',
			'product_id' => '9',
			'tag_id'     => '23',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '55',
			'product_id' => '9',
			'tag_id'     => '24',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '56',
			'product_id' => '9',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '57',
			'product_id' => '9',
			'tag_id'     => '27',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '58',
			'product_id' => '9',
			'tag_id'     => '26',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '59',
			'product_id' => '10',
			'tag_id'     => '23',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '60',
			'product_id' => '10',
			'tag_id'     => '28',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '61',
			'product_id' => '10',
			'tag_id'     => '29',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '62',
			'product_id' => '10',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '63',
			'product_id' => '10',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '64',
			'product_id' => '10',
			'tag_id'     => '25',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '65',
			'product_id' => '10',
			'tag_id'     => '26',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '66',
			'product_id' => '11',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '67',
			'product_id' => '11',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '68',
			'product_id' => '11',
			'tag_id'     => '32',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '69',
			'product_id' => '11',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '70',
			'product_id' => '11',
			'tag_id'     => '33',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '71',
			'product_id' => '11',
			'tag_id'     => '34',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '72',
			'product_id' => '12',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '73',
			'product_id' => '12',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '74',
			'product_id' => '12',
			'tag_id'     => '32',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '75',
			'product_id' => '12',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '76',
			'product_id' => '12',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '77',
			'product_id' => '12',
			'tag_id'     => '33',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '78',
			'product_id' => '12',
			'tag_id'     => '34',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '79',
			'product_id' => '13',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '80',
			'product_id' => '13',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '81',
			'product_id' => '13',
			'tag_id'     => '35',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '82',
			'product_id' => '13',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '83',
			'product_id' => '14',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '84',
			'product_id' => '14',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '85',
			'product_id' => '14',
			'tag_id'     => '35',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '86',
			'product_id' => '14',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '87',
			'product_id' => '14',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '88',
			'product_id' => '15',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '89',
			'product_id' => '15',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '90',
			'product_id' => '15',
			'tag_id'     => '35',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '91',
			'product_id' => '15',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '92',
			'product_id' => '15',
			'tag_id'     => '37',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '93',
			'product_id' => '16',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '94',
			'product_id' => '16',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '95',
			'product_id' => '16',
			'tag_id'     => '32',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '96',
			'product_id' => '16',
			'tag_id'     => '38',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '97',
			'product_id' => '16',
			'tag_id'     => '39',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '98',
			'product_id' => '16',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '99',
			'product_id' => '16',
			'tag_id'     => '19',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '100',
			'product_id' => '17',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '101',
			'product_id' => '17',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '102',
			'product_id' => '17',
			'tag_id'     => '35',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '103',
			'product_id' => '17',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '104',
			'product_id' => '17',
			'tag_id'     => '41',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '105',
			'product_id' => '18',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '106',
			'product_id' => '18',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '107',
			'product_id' => '18',
			'tag_id'     => '32',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '108',
			'product_id' => '18',
			'tag_id'     => '38',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '109',
			'product_id' => '18',
			'tag_id'     => '39',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '110',
			'product_id' => '18',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '111',
			'product_id' => '18',
			'tag_id'     => '42',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '112',
			'product_id' => '19',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '113',
			'product_id' => '19',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '114',
			'product_id' => '19',
			'tag_id'     => '35',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '115',
			'product_id' => '19',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '116',
			'product_id' => '20',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '117',
			'product_id' => '20',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '118',
			'product_id' => '20',
			'tag_id'     => '35',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '119',
			'product_id' => '20',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '120',
			'product_id' => '20',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '121',
			'product_id' => '20',
			'tag_id'     => '41',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '122',
			'product_id' => '21',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '123',
			'product_id' => '21',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '124',
			'product_id' => '21',
			'tag_id'     => '44',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '125',
			'product_id' => '21',
			'tag_id'     => '45',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '126',
			'product_id' => '21',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '127',
			'product_id' => '21',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '128',
			'product_id' => '22',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '129',
			'product_id' => '22',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '130',
			'product_id' => '22',
			'tag_id'     => '44',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '131',
			'product_id' => '22',
			'tag_id'     => '45',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '132',
			'product_id' => '22',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '133',
			'product_id' => '22',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '134',
			'product_id' => '23',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '135',
			'product_id' => '23',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '136',
			'product_id' => '23',
			'tag_id'     => '44',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '137',
			'product_id' => '23',
			'tag_id'     => '45',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '138',
			'product_id' => '23',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '139',
			'product_id' => '23',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '140',
			'product_id' => '24',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '141',
			'product_id' => '24',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '142',
			'product_id' => '24',
			'tag_id'     => '35',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '143',
			'product_id' => '24',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '144',
			'product_id' => '24',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '145',
			'product_id' => '25',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '146',
			'product_id' => '25',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '147',
			'product_id' => '25',
			'tag_id'     => '47',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '148',
			'product_id' => '25',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '149',
			'product_id' => '25',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '150',
			'product_id' => '26',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '151',
			'product_id' => '26',
			'tag_id'     => '31',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '152',
			'product_id' => '26',
			'tag_id'     => '47',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '153',
			'product_id' => '26',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '154',
			'product_id' => '26',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '155',
			'product_id' => '26',
			'tag_id'     => '48',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '156',
			'product_id' => '27',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '157',
			'product_id' => '27',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '158',
			'product_id' => '27',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '159',
			'product_id' => '27',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '160',
			'product_id' => '27',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '161',
			'product_id' => '28',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '162',
			'product_id' => '28',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '163',
			'product_id' => '28',
			'tag_id'     => '52',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '164',
			'product_id' => '28',
			'tag_id'     => '53',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '165',
			'product_id' => '28',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '166',
			'product_id' => '28',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '167',
			'product_id' => '28',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '168',
			'product_id' => '28',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '169',
			'product_id' => '28',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '170',
			'product_id' => '29',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '171',
			'product_id' => '29',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '172',
			'product_id' => '29',
			'tag_id'     => '52',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '173',
			'product_id' => '29',
			'tag_id'     => '53',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '174',
			'product_id' => '29',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '175',
			'product_id' => '29',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '176',
			'product_id' => '29',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '177',
			'product_id' => '29',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '178',
			'product_id' => '29',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '179',
			'product_id' => '30',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '180',
			'product_id' => '30',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '181',
			'product_id' => '30',
			'tag_id'     => '52',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '182',
			'product_id' => '30',
			'tag_id'     => '54',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '183',
			'product_id' => '30',
			'tag_id'     => '55',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '184',
			'product_id' => '30',
			'tag_id'     => '41',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '185',
			'product_id' => '31',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '186',
			'product_id' => '31',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '187',
			'product_id' => '31',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '188',
			'product_id' => '31',
			'tag_id'     => '56',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '189',
			'product_id' => '31',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '190',
			'product_id' => '31',
			'tag_id'     => '57',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '191',
			'product_id' => '32',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '192',
			'product_id' => '32',
			'tag_id'     => '58',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '193',
			'product_id' => '32',
			'tag_id'     => '52',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '194',
			'product_id' => '32',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '195',
			'product_id' => '32',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '196',
			'product_id' => '32',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '197',
			'product_id' => '33',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '198',
			'product_id' => '33',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '199',
			'product_id' => '33',
			'tag_id'     => '22',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '200',
			'product_id' => '33',
			'tag_id'     => '59',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '201',
			'product_id' => '33',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '202',
			'product_id' => '34',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '203',
			'product_id' => '34',
			'tag_id'     => '58',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '204',
			'product_id' => '34',
			'tag_id'     => '52',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '205',
			'product_id' => '34',
			'tag_id'     => '60',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '206',
			'product_id' => '34',
			'tag_id'     => '55',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '207',
			'product_id' => '34',
			'tag_id'     => '61',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '208',
			'product_id' => '35',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '209',
			'product_id' => '35',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '210',
			'product_id' => '35',
			'tag_id'     => '58',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '211',
			'product_id' => '35',
			'tag_id'     => '52',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '212',
			'product_id' => '35',
			'tag_id'     => '62',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '213',
			'product_id' => '35',
			'tag_id'     => '61',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '214',
			'product_id' => '35',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '215',
			'product_id' => '36',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '216',
			'product_id' => '36',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '217',
			'product_id' => '36',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '218',
			'product_id' => '36',
			'tag_id'     => '56',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '219',
			'product_id' => '36',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '220',
			'product_id' => '36',
			'tag_id'     => '42',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '221',
			'product_id' => '37',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '222',
			'product_id' => '37',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '223',
			'product_id' => '37',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '224',
			'product_id' => '37',
			'tag_id'     => '56',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '225',
			'product_id' => '37',
			'tag_id'     => '64',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '226',
			'product_id' => '37',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '227',
			'product_id' => '38',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '228',
			'product_id' => '38',
			'tag_id'     => '52',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '229',
			'product_id' => '38',
			'tag_id'     => '65',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '230',
			'product_id' => '38',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '231',
			'product_id' => '38',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '232',
			'product_id' => '38',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '233',
			'product_id' => '38',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '234',
			'product_id' => '39',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '235',
			'product_id' => '39',
			'tag_id'     => '52',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '236',
			'product_id' => '39',
			'tag_id'     => '65',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '237',
			'product_id' => '39',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '238',
			'product_id' => '39',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '239',
			'product_id' => '39',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '240',
			'product_id' => '39',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '241',
			'product_id' => '40',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '242',
			'product_id' => '40',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '243',
			'product_id' => '40',
			'tag_id'     => '66',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '244',
			'product_id' => '40',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '245',
			'product_id' => '40',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '246',
			'product_id' => '40',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '247',
			'product_id' => '40',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '248',
			'product_id' => '41',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '249',
			'product_id' => '41',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '250',
			'product_id' => '41',
			'tag_id'     => '66',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '251',
			'product_id' => '41',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '252',
			'product_id' => '41',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '253',
			'product_id' => '41',
			'tag_id'     => '67',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '254',
			'product_id' => '41',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '255',
			'product_id' => '42',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '256',
			'product_id' => '42',
			'tag_id'     => '50',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '257',
			'product_id' => '42',
			'tag_id'     => '52',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '258',
			'product_id' => '42',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '259',
			'product_id' => '42',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '260',
			'product_id' => '42',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '261',
			'product_id' => '42',
			'tag_id'     => '25',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '262',
			'product_id' => '42',
			'tag_id'     => '26',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '263',
			'product_id' => '43',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '264',
			'product_id' => '43',
			'tag_id'     => '68',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '265',
			'product_id' => '43',
			'tag_id'     => '69',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '266',
			'product_id' => '43',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '267',
			'product_id' => '43',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '268',
			'product_id' => '44',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '269',
			'product_id' => '44',
			'tag_id'     => '68',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '270',
			'product_id' => '44',
			'tag_id'     => '69',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '271',
			'product_id' => '44',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '272',
			'product_id' => '44',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '273',
			'product_id' => '44',
			'tag_id'     => '22',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '274',
			'product_id' => '45',
			'tag_id'     => '70',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '275',
			'product_id' => '45',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '276',
			'product_id' => '46',
			'tag_id'     => '70',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '277',
			'product_id' => '46',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '278',
			'product_id' => '46',
			'tag_id'     => '73',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '279',
			'product_id' => '46',
			'tag_id'     => '74',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '280',
			'product_id' => '46',
			'tag_id'     => '75',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '281',
			'product_id' => '47',
			'tag_id'     => '70',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '282',
			'product_id' => '47',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '283',
			'product_id' => '47',
			'tag_id'     => '73',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '284',
			'product_id' => '47',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '285',
			'product_id' => '47',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '286',
			'product_id' => '47',
			'tag_id'     => '74',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '287',
			'product_id' => '47',
			'tag_id'     => '75',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '288',
			'product_id' => '48',
			'tag_id'     => '70',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '289',
			'product_id' => '48',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '290',
			'product_id' => '48',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '291',
			'product_id' => '48',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '292',
			'product_id' => '49',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '293',
			'product_id' => '49',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '294',
			'product_id' => '49',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '295',
			'product_id' => '49',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '296',
			'product_id' => '49',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '297',
			'product_id' => '49',
			'tag_id'     => '37',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '298',
			'product_id' => '50',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '299',
			'product_id' => '50',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '300',
			'product_id' => '50',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '301',
			'product_id' => '50',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '302',
			'product_id' => '50',
			'tag_id'     => '81',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '303',
			'product_id' => '50',
			'tag_id'     => '82',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '304',
			'product_id' => '51',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '305',
			'product_id' => '51',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '306',
			'product_id' => '51',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '307',
			'product_id' => '51',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '308',
			'product_id' => '51',
			'tag_id'     => '83',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '309',
			'product_id' => '51',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '310',
			'product_id' => '51',
			'tag_id'     => '84',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '311',
			'product_id' => '52',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '312',
			'product_id' => '52',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '313',
			'product_id' => '52',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '314',
			'product_id' => '52',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '315',
			'product_id' => '52',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '316',
			'product_id' => '52',
			'tag_id'     => '83',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '317',
			'product_id' => '53',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '318',
			'product_id' => '53',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '319',
			'product_id' => '53',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '320',
			'product_id' => '53',
			'tag_id'     => '85',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '321',
			'product_id' => '53',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '322',
			'product_id' => '53',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '323',
			'product_id' => '54',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '324',
			'product_id' => '54',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '325',
			'product_id' => '54',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '326',
			'product_id' => '54',
			'tag_id'     => '85',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '327',
			'product_id' => '54',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '328',
			'product_id' => '54',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '329',
			'product_id' => '55',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '330',
			'product_id' => '55',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '331',
			'product_id' => '55',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '332',
			'product_id' => '55',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '333',
			'product_id' => '55',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '334',
			'product_id' => '55',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '335',
			'product_id' => '55',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '336',
			'product_id' => '56',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '337',
			'product_id' => '56',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '338',
			'product_id' => '56',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '339',
			'product_id' => '56',
			'tag_id'     => '85',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '340',
			'product_id' => '56',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '341',
			'product_id' => '56',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '342',
			'product_id' => '57',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '343',
			'product_id' => '57',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '344',
			'product_id' => '57',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '345',
			'product_id' => '57',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '346',
			'product_id' => '57',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '347',
			'product_id' => '57',
			'tag_id'     => '86',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '348',
			'product_id' => '58',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '349',
			'product_id' => '58',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '350',
			'product_id' => '58',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '351',
			'product_id' => '58',
			'tag_id'     => '85',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '352',
			'product_id' => '58',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '353',
			'product_id' => '58',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '354',
			'product_id' => '58',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '355',
			'product_id' => '59',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '356',
			'product_id' => '59',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '357',
			'product_id' => '59',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '358',
			'product_id' => '59',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '359',
			'product_id' => '59',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '360',
			'product_id' => '59',
			'tag_id'     => '64',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '361',
			'product_id' => '59',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '362',
			'product_id' => '60',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '363',
			'product_id' => '60',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '364',
			'product_id' => '60',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '365',
			'product_id' => '60',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '366',
			'product_id' => '60',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '367',
			'product_id' => '60',
			'tag_id'     => '64',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '368',
			'product_id' => '60',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '369',
			'product_id' => '61',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '370',
			'product_id' => '61',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '371',
			'product_id' => '61',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '372',
			'product_id' => '61',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '373',
			'product_id' => '61',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '374',
			'product_id' => '61',
			'tag_id'     => '37',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '375',
			'product_id' => '61',
			'tag_id'     => '87',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '376',
			'product_id' => '61',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '377',
			'product_id' => '62',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '378',
			'product_id' => '62',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '379',
			'product_id' => '62',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '380',
			'product_id' => '62',
			'tag_id'     => '85',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '381',
			'product_id' => '62',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '382',
			'product_id' => '62',
			'tag_id'     => '64',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '383',
			'product_id' => '62',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '384',
			'product_id' => '63',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '385',
			'product_id' => '63',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '386',
			'product_id' => '63',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '387',
			'product_id' => '63',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '388',
			'product_id' => '63',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '389',
			'product_id' => '63',
			'tag_id'     => '82',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '390',
			'product_id' => '64',
			'tag_id'     => '88',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '391',
			'product_id' => '64',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '392',
			'product_id' => '64',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '393',
			'product_id' => '64',
			'tag_id'     => '85',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '394',
			'product_id' => '64',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '395',
			'product_id' => '64',
			'tag_id'     => '18',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '396',
			'product_id' => '64',
			'tag_id'     => '89',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '397',
			'product_id' => '65',
			'tag_id'     => '88',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '398',
			'product_id' => '65',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '399',
			'product_id' => '65',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '400',
			'product_id' => '65',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '401',
			'product_id' => '65',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '402',
			'product_id' => '65',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '403',
			'product_id' => '66',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '404',
			'product_id' => '66',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '405',
			'product_id' => '66',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '406',
			'product_id' => '66',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '407',
			'product_id' => '66',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '408',
			'product_id' => '67',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '409',
			'product_id' => '67',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '410',
			'product_id' => '67',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '411',
			'product_id' => '67',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '412',
			'product_id' => '67',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '413',
			'product_id' => '67',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '414',
			'product_id' => '67',
			'tag_id'     => '37',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '415',
			'product_id' => '67',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '416',
			'product_id' => '67',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '417',
			'product_id' => '68',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '418',
			'product_id' => '68',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '419',
			'product_id' => '68',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '420',
			'product_id' => '68',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '421',
			'product_id' => '68',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '422',
			'product_id' => '68',
			'tag_id'     => '91',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '423',
			'product_id' => '69',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '424',
			'product_id' => '69',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '425',
			'product_id' => '69',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '426',
			'product_id' => '69',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '427',
			'product_id' => '69',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '428',
			'product_id' => '69',
			'tag_id'     => '92',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '429',
			'product_id' => '69',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '430',
			'product_id' => '70',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '431',
			'product_id' => '70',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '432',
			'product_id' => '70',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '433',
			'product_id' => '70',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '434',
			'product_id' => '70',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '435',
			'product_id' => '70',
			'tag_id'     => '48',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '436',
			'product_id' => '71',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '437',
			'product_id' => '71',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '438',
			'product_id' => '71',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '439',
			'product_id' => '71',
			'tag_id'     => '85',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '440',
			'product_id' => '71',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '441',
			'product_id' => '72',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '442',
			'product_id' => '72',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '443',
			'product_id' => '72',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '444',
			'product_id' => '72',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '445',
			'product_id' => '72',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '446',
			'product_id' => '72',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '447',
			'product_id' => '72',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '448',
			'product_id' => '72',
			'tag_id'     => '22',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '449',
			'product_id' => '73',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '450',
			'product_id' => '73',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '451',
			'product_id' => '73',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '452',
			'product_id' => '73',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '453',
			'product_id' => '73',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '454',
			'product_id' => '73',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '455',
			'product_id' => '74',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '456',
			'product_id' => '74',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '457',
			'product_id' => '74',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '458',
			'product_id' => '74',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '459',
			'product_id' => '74',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '460',
			'product_id' => '74',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '461',
			'product_id' => '75',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '462',
			'product_id' => '75',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '463',
			'product_id' => '75',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '464',
			'product_id' => '75',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '465',
			'product_id' => '75',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '466',
			'product_id' => '75',
			'tag_id'     => '22',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '467',
			'product_id' => '76',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '468',
			'product_id' => '76',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '469',
			'product_id' => '76',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '470',
			'product_id' => '76',
			'tag_id'     => '85',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '471',
			'product_id' => '76',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '472',
			'product_id' => '76',
			'tag_id'     => '22',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '473',
			'product_id' => '77',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '474',
			'product_id' => '77',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '475',
			'product_id' => '77',
			'tag_id'     => '85',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '476',
			'product_id' => '77',
			'tag_id'     => '93',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '477',
			'product_id' => '77',
			'tag_id'     => '94',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '478',
			'product_id' => '77',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '479',
			'product_id' => '77',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '480',
			'product_id' => '78',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '481',
			'product_id' => '78',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '482',
			'product_id' => '78',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '483',
			'product_id' => '78',
			'tag_id'     => '95',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '484',
			'product_id' => '78',
			'tag_id'     => '96',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '485',
			'product_id' => '78',
			'tag_id'     => '97',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '486',
			'product_id' => '78',
			'tag_id'     => '98',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '487',
			'product_id' => '78',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '488',
			'product_id' => '79',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '489',
			'product_id' => '79',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '490',
			'product_id' => '79',
			'tag_id'     => '78',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '491',
			'product_id' => '79',
			'tag_id'     => '95',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '492',
			'product_id' => '79',
			'tag_id'     => '96',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '493',
			'product_id' => '79',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '494',
			'product_id' => '79',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '495',
			'product_id' => '79',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '496',
			'product_id' => '80',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '497',
			'product_id' => '80',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '498',
			'product_id' => '80',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '499',
			'product_id' => '80',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '500',
			'product_id' => '80',
			'tag_id'     => '68',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '501',
			'product_id' => '80',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '502',
			'product_id' => '80',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '503',
			'product_id' => '81',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '504',
			'product_id' => '81',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '505',
			'product_id' => '81',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '506',
			'product_id' => '81',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '507',
			'product_id' => '81',
			'tag_id'     => '100',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '508',
			'product_id' => '81',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '509',
			'product_id' => '81',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '510',
			'product_id' => '81',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '511',
			'product_id' => '82',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '512',
			'product_id' => '82',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '513',
			'product_id' => '82',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '514',
			'product_id' => '82',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '515',
			'product_id' => '82',
			'tag_id'     => '100',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '516',
			'product_id' => '82',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '517',
			'product_id' => '82',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '518',
			'product_id' => '83',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '519',
			'product_id' => '83',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '520',
			'product_id' => '83',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '521',
			'product_id' => '83',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '522',
			'product_id' => '83',
			'tag_id'     => '100',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '523',
			'product_id' => '83',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '524',
			'product_id' => '83',
			'tag_id'     => '101',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '525',
			'product_id' => '84',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '526',
			'product_id' => '84',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '527',
			'product_id' => '84',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '528',
			'product_id' => '84',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '529',
			'product_id' => '84',
			'tag_id'     => '100',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '530',
			'product_id' => '84',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '531',
			'product_id' => '84',
			'tag_id'     => '42',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '532',
			'product_id' => '85',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '533',
			'product_id' => '85',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '534',
			'product_id' => '85',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '535',
			'product_id' => '85',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '536',
			'product_id' => '85',
			'tag_id'     => '100',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '537',
			'product_id' => '85',
			'tag_id'     => '102',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '538',
			'product_id' => '85',
			'tag_id'     => '103',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '539',
			'product_id' => '86',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '540',
			'product_id' => '86',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '541',
			'product_id' => '86',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '542',
			'product_id' => '86',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '543',
			'product_id' => '86',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '544',
			'product_id' => '86',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '545',
			'product_id' => '87',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '546',
			'product_id' => '87',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '547',
			'product_id' => '87',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '548',
			'product_id' => '87',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '549',
			'product_id' => '87',
			'tag_id'     => '68',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '550',
			'product_id' => '87',
			'tag_id'     => '104',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '551',
			'product_id' => '87',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '552',
			'product_id' => '87',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '553',
			'product_id' => '88',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '554',
			'product_id' => '88',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '555',
			'product_id' => '88',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '556',
			'product_id' => '88',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '557',
			'product_id' => '88',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '558',
			'product_id' => '88',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '559',
			'product_id' => '88',
			'tag_id'     => '18',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '560',
			'product_id' => '88',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '561',
			'product_id' => '89',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '562',
			'product_id' => '89',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '563',
			'product_id' => '89',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '564',
			'product_id' => '89',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '565',
			'product_id' => '89',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '566',
			'product_id' => '89',
			'tag_id'     => '18',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '567',
			'product_id' => '89',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '568',
			'product_id' => '90',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '569',
			'product_id' => '90',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '570',
			'product_id' => '90',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '571',
			'product_id' => '90',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '572',
			'product_id' => '90',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '573',
			'product_id' => '90',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '574',
			'product_id' => '90',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '575',
			'product_id' => '91',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '576',
			'product_id' => '91',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '577',
			'product_id' => '91',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '578',
			'product_id' => '91',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '579',
			'product_id' => '91',
			'tag_id'     => '105',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '580',
			'product_id' => '91',
			'tag_id'     => '106',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '581',
			'product_id' => '91',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '582',
			'product_id' => '91',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '583',
			'product_id' => '91',
			'tag_id'     => '41',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '584',
			'product_id' => '92',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '585',
			'product_id' => '92',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '586',
			'product_id' => '92',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '587',
			'product_id' => '92',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '588',
			'product_id' => '92',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '589',
			'product_id' => '92',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '590',
			'product_id' => '92',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '591',
			'product_id' => '92',
			'tag_id'     => '20',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '592',
			'product_id' => '92',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '593',
			'product_id' => '93',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '594',
			'product_id' => '93',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '595',
			'product_id' => '93',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '596',
			'product_id' => '93',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '597',
			'product_id' => '93',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '598',
			'product_id' => '93',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '599',
			'product_id' => '93',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '600',
			'product_id' => '93',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '601',
			'product_id' => '94',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '602',
			'product_id' => '94',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '603',
			'product_id' => '94',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '604',
			'product_id' => '94',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '605',
			'product_id' => '94',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '606',
			'product_id' => '94',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '607',
			'product_id' => '94',
			'tag_id'     => '64',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '608',
			'product_id' => '94',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '609',
			'product_id' => '95',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '610',
			'product_id' => '95',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '611',
			'product_id' => '95',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '612',
			'product_id' => '95',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '613',
			'product_id' => '95',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '614',
			'product_id' => '95',
			'tag_id'     => '41',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '615',
			'product_id' => '96',
			'tag_id'     => '88',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '616',
			'product_id' => '96',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '617',
			'product_id' => '96',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '618',
			'product_id' => '96',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '619',
			'product_id' => '96',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '620',
			'product_id' => '96',
			'tag_id'     => '18',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '621',
			'product_id' => '96',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '622',
			'product_id' => '96',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '623',
			'product_id' => '96',
			'tag_id'     => '107',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '624',
			'product_id' => '97',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '625',
			'product_id' => '97',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '626',
			'product_id' => '97',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '627',
			'product_id' => '97',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '628',
			'product_id' => '97',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '629',
			'product_id' => '97',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '630',
			'product_id' => '97',
			'tag_id'     => '108',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '631',
			'product_id' => '97',
			'tag_id'     => '42',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '632',
			'product_id' => '98',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '633',
			'product_id' => '98',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '634',
			'product_id' => '98',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '635',
			'product_id' => '98',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '636',
			'product_id' => '98',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '637',
			'product_id' => '98',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '638',
			'product_id' => '98',
			'tag_id'     => '19',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '639',
			'product_id' => '99',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '640',
			'product_id' => '99',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '641',
			'product_id' => '99',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '642',
			'product_id' => '99',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '643',
			'product_id' => '99',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '644',
			'product_id' => '99',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '645',
			'product_id' => '100',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '646',
			'product_id' => '100',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '647',
			'product_id' => '100',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '648',
			'product_id' => '100',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '649',
			'product_id' => '100',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '650',
			'product_id' => '100',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '651',
			'product_id' => '100',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '652',
			'product_id' => '100',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '653',
			'product_id' => '101',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '654',
			'product_id' => '101',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '655',
			'product_id' => '101',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '656',
			'product_id' => '101',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '657',
			'product_id' => '101',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '658',
			'product_id' => '101',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '659',
			'product_id' => '101',
			'tag_id'     => '64',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '660',
			'product_id' => '101',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '661',
			'product_id' => '102',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '662',
			'product_id' => '102',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '663',
			'product_id' => '102',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '664',
			'product_id' => '102',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '665',
			'product_id' => '102',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '666',
			'product_id' => '102',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '667',
			'product_id' => '102',
			'tag_id'     => '87',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '668',
			'product_id' => '102',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '669',
			'product_id' => '103',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '670',
			'product_id' => '103',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '671',
			'product_id' => '103',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '672',
			'product_id' => '103',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '673',
			'product_id' => '103',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '674',
			'product_id' => '103',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '675',
			'product_id' => '103',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '676',
			'product_id' => '103',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '677',
			'product_id' => '103',
			'tag_id'     => '64',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '678',
			'product_id' => '104',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '679',
			'product_id' => '104',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '680',
			'product_id' => '104',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '681',
			'product_id' => '104',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '682',
			'product_id' => '104',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '683',
			'product_id' => '104',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '684',
			'product_id' => '104',
			'tag_id'     => '18',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '685',
			'product_id' => '104',
			'tag_id'     => '42',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '686',
			'product_id' => '105',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '687',
			'product_id' => '105',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '688',
			'product_id' => '105',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '689',
			'product_id' => '105',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '690',
			'product_id' => '105',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '691',
			'product_id' => '105',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '692',
			'product_id' => '105',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '693',
			'product_id' => '106',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '694',
			'product_id' => '106',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '695',
			'product_id' => '106',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '696',
			'product_id' => '106',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '697',
			'product_id' => '106',
			'tag_id'     => '25',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '698',
			'product_id' => '106',
			'tag_id'     => '26',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '699',
			'product_id' => '106',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '700',
			'product_id' => '107',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '701',
			'product_id' => '107',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '702',
			'product_id' => '107',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '703',
			'product_id' => '107',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '704',
			'product_id' => '107',
			'tag_id'     => '20',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '705',
			'product_id' => '107',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '706',
			'product_id' => '107',
			'tag_id'     => '18',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '707',
			'product_id' => '107',
			'tag_id'     => '109',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '708',
			'product_id' => '108',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '709',
			'product_id' => '108',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '710',
			'product_id' => '108',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '711',
			'product_id' => '108',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '712',
			'product_id' => '108',
			'tag_id'     => '20',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '713',
			'product_id' => '108',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '714',
			'product_id' => '108',
			'tag_id'     => '19',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '715',
			'product_id' => '109',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '716',
			'product_id' => '109',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '717',
			'product_id' => '109',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '718',
			'product_id' => '109',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '719',
			'product_id' => '109',
			'tag_id'     => '110',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '720',
			'product_id' => '109',
			'tag_id'     => '51',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '721',
			'product_id' => '109',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '722',
			'product_id' => '110',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '723',
			'product_id' => '110',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '724',
			'product_id' => '110',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '725',
			'product_id' => '110',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '726',
			'product_id' => '110',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '727',
			'product_id' => '110',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '728',
			'product_id' => '110',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '729',
			'product_id' => '110',
			'tag_id'     => '19',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '730',
			'product_id' => '110',
			'tag_id'     => '82',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '731',
			'product_id' => '111',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '732',
			'product_id' => '111',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '733',
			'product_id' => '111',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '734',
			'product_id' => '111',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '735',
			'product_id' => '111',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '736',
			'product_id' => '111',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '737',
			'product_id' => '111',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '738',
			'product_id' => '112',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '739',
			'product_id' => '112',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '740',
			'product_id' => '112',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '741',
			'product_id' => '112',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '742',
			'product_id' => '112',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '743',
			'product_id' => '112',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '744',
			'product_id' => '112',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '745',
			'product_id' => '112',
			'tag_id'     => '87',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '746',
			'product_id' => '112',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '747',
			'product_id' => '113',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '748',
			'product_id' => '113',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '749',
			'product_id' => '113',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '750',
			'product_id' => '113',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '751',
			'product_id' => '113',
			'tag_id'     => '68',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '752',
			'product_id' => '113',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '753',
			'product_id' => '113',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '754',
			'product_id' => '114',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '755',
			'product_id' => '114',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '756',
			'product_id' => '114',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '757',
			'product_id' => '114',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '758',
			'product_id' => '114',
			'tag_id'     => '111',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '759',
			'product_id' => '114',
			'tag_id'     => '112',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '760',
			'product_id' => '114',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '761',
			'product_id' => '114',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '762',
			'product_id' => '114',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '763',
			'product_id' => '114',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '764',
			'product_id' => '114',
			'tag_id'     => '113',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '765',
			'product_id' => '115',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '766',
			'product_id' => '115',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '767',
			'product_id' => '115',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '768',
			'product_id' => '115',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '769',
			'product_id' => '115',
			'tag_id'     => '68',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '770',
			'product_id' => '115',
			'tag_id'     => '104',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '771',
			'product_id' => '115',
			'tag_id'     => '114',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '772',
			'product_id' => '115',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '773',
			'product_id' => '115',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '774',
			'product_id' => '115',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '775',
			'product_id' => '116',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '776',
			'product_id' => '116',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '777',
			'product_id' => '116',
			'tag_id'     => '99',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '778',
			'product_id' => '116',
			'tag_id'     => '79',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '779',
			'product_id' => '116',
			'tag_id'     => '93',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '780',
			'product_id' => '116',
			'tag_id'     => '94',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '781',
			'product_id' => '116',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '782',
			'product_id' => '116',
			'tag_id'     => '17',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '783',
			'product_id' => '116',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '784',
			'product_id' => '116',
			'tag_id'     => '115',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '785',
			'product_id' => '116',
			'tag_id'     => '116',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '786',
			'product_id' => '117',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '787',
			'product_id' => '117',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '788',
			'product_id' => '117',
			'tag_id'     => '117',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '789',
			'product_id' => '117',
			'tag_id'     => '118',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '790',
			'product_id' => '117',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '791',
			'product_id' => '117',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '792',
			'product_id' => '118',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '793',
			'product_id' => '118',
			'tag_id'     => '90',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '794',
			'product_id' => '118',
			'tag_id'     => '117',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '795',
			'product_id' => '118',
			'tag_id'     => '118',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '796',
			'product_id' => '118',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '797',
			'product_id' => '118',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '798',
			'product_id' => '119',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '799',
			'product_id' => '119',
			'tag_id'     => '120',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '800',
			'product_id' => '119',
			'tag_id'     => '80',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '801',
			'product_id' => '119',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '802',
			'product_id' => '119',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '803',
			'product_id' => '120',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '804',
			'product_id' => '120',
			'tag_id'     => '120',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '805',
			'product_id' => '120',
			'tag_id'     => '85',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '806',
			'product_id' => '120',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '807',
			'product_id' => '120',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '808',
			'product_id' => '121',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '809',
			'product_id' => '121',
			'tag_id'     => '121',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '810',
			'product_id' => '121',
			'tag_id'     => '122',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '811',
			'product_id' => '121',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '812',
			'product_id' => '121',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '813',
			'product_id' => '121',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '814',
			'product_id' => '122',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '815',
			'product_id' => '122',
			'tag_id'     => '121',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '816',
			'product_id' => '122',
			'tag_id'     => '122',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '817',
			'product_id' => '122',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '818',
			'product_id' => '122',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '819',
			'product_id' => '122',
			'tag_id'     => '124',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '820',
			'product_id' => '122',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '821',
			'product_id' => '123',
			'tag_id'     => '77',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '822',
			'product_id' => '123',
			'tag_id'     => '121',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '823',
			'product_id' => '123',
			'tag_id'     => '122',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '824',
			'product_id' => '123',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '825',
			'product_id' => '123',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '826',
			'product_id' => '123',
			'tag_id'     => '84',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '827',
			'product_id' => '123',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '828',
			'product_id' => '124',
			'tag_id'     => '49',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '829',
			'product_id' => '124',
			'tag_id'     => '125',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '830',
			'product_id' => '124',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '831',
			'product_id' => '124',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '832',
			'product_id' => '125',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '833',
			'product_id' => '125',
			'tag_id'     => '126',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '834',
			'product_id' => '125',
			'tag_id'     => '127',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '835',
			'product_id' => '125',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '836',
			'product_id' => '125',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '837',
			'product_id' => '125',
			'tag_id'     => '128',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '838',
			'product_id' => '126',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '839',
			'product_id' => '126',
			'tag_id'     => '126',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '840',
			'product_id' => '126',
			'tag_id'     => '127',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '841',
			'product_id' => '126',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '842',
			'product_id' => '126',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '843',
			'product_id' => '126',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '844',
			'product_id' => '127',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '845',
			'product_id' => '127',
			'tag_id'     => '126',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '846',
			'product_id' => '127',
			'tag_id'     => '127',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '847',
			'product_id' => '127',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '848',
			'product_id' => '127',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '849',
			'product_id' => '127',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '850',
			'product_id' => '127',
			'tag_id'     => '19',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '851',
			'product_id' => '128',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '852',
			'product_id' => '128',
			'tag_id'     => '129',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '853',
			'product_id' => '128',
			'tag_id'     => '130',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '854',
			'product_id' => '128',
			'tag_id'     => '131',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '855',
			'product_id' => '128',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '856',
			'product_id' => '128',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '857',
			'product_id' => '129',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '858',
			'product_id' => '129',
			'tag_id'     => '129',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '859',
			'product_id' => '129',
			'tag_id'     => '130',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '860',
			'product_id' => '129',
			'tag_id'     => '131',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '861',
			'product_id' => '129',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '862',
			'product_id' => '129',
			'tag_id'     => '18',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '863',
			'product_id' => '129',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '864',
			'product_id' => '130',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '865',
			'product_id' => '130',
			'tag_id'     => '129',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '866',
			'product_id' => '130',
			'tag_id'     => '130',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '867',
			'product_id' => '130',
			'tag_id'     => '131',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '868',
			'product_id' => '130',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '869',
			'product_id' => '130',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '870',
			'product_id' => '131',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '871',
			'product_id' => '131',
			'tag_id'     => '129',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '872',
			'product_id' => '131',
			'tag_id'     => '130',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '873',
			'product_id' => '131',
			'tag_id'     => '131',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '874',
			'product_id' => '131',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '875',
			'product_id' => '131',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '876',
			'product_id' => '132',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '877',
			'product_id' => '132',
			'tag_id'     => '129',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '878',
			'product_id' => '132',
			'tag_id'     => '130',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '879',
			'product_id' => '132',
			'tag_id'     => '131',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '880',
			'product_id' => '132',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '881',
			'product_id' => '132',
			'tag_id'     => '18',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '882',
			'product_id' => '132',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '883',
			'product_id' => '132',
			'tag_id'     => '37',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '884',
			'product_id' => '133',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '885',
			'product_id' => '133',
			'tag_id'     => '132',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '886',
			'product_id' => '133',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '887',
			'product_id' => '133',
			'tag_id'     => '64',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '888',
			'product_id' => '133',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '889',
			'product_id' => '134',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '890',
			'product_id' => '134',
			'tag_id'     => '132',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '891',
			'product_id' => '134',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '892',
			'product_id' => '134',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '893',
			'product_id' => '135',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '894',
			'product_id' => '135',
			'tag_id'     => '132',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '895',
			'product_id' => '135',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '896',
			'product_id' => '135',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '897',
			'product_id' => '136',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '898',
			'product_id' => '136',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '899',
			'product_id' => '136',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '900',
			'product_id' => '136',
			'tag_id'     => '133',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '901',
			'product_id' => '136',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '902',
			'product_id' => '137',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '903',
			'product_id' => '137',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '904',
			'product_id' => '137',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '905',
			'product_id' => '137',
			'tag_id'     => '133',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '906',
			'product_id' => '137',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '907',
			'product_id' => '137',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '908',
			'product_id' => '138',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '909',
			'product_id' => '138',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '910',
			'product_id' => '138',
			'tag_id'     => '30',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '911',
			'product_id' => '138',
			'tag_id'     => '133',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '912',
			'product_id' => '138',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '913',
			'product_id' => '138',
			'tag_id'     => '64',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '914',
			'product_id' => '138',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '915',
			'product_id' => '139',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '916',
			'product_id' => '139',
			'tag_id'     => '134',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '917',
			'product_id' => '139',
			'tag_id'     => '135',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '918',
			'product_id' => '139',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '919',
			'product_id' => '139',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '920',
			'product_id' => '140',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '921',
			'product_id' => '140',
			'tag_id'     => '134',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '922',
			'product_id' => '140',
			'tag_id'     => '135',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '923',
			'product_id' => '140',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '924',
			'product_id' => '141',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '925',
			'product_id' => '141',
			'tag_id'     => '134',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '926',
			'product_id' => '141',
			'tag_id'     => '135',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '927',
			'product_id' => '141',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '928',
			'product_id' => '142',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '929',
			'product_id' => '142',
			'tag_id'     => '134',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '930',
			'product_id' => '142',
			'tag_id'     => '135',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '931',
			'product_id' => '142',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '932',
			'product_id' => '142',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '933',
			'product_id' => '143',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '934',
			'product_id' => '143',
			'tag_id'     => '134',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '935',
			'product_id' => '143',
			'tag_id'     => '135',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '936',
			'product_id' => '143',
			'tag_id'     => '136',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '937',
			'product_id' => '143',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '938',
			'product_id' => '143',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '939',
			'product_id' => '144',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '940',
			'product_id' => '144',
			'tag_id'     => '134',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '941',
			'product_id' => '144',
			'tag_id'     => '135',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '942',
			'product_id' => '144',
			'tag_id'     => '136',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '943',
			'product_id' => '144',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '944',
			'product_id' => '144',
			'tag_id'     => '86',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '945',
			'product_id' => '145',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '946',
			'product_id' => '145',
			'tag_id'     => '134',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '947',
			'product_id' => '145',
			'tag_id'     => '135',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '948',
			'product_id' => '145',
			'tag_id'     => '136',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '949',
			'product_id' => '145',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '950',
			'product_id' => '145',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '951',
			'product_id' => '146',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '952',
			'product_id' => '146',
			'tag_id'     => '134',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '953',
			'product_id' => '146',
			'tag_id'     => '135',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '954',
			'product_id' => '146',
			'tag_id'     => '136',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '955',
			'product_id' => '146',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '956',
			'product_id' => '146',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '957',
			'product_id' => '147',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '958',
			'product_id' => '147',
			'tag_id'     => '134',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '959',
			'product_id' => '147',
			'tag_id'     => '135',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '960',
			'product_id' => '147',
			'tag_id'     => '136',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '961',
			'product_id' => '147',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '962',
			'product_id' => '147',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '963',
			'product_id' => '148',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '964',
			'product_id' => '148',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '965',
			'product_id' => '148',
			'tag_id'     => '137',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '966',
			'product_id' => '148',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '967',
			'product_id' => '148',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '968',
			'product_id' => '149',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '969',
			'product_id' => '149',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '970',
			'product_id' => '149',
			'tag_id'     => '137',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '971',
			'product_id' => '149',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '972',
			'product_id' => '150',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '973',
			'product_id' => '150',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '974',
			'product_id' => '150',
			'tag_id'     => '137',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '975',
			'product_id' => '150',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '976',
			'product_id' => '150',
			'tag_id'     => '138',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '977',
			'product_id' => '151',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '978',
			'product_id' => '151',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '979',
			'product_id' => '151',
			'tag_id'     => '137',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '980',
			'product_id' => '151',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '981',
			'product_id' => '151',
			'tag_id'     => '86',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '982',
			'product_id' => '152',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '983',
			'product_id' => '152',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '984',
			'product_id' => '152',
			'tag_id'     => '137',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '985',
			'product_id' => '152',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '986',
			'product_id' => '152',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '987',
			'product_id' => '153',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '988',
			'product_id' => '153',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '989',
			'product_id' => '153',
			'tag_id'     => '137',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '990',
			'product_id' => '153',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '991',
			'product_id' => '153',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '992',
			'product_id' => '154',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '993',
			'product_id' => '154',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '994',
			'product_id' => '154',
			'tag_id'     => '137',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '995',
			'product_id' => '154',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '996',
			'product_id' => '154',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '997',
			'product_id' => '155',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '998',
			'product_id' => '155',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '999',
			'product_id' => '155',
			'tag_id'     => '139',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1000',
			'product_id' => '155',
			'tag_id'     => '140',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1001',
			'product_id' => '155',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1002',
			'product_id' => '155',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1003',
			'product_id' => '156',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1004',
			'product_id' => '156',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1005',
			'product_id' => '156',
			'tag_id'     => '139',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1006',
			'product_id' => '156',
			'tag_id'     => '140',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1007',
			'product_id' => '156',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1008',
			'product_id' => '156',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1009',
			'product_id' => '157',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1010',
			'product_id' => '157',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1011',
			'product_id' => '157',
			'tag_id'     => '139',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1012',
			'product_id' => '157',
			'tag_id'     => '140',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1013',
			'product_id' => '157',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1014',
			'product_id' => '157',
			'tag_id'     => '19',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1015',
			'product_id' => '157',
			'tag_id'     => '41',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1016',
			'product_id' => '158',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1017',
			'product_id' => '158',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1018',
			'product_id' => '158',
			'tag_id'     => '139',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1019',
			'product_id' => '158',
			'tag_id'     => '140',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1020',
			'product_id' => '158',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1021',
			'product_id' => '158',
			'tag_id'     => '141',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1022',
			'product_id' => '159',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1023',
			'product_id' => '159',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1024',
			'product_id' => '159',
			'tag_id'     => '139',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1025',
			'product_id' => '159',
			'tag_id'     => '140',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1026',
			'product_id' => '159',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1027',
			'product_id' => '159',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1028',
			'product_id' => '159',
			'tag_id'     => '101',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1029',
			'product_id' => '160',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1030',
			'product_id' => '160',
			'tag_id'     => '71',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1031',
			'product_id' => '160',
			'tag_id'     => '139',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1032',
			'product_id' => '160',
			'tag_id'     => '140',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1033',
			'product_id' => '160',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1034',
			'product_id' => '160',
			'tag_id'     => '41',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1035',
			'product_id' => '161',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1036',
			'product_id' => '161',
			'tag_id'     => '142',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1037',
			'product_id' => '161',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1038',
			'product_id' => '161',
			'tag_id'     => '143',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1039',
			'product_id' => '161',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1040',
			'product_id' => '162',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1041',
			'product_id' => '162',
			'tag_id'     => '142',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1042',
			'product_id' => '162',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1043',
			'product_id' => '162',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1044',
			'product_id' => '162',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1045',
			'product_id' => '163',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1046',
			'product_id' => '163',
			'tag_id'     => '142',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1047',
			'product_id' => '163',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1048',
			'product_id' => '163',
			'tag_id'     => '141',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1049',
			'product_id' => '163',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1050',
			'product_id' => '164',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1051',
			'product_id' => '164',
			'tag_id'     => '144',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1052',
			'product_id' => '164',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1053',
			'product_id' => '164',
			'tag_id'     => '141',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1054',
			'product_id' => '164',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1055',
			'product_id' => '165',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1056',
			'product_id' => '165',
			'tag_id'     => '145',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1057',
			'product_id' => '165',
			'tag_id'     => '100',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1058',
			'product_id' => '165',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1059',
			'product_id' => '165',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1060',
			'product_id' => '166',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1061',
			'product_id' => '166',
			'tag_id'     => '146',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1062',
			'product_id' => '166',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1063',
			'product_id' => '166',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1064',
			'product_id' => '166',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1065',
			'product_id' => '167',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1066',
			'product_id' => '167',
			'tag_id'     => '147',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1067',
			'product_id' => '167',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1068',
			'product_id' => '167',
			'tag_id'     => '41',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1069',
			'product_id' => '167',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1070',
			'product_id' => '168',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1071',
			'product_id' => '168',
			'tag_id'     => '147',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1072',
			'product_id' => '168',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1073',
			'product_id' => '168',
			'tag_id'     => '141',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1074',
			'product_id' => '168',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1075',
			'product_id' => '169',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1076',
			'product_id' => '169',
			'tag_id'     => '147',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1077',
			'product_id' => '169',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1078',
			'product_id' => '169',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1079',
			'product_id' => '169',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1080',
			'product_id' => '170',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1081',
			'product_id' => '170',
			'tag_id'     => '148',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1082',
			'product_id' => '170',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1083',
			'product_id' => '170',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1084',
			'product_id' => '170',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1085',
			'product_id' => '171',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1086',
			'product_id' => '171',
			'tag_id'     => '148',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1087',
			'product_id' => '171',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1088',
			'product_id' => '171',
			'tag_id'     => '141',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1089',
			'product_id' => '171',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1090',
			'product_id' => '172',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1091',
			'product_id' => '172',
			'tag_id'     => '149',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1092',
			'product_id' => '172',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1093',
			'product_id' => '172',
			'tag_id'     => '92',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1094',
			'product_id' => '172',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1095',
			'product_id' => '172',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1096',
			'product_id' => '173',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1097',
			'product_id' => '173',
			'tag_id'     => '149',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1098',
			'product_id' => '173',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1099',
			'product_id' => '173',
			'tag_id'     => '92',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1100',
			'product_id' => '173',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1101',
			'product_id' => '173',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1102',
			'product_id' => '174',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1103',
			'product_id' => '174',
			'tag_id'     => '149',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1104',
			'product_id' => '174',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1105',
			'product_id' => '174',
			'tag_id'     => '92',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1106',
			'product_id' => '174',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1107',
			'product_id' => '174',
			'tag_id'     => '123',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1108',
			'product_id' => '175',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1109',
			'product_id' => '175',
			'tag_id'     => '150',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1110',
			'product_id' => '175',
			'tag_id'     => '76',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1111',
			'product_id' => '175',
			'tag_id'     => '141',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1112',
			'product_id' => '176',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1113',
			'product_id' => '176',
			'tag_id'     => '150',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1114',
			'product_id' => '176',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1115',
			'product_id' => '176',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1116',
			'product_id' => '177',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1117',
			'product_id' => '177',
			'tag_id'     => '150',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1118',
			'product_id' => '177',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1119',
			'product_id' => '178',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1120',
			'product_id' => '178',
			'tag_id'     => '151',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1121',
			'product_id' => '178',
			'tag_id'     => '152',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1122',
			'product_id' => '178',
			'tag_id'     => '153',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1123',
			'product_id' => '178',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1124',
			'product_id' => '178',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1125',
			'product_id' => '179',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1126',
			'product_id' => '179',
			'tag_id'     => '151',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1127',
			'product_id' => '179',
			'tag_id'     => '152',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1128',
			'product_id' => '179',
			'tag_id'     => '153',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1129',
			'product_id' => '179',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1130',
			'product_id' => '179',
			'tag_id'     => '37',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1131',
			'product_id' => '180',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1132',
			'product_id' => '180',
			'tag_id'     => '151',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1133',
			'product_id' => '180',
			'tag_id'     => '152',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1134',
			'product_id' => '180',
			'tag_id'     => '153',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1135',
			'product_id' => '180',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1136',
			'product_id' => '180',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1137',
			'product_id' => '181',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1138',
			'product_id' => '181',
			'tag_id'     => '151',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1139',
			'product_id' => '181',
			'tag_id'     => '152',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1140',
			'product_id' => '181',
			'tag_id'     => '153',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1141',
			'product_id' => '181',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1142',
			'product_id' => '181',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1143',
			'product_id' => '182',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1144',
			'product_id' => '182',
			'tag_id'     => '151',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1145',
			'product_id' => '182',
			'tag_id'     => '152',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1146',
			'product_id' => '182',
			'tag_id'     => '153',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1147',
			'product_id' => '182',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1148',
			'product_id' => '183',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1149',
			'product_id' => '183',
			'tag_id'     => '151',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1150',
			'product_id' => '183',
			'tag_id'     => '152',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1151',
			'product_id' => '183',
			'tag_id'     => '153',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1152',
			'product_id' => '183',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1153',
			'product_id' => '183',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1154',
			'product_id' => '184',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1155',
			'product_id' => '184',
			'tag_id'     => '154',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1156',
			'product_id' => '184',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1157',
			'product_id' => '184',
			'tag_id'     => '92',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1158',
			'product_id' => '184',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1159',
			'product_id' => '184',
			'tag_id'     => '155',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1160',
			'product_id' => '184',
			'tag_id'     => '156',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1161',
			'product_id' => '185',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1162',
			'product_id' => '185',
			'tag_id'     => '154',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1163',
			'product_id' => '185',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1164',
			'product_id' => '185',
			'tag_id'     => '92',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1165',
			'product_id' => '185',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1166',
			'product_id' => '185',
			'tag_id'     => '155',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1167',
			'product_id' => '185',
			'tag_id'     => '156',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1168',
			'product_id' => '186',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1169',
			'product_id' => '186',
			'tag_id'     => '154',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1170',
			'product_id' => '186',
			'tag_id'     => '157',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1171',
			'product_id' => '186',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1172',
			'product_id' => '186',
			'tag_id'     => '155',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1173',
			'product_id' => '186',
			'tag_id'     => '156',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1174',
			'product_id' => '187',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1175',
			'product_id' => '187',
			'tag_id'     => '154',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1176',
			'product_id' => '187',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1177',
			'product_id' => '187',
			'tag_id'     => '40',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1178',
			'product_id' => '187',
			'tag_id'     => '155',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1179',
			'product_id' => '187',
			'tag_id'     => '156',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1180',
			'product_id' => '188',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1181',
			'product_id' => '188',
			'tag_id'     => '158',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1182',
			'product_id' => '188',
			'tag_id'     => '143',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1183',
			'product_id' => '188',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1184',
			'product_id' => '188',
			'tag_id'     => '82',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1185',
			'product_id' => '189',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1186',
			'product_id' => '189',
			'tag_id'     => '158',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1187',
			'product_id' => '189',
			'tag_id'     => '63',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1188',
			'product_id' => '189',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1189',
			'product_id' => '189',
			'tag_id'     => '46',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1190',
			'product_id' => '190',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1191',
			'product_id' => '190',
			'tag_id'     => '158',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1192',
			'product_id' => '190',
			'tag_id'     => '21',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1193',
			'product_id' => '190',
			'tag_id'     => '37',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1194',
			'product_id' => '191',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1195',
			'product_id' => '191',
			'tag_id'     => '158',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1196',
			'product_id' => '191',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1197',
			'product_id' => '191',
			'tag_id'     => '138',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1198',
			'product_id' => '191',
			'tag_id'     => '82',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1199',
			'product_id' => '192',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1200',
			'product_id' => '192',
			'tag_id'     => '158',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1201',
			'product_id' => '192',
			'tag_id'     => '119',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1202',
			'product_id' => '192',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1203',
			'product_id' => '192',
			'tag_id'     => '82',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1204',
			'product_id' => '193',
			'tag_id'     => '72',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1205',
			'product_id' => '193',
			'tag_id'     => '158',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1206',
			'product_id' => '193',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1207',
			'product_id' => '193',
			'tag_id'     => '36',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1208',
			'product_id' => '193',
			'tag_id'     => '82',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1209',
			'product_id' => '194',
			'tag_id'     => '159',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1210',
			'product_id' => '194',
			'tag_id'     => '160',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1211',
			'product_id' => '194',
			'tag_id'     => '161',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1212',
			'product_id' => '194',
			'tag_id'     => '7',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1213',
			'product_id' => '194',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1214',
			'product_id' => '194',
			'tag_id'     => '41',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1215',
			'product_id' => '195',
			'tag_id'     => '162',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1216',
			'product_id' => '195',
			'tag_id'     => '163',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1217',
			'product_id' => '195',
			'tag_id'     => '16',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1218',
			'product_id' => '195',
			'tag_id'     => '8',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1219',
			'product_id' => '195',
			'tag_id'     => '43',
		]);
		$this->insert('{{%product_tag}}', [
			'id'         => '1220',
			'product_id' => '195',
			'tag_id'     => '7',
		]);
	}

	public function safeDown() {
		$this->dropIndex('product_id', '{{%product_tag}}');
		$this->dropIndex('tag_id', '{{%product_tag}}');
		$this->dropTable('{{%product_tag}}');
	}
}

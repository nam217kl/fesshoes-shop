<?php

namespace frontend\models\search;

use common\models\Category;
use frontend\models\Product;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * ProductSearch represents the model behind the search form of `common\models\Product`.
 */
class ProductSearch extends Product {

	public $keyword           = '';

	public $super_category_id = '';

	const SORT_BEST_SELLER       = 'best-seller';

	const SORT_NEWEST_ITEMS      = 'newest-items';

	const SORT_PRICE_LOW_TO_HIGH = 'price-low-to-high';

	const SORT_PRICE_HIGH_TO_LOW = 'price-high-to-low';

	const SORT                   = [
		self::SORT_BEST_SELLER       => 'Best seller',
		self::SORT_NEWEST_ITEMS      => 'Newest items',
		self::SORT_PRICE_LOW_TO_HIGH => 'Price: Low to high',
		self::SORT_PRICE_HIGH_TO_LOW => 'Price: High to low',
	];

	const LIMIT_20               = 20;

	const LIMIT_40               = 40;

	const LIMIT_60               = 60;

	const LIMIT_80               = 80;

	const LIMIT_100              = 100;

	const LIMIT                  = [
		self::LIMIT_20  => 20,
		self::LIMIT_40  => 40,
		self::LIMIT_60  => 60,
		self::LIMIT_80  => 80,
		self::LIMIT_100 => 100,
	];

	public $limit = self::LIMIT_20;

	public $sort  = self::SORT_NEWEST_ITEMS;

	/**
	 * @var mixed|null
	 */
	public $tag_id = '';

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'category_id',
					'super_category_id',
					'id',
					'tag_id',
				],
				'integer',
			],
			[
				[
					'name',
					'condition',
					'short_desc',
					'long_desc',
					'color',
					'size',
					'featured',
					'keyword',
					'sort',
					'limit',
				],
				'safe',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Product::find()->alias('product');
		switch ($this->sort) {
			default:
			case self::SORT_NEWEST_ITEMS:
				$query->orderBy(['product.id' => SORT_DESC]);
				break;
			case self::SORT_BEST_SELLER:
				//todo làm sau
				break;
			case self::SORT_PRICE_LOW_TO_HIGH:
				$query->leftJoin('product_detail', 'product.id = product_detail.product_id')->andWhere("product_detail.price <> 0")->groupBy('product_detail.product_id')->orderBy(['product_detail.price' => SORT_ASC]);
				break;
			case self::SORT_PRICE_HIGH_TO_LOW:
				$query->leftJoin('product_detail', 'product.id = product_detail.product_id')->andWhere("product_detail.price <> 0")->groupBy('product_detail.product_id')->orderBy(['product_detail.price' => SORT_DESC]);
				break;
		}
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'pagination' => [
				'pageSize' => $this->limit,
			],
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'product.id'          => $this->id,
			'product.category_id' => $this->category_id,
		]);
		$query->andFilterWhere([
			'like',
			'product.name',
			$this->name,
		])->andFilterWhere([
			'like',
			'product.condition',
			$this->condition,
		])->andFilterWhere([
			'like',
			'product.short_desc',
			$this->short_desc,
		])->andFilterWhere([
			'like',
			'product.long_desc',
			$this->long_desc,
		])->andFilterWhere([
			'like',
			'product.color',
			$this->color,
		])->andFilterWhere([
			'like',
			'product.size',
			$this->size,
		])->andFilterWhere([
			'like',
			'product.featured',
			$this->featured,
		]);
		if ($this->super_category_id != '') {
			$superCategory = Category::findOne($this->super_category_id);
			if ($superCategory !== null) {
				$categories_id      = ArrayHelper::map($superCategory->categories, 'id', 'id');
				$categories_id[- 1] = $this->super_category_id;
				ksort($categories_id);
				$query->andWhere(['product.category_id' => $categories_id]);
			}
		}
		if ($this->keyword != '') {
			$query->andFilterWhere([
				'LIKE',
				'product.name',
				$this->keyword,
			]);
		}
		if ($this->tag_id != '') {
			$query->leftJoin('product_tag', 'product_tag.product_id=product.id')->andWhere(['product_tag.tag_id' => $this->tag_id]);
		}
		return $dataProvider;
	}
}

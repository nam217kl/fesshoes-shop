<?php

namespace frontend\models;

use yii\base\Model;

class CategorySearchForm extends Model {

	public $category_id;

	public $keyword;

	public function rules() {
		return [
			[
				[
					'category_id',
					'keyword',
				],
				'safe'
			],
		];
	}
}

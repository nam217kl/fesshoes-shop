<?php

namespace frontend\widgets;

use common\models\Cart;
use yii\base\Widget;

class CartShopping extends Widget {

	public function run() {
		if (!\Yii::$app->user->isGuest) {
			$carts = Cart::find()->andWhere(['user_id' => \Yii::$app->user->identity->id])->all();
		} else {
			$carts = [];
		}
		return $this->render('cart', ['carts' => $carts]);
	}
}

<?php

namespace frontend\widgets;

use common\models\Category;
use frontend\models\CategorySearchForm;
use yii\base\Widget;

class CategorySearch extends Widget {

	public function run() {
		$model = new CategorySearchForm();
		$model->load(\Yii::$app->request->get());
		return $this->render('categorysearch', [
			'model' => $model,
		]);
	}
}


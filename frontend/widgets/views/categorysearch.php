<?php
/**
 * @var yii\web\View       $this
 * @var CategorySearchForm $model
 *
 */

use common\models\Category;
use frontend\models\CategorySearchForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="col-lg-6 col-md-8">
	<!-- header search bar -->
	<div class="header-search-bar">
		<?php $form = ActiveForm::begin([
			'action' => '/product/index',
			'method' => 'get',
		]) ?>
		<div class="input-group">
			<?= $form->field($model, 'category_id', [
				'template' => '{input}',
				'options'  => ['tag' => false],
			])->dropDownList(ArrayHelper::map(Category::find()->andWhere("parent_id IS NULL")->all(), 'id', 'name'), ['prompt' => 'Please select'])->label(false); ?>
			<div class="input-group-append">
				<?= $form->field($model, 'keyword', [
					'template' => '{input}',
					'options'  => ['tag' => false],
				])->textInput(['class' => ''])->label(false); ?>
				<?= Html::submitButton('<i class="fa fa-search"></i>') ?>
			</div>
		</div>
		<?php ActiveForm::end() ?>
	</div>
	<!-- end of header search bar -->
</div>

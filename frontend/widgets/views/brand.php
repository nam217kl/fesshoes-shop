<?php
/**
 * @var yii\web\View              $this
 * @var \common\models\Branding[] $brands
 */
?>
<!--=======================================
					=            brand logo slider            =
					========================================-->

<div class="brand-logo-slider mb-50">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12">
				<div class="brand-logo-list">
					<!-- ======  single brand logo block  ======= -->
					<?php foreach ($brands as $brand): ?>
						<div class="single-brand-logo">
							<a href="<?php echo $brand->url ?>">
								<img width="120" height="85" src="<?php echo $brand->image ?>" alt="">
							</a>
						</div>
					<?php endforeach; ?>
					<!-- ====  End of single brand logo block  ==== -->

				</div>
			</div>
		</div>
	</div>
</div>

<!--====  End of brand logo slider  ====-->

<?php
/**
 * @var yii\web\View $this
 * @var Cart[]       $carts
 */

use common\models\Cart;
use yii\helpers\Url;

?>
<div class="col-lg-3 col-md-4">
	<!-- shopping cart -->
	<div class="shopping-cart float-lg-end d-flex justify-content-start" id="shopping-cart">
		<div class="cart-icon">
			<a href="<?= Url::to([
				'/cart/view',
			]) ?>"><img width="39" height="27" src="/images/icon-topcart.webp" class="img-fluid" alt=""></a>
		</div>
		<div class="cart-content">
			<h2><a href="<?= Url::to([
					'/cart/view',
				]) ?>">Shopping Cart
					<span><span id="cartStatus">(<?= $carts != null ? count($carts) . ' items' : 'Empty' ?>)</span></span></a>
			</h2>
		</div>

		<div class="cart-floating-box" id="cart-floating-box">
			<div class="cart-items">
				<?php $total = 0 ?>
				<?php foreach ($carts as $cart) : ?>
					<div class="cart-float-single-item d-flex">
						<span class="remove-item"><a href="#"><i class="fa fa-trash"></i></a></span>
						<div class="cart-float-single-item-image">
							<img width="250" height="250" src="<?= $cart->productDetail->product->firstProductImage->image ?>" class="img-fluid" alt="">
						</div>
						<div class="cart-float-single-item-desc">
							<p class="product-title"><span class="count"><?= $cart->quantity ?>x</span> <a
										href="<?= Url::to([
											'/product/view',
											'id' => $cart->productDetail->product_id,
										]) ?>"><?= $cart->productDetail->product->name ?></a></p>
							<p class="size">
								<span><?= $cart->productDetail->color ?> <?= $cart->productDetail->size ?></span></p>
							<p class="price">$<?= $cart->productDetail->price * $cart->quantity ?></p>
							<?php
							$subtotal = $cart->productDetail->price * $cart->quantity;
							$total    += $subtotal;
							?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="cart-calculation d-flex">
				<div class="calculation-details">
					<p class="total">Total <span>$<?= $total ?></span></p>
				</div>
				<div class="checkout-button">
					<a href="<?= Url::to([
						'/checkout/index',
					]) ?>">Checkout</a>
				</div>
			</div>

		</div>

		<!-- end of shopping cart -->
	</div>
</div>
<script>
	$(document).on('click', '.shopping-cart', function() {
		<?php if(Yii::$app->user->isGuest) :?>
		alert('Please login first!');
		<?php endif;?>
	});
</script>

<?php

namespace frontend\widgets;

use common\models\Branding;
use yii\base\Widget;
use yii\helpers\Html;

class Brand extends Widget {

	public function run() {
		$brands = Branding::find()->all(    );
		return $this->render('brand', [
			'brands' => $brands,
		]);
	}
}

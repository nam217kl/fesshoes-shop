<?php
/**
 * @var yii\web\View       $this
 * @var Product[]          $listProducts
 * @var Category[]         $categories
 * @var ActiveDataProvider $dataProvider
 * @var string             $sort
 * @var int                $limit
 * @var array              $tag_data
 */

use common\models\Category;
use common\models\Product;
use frontend\models\search\ProductSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = 'Danh mục sản phẩm';
?>
<section class="shop-content mt-40 mb-40">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-4 order-2 order-lg-1">
				<div class="shoppage-sidebar">
					<!-- category list -->
					<!-- Header Category -->
					<div class="hero-side-category shop-side-category">

						<h2 class="block-title">CATEGORIES</h2>
						<!-- Category Menu -->
						<nav class="shop-category-menu mb-50">
							<ul>
								<?php foreach ($categories as $category): ?>
									<li>
										<a href="<?= Url::to([
											'/category/view',
											'id' => $category->id,
										]) ?>"><?= $category->name ?></a></li>
							<?php endforeach; ?>
							</ul>
						</nav>
						<!-- end of Category menu -->
					</div>
					<!-- End of Header Category -->
					<!-- end of category list -->

					<!-- oue store widget -->
					<div class="sidebar">
						<div class="store-container mb-50">
							<h2 class="block-title">OUR Stores</h2>
							<div class="store-widget-container">
								<div class="store-image mb-20">
									<a href="store.html">
										<img width="180" height="110" src="assets/images/store.webp" alt="" class="img-fluid"></a>
								</div>
							</div>
							<a href="store.html" class="store-btn">Discover our store <i
										class="fa fa-chevron-right"></i></a>
						</div>
					</div>
					<!-- end of oue store widget -->

					<!-- tag container -->
					<div class="sidebar">
						<h2 class="block-title">TAGS</h2>
						<div class="tag-container">
							<?= \coderius\jqcloud2\jQCloud::widget([
								'tagOptions'   => [
									'style' => 'width:100%; height: 200px',
								],
								'wordsOptions' => $tag_data,
							]); ?>
						</div>
					</div>
					<!-- end of tag container -->

				</div>
			</div>
			<div class="col-lg-9 col-md-8  order-1 order-lg-2">
				<div class="shop-page-container">
					<div class="shop-page-header">
						<div class="row">
							<div class="col-12">
								<h2>lIST PRODUCTS</h2>
							</div>
							<div class="col-lg-4 col-sm-12 d-flex justify-content-start align-items-center">
								<!-- Product view mode -->
								<p class="view-mode">View:</p>
								<div class="view-mode-icons">
									<a class="active" href="#" data-target="grid"><i class="fa fa-th"></i>
										<span>Grid</span></a>
									<a href="#" data-target="list"><i class="fa fa-list"></i><span>List</span></a>
								</div>
							</div>
							<div
									class="col-lg-8 col-sm-12 d-flex flex-column flex-sm-row justify-content-lg-end justify-content-start">
								<!-- Product Showing -->
								<div class="product-showing mr-20 mb-sm-10">
									<p>Show
										<?= Html::dropDownList('limit', $limit, ProductSearch::LIMIT) ?>
										<span>per page</span>
									</p>
								</div>
								<!-- Product Short -->
								<div class="product-sort">
									<p>Sort by
										<?= Html::dropDownList('sort', $sort, ProductSearch::SORT) ?>
									</p>
								</div>
							</div>
						</div>
					</div>

					<!-- ======  Shop product list  ====== -->
					<?= ListView::widget([
						'dataProvider' => $dataProvider,
						'itemView'     => '_item',
						'layout'       => '{items}<div class="row"><div class="col-3">{summary}</div><div class="col-9 pull-right">{pager}</div></div>',
						'options'      => [
							'class' => 'shop-product-wrap grid row',
						],
						'itemOptions'  => [
							'class' => 'col-xl-3 col-lg-4 col-md-6 col-12 pb-30 pt-10',
						],
					]) ?>
					<!-- ====  End of Shop product list  ==== -->

				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$(document).on('change', '[name="limit"]', function() {
		let th        = $(this);
		location.href = '<?=Url::current(['limit' => ''])?>' + th.val();
	});
	$(document).on('change', '[name="sort"]', function() {
		let th        = $(this);
		location.href = '<?=Url::current(['sort' => ''])?>' + th.val()
	});
	$(document).on('click', '.wish-list', function() {
		let th = $(this);
		$.ajax({
			type    : "post",
			cache   : false,
			url     : th.attr('data-href'),
			dataType: 'json',
			success : function(response) {
				if(response.message === "ADD") {
					th.css({
						"backgroundColor": "#f36b63",
						"color"          : "#FFFFFF"
					})
				} else {
					th.css({
						"backgroundColor": "#eee",
						"color"          : 'gray'
					})
				}
			},
			error   : function(e) {
				alert(e);
			}
		})
	})
</script>

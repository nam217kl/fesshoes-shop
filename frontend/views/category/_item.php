<?php
/**
 * @var \frontend\models\Product $model
 */

use yii\helpers\Url; ?>
<!-- product start -->
<div class="single-product shop-page-product single-grid-product product-item">
	<div class="single-product-content">
		<div class="product-image">
			<a href="<?= Url::to([
				'/product/view',
				'id' => $model->id,
			]) ?>">
				<img width="250" height="250" src="<?= $model->firstProductImage->image ?>" class="img-fluid" alt="">
				<img width="250" height="250" src="<?= $model->firstProductImage->image ?>" class="img-fluid" alt="">
			</a>
			<div class="image-btn">
				<a href="#" data-bs-toggle="modal" data-bs-target="#quick-view-modal-container"><i
						class="fa fa-search"></i></a>
				<a href="#"><i class="fa fa-refresh"></i></a>
				<a href="#"><i class="fa fa-heart-o"></i></a>
			</div>
		</div>
		<h5 class="product-name">
			<a href="<?= Url::to([
				'/product/view',
				'id' => $model->id,
			]) ?>"><?= $model->name ?></a></h5>
		<div class="price-box">
			<h4>$ <?= number_format($model->productPriceMin->price, 2) ?> <?= ($model->productPriceMin->price != $model->productPriceMax->price ? "- $ " . number_format($model->productPriceMax->price, 2) : "") ?></h4>
		</div>
	</div>
</div>
<!-- product end -->

<!-- product list start -->
<div class="single-list-product product-item">
	<div class="list-product-image">
		<a href="<?= Url::to([
			'/product/view',
			'id' => $model->id,
		]) ?>">
			<img width="250" height="250" src="<?= $model->firstProductImage->image ?>" class="img-fluid" alt="">
			<img width="250" height="250" src="<?= $model->firstProductImage->image ?>" class="img-fluid" alt="">
		</a>
		<div class="image-btn">
			<a href="#" data-bs-toggle="modal" data-bs-target="#quick-view-modal-container"><i
					class="fa fa-search"></i></a>
			<a href="#"><i class="fa fa-refresh"></i></a>
			<a href="#"><i class="fa fa-heart-o"></i></a>
		</div>
	</div>

	<div class="list-product-desc">
		<h5 class="product-name">
			<a href="<?= Url::to([
				'/product/view',
				'id' => $model->id,
			]) ?>"><?= $model->name ?></a></h5>
		<div class="price-box">
			<h4>$ <?= number_format($model->productPriceMin->price, 2) ?> <?= ($model->productPriceMin->price != $model->productPriceMax->price ? "- $ " . number_format($model->productPriceMax->price, 2) : "") ?></h4>
		</div>
		<p class="product-description"><?= $model->long_desc ?></p>
		<!--									<p class="color">-->
		<!--										<a href="#"><span class="color-block color-choice-1"></span></a>-->
		<!--										<a href="#"><span class="color-block color-choice-2"></span></a>-->
		<!--										<a href="#"><span class="color-block color-choice-3 active"></span></a>-->
		<!--									</p>-->
		<!--									<p class="stock-status"><span class="stock-status in-stock">In Stock</span></p>-->
	</div>
</div>
<!-- product list end -->

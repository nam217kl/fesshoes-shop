<?php
/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */

/** @var \frontend\models\PasswordResetRequestForm $model */

use yii\bootstrap5\Html;
use yii\bootstrap5\ActiveForm;

$this->title                   = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset">
	<h1><?= Html::encode($this->title) ?></h1>

	<p>Please fill out your email. A link to reset password will be sent there.</p>

	<div class="row">
		<div class="col-lg-5">
			<?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

			<?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

			<div class="form-group">
				<?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
			</div>
			<?php if (Yii::$app->session->hasFlash('success')): ?>
				<div class="alert alert-success alert-dismissable">
					<h4><i class="icon fa fa-check"></i> Success!</h4>
					<?= Yii::$app->session->getFlash('success') ?>
				</div>
			<?php endif; ?>

			<?php if (Yii::$app->session->hasFlash('error')): ?>
				<div class="alert alert-danger alert-dismissable">
					<h4><i class="icon fa fa-times"></i> Failed!</h4>
					<?= Yii::$app->session->getFlash('error') ?>
				</div>
			<?php endif; ?>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>

<?php
/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */

/** @var \frontend\models\LoginForm $model */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

$this->title                   = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-area page-content mb-50">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h2>Login</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<?php $form = ActiveForm::begin([
					'id'      => 'login-form',
					'options' => ['class' => 'login-form'],
				]); ?>
				<div id="login-form">
					<div class="row">
						<div class="col-lg-6 offset-3">
							<h4 class="login-title">Login</h4>
							<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
							<?= $form->field($model, 'password')->passwordInput() ?>
							<div class="row">
								<div class="col-md-8">
									<?= Html::submitButton('Login', [
										'class' => 'register-button mt-0',
										'name'  => 'login-button',
									]) ?>
									<?= $form->field($model, 'rememberMe',)->checkbox(['template' => '<div class="check-box d-inline-block ms-2 mt-10">{input}{label}</div>']) ?>
								</div>
								<div class="col-md-4 mt-10 text-start text-md-end">
									<?= Html::a('Forgot password?', ['site/request-password-reset']) ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php ActiveForm::end(); ?>
				<div class="my-1 mx-0 text-center" style="color:#999;">
					<?= Html::a('New to us? Register now', ['site/signup']) ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
/** @var yii\web\View $this */
/** @var yii\bootstrap5\ActiveForm $form */

/** @var \frontend\models\SignupForm $model */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;

$this->title                   = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="account-area page-content mb-50">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h2>Signup</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<?php $form = ActiveForm::begin([
					'id'      => 'form-signup',
					'options' => ['class' => 'login-form'],
				]); ?>
				<div id="register-form">
					<div class="row">
						<div class="col-lg-6 offset-3">
							<h4 class="register-title">Register</h4>

							<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

							<?= $form->field($model, 'email') ?>

							<?= $form->field($model, 'password')->passwordInput() ?>
							<div class="form-group">
								<?= Html::submitButton('Signup', [
									'class' => 'btn btn-primary',
									'name'  => 'signup-button',
								]) ?>
							</div>

						</div>
					</div>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>

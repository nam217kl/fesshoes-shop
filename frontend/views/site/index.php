<?php
/**
 * @var yii\web\View              $this
 * @var \common\models\Category[] $twoRandomCategories
 * @var \common\models\Category[] $categories
 * @var \common\models\Slider[]   $sliders
 * @var \common\models\Slider[]   $slide_downs
 * @var \common\models\Product[]  $newProducts
 * @var \common\models\Product[]  $latestProducts
 * @var \common\models\Product[]  $featuredProducts
 * @var string                    $keyword
 */

use yii\helpers\Url;

$this->title = 'Trang chủ';
?>
<!--===========================================
					=            homepage content section            =
					============================================-->

<div class="homepage-content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 mb-50">
				<div class="row">
					<div class="col-lg-3 col-md-4">
						<!-- Header category list -->
						<div class="hero-side-category">

							<!-- Category Toggle Wrap -->
							<div class="category-toggle-wrap">
								<!-- Category Toggle -->
								<button class="category-toggle">Categories <i class="ti-menu"></i>
								</button>
							</div>

							<!-- Category Menu -->
							<nav class="category-menu mb-40">
								<ul>
									<!-- parent_id = id thi mac dinh se la thu muc con-->
									<?php foreach ($categories as $category): ?>
										<li class="<?php echo $category->getCategories()->exists() ? 'menu-item-has-children' : '' ?>">
											<a href="<?= Url::to([
												'/category/view',
												'category_id' => $category->id,
											]) ?>"><?php echo $category->name ?></a>
											<?php if ($category->getCategories()->exists()): ?>
												<ul class="category-mega-menu">
													<?php foreach ($category->categories as $childCategory) : ?>
														<li class="menu-item-has-children">
															<a class="megamenu-head" href="<?= Url::to([
																'/category/view',
																'category_id' => $childCategory->id,
															]) ?>"><?php echo $childCategory->name ?></a>
														</li>
													<?php endforeach; ?>
												</ul><!-- Mega Category Menu End -->
											<?php endif; ?>
										</li>
									<?php endforeach ?>
								</ul>
							</nav>
						</div>
						<!-- end of Header category list -->
					</div>
					<div class="col-lg-9 col-md-8">
						<!-- ======  Hero slider content  ======= -->

						<div class="hero-slider hero-slider-one">
							<!-- Hero Item Start -->
							<?php foreach ($sliders as $slider): ?>
								<div class="hero-item">
									<img src="<?php echo $slider->image ?>" class="img-fluid" alt="">
									<div class="row align-items-center justify-content-between">
										<!-- Hero Content -->
										<div class="hero-content col-md-8 offset-md-4 col-12 offset-0">
											<h1><?= $slider->name ?></h1>
											<p><?= $slider->description ?></p>
											<a href="<?= Url::to([
												'/category/view',
												'id' => $slider->id,
											]) ?>">shop now</a>
										</div>
									</div>
								</div><!-- Hero Item End -->
							<?php endforeach; ?>
						</div>

						<!-- ====  End of Hero slider content  ==== -->

						<!-- ======  Featured service content  ======= -->

						<div class="featured-service-container">
							<div class="row">
								<?php foreach ($slide_downs as $slider1): ?>
									<div class="col-lg-4 col-md-6">
										<!-- single-feature -->
										<div class="single-featured-service ">
											<img src="<?php echo $slider1->image ?>" class="img-fluid" alt="">
											<div class="single-featured-service-content">
												<h3><?= $slider1->name ?></h3>
												<p><?= $slider1->description ?></p>
												<a href="<?= Url::to([
													'/category/view',
													'id' => $slider1->id,
												]) ?>">View Collection</a>
											</div>
										</div>
										<!-- end of single feature -->
									</div>
								<?php endforeach; ?>
							</div>
						</div>

						<!-- ====  End of Featured service content  ==== -->

					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="row">
					<div class="col-lg-3 col-md-4 mb-50">

						<!-- ======  Homepage sidebar  ======= -->

						<div class="homepage-sidebar">
							<!-- vertical auto slider container -->
							<div class="sidebar">
								<h2 class="block-title">BESTSELLER</h2>
								<div class="vertical-product-slider-container">
									<div class="single-vertical-slider">
										<div class="vertical-auto-slider-product-list">
											<!-- single vertical product -->
											<div class="single-auto-vertical-product d-flex">
												<div class="product-image">
													<a href="<?= Url::to([
														'/product/view',
														'id' => 1,
													]) ?>"><img width="250" height="250" src="/images/products/1.webp"
													            class="img-fluid" alt=""></a>
												</div>
												<div class="product-description">
													<h5 class="product-name">
														<a href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Faded
															Short Sleeve</a></h5>
													<div class="price-box">
														<h4>$ 12.00</h4>
													</div>

												</div>
											</div>
											<!-- end of single vertical product -->
											<!-- single vertical product -->
											<div class="single-auto-vertical-product d-flex">
												<div class="product-image">
													<a href="<?= Url::to([
														'/product/view',
														'id' => 1,
													]) ?>"><img width="250" height="250" src="/images/products/2.webp"
													            class="img-fluid" alt=""></a>
												</div>
												<div class="product-description">
													<h5 class="product-name">
														<a href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Printed
															Dress</a></h5>
													<div class="price-box">
														<h4>$ 12.00</h4>
													</div>

												</div>
											</div>
											<!-- end of single vertical product -->
											<!-- single vertical product -->
											<div class="single-auto-vertical-product d-flex">
												<div class="product-image">
													<a href="<?= Url::to([
														'/product/view',
														'id' => 1,
													]) ?>"><img width="250" height="250" src="/images/products/3.webp"
													            class="img-fluid" alt=""></a>
												</div>
												<div class="product-description">
													<h5 class="product-name">
														<a href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Faded
															Short Sleeve</a></h5>
													<div class="price-box">
														<h4>$ 12.00</h4>
													</div>

												</div>
											</div>
											<!-- end of single vertical product -->
											<!-- single vertical product -->
											<div class="single-auto-vertical-product d-flex">
												<div class="product-image">
													<a href="<?= Url::to([
														'/product/view',
														'id' => 1,
													]) ?>"><img width="250" height="250" src="/images/products/4.webp"
													            class="img-fluid" alt=""></a>
												</div>
												<div class="product-description">
													<h5 class="product-name">
														<a href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Printed
															Dress</a></h5>
													<div class="price-box">
														<h4>$ 12.00</h4>
													</div>

												</div>
											</div>
											<!-- end of single vertical product -->
											<!-- single vertical product -->
											<div class="single-auto-vertical-product d-flex">
												<div class="product-image">
													<a href="<?= Url::to([
														'/product/view',
														'id' => 1,
													]) ?>"><img width="250" height="250" src="/images/products/5.webp"
													            class="img-fluid" alt=""></a>
												</div>
												<div class="product-description">
													<h5 class="product-name">
														<a href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Faded
															Short Sleeve</a></h5>
													<div class="price-box">
														<h4>$ 12.00</h4>
													</div>

												</div>
											</div>
											<!-- end of single vertical product -->
											<!-- single vertical product -->
											<div class="single-auto-vertical-product d-flex">
												<div class="product-image">
													<a href="<?= Url::to([
														'/product/view',
														'id' => 1,
													]) ?>"><img width="250" height="250" src="/images/products/6.webp"
													            class="img-fluid" alt=""></a>
												</div>
												<div class="product-description">
													<h5 class="product-name">
														<a href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Printed
															Dress</a></h5>
													<div class="price-box">
														<h4>$ 12.00</h4>
													</div>

												</div>
											</div>
											<!-- end of single vertical product -->
											<!-- single vertical product -->
											<div class="single-auto-vertical-product d-flex">
												<div class="product-image">
													<a href="<?= Url::to([
														'/product/view',
														'id' => 1,
													]) ?>"><img width="250" height="250" src="/images/products/7.webp"
													            class="img-fluid" alt=""></a>
												</div>
												<div class="product-description">
													<h5 class="product-name">
														<a href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Faded
															Short Sleeve</a></h5>
													<div class="price-box">
														<h4>$ 12.00</h4>
													</div>

												</div>
											</div>
											<!-- end of single vertical product -->

										</div>
									</div>
								</div>
							</div>
							<!-- end of vertical auto slider container -->

							<!--							homepage sidebar banner-->
							<!--							<div class="sidebar">-->
							<!--								<div class="homepage-sidebar-banner-container">-->
							<!--									<a href="">-->
							<!--										<img width="269" height="389" src="/images/banners/banner-left.webp" class="img-fluid" alt="">-->
							<!--									</a>-->
							<!--								</div>-->
							<!--							</div>-->
							<!--							end of homepage sidebar banner-->

							<!-- vertical auto slider container -->
							<div class="sidebar">
								<h2 class="block-title">LATEST PRODUCTS</h2>
								<div class="vertical-product-slider-container">
									<div class="single-vertical-slider">
										<div class="vertical-auto-slider-product-list">
											<?php foreach ($latestProducts as $latestProduct) { ?>
												<!-- single vertical product -->
												<div class="single-auto-vertical-product d-flex">
													<div class="product-image">
														<a href="<?= Url::to([
															'/product/view',
															'id' => $latestProduct->id,
														]) ?>"><img width="250" height="250" src="<?= $latestProduct->firstProductImage->image ?>"
														            class="img-fluid" alt=""></a>
													</div>
													<div class="product-description">
														<h5 class="product-name">
															<a href="<?= Url::to([
																'/product/view',
																'id' => $latestProduct->id,
															]) ?>"><?= $latestProduct->name ?></a></h5>
														<div class="price-box">
															<h4>$ <?= number_format($latestProduct->productPriceMin->price, 2) ?> <?= ($latestProduct->productPriceMin->price != $latestProduct->productPriceMax->price ? "- $ " . number_format($latestProduct->productPriceMax->price, 2) : "") ?></h4>
														</div>

													</div>
												</div>
												<!-- end of single vertical product -->
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							<!-- end of vertical auto slider container -->
						</div>

						<!-- ====  End of Homepage sidebar  ==== -->

					</div>
					<div class="col-lg-9 col-md-8 mb-50">

						<div class="homepage-main-content">
							<!--Begin issue 25-->
							<?php foreach ($twoRandomCategories as $twoRandomCategory) : ?>
								<!-- horizontal product slider -->
								<div class="horizontal-product-slider">
									<div class="row">
										<div class="col-lg-12">
											<!-- Block title -->
											<div class="block-title">
												<h2><a href="<?= Url::to([
														'/category/view',
														'id' => $twoRandomCategory->id,
													]) ?>"><?= $twoRandomCategory->name ?></a></h2>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<!-- horizontal product slider container -->
											<div class="horizontal-product-list">
												<?php foreach ($twoRandomCategory->allProducts as $allProduct) : ?>
													<!-- single product -->
													<div class="single-product">
														<div class="single-product-content">
															<div class="product-image">
																<a href="<?= Url::to([
																	'/product/view',
																	'id' => $allProduct->id,
																]) ?>">
																	<img width="250" height="250" src="<?= $allProduct->firstProductImage->image ?>" class="img-fluid" alt="">
																	<img width="250" height="250" src="<?= $allProduct->firstProductImage->image ?>" class="img-fluid" alt="">
																</a>
																<div class="image-btn">
																	<a href="<?= Url::to([
																		'/product/view',
																		'id' => $allProduct->id,
																	]) ?>"><i class="fa fa-search"></i></a>
																	<a class="wish-list" href="javascript:;" data-href="<?= Url::to([
																		'/wishlist/toggle',
																		'id' => $allProduct->id,
																	]) ?>" style="background-color: <?php if (!$allProduct->isInWishlist()) {
																		echo '#eee;';
																	} else {
																		echo '#f36b63; color:#FFFFFF';
																	} ?>">
																		<i class="fa fa-heart-o"></i>
																	</a>
																</div>
															</div>
															<h5 class="product-name">
																<a href="<?= Url::to([
																	'/product/view',
																	'id' => $allProduct->id,
																]) ?>"><?= $allProduct->name ?></a></h5>
															<div class="price-box">
																<h4>$ <?= number_format($allProduct->productPriceMin->price, 2) ?> <?= ($allProduct->productPriceMin->price != $allProduct->productPriceMax->price ? "- $ " . number_format($allProduct->productPriceMax->price, 2) : "") ?></h4>
															</div>

															<a href="#" class="add-to-cart-btn" data-bs-toggle="modal"
															   data-bs-target="#add-to-cart-modal-container"><i class="fa fa-shopping-cart"></i> Add
																to cart</a>
														</div>
													</div>
													<!-- end of single product -->
												<?php endforeach; ?>
											</div>
											<!-- end of horizontal product slider container -->
										</div>
									</div>
								</div>
								<!-- end of horizontal product slider -->
							<?php endforeach; ?>
							<!--End issue 25-->
							<!-- vertical slider container -->
							<div class="vertical-product-slider-container">
								<div class="row">
									<div class="col-lg-6">
										<!-- ======  single vertical product slider  ======= -->

										<div class="single-vertical-slider">
											<h2 class="block-title vertical-slider-block-title">NEW PRODUCTS</h2>
											<div class="vertical-product-list">
												<?php foreach ($newProducts as $newProduct) { ?>
													<!-- single vertical product -->
													<div class="single-vertical-product d-flex">
														<div class="product-image">
															<a href="<?= Url::to([
																'/product/view',
																'id' => $newProduct->id,
															]) ?>"><img width="250" height="250" src="<?= $newProduct->firstProductImage->image ?>" class="img-fluid" alt=""></a>
														</div>
														<div class="product-description">
															<h5 class="product-name">
																<a href="<?= Url::to([
																	'/product/view',
																	'id' => $newProduct->id,
																]) ?>"><?= $newProduct->name ?></a>
															</h5>
															<div class="price-box">
																<h4> $ <?= number_format($newProduct->productPriceMin->price, 2) ?> <?= ($newProduct->productPriceMin->price != $newProduct->productPriceMax->price ? "- $ " . number_format($newProduct->productPriceMax->price, 2) : "") ?>
																</h4>
															</div>
														</div>
													</div>
													<!-- end of single vertical product -->
												<?php } ?>
											</div>
										</div>

										<!-- ====  End of single vertical product slider  ==== -->

									</div>
									<div class="col-lg-6">
										<!-- ======  single vertical product slider  ======= -->

										<div class="single-vertical-slider">
											<h2 class="block-title vertical-slider-block-title">FEATURED PRODUCTS</h2>
											<div class="vertical-product-list">
												<?php foreach ($featuredProducts as $featuredProduct) { ?>
													<!-- single vertical product -->
													<div class="single-vertical-product d-flex">
														<div class="product-image">
															<a href="<?= Url::to([
																'/product/view',
																'id' => $featuredProduct->id,
															]) ?>"><img width="250" height="250" src="<?= $featuredProduct->firstProductImage->image ?>" class="img-fluid" alt=""></a>
														</div>
														<div class="product-description">
															<h5 class="product-name">
																<a href="<?= Url::to([
																	'/product/view',
																	'id' => $featuredProduct->id,
																]) ?>"><?= $featuredProduct->name ?></a>
															</h5>
															<div class="price-box">
																<h4>$ <?= number_format($featuredProduct->productPriceMin->price, 2) ?> <?= ($featuredProduct->productPriceMin->price != $featuredProduct->productPriceMax->price ? "- $ " . number_format($featuredProduct->productPriceMax->price, 2) : "") ?></h4>
															</div>
														</div>
													</div>
													<!-- end of single vertical product -->
												<?php } ?>
											</div>
										</div>

										<!-- ====  End of single vertical product slider  ==== -->
									</div>
								</div>
							</div>
							<!-- end of vertical slider container -->
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- latest product section -->

		<div class="latest-product-section  mb-50">
			<div class="row">
				<div class="col-lg-12">
					<!-- Block title -->
					<div class="block-title">
						<h2>LATEST PRODUCTS</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<?php foreach ($latestProducts as $latestProduct) { ?>
					<div class="col-lg-2 col-md-4 col-sm-6">
						<!-- single latest product -->
						<div class="single-latest-product">
							<div class="product-image">
								<a href="<?= Url::to([
									'/product/view',
									'id' => $latestProduct->id,
								]) ?>">
									<img width="250" height="250" src="<?= $latestProduct->firstProductImage->image ?>" class="img-fluid" alt="">
									<img width="250" height="250" src="<?= $latestProduct->firstProductImage->image ?>" class="img-fluid" alt="">
								</a>
							</div>
							<div class="product-description">
								<h5 class="product-name"><a href="#"><?= $latestProduct->name ?></a></h5>
								<div class="price-box">
									<h4>$ <?= number_format($latestProduct->productPriceMin->price, 2) ?> <?= ($latestProduct->productPriceMin->price != $latestProduct->productPriceMax->price ? "- $ " . number_format($latestProduct->productPriceMax->price, 2) : "") ?></h4>
								</div>
							</div>

							<div class="latest-product-hover-content">
								<a href="javascript:;" data-bs-toggle="modal" data-bs-target="#add-to-cart-modal-container">
								</a>
								<p>
									<a href="<?= Url::to([
										'/product/view',
										'id' => $latestProduct->id,
									]) ?>">Quick View</a> |
									<a class="wish-list-2" href="javascript:;" data-href="<?= Url::to([
										'/wishlist/toggle',
										'id' => $allProduct->id,
									]) ?>" style="color: <?php if (!$allProduct->isInWishlist()) {
										echo '#333';
									} else {
										echo '#f36b63';
									} ?>">
										Wishlist
									</a>
								</p>
							</div>
						</div>
						<!-- end of single latest product -->
					</div>
				<?php } ?>
			</div>
		</div>

		<!-- end of latest product section -->
	</div>
</div>

<!--====  End of homepage content section  ====-->
<script>
	$(document).on('click', '.wish-list', function() {
		let th = $(this);
		$.ajax({
			type    : "post",
			cache   : false,
			url     : th.attr('data-href'),
			dataType: 'json',
			success : function(response) {
				if(response.message === "ADD") {
					th.css({
						"backgroundColor": "#f36b63",
						"color"          : "#FFFFFF"
					})
				} else {
					th.css({
						"backgroundColor": "#eee",
						"color"          : 'gray'
					})
				}
			},
			error   : function(e) {
				alert("Please Login");
			}
		})
	})
	$(document).on('click', '.wish-list-2', function() {
		let th = $(this);
		$.ajax({
			type    : "post",
			cache   : false,
			url     : th.attr('data-href'),
			dataType: 'json',
			success : function(response) {
				if(response.message === "ADD") {
					th.css({"color": "#f36b63"})
				} else {
					th.css({"color": '#000000'})
				}
			},
			error   : function(e) {
				alert("Please Login");
			}
		})
	})
</script>


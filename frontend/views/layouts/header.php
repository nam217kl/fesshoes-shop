<?php

use frontend\widgets\CartShopping;
use yii\helpers\Url;
use yii\web\View;
use frontend\widgets\CategorySearch;

/**
 * @var View $this
 */
?>
<!--===================================
=            Header            		   =
=====================================-->

<header>
	<!-- header top nav -->
	<div class="header-top-nav">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 offset-lg-3 col-md-6 col-sm-12"></div>

				<div class="col-md-6 col-sm-12">
					<!-- user information menu -->
					<div class="user-information-menu">
						<ul>
							<li><a href="<?=Url::to(['wishlist/index'])?>">My Wishlist</a>
								<span class="separator">|</span></li>
							<li><a href="checkout.html">Check Out</a>
								<span class="separator">|</span></li>
							<li><a href="cart.html">Cart (<span id="cart-status">Empty</span>)</a>
								<span
										class="separator">|</span></li>
							<?php
							if (Yii::$app->user->isGuest): ?>
								<li><a href="<?= Url::to(['/site/login']) ?>">Sign In</a></li>
							<?php else: ?>
								<li><a href="#">Welcome back, <?= Yii::$app->user->identity->username ?></a></li>
								<li><a href="<?= Url::to(['/site/logout']) ?>" data-method="post">Logout</a></li>
							<?php endif ?>
						</ul>
					</div>
					<!-- end of user information menu -->
				</div>

			</div>
		</div>
	</div>
	<!-- end of header top nav -->

	<!-- header bottom -->

	<!-- header content -->
	<div class="header-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-4 offset-lg-0 text-md-start text-sm-center">
					<!-- logo -->
					<div class="logo">
						<a href="<?= Url::home() ?>"><img width="167" height="69" src="/images/logo.webp" class="img-fluid" alt="logo"></a>
					</div>
					<!-- end of logo -->
				</div>
				<?= CategorySearch::widget() ?>
				<?= CartShopping::widget() ?>

			</div>
		</div>
	</div>
	<!-- end of header content -->

	<!-- header navigation section -->
	<div class="header-navigation">
		<div class="container">
			<div class="navigation-container">
				<div class="row">
					<div class="col-lg-3 d-none d-lg-block">
						<!-- ======  Header menu left text  ======= -->
						<p class="call-us-text">Call us 24/7: (+66) 123-456-789</p>
					</div>
					<div class="col-lg-9 col-md-12">

						<!-- Header navigation right side-->

						<!-- main menu start -->
						<div class="main-menu">
							<nav>
								<ul>
									<li class="active"><a href="<?= Url::home() ?>">Home</a></li>
									<li class="menu-item-has-children"><a href="#">Category</a>

										<!-- ======  Submenu block  ======= -->

										<ul class="sub-menu">
											<li class="menu-item-has-children">
												<a href="#">shop grid</a>
												<ul class="sub-menu">
													<li class="active">
														<a href="shop-left-sidebar.html">shop left sidebar</a>
													</li>
													<li>
														<a href="shop-left-sidebar-wide.html">shop left sidebar wide</a>
													</li>
													<li>
														<a href="shop-right-sidebar.html">shop right sidebar</a>
													</li>
													<li>
														<a href="shop-right-sidebar-wide.html">shop right sidebar wide</a>
													</li>
													<li>
														<a href="shop-no-sidebar-3.html">shop no sidebar 3 column</a>
													</li>
													<li>
														<a href="shop-no-sidebar-3-wide.html">shop no sidebar 3 column wide</a>
													</li>
													<li>
														<a href="shop-no-sidebar-4.html">shop no sidebar 4 column</a>
													</li>
													<li>
														<a href="shop-no-sidebar-4-wide.html">shop no sidebar 4 column wide</a>
													</li>
													<li>
														<a href="shop-no-sidebar-5.html">shop no sidebar 5 column</a>
													</li>
													<li>
														<a href="shop-no-sidebar-5-wide.html">shop no sidebar 5 column wide</a>
													</li>
												</ul>
											</li>
											<li class="menu-item-has-children">
												<a href="#">shop List</a>
												<ul class="sub-menu">
													<li><a href="shop-list.html">shop list</a></li>
													<li>
														<a href="shop-list-wide.html">shop list wide</a>
													</li>
													<li>
														<a href="shop-left-sidebar-list.html">shop left sidebar List</a>
													</li>
													<li>
														<a href="shop-left-sidebar-list-wide.html">shop left sidebar List wide</a>
													</li>
													<li>
														<a href="shop-right-sidebar-list.html">shop right sidebar List</a>
													</li>
													<li>
														<a href="shop-right-sidebar-list-wide.html">shop right sidebar List wide</a>
													</li>
												</ul>
											</li>
											<li class="menu-item-has-children">
												<a href="#">Shop product</a>
												<ul class="sub-menu">
													<li>
														<a href="single-product.html">shop product</a>
													</li>
													<li>
														<a href="single-product-wide.html">shop product wide</a>
													</li>
													<li>
														<a href="single-product-external.html">shop product external</a>
													</li>
													<li>
														<a href="single-product-external-wide.html">shop product external wide</a>
													</li>
													<li>
														<a href="single-product-variable.html">shop product variable</a>
													</li>
													<li>
														<a href="single-product-variable-wide.html">shop product variable wide</a>
													</li>
													<li>
														<a href="single-product-group.html">shop product group</a>
													</li>
													<li>
														<a href="single-product-group-wide.html">shop product group wide</a>
													</li>
												</ul>
											</li>
										</ul>
										<!-- ====  End of Submenu block  ==== -->

									</li>
									<li><a href="about.html">About</a></li>
									<li><a href="contact.html">Contact</a></li>
								</ul>
							</nav>

							<!-- Mobile Menu -->
							<div class="mobile-menu order-12 d-block d-lg-none"></div>

						</div>

						<!-- end of Header navigation right side-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end of header navigation section -->

	<!-- end of header bottom -->
</header>

<!--=====  End of Header  ======-->

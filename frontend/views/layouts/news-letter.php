<!--========================================
					=            newsletter section            =
					=========================================-->

<section class="newsletter-section">
	<div class="container">
		<div class="newsletter-container dark-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-12 col-sm-12">

						<!-- ======  Newsletter input box  ======= -->

						<div class="newsletter-wrapper d-md-flex">
							<!-- newsletter text -->
							<div class="newsletter-text">
								<h2>newsletter <span>Sign up for our newsletter</span></h2>
							</div>
							<!-- end of newsletter text -->

							<!-- newsletter input -->
							<div class="newsletter-input">
								<div class="input-group">
									<div class="input-group-append">
										<form id="mc-form" class="mc-form subscribe-form">
											<input type="email" id="mc-email" type="email" autocomplete="off"
											       placeholder="Enter your e-mail" required>
											<button id="mc-submit" type="submit">Subscribe</button>
										</form>

									</div>
								</div>
								<!-- mailchimp-alerts Start -->
								<div class="mailchimp-alerts">
									<div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
									<div class="mailchimp-success"></div><!-- mailchimp-success end -->
									<div class="mailchimp-error"></div><!-- mailchimp-error end -->
								</div><!-- mailchimp-alerts end -->
							</div>
							<!-- end of newsletter input -->
						</div>

						<!-- ====  End of Newsletter input box  ==== -->

					</div>
					<div class="col-lg-4 col-md-12 col-sm-12">
						<!-- ======  Social icon list  ======= -->

						<div class="social-icons text-end mt-5">
							<ul>
								<li>
									<a class="facebook-link" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a>
									<span class="popup">facebook</span>
								</li>
								<li>
									<a class="rss-link" href="http://rss.com/"><i class="fa fa-rss"></i></a>
									<span class="popup">rss</span>
								</li>
								<li>
									<a class="twitter-link" href="http://www.twitter.com/"><i class="fa fa-twitter"></i></a>
									<span class="popup">twitter</span>
								</li>
								<li>
									<a class="skype-link" href="http://www.skype.com/"><i class="fa fa-skype"></i></a>
									<span class="popup">Skype</span>
								</li>
								<li>
									<a class="dribbble-link" href="http://www.dribbble.com/"><i class="fa fa-dribbble"></i></a>
									<span class="popup">Dribbble</span>
								</li>
							</ul>
						</div>

						<!-- ====  End of Social icon list  ==== -->

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--====  End of newsletter section  ====-->

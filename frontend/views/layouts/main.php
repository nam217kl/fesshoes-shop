<?php
/** @var \yii\web\View $this */

/** @var string $content */

use frontend\assets\AppAsset;
use frontend\widgets\Brand;
use yii\bootstrap5\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html lang="<?= Yii::$app->language ?>" class="h-100">
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?php $this->registerCsrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
	</head>
	<body class="d-flex flex-column h-100">
	<?php $this->beginBody() ?>
	<div id="page">
		<div class="container">
			<div class="outer-row row">
				<div class="col-md-12">
					<?= $this->render('header') ?>

					<?= $content ?>

					<?= Brand::widget() ?>

					<?= $this->render('news-letter') ?>

					<?= $this->render('footer') ?>
				</div>
			</div>
		</div>
	</div>
	<!-- scroll to top  -->
	<a href="#" class="scroll-top"></a>
	<!-- end of scroll to top -->

	<?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage();

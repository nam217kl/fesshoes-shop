<!--=======================================
					=            brand logo slider            =
					========================================-->

<div class="brand-logo-slider mb-50">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12">
				<div class="brand-logo-list">
					<!-- ======  single brand logo block  ======= -->

					<div class="single-brand-logo">
						<a href="#">
							<img width="146" height="85" src="/images/brand-logos/1.webp" alt="">
						</a>
					</div>

					<!-- ====  End of single brand logo block  ==== -->

				</div>
			</div>
		</div>
	</div>
</div>

<!--====  End of brand logo slider  ====-->

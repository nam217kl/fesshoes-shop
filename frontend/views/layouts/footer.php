<!--============================
					=            footer            =
					=============================-->

<footer>
	<div class="container">
		<!-- footer navigation -->
		<div class="footer-navigation section-padding">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<!-- footer description -->
					<div class="footer-description">
						<div class="footer-logo">
							<img width="167" height="69" src="/images/logo.webp" alt="">
						</div>
						<p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
							sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id
							elit...</p>
					</div>
					<!-- end of footer description -->
				</div>
				<div class="col-lg-8 col-md-8">
					<!-- footer nav links -->
					<div class="row">
						<div class="col-lg-3 col-md-6">
							<!-- single footer nav block -->
							<div class="single-footer-nav-block">
								<h2 class="block-title">INFORMATION</h2>
								<ul class="footer-nav-links">
									<li><a href="shop-left-sidebar.html">Specials</a></li>
									<li><a href="shop-left-sidebar.html">New Products</a></li>
									<li><a href="shop-left-sidebar.html">Best Sellers</a></li>
									<li><a href="contact.html">Contact Us</a></li>
									<li><a href="about.html">About Us</a></li>
								</ul>
							</div>
							<!-- end of single footer nav block -->
						</div>
						<div class="col-lg-3 col-md-6">
							<!-- single footer nav block -->
							<div class="single-footer-nav-block">
								<h2 class="block-title"><a href="my-account.html">MY ACCOUNT</a>
								</h2>
								<ul class="footer-nav-links">
									<li><a href="#">My Orders</a></li>
									<li><a href="#">My Credit Slips</a></li>
									<li><a href="my-account.html">My Addresses</a></li>
									<li><a href="my-account.html">My Personal Info</a></li>
								</ul>
							</div>
							<!-- end of single footer nav block -->
						</div>
						<div class="col-lg-3 col-md-6">
							<!-- single footer nav block -->
							<div class="single-footer-nav-block">
								<h2 class="block-title">CATEGORIES</h2>
								<ul class="footer-nav-links">
									<li><a href="shop-left-sidebar.html">Football</a></li>
									<li><a href="shop-left-sidebar.html">Tennis</a></li>
									<li><a href="shop-left-sidebar.html">Formula</a></li>
									<li><a href="shop-left-sidebar.html">Cricket</a></li>
									<li><a href="shop-left-sidebar.html">Baseball</a></li>
								</ul>
							</div>
							<!-- end of single footer nav block -->
						</div>
						<div class="col-lg-3 col-md-6">
							<!-- single footer nav block -->
							<div class="single-footer-nav-block">
								<h2 class="block-title">OUR SERVICES</h2>
								<ul class="footer-nav-links">
									<li><a href="store.html">Our Stores</a></li>
									<li><a href="about.html">Information</a></li>
									<li><a href="#">Privacy Policy</a></li>
									<li><a href="#">Terms & Condition</a></li>
								</ul>
							</div>
							<!-- end of single footer nav block -->
						</div>
					</div>
					<!-- end of footer nav links -->

				</div>
			</div>
		</div>
		<!-- end of footer navigation -->

		<!-- copyright section -->
		<div class="copyright-section">
			<div class="copyright-container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12">
						<p class="copyright-text text-center text-md-start">Copyright &copy; 2021
							<a href="#">Rossi</a>.
							All Rights Reserved</p>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12">
						<div class="payment-logos text-md-end text-center">
							<img width="252" height="28" src="/images/payment.webp" alt="payment logo">
						</div>
					</div>
				</div>
			</div>
			<!-- end of copyright section -->
		</div>
	</div>
</footer>

<!--====  End of footer  ====-->

<?php
/**
 * @var yii\web\View $this
 * @var Cart[]       $cartview
 */

use common\models\Cart;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Giỏ hàng';
?>
<div id="page">
	<div class="container">
		<div class="col-md-12">
			<div class="page-content mt-50 mb-10">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="page-title">
								<h2>Giỏ hàng</h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<form action="" method="post">
								<!-- Cart Table -->
								<div class="cart-table table-responsive mb-40">
									<table class="table">
										<thead>
										<tr>
											<th class="pro-thumbnail">Ảnh</th>
											<th class="pro-title">Sản phẩm</th>
											<th class="pro-title">Phân loại</th>
											<th class="pro-price">Giá</th>
											<th class="pro-quantity">Số lượng</th>
											<th class="pro-subtotal">Tổng tiền</th>
											<th class="pro-remove">Xóa</th>
										</tr>
										</thead>
										<tbody>
										<?php $total = 0 ?>
										<?php foreach ($cartview as $cart): ?>
											<tr>
												<td class="pro-thumbnail">
													<?= Html::hiddenInput("Cart[" . $cart->id . "][id]", $cart->id) ?>
													<a href="<?= Url::to([
														'/product/view',
														'id' => $cart->productDetail->product_id,
													]) ?>"><img class="img-fluid" width="250" height="250" src="<?= $cart->productDetail->product->firstProductImage->image ?>" alt="Product"></a>
												</td>
												<td class="pro-title">
													<a href="<?= Url::to([
														'/product/view',
														'id' => $cart->productDetail->product_id,
													]) ?>"><?= $cart->productDetail->product->name ?></a>
												</td>
												<td class="pro-title">
													<span><?= $cart->productDetail->color ?>, <?= $cart->productDetail->size ?></span>
												</td>
												<td class="pro-price">
													<span>$<?= $cart->productDetail->price ?></span>
												</td>
												<td class="pro-quantity">
													<span class="pro-qty-cart counter">
														<input type="text" name="Cart[<?= $cart->id ?>][quantity]" value="<?= $cart->quantity ?>">
													</span>
												</td>
												<td class="pro-subtotal">
													<span>$<?= $cart->productDetail->price * $cart->quantity ?></span>
												</td>
												<td class="pro-remove">
													<a href="javascript:;" data-href="<?= Url::to([
														'cart/delete',
														'id' => $cart->id,
													]) ?>"><i class="fa fa-trash-o"></i> </a>
												</td>
											</tr>
											<?php
											$subtotal = $cart->productDetail->price * $cart->quantity;
											$total    += $subtotal;
											?>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
								<div class="cart-summary">
									<div class="cart-summary-wrap">
										<h4>Đơn hàng</h4>
										<h2>Tổng cộng <span>$<?= $total ?></span></h2>
									</div>
									<div class="cart-summary-button">
										<button class="checkout-btn">
											<a href="<?= Url::to([
												'/checkout/index',
											]) ?>">Checkout</a>
										</button>
										<button class="update-btn">Update Cart</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).on('click', '.pro-remove a', function() {
		let th = $(this);
		$.ajax({
			type   : "POST",
			cache  : false,
			url    : th.attr('data-href'),
			success: function() {
				th.closest('tr').fadeOut();
			},
			error  : function(e) {
				alert(e);
			}
		});
	})
</script>

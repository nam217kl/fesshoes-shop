<?php
/**
 * @var yii\web\View   $this
 * @var Product        $product
 * @var Product[]      $productsRelated
 * @var ProductImage[] $productImages
 * @var array          $product_size_colors
 * @var integer        $product_detail_quantities
 * @var ProductTag[]   $productTags
 */

use common\helpers\StringHelper;
use common\models\Product;
use common\models\ProductImage;
use common\models\ProductTag;
use yii\helpers\Json;
use yii\helpers\Url;

$this->title = $product->name;
?>
<!--===========================================
=            Single product content section            =
============================================-->
<script>
	let product_size_colors = <?=Json::encode($product_size_colors)?>;
	let size, color;
</script>
<section class="single-product-page-content">
	<div class="container">
		<div class="row">

			<div class="col-lg-9 col-md-12">
				<div class="row">
					<div class="col-lg-5 col-md-7">

						<div class="single-product-page-image-gallery">
							<!-- product quickview image gallery -->
							<!--Modal Tab Content Start-->
							<div class="tab-content product-details-large   new-badge">
								<div class="tab-pane fade show active" id="single-slide1"
								     role="tabpanel" aria-labelledby="single-slide-tab-1">
									<!--Single Product Image Start-->
									<div class="single-product-img img-full">
										<img width="458" height="458" src="<?= $product->firstProductImage->image ?>"
										     class="img-fluid" alt="">
										<a href="<?= $product->firstProductImage->image ?>"
										   class="big-image-popup"><i
													class="fa fa-search-plus"></i></a>
									</div>
									<!--Single Product Image End-->
								</div>
								<?php foreach ($productImages as $productImage): ?>
									<div class="tab-pane fade" id="single-slide<?= $productImage->id ?>" role="tabpanel"
									     aria-labelledby="single-slide-tab-2">
										<!--Single Product Image Start-->
										<div class="single-product-img img-full">
											<img width="458" height="458" src="<?= $productImage->image ?>"
											     class="img-fluid" alt="">
											<a href="<?= $productImage->image ?>"
											   class="big-image-popup"><i
														class="fa fa-search-plus"></i></a>
										</div>
										<!--Single Product Image End-->
									</div>
								<?php endforeach; ?>
							</div>
							<!--Modal Content End-->
							<!--Modal Tab Menu Start-->

							<div class="single-product-menu">
								<div class="nav single-slide-menu" role="tablist">
									<?php foreach ($productImages as $productImage): ?>
										<div class="single-tab-menu img-full">
											<a data-bs-toggle="tab" id="single-slide-tab-1"
											   href="#single-slide<?= $productImage->id ?>"><img
														width="458" height="458" src="<?= $productImage->image ?>"
														class="img-fluid" alt=""></a>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<!--Modal Tab Menu End-->
							<!-- end of product quickview image gallery -->
						</div>
					</div>
					<div class="col-lg-7 col-md-5">
						<!-- product quick view description -->
						<div class="product-options">
							<h2 class="product-title"><?= $product->name ?></h2>
							<p class="condition"><span>Condition: </span><?= Product::CONDITION[$product->condition] ?>
							</p>
							<h2 class="product-price">$ <?= number_format($product->productPriceMin->price, 2) ?> <?= ($product->productPriceMin->price != $product->productPriceMax->price ? "- $ " . number_format($product->productPriceMax->price, 2) : "") ?></h2>
							<p class="product-description"><?= $product->short_desc ?></p>

							<div class="single-product-user-action">
								<ul>
									<li><a href="#"> <i class="fa fa-envelope-o"></i> Send to a
											friend</a></li>
									<li><a href="#"> <i class="fa fa-print"></i> Print</a></li>
									<li>
										<a class="wish-list" href="javascript:;" data-href="<?= Url::to([
											'/wishlist/toggle',
											'id' => $product->id,
										]) ?>" style=" text color: <?php if (!$product->isInWishlist()) {
											echo 'gray';
										} else {
											echo '#f36b63';
										} ?>">
											<i class="fa fa-heart-o"></i> Add to
											wishlist
										</a>
									</li>
								</ul>
							</div>
							<div class="social-share-buttons">
								<ul>
									<li><a class="twitter" href="#"><i class="fa fa-twitter"></i>
											Tweet</a></li>
									<li><a class="facebook" href="#"><i class="fa fa-facebook"></i>
											Share</a></li>
									<li><a class="google-plus" href="#"><i
													class="fa fa-google-plus"></i>
											Google+</a></li>
									<li><a class="pinterest" href="#"><i
													class="fa fa-pinterest"></i>
											Pinterest</a></li>
								</ul>
							</div>

							<p class="size">
								Size: <br>
								<select name="chooseSize" id="chooseSize">
									<option>Please select</option>
									<?php foreach ($product->sizes as $size) : ?>
										<option value="<?= $size ?>"><?= $size ?></option>
									<?php endforeach; ?>
								</select>
							</p>
							<p class="color">
								Color: <br><br>
								<?php foreach ($product->colors as $color) : ?>
									<a class="chooseColor" href="javascript:;" data-value="<?= StringHelper::removeSign($color) ?>"><span class="product-color <?= StringHelper::removeSign($color) ?>"></span></a>
								<?php endforeach; ?>
							</p>
							<div class="error"></div>
							<p class="stock-details"><?= $product_detail_quantities ?> items<span
										class="stock-status in-stock">In
                                                        Stock</span></p>
							<p class="quantity">Quantity:

								<span class="pro-qty counter"><input type="text" value="1" name="quantity"
								                                     class="mr-5"></span>

							</p>
							<a style="margin-top: 5px" href="#" class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i>
								Add to Cart</a>
						</div>
						<!-- end ofproduct quick view description -->
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<div class="product-description-tab-container section-padding">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active" id="home-tab" data-bs-toggle="tab"
									   href="#more-info" role="tab" aria-selected="true">MORE
										INFO</a>
								</li>
							</ul>
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="more-info"
								     role="tabpanel" aria-labelledby="home-tab">
									<p><?= $product->long_desc ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- related horizontal product slider -->
				<div class="horizontal-product-slider">
					<div class="row">
						<div class="col-lg-12">
							<div class="block-title">
								<h2><a href="#">RELATED PRODUCTS</a></h2>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<!-- horizontal product slider container -->
							<div class="horizontal-product-list">
								<?php foreach ($productsRelated as $product): ?>
									<!-- single product -->
									<div class="single-product">
										<div
												class="single-product-content single-related-product-content">
											<div class="product-image">
												<a href="<?= Url::to([
													'/product/view',
													'id' => $product->id,
												]) ?>">
													<img width="250" height="250" src="<?= $product->firstProductImage->image ?>"
													     class="img-fluid" alt="">
													<img width="250" height="250" src="<?= $product->firstProductImage->image ?>"
													     class="img-fluid" alt="">
												</a>
											</div>
											<h5 class="product-name"><a
														href="<?= Url::to([
															'/product/view',
															'id' => $product->id,
														]) ?>"><?= $product->name ?></a>
											</h5>
											<div class="price-box">
												<h4>$ <?= number_format($product->productPriceMin->price, 2) ?> <?= ($product->productPriceMin->price != $product->productPriceMax->price ? "- $ " . number_format($product->productPriceMax->price, 2) : "") ?></h4>
											</div>
										</div>
									</div>
									<!-- end of single product -->
								<?php endforeach; ?>
							</div>
							<!-- end of horizontal product slider container -->
						</div>
					</div>
				</div>
			</div>
			<!-- end of related horizontal product slider -->

			<div class="col-lg-3 col-md-4">
				<!-- ======  Single product sidebar  ====== -->

				<div class="single-product-page-sidebar">
					<!-- vertical auto slider container -->
					<div class="sidebar">
						<h2 class="block-title">BESTSELLER</h2>
						<div class="vertical-product-slider-container mb-50">

							<div class="single-vertical-slider">

								<div class="vertical-auto-slider-product-list">
									<!-- single vertical product -->
									<div class="single-auto-vertical-product d-flex">
										<div class="product-image">
											<a href="<?= Url::to([
												'/product/view',
												'id' => 1,
											]) ?>"><img
														width="250" height="250" src="/images/products/1.webp"
														class="img-fluid" alt=""></a>
										</div>
										<div class="product-description">
											<h5 class="product-name"><a
														href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Faded
													Short Sleeve</a></h5>
											<div class="price-box">
												<h4>$ 12.00</h4>
											</div>

										</div>
									</div>
									<!-- end of single vertical product -->
									<!-- single vertical product -->
									<div class="single-auto-vertical-product d-flex">
										<div class="product-image">
											<a href="<?= Url::to([
												'/product/view',
												'id' => 1,
											]) ?>"><img
														width="250" height="250" src="/images/products/2.webp"
														class="img-fluid" alt=""></a>
										</div>
										<div class="product-description">
											<h5 class="product-name"><a
														href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Printed
													Dress</a></h5>
											<div class="price-box">
												<h4>$ 12.00</h4>
											</div>

										</div>
									</div>
									<!-- end of single vertical product -->
									<!-- single vertical product -->
									<div class="single-auto-vertical-product d-flex">
										<div class="product-image">
											<a href="<?= Url::to([
												'/product/view',
												'id' => 1,
											]) ?>"><img
														width="250" height="250" src="/images/products/3.webp"
														class="img-fluid" alt=""></a>
										</div>
										<div class="product-description">
											<h5 class="product-name"><a
														href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Faded
													Short Sleeve</a></h5>
											<div class="price-box">
												<h4>$ 12.00</h4>
											</div>

										</div>
									</div>
									<!-- end of single vertical product -->
									<!-- single vertical product -->
									<div class="single-auto-vertical-product d-flex">
										<div class="product-image">
											<a href="<?= Url::to([
												'/product/view',
												'id' => 1,
											]) ?>"><img
														width="250" height="250" src="/images/products/4.webp"
														class="img-fluid" alt=""></a>
										</div>
										<div class="product-description">
											<h5 class="product-name"><a
														href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Printed
													Dress</a></h5>
											<div class="price-box">
												<h4>$ 12.00</h4>
											</div>

										</div>
									</div>
									<!-- end of single vertical product -->
									<!-- single vertical product -->
									<div class="single-auto-vertical-product d-flex">
										<div class="product-image">
											<a href="<?= Url::to([
												'/product/view',
												'id' => 1,
											]) ?>"><img
														width="250" height="250" src="/images/products/5.webp"
														class="img-fluid" alt=""></a>
										</div>
										<div class="product-description">
											<h5 class="product-name"><a
														href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Faded
													Short Sleeve</a></h5>
											<div class="price-box">
												<h4>$ 12.00</h4>
											</div>

										</div>
									</div>
									<!-- end of single vertical product -->
									<!-- single vertical product -->
									<div class="single-auto-vertical-product d-flex">
										<div class="product-image">
											<a href="<?= Url::to([
												'/product/view',
												'id' => 1,
											]) ?>"><img
														width="250" height="250" src="/images/products/6.webp"
														class="img-fluid" alt=""></a>
										</div>
										<div class="product-description">
											<h5 class="product-name"><a
														href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Printed
													Dress</a></h5>
											<div class="price-box">
												<h4>$ 12.00</h4>
											</div>

										</div>
									</div>
									<!-- end of single vertical product -->
									<!-- single vertical product -->
									<div class="single-auto-vertical-product d-flex">
										<div class="product-image">
											<a href="<?= Url::to([
												'/product/view',
												'id' => 1,
											]) ?>"><img
														width="250" height="250" src="/images/products/7.webp"
														class="img-fluid" alt=""></a>
										</div>
										<div class="product-description">
											<h5 class="product-name"><a
														href="<?= Url::to([
															'/product/view',
															'id' => 1,
														]) ?>">Faded
													Short Sleeve</a></h5>
											<div class="price-box">
												<h4>$ 12.00</h4>
											</div>

										</div>
									</div>
									<!-- end of single vertical product -->

								</div>
							</div>
						</div>
					</div>
					<!-- end of vertical auto slider container -->

					<!-- homepage sidebar banner -->
					<div class="sidebar">
						<div class="homepage-sidebar-banner-container mb-50">
							<a href="shop-left-sidebar.html">
								<img width="269" height="389" src="/images/banners/banner-left.webp" class="img-fluid"
								     alt="">
							</a>
						</div>
					</div>
					<!-- end of homepage sidebar banner -->

					<!-- tag container -->
					<div class="sidebar">
						<h2 class="block-title">TAGS</h2>
						<div class="tag-container">
							<ul>
								<?php foreach ($productTags as $productTag): ?>
									<li><a href="<?= Url::to([
											'/product/tag',
											'tag_id' => $productTag->tag_id,
										]) ?>"><?= $productTag->tag->name ?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
					<!-- end of tag container -->

				</div>

				<!-- ====  End of Single product sidebar  ==== -->

			</div>
		</div>
	</div>
</section>

<!--====  End of Single product content section  ====-->
<script>
	$(document).on('change', '#chooseSize', function() {
		let th = $(this);
		size   = th.val();
		displayPrice(size, color);
		displayQuantity(size, color);
	});
	$(document).on('click', '.chooseColor', function() {
		let th = $(this);
		$('.chooseColor').removeClass('active');
		th.addClass('active');
		color = th.data("value");
		displayPrice(size, color);
		displayQuantity(size, color);
	});

	function displayPrice(size, color) {
		if(size !== undefined && color !== undefined) {
			$(".product-price").html('$ ' + product_size_colors[size + '_' + color].price + '.00');
		}
	}

	function displayQuantity(size, color) {
		if(size !== undefined && color !== undefined) {
			let quantity = product_size_colors[size + '_' + color].quantity;
			let html     = quantity + ' items';
			if(quantity <= 0) {
				html += '<span class="stock-status out-of-stock">Out Stock</span>';
			} else {
				html += '<span class="stock-status in-stock">In Stock</span>';
			}
			$(".stock-details").html(html);
		}
	}

	$(document).on('click', '.add-to-cart-btn', function() {
		<?php if(Yii::$app->user->isGuest) :?>
		alert('Please login first!');
		<?php else:?>
		if(size !== undefined && color !== undefined) {
			let quantity = $('[name="quantity"]').val();
			if(quantity > 0) {
				console.log(product_size_colors[size + '_' + color].id);
				$(".error").html('');
				var cart      = $(".shopping-cart");
				var parent    = $(this).closest(".row");
				var imgtodrag = parent.find("img.img-fluid").eq(0);
				if(imgtodrag) {
					var imgclone = imgtodrag.clone()
						.offset({
							top : imgtodrag.offset().top,
							left: imgtodrag.offset().left
						})
						.css({
							"opacity" : "0.5",
							"position": "absolute",
							"height"  : "150px",
							"width"   : "150px",
							"z-index" : "100"
						})
						.appendTo($("body"))
						.animate({
							"top"   : cart.offset().top + 10,
							"left"  : cart.offset().left + 10,
							"width" : 75,
							"height": 75
						}, 1000, "easeInOutExpo");
					$.ajax({
						type    : 'post',
						cache   : false,
						data    : 'quantity=' + $('[name="quantity"]').val(),
						url     : '<?=Url::to([
							'/cart/add',
							'product_detail_id' => '',
						])?>' + product_size_colors[size + '_' + color].id,
						dataType: 'json',
						success : function(response) {
							$("#cartStatus").html(response.count);
							let html = '<div class="cart-float-single-item d-flex">' +
								'<span class="remove-item"><a href="#"><i class="fa fa-trash"></i></a></span>' +
								'<div class="cart-float-single-item-image">' +
								'<img width="250" height="250" src="' + response.image + '" class="img-fluid" alt="">' +
								'</div>' +
								'<div class="cart-float-single-item-desc">' +
								'<p class="product-title"><span class="count">1x</span> ' +
								'<a href="' + response.url + '">' + response.name + '</a></p>' +
								'<p class="size">' +
								'<span>' + response.color + ' ' + response.size + '</span></p>' +
								'<p class="price">$' + response.price + '</p>' +
								'</div>' +
								'</div>';
							$(".cart-items").append(html);
						},
						error   : function() {
							alert('Please login');
						}
					})
					setTimeout(function() {
						cart.effect("shake", {
							times: 2
						}, 200);
					}, 1500);

					imgclone.animate({
						"width" : 0,
						"height": 0
					}, function() {
						$(this).detach()
					});
				}
			} else {
				$(".error").html('Quantity must be greater than 0');
			}
		} else {
			$(".error").html('Please select color & size');
		}
		<?php endif;?>
	});
	$(document).on('click', '.wish-list', function() {
		let th = $(this);
		$.ajax({
			type    : "post",
			cache   : false,
			url     : th.attr('data-href'),
			dataType: 'json',
			success : function(response) {
				if(response.message === "ADD") {
					th.css({
						"color"          : "#f30000"
					})
				} else {
					th.css({
						"color"          : 'gray'
					})
				}
			},
			error   : function(e) {
				alert('Please Login');
			}
		})
	});
</script>


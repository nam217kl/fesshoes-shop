<?php
/**
 * @var yii\web\View              $this
 * @var \common\models\Wishlist[] $wishlists
 * @var Product                   $product
 */

use common\models\Product;
use yii\helpers\Url;

$this->title = 'Wishlist';
?>
<div class="page-content mt-50 mb-10">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-title">
					<h2>Wishlist</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<form action="#">
					<!-- Cart Table -->
					<div class="cart-table table-responsive mb-40">
						<table class="table">
							<thead>
							<tr>
								<th class="pro-thumbnail">Image</th>
								<th class="pro-title">Product</th>
								<th class="pro-price">Price</th>
								<th class="pro-remove">Remove</th>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($wishlists as $wishlist): ?>
								<tr>
									<td class="pro-thumbnail"><a
												href="<?= Url::to([
													'/product/view',
													'id' => $wishlist->product_id,
												]) ?>"><img
													class="img-fluid" width="250" height="250" src="<?= $wishlist->product->firstProductImage->image ?>"
													alt="Product"></a></td>
									<td class="pro-title"><a
												href="<?= Url::to([
													'/product/view',
													'id' => $wishlist->product->id,
												]) ?>"><?= $wishlist->product->name ?></a></td>
									<td class="pro-price">
										<span>$ <?= number_format($wishlist->product->productPriceMin->price, 2) ?> <?= ($wishlist->product->productPriceMin->price != $wishlist->product->productPriceMax->price ? "- $ " . number_format($wishlist->product->productPriceMax->price, 2) : "") ?></span>
									</td>
									<td class="pro-remove"><a href="javascript:;" data-href="<?= Url::to([
											'/wishlist/delete',
											'id' => $wishlist->id,
										]) ?>"><i class="fa fa-trash-o"></i></a></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).on('click', '.pro-remove a', function() {
		let th = $(this);
		$.ajax({
			type   : "post",
			cache  : false,
			url    : th.attr('data-href'),
			success: function() {
				th.closest('tr').fadeOut();
			},
			error  : function(e) {
				alert('Please login');
			}
		})
	})
</script>

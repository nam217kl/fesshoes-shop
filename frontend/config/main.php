<?php

use yii\web\UrlManager;

$params = array_merge(require __DIR__ . '/../../common/config/params.php', require __DIR__ . '/../../common/config/params-local.php', require __DIR__ . '/params.php', require __DIR__ . '/params-local.php');
return [
	'id'                  => 'app-frontend',
	'basePath'            => dirname(__DIR__),
	'bootstrap'           => ['log'],
	'controllerNamespace' => 'frontend\controllers',
	'components'          => [
		'assetManager' => [
			'bundles' => [
				'yii\bootstrap5\BootstrapAsset'        => [
					'basePath' => '@webroot',
					'baseUrl'  => '@web',
					'css'      => [
						'css/bootstrap.min.css',
					],
				],
				'\yii\web\JqueryAsset'                 => [
					'basePath' => '@webroot',
					'baseUrl'  => '@web',
					'js'       => [
						'js/vendor/jquery.min.js',
					],
				],
				'\yii\bootstrap5\BootstrapPluginAsset' => [
					'basePath' => '@webroot',
					'baseUrl'  => '@web',
					'js'       => [
						'js/bootstrap.min.js',
					],
				],
			],
		],
		'request'      => [
			'csrfParam' => '_csrf-frontend',
		],
		'user'         => [
			'identityClass'   => 'common\models\User',
			'enableAutoLogin' => true,
			'identityCookie'  => [
				'name'     => '_identity-frontend',
				'httpOnly' => true,
			],
		],
		'session'      => [
			// this is the name of the session cookie used for login on the frontend
			'name' => 'advanced-frontend',
		],
		'log'          => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => \yii\log\FileTarget::class,
					'levels' => [
						'error',
						'warning',
					],
				],
			],
		],
		'errorHandler' => [
			'errorAction' => 'site/error',
		],

		'urlManager'   => [
			'class'           => UrlManager::class,
			'enablePrettyUrl' => true,
			'showScriptName'  => false,
			'normalizer'      => [
				'class'                  => 'yii\web\UrlNormalizer',
				'collapseSlashes'        => true,
				'normalizeTrailingSlash' => true,
			],
			'rules' => [
				''                                       => 'site/index',
				'<action:(index|login|logout)>'          => 'site/<action>',
				'<controller>/<id:\d+>-<slug:.*?>'       => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>'          => '<controller>/<action>',
			]
		],

	],
	'params'              => $params,
];

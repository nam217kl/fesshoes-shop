<?php

namespace frontend\controllers;

use common\helpers\StringHelper;
use common\models\Category;
use common\models\Product;
use common\models\ProductDetail;
use common\models\ProductImage;
use common\models\ProductTag;
use common\models\Tag;
use frontend\models\CategorySearchForm;
use frontend\models\search\ProductSearch;
use yii\helpers\Url;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class ProductController extends Controller {

	public function actionView($id) {
		$product                   = Product::find()->andWhere(['id' => $id])->one();
		$productTags               = ProductTag::find()->andWhere(['product_tag.product_id' => $id])->all();
		$list_tags                 = ArrayHelper::map($productTags, 'id', 'tag_id');
		$productsRelated           = Product::find()->leftJoin('product_tag', 'product.id=product_tag.product_id')->select([
			'product.id as id',
			'product.name',
			'tag_id',
		])->andWhere(['product_tag.tag_id' => $list_tags])->orderBy(new Expression('rand()'))->limit(10)->all();
		$productImages             = ProductImage::find()->andWhere(['product_id' => $id])->all();
		$productDetails            = ProductDetail::find()->andWhere(['product_id' => $id])->all();
		$product_detail_quantities = ProductDetail::find()->andWhere(['product_id' => $id])->sum('quantity');
		$product_size_colors       = ArrayHelper::map($productDetails, function(ProductDetail $data) {
			return $data->size . '_' . StringHelper::removeSign($data->color);
		}, function(ProductDetail $data) {
			return [
				'id'       => $data->id,
				'price'    => $data->price,
				'quantity' => $data->quantity,
			];
		});
		return $this->render('view', [
			'product'                   => $product,
			'productImages'             => $productImages,
			'product_size_colors'       => $product_size_colors,
			'product_detail_quantities' => $product_detail_quantities,
			'productsRelated'           => $productsRelated,
			'productTags'               => $productTags,
		]);
	}

	public function actionIndex($limit = ProductSearch::LIMIT_20, $sort = ProductSearch::SORT_NEWEST_ITEMS) {
		$model = new CategorySearchForm();
		$model->load(\Yii::$app->request->get());
		$keyword     = $model->keyword;
		$category_id = $model->category_id;
		$searchModel = new ProductSearch();
		if ($category_id != null) {
			$searchModel->super_category_id = $category_id;
		} else {
			$searchModel->super_category_id = '';
		}
		$searchModel->limit   = $limit;
		$searchModel->sort    = $sort;
		$searchModel->keyword = $keyword;
		$dataProvider         = $searchModel->search(\Yii::$app->request->queryParams);
		$productQuery         = Product::find()->select('category_id')->andWhere([
			'LIKE',
			'name',
			$keyword,
		]);
		if ($category_id != null) {
			$superCategory = Category::findOne($category_id);
			if ($superCategory !== null) {
				$categories_id      = ArrayHelper::map($superCategory->categories, 'id', 'id');
				$categories_id[- 1] = $category_id;
				ksort($categories_id);
				$productQuery->andWhere(['category_id' => $categories_id]);
			}
			$categories = Category::find()->andWhere([
				'id' => $productQuery,
			])->all();
		} else {
			//todo $productQuery tại vị trí này ko bao gồm category có parent_id is null
			$categories = Category::find()->andWhere([
				'id' => $productQuery,
			])->andWhere("parent_id IS NULL")->all();
		}
		// dataprovider -> mảng product_> product tag-> tag-> đếm tag ra weght
		$tag_data = [];
		foreach ($dataProvider->models as $product) {
			/**@var Product $product */
			foreach ($product->productTags as $productTag) {
				if (isset($tag_data[$productTag->tag_id])) {
					$tag_data[$productTag->tag_id]['weight'] += 1;
				} else {
					$tag_data[$productTag->tag_id] = [
						'text'   => $productTag->tag->name,
						'weight' => 1,
						'link'   => Url::to([
							'/product/tag',
							'tag_id' => $productTag->tag_id,
						]),
					];
				}
			}
		}
		$tag_data = array_values($tag_data);
		return $this->render('index', [
			'keyword'      => $keyword,
			'category_id'  => $category_id,
			'limit'        => $limit,
			'sort'         => $sort,
			'dataProvider' => $dataProvider,
			'categories'   => $categories,
			'tag_data'     => $tag_data,
		]);
	}

	public function actionTag($tag_id, $limit = ProductSearch::LIMIT_20, $sort = ProductSearch::SORT_NEWEST_ITEMS) {
		$categories  = Category::find()->andWhere(['parent_id' => null])->all();
		$searchModel = new ProductSearch();
		if ($tag_id != null) {
			$searchModel->tag_id = $tag_id;
		}
		$searchModel->limit = $limit;
		$searchModel->sort  = $sort;
		$dataProvider       = $searchModel->search(\Yii::$app->request->queryParams);
		$tag_data           = [];
		foreach ($dataProvider->models as $product) {
			/**@var Product $product */
			foreach ($product->productTags as $productTag) {
				if (isset($tag_data[$productTag->tag_id])) {
					$tag_data[$productTag->tag_id]['weight'] += 1;
				} else {
					$tag_data[$productTag->tag_id] = [
						'text'   => $productTag->tag->name,
						'weight' => 1,
						'link'   => Url::to([
							'/product/tag',
							'tag_id' => $productTag->tag_id,
						]),
					];
				}
			}
		}
		$tag_data = array_values($tag_data);
		return $this->render('tag', [
			'dataProvider' => $dataProvider,
			'categories'   => $categories,
			'tag_data'     => $tag_data,
			'limit'        => $limit,
			'sort'         => $sort,
		]);
	}
}

<?php

namespace frontend\controllers;

use frontend\models\search\ProductSearch;
use common\models\Category;
use common\models\Product;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

class CategoryController extends Controller {

	/**
	 * tôi đã code xong cái 54 có đuôi là -2 rồi
	 *
	 * @param $id
	 *
	 * @return string
	 */
	public function actionView($limit = ProductSearch::LIMIT_20, $sort = ProductSearch::SORT_NEWEST_ITEMS, $category_id = null){
		$searchModel = new ProductSearch();
		if ($category_id != null) {
			$searchModel->super_category_id = $category_id;
		} else {
			$searchModel->super_category_id = '';
		}
		$searchModel->limit = $limit;
		$searchModel->sort  = $sort;
		$dataProvider       = $searchModel->search(\Yii::$app->request->queryParams);
		$productQuery       = Product::find()->select('category_id');
		if ($category_id != null) {
			$superCategory = Category::findOne($category_id);
			if ($superCategory !== null) {
				$categories_id      = ArrayHelper::map($superCategory->categories, 'id', 'id');
				$categories_id[- 1] = $category_id;
				ksort($categories_id);
				$productQuery->andWhere(['category_id' => $categories_id]);
			}
			$categories = Category::find()->andWhere([
				'id' => $productQuery,
			])->all();
		} else {
			//todo $productQuery tại vị trí này ko bao gồm category có parent_id is null
			$categories = Category::find()->andWhere([
				'id' => $productQuery,
			])->andWhere("parent_id IS NULL")->all();
		}
		$tag_data = [];
		foreach ($dataProvider->models as $product) {
			/**@var Product $product */
			foreach ($product->productTags as $productTag) {
				if (isset($tag_data[$productTag->tag_id])) {
					$tag_data[$productTag->tag_id]['weight'] += 1;
				} else {
					$tag_data[$productTag->tag_id] = [
						'text'   => $productTag->tag->name,
						'weight' => 1,
						'link'   => Url::to([
							'/product/tag',
							'tag_id' => $productTag->tag_id,
						]),
					];
				}
			}
		}
		$tag_data = array_values($tag_data);
		return $this->render('view', [
			'categories'   => $categories,
			'dataProvider' => $dataProvider,
			'limit'        => $limit,
			'sort'         => $sort,
			'tag_data'     => $tag_data,
		]);
	}
}

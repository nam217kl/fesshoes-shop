<?php

namespace frontend\controllers;

use common\models\Cart;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CartController extends Controller {

	//todo thêm access control
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'only'  => [
					'delete',
					'add',
					'view',
				],
				'rules' => [
					[
						'actions' => [
							'view',
							'delete',
							'add',
						],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => ['post'],
					'add'    => ['post'],
				],
			],
		];
	}

	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	public function actionAdd($product_detail_id) {
		\Yii::$app->response->format = 'json';
		if (isset($_POST['quantity'])) {
			$quantity = $_POST['quantity'];
			$cart     = Cart::findOne([
				'user_id'           => \Yii::$app->user->identity->id,
				'product_detail_id' => $product_detail_id,
			]);
			if ($cart === null) {
				$cart                    = new Cart();
				$cart->user_id           = \Yii::$app->user->identity->id;
				$cart->product_detail_id = $product_detail_id;
				$cart->quantity          = $quantity;
				$cart->created_at        = time();
			} else {
				$cart->quantity += $quantity;
			}
			$cart->save();
			$carts = Cart::find()->andWhere(['user_id' => \Yii::$app->user->identity->id])->all();
			return [
				'count' => '(' . ($carts != null ? count($carts) . ' items' : 'Empty') . ')',
				'image' => $cart->productDetail->product->firstProductImage->image,
				'url'   => \yii\helpers\Url::to([
					'/product/view',
					'id' => $cart->productDetail->product_id,
				]),
				'name'  => $cart->productDetail->product->name,
				'color' => $cart->productDetail->color,
				'size'  => $cart->productDetail->size,
				'price' => $cart->productDetail->price,
			];
		}
		return [];
	}

	public function actionView() {
		if (isset($_POST['Cart'])) {
			foreach ($_POST['Cart'] as $cart_id => $cart_data) {
				$updateCart             = Cart::findOne($cart_id);
				$updateCart->attributes = $cart_data;
				$updateCart->save();
			}
		}
		$cartview = Cart::find()->andWhere(['user_id' => \Yii::$app->getUser()->id])->all();
		return $this->render('view', [
			'cartview' => $cartview,
		]);
	}

	public function actionDelete($id) {
		Cart::deleteAll(['id' => $id]);
	}
}

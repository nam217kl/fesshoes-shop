<?php

namespace frontend\controllers;

use common\models\Wishlist;
use common\models\Product;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class WishlistController extends Controller {

	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'only'  => [
					'index',
					'delete',
					'toggle',
				],
				'rules' => [
					[
						'actions' => [
							'index',
							'delete',
							'toggle',
						],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::class,
				'actions' => [
					'delete' => ['post'],
					'toggle' => ['post'],
				],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actionIndex() {
		$wishlists = Wishlist::find()->where(['user_id' => \Yii::$app->getUser()->id])->all();
		return $this->render('index', [
			'wishlists' => $wishlists,
		]);
	}

	public function actionToggle($id) {
		\Yii::$app->response->format = 'json';
		$user_id                     = \Yii::$app->user->identity->id;
		$wishlist                    = Wishlist::findOne([
			'user_id'    => $user_id,
			'product_id' => $id,
		]);
		$response                    = [];
		if (empty($wishlist)) {
			$model                       = new Wishlist();
			$model->product_id = $id;
			$model->user_id    = $user_id;
			$model->save();
			$response = [
				'status'  => 200,
				'message' => 'ADD',
			];
		} else {
			$wishlist->delete();
			$response = [
				'status'  => 400,
				'message' => 'DELETE',
			];
		}
		return $response;
	}

	public function actionDelete($id) {
		Wishlist::deleteAll(['id' => $id]);
	}
}

<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle {

	public $basePath  = '@webroot';

	public $baseUrl   = '@web';

	public $css       = [
		'css/font-awesome.min.css',
		'css/icon-font.min.css',
		'css/plugins.css',
		'css/main.css',
		'css/custom.css',
		'css/color.css',
	];

	public $js        = [
		'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js',
		'js/vendor/modernizr-2.8.3.min.js',
		'js/popper.min.js',
		'js/plugins.js',
		'js/main.js',
	];

	public $jsOptions = [
		'position' => View::POS_HEAD,
	];

	public $depends   = [
		'yii\web\YiiAsset',
		'yii\bootstrap5\BootstrapAsset',
		'yii\bootstrap5\BootstrapPluginAsset',
	];
}

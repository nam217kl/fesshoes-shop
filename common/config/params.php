<?php
return [
	'uploadUrl'                     => 'http://fesshoes.shop',
	'supportEmail'                  => 'support@example.com',
	'senderEmail'                   => 'noreply@example.com',
	'senderName'                    => 'Example.com mailer',
	'user.passwordResetTokenExpire' => 3600,
	'user.passwordMinLength'        => 8,
	'bsVersion'                     => 5,
];

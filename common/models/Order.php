<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property int           $id
 * @property int|null      $user_address_id
 * @property int           $total_amount
 * @property int           $status
 *
 * @property OrderDetail[] $orderDetails
 * @property UserAddress   $userAddress
 */
class Order extends ActiveRecord {

	const STATUS_CANCELED = 0;

	const STATUS_PAID     = 1;

	const STATUS_CONFIRM  = 2;

	const STATUS_DELIVERY = 3;

	const STATUS_SUCCESS  = 4;

	const STATUS_FAIL     = 5;

	const STATUS          = [
		self::STATUS_CANCELED => 'Đơn hàng bị huỷ',
		self::STATUS_PAID     => 'Đã thanh toán',
		self::STATUS_CONFIRM  => 'Đang xác nhận đơn hàng',
		self::STATUS_DELIVERY => 'Đang giao hàng',
		self::STATUS_SUCCESS  => 'Giao hàng thành công',
		self::STATUS_FAIL     => 'Giao hàng không thành công',
	];

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%order}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'user_address_id',
					'total_amount',
					'status',
				],
				'integer',
			],
			[
				[
					'total_amount',
					'status',
				],
				'required',
			],
			[
				['user_address_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => UserAddress::class,
				'targetAttribute' => ['user_address_id' => 'id'],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'              => 'ID',
			'user_address_id' => 'User Address ID',
			'total_amount'    => 'Tổng tiền',
			'status'          => 'Status',
		];
	}

	/**
	 * Gets query for [[OrderDetails]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrderDetails() {
		return $this->hasMany(OrderDetail::class, ['order_id' => 'id']);
	}

	/**
	 * Gets query for [[UserAddress]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getUserAddress() {
		return $this->hasOne(UserAddress::class, ['id' => 'user_address_id']);
	}
}

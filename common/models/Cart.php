<?php

namespace common\models;
/**
 * This is the model class for table "{{%cart}}".
 *
 * @property int           $id
 * @property int           $user_id
 * @property int           $product_detail_id
 * @property int           $quantity
 * @property int           $created_at
 *
 * @property ProductDetail $productDetail
 * @property OrderDetail   $orderDetail
 * @property User          $user
 * @property ProductImage  $firstProductImage
 */
class Cart extends \yii\db\ActiveRecord {

	const  STATUS_DRAFT     = 'draft';

	const  STATUS_PENDING   = 'pending';

	const  STATUS_COMPLETED = 'completed';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%cart}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'product_detail_id',
					'quantity',
				],
				'required',
			],
			[
				[
					'user_id',
					'product_detail_id',
					'quantity',
					'created_at',
				],
				'integer',
			],
			[
				['product_detail_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => ProductDetail::class,
				'targetAttribute' => ['product_detail_id' => 'id'],
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::class,
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'                => 'ID',
			'user_id'           => 'User ID',
			'product_detail_id' => 'Product Detail ID',
			'quantity'          => 'Số lượng',
			'create_at'         => 'Thời gian thêm ',
		];
	}

	/**
	 * Gets query for [[ProductDetail]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductDetail() {
		return $this->hasOne(ProductDetail::class, ['id' => 'product_detail_id']);
	}

	public function getOrderDetail() {
		return $this->hasMany(ProductDetail::class, ['order_id' => 'id']);
	}

	public function getFirstProductImage() {
		return $this->hasOne(ProductImage::class, ['product_id' => 'id'])->orderBy(['position' => SORT_ASC]);
	}

	/**
	 * Gets query for [[User]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::class, ['id' => 'user_id']);
	}
}

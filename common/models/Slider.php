<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property int    $id
 * @property string $name
 * @property string $image
 * @property string $description
 * @property string $url
 * @property int    $status
 */
class Slider extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'slider';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'name',
					'image',
					'description',
					'url',
					'status',
				],
				'required',
			],
			[
				[
					'image',
					'description',
					'url',
				],
				'string',
			],
			[
				['status'],
				'integer',
			],
			[
				['name'],
				'string',
				'max' => 255,
			],
			[
				['image'],
				'file',
				'skipOnEmpty' => true,
				'extensions'  => 'png, jpg, jpeg, gif, bmp, tiff',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'          => 'ID',
			'name'        => 'Tên',
			'image'       => 'Ảnh',
			'description' => 'Mô tả',
			'url'         => 'Url',
			'status'      => 'Trạng thái',
		];
	}
}

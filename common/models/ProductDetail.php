<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%product_detail}}".
 *
 * @property int     $id
 * @property int     $product_id
 * @property string  $size
 * @property string  $color
 * @property int     $quantity
 * @property float   $price
 *
 * @property Cart[]  $carts
 * @property Product $product
 */
class ProductDetail extends ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%product_detail}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'product_id',
					'size',
					'color',
				],
				'required',
			],
			[
				[
					'product_id',
					'quantity',
				],
				'integer',
			],
			[
				[
					'price',
				],
				'number',
			],
			[
				[
					'size',
					'color',
				],
				'string',
				'max' => 255,
			],
			[
				['product_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Product::class,
				'targetAttribute' => ['product_id' => 'id'],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'product_id' => 'Product ID',
			'size'       => 'Size',
			'price'      => 'Price',
			'color'      => 'Color',
			'quantity'   => 'Quantity',
		];
	}

	/**
	 * Gets query for [[Carts]].
	 *
	 * @return ActiveQuery
	 */
	public function getCarts() {
		return $this->hasMany(Cart::class, ['product_detail_id' => 'id']);
	}

	/**
	 * Gets query for [[Product]].
	 *
	 * @return ActiveQuery
	 */
	public function getProduct() {
		return $this->hasOne(Product::class, ['id' => 'product_id']);
	}

	/**
	 * @param $insert
	 *
	 * @return bool
	 */
	public function beforeSave($insert) {
		$this->price = floor(trim(str_replace([
			'$',
			',',
		], [
			'',
			'',
		], $this->price)));
		return parent::beforeSave($insert);
	}

	/**
	 * @return bool
	 */
	public function beforeValidate() {
		$this->price = floor(trim(str_replace([
			'$',
			',',
		], [
			'',
			'',
		], $this->price)));
		return parent::beforeValidate();
	}
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_detail}}".
 *
 * @property int           $id
 * @property int           $order_id
 * @property int|null      $product_detail_id
 * @property int           $quantity
 * @property int           $amount
 *
 * @property Order         $order
 * @property ProductDetail $productDetail
 */
class OrderDetail extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%order_detail}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'order_id',
					'quantity',
					'amount',
				],
				'required',
			],
			[
				[
					'order_id',
					'product_detail_id',
					'quantity',
					'amount',
				],
				'integer',
			],
			[
				['order_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Order::class,
				'targetAttribute' => ['order_id' => 'id'],
			],
			[
				['product_detail_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => ProductDetail::class,
				'targetAttribute' => ['product_detail_id' => 'id'],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'                => 'ID',
			'order_id'          => 'Order ID',
			'product_detail_id' => 'Product Detail ID',
			'quantity'          => 'Số lượng',
			'amount'            => 'Thành tiền',
		];
	}

	/**
	 * Gets query for [[Order]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrder() {
		return $this->hasOne(Order::class, ['id' => 'order_id']);
	}

	/**
	 * Gets query for [[ProductDetail]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getProductDetail() {
		return $this->hasOne(ProductDetail::class, ['id' => 'product_detail_id']);
	}
}

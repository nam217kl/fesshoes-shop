<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_address}}".
 *
 * @property int     $id
 * @property int     $user_id
 * @property string  $fullname
 * @property string  $phone
 * @property string  $address
 * @property string  $city
 * @property string  $province
 * @property string  $country
 *
 * @property Order[] $orders
 * @property User    $user
 */
class UserAddress extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%user_address}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'fullname',
					'phone',
					'address',
					'city',
					'province',
					'country',
				],
				'required',
			],
			[
				['user_id'],
				'integer',
			],
			[
				['phone'],
				'string',
			],
			[
				[
					'fullname',
					'address',
				],
				'string',
				'max' => 1000,
			],
			[
				[
					'city',
					'province',
					'country',
				],
				'string',
				'max' => 500,
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::class,
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'       => 'ID',
			'user_id'  => 'User ID',
			'fullname' => 'Họ và tên',
			'phone'    => 'Số điện thoại',
			'address'  => 'Địa chỉ',
			'city'     => 'Thành Phố',
			'province' => 'Tỉnh',
			'country'  => 'Nước',
		];
	}

	/**
	 * Gets query for [[Orders]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrders() {
		return $this->hasMany(Order::class, ['user_address_id' => 'id']);
	}

	/**
	 * Gets query for [[User]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::class, ['id' => 'user_id']);
	}
}

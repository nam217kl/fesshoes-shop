<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%wishlist}}".
 *
 * @property int     $id
 * @property int     $product_id
 * @property int     $user_id
 *
 * @property Product $product
 * @property User    $user
 */
class Wishlist extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%wishlist}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'product_id',
					'user_id',
				],
				'required',
			],
			[
				[
					'product_id',
					'user_id',
					'created_at',
				],
				'integer',
			],
			[
				['product_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Product::class,
				'targetAttribute' => ['product_id' => 'id'],
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::class,
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}
	public function behaviors() {
		return [
			TimestampBehavior::class,
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'product_id' => 'Product ID',
			'user_id'    => 'User ID',
			'created_at' => 'Thời gian yêu thích',
		];
	}

	/**
	 * Gets query for [[Product]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getProduct() {
		return $this->hasOne(Product::class, ['id' => 'product_id']);
	}

	/**
	 * Gets query for [[User]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::class, ['id' => 'user_id']);
	}
}

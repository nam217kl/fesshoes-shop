<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "branding".
 *
 * @property int    $id
 * @property string $image
 * @property string $url
 */
class Branding extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'branding';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'image',
				],
				'required',
			],
			[
				[
					'image',
					'url',
				],
				'string',
			],
			[
				['image'],
				'file',
				'skipOnEmpty' => true,
				'extensions'  => 'png, jpg, jpeg, gif, bmp, tiff, webp, avif',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'    => 'ID',
			'image' => 'Ảnh',
			'url'   => 'Url',
		];
	}
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_image".
 *
 * @property int     $id
 * @property int     $product_id
 * @property string  $image
 * @property int     $position
 *
 * @property Product $product
 */
class ProductImage extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'product_image';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'product_id',
					'image',
					'position',
				],
				'required',
			],
			[
				[
					'product_id',
					'position',
				],
				'integer',
			],
			[
				['image'],
				'string',
			],
			[
				['product_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Product::class,
				'targetAttribute' => ['product_id' => 'id'],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'product_id' => 'Product ID',
			'image'      => 'Image',
			'position'   => 'Position',
		];
	}

	/**
	 * Gets query for [[Product]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getProduct() {
		return $this->hasOne(Product::class, ['id' => 'product_id']);
	}
}

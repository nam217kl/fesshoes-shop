<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the model class for table "category".
 *
 * @property int        $id
 * @property string     $name
 * @property int        $parent_id
 *
 * @property Category[] $categories
 * @property Category   $parent
 * @property Product[]  $products
 * @property Product[]  $allProducts
 */
class Category extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'category';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'name',
				],
				'required',
			],
			[
				['parent_id'],
				'integer',
			],
			[
				['name'],
				'string',
				'max' => 255,
			],
			[
				['parent_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Category::class,
				'targetAttribute' => ['parent_id' => 'id'],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'        => 'ID',
			'name'      => 'Name',
			'parent_id' => 'Parent ID',
		];
	}

	/**
	 * Gets query for [[Categories]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategories() {
		return $this->hasMany(Category::class, ['parent_id' => 'id']);
	}

	/**
	 * Gets query for [[Parent]].
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getParent() {
		return $this->hasOne(Category::class, ['id' => 'parent_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getProducts() {
		return $this->hasMany(Product::class, ['category_id' => 'id']);
	}

	/**
	 * @return Product[]
	 */
	public function getAllProducts() {
		$category_ids = [$this->id];
		if ($this->getCategories()->exists()) {
			foreach ($this->categories as $category) {
				$category_ids[] = $category->id;
			}
		}
		return Product::find()->andWhere(['category_id' => $category_ids])->all();
	}
}

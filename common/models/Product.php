<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;
use yii\helpers\Json;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property int             $id
 * @property int             $category_id
 * @property string          $name
 * @property string          $condition
 * @property string          $short_desc
 * @property string          $long_desc
 * @property string          $size
 * @property string          $color
 * @property string          $featured
 *
 * @property Cart[]          $carts
 * @property Category        $category
 * @property OrderDetail[]   $orderDetails
 * @property ProductDetail[] $productDetails
 * @property ProductDetail   $productPriceMax
 * @property ProductDetail   $productPriceMin
 * @property ProductDetail   $productDetail
 * @property ProductImage[]  $productImages
 * @property ProductImage    $firstProductImage
 * @property ProductTag[]    $productTags
 * @property Wishlist[]      $wishlists
 */
class Product extends ActiveRecord {

	const FEATURED_ACTIVE   = 1;

	const FEATURED_NONATIVE = 0;

	const FEATURED          = [
		self::FEATURED_ACTIVE   => 'Active',
		self::FEATURED_NONATIVE => 'Nonactive',
	];

	const CONDITION_DRAFT   = 0;

	const CONDITION_ACTIVE  = 1;

	const CONDITION         = [
		self::CONDITION_DRAFT  => 'Draft',
		self::CONDITION_ACTIVE => 'Active',
	];

	public $colors = [];

	public $sizes  = [];

	/**
	 * @var UploadedFile[]
	 */
	public $images;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%product}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				['images'],
				'file',
				'skipOnEmpty' => false,
				'extensions'  => 'png, jpg',
			],
			[
				[
					'name',
					'category_id',
					'condition',
					'short_desc',
					'long_desc',
					'featured',
				],
				'required',
			],
			[
				[
					'short_desc',
					'long_desc',
					'size',
					'color',
				],
				'string',
			],
			[
				[
					'featured',
				],
				'integer',
			],
			[
				[
					'name',
					'condition',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'name'       => 'Name',
			'condition'  => 'Condition',
			'short_desc' => 'Short Desc',
			'long_desc'  => 'Long Desc',
		];
	}

	/**
	 * Gets query for [[Carts]].
	 *
	 * @return ActiveQuery
	 */
	public function getCarts() {
		return $this->hasMany(Cart::class, ['product_id' => 'id']);
	}

	/**
	 * Gets query for [[OrderDetails]].
	 *
	 * @return ActiveQuery
	 */
	public function getOrderDetails() {
		return $this->hasMany(OrderDetail::class, ['product_id' => 'id']);
	}

	/**
	 * Gets query for [[ProductDetails]].
	 *
	 * @return ActiveQuery
	 */
	public function getProductDetails() {
		return $this->hasMany(ProductDetail::class, ['product_id' => 'id']);
	}

	/**
	 * Gets query for [[ProductDetails]].
	 *
	 * @return ActiveQuery
	 */
	public function getProductDetail() {
		return $this->hasOne(ProductDetail::class, ['product_id' => 'id']);
	}

	/**
	 * Gets query for [[ProductImages]].
	 *
	 * @return ActiveQuery
	 */
	public function getProductImages() {
		return $this->hasMany(ProductImage::class, ['product_id' => 'id']);
	}

	/**
	 * Gets query for [[ProductImages]].
	 *
	 * @return ActiveQuery
	 */
	public function getFirstProductImage() {
		return $this->hasOne(ProductImage::class, ['product_id' => 'id'])->orderBy(['position' => SORT_ASC]);
	}

	public function getProductPriceMax() {
		return $this->hasOne(ProductDetail::class, ['product_id' => 'id'])->orderBy(['price' => SORT_DESC])->limit(1);
	}

	public function getProductPriceMin() {
		return $this->hasOne(ProductDetail::class, ['product_id' => 'id'])->orderBy(['price' => SORT_ASC])->limit(1);
	}

	/**
	 * Gets query for [[ProductTags]].
	 *
	 * @return ActiveQuery
	 */
	public function getProductTags() {
		return $this->hasMany(ProductTag::class, ['product_id' => 'id']);
	}

	/**
	 * Gets query for [[Wishlists]].
	 *
	 * @return ActiveQuery
	 */
	public function getWishlists() {
		return $this->hasMany(Wishlist::class, ['product_id' => 'id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getCategory() {
		return $this->hasOne(Category::class, ['id' => 'category_id']);
	}

	/**
	 * @return void
	 */
	public function afterFind() {
		parent::afterFind();
		if ($this->size != null) {
			$this->sizes = Json::decode($this->size);
		}
		if ($this->color != null) {
			$this->colors = Json::decode($this->color);
		}
	}

	/**
	 * @return bool
	 * @throws \Throwable
	 * @throws StaleObjectException
	 */
	public function upload() {
		if ($this->validate()) {
			if ($this->images != null) {
				$oldProductImages = ProductImage::find()->andWhere(['product_id' => $this->id])->all();
				foreach ($oldProductImages as $oldProductImage) {
					unlink(\Yii::getAlias('@frontend/web/uploads/product/' . basename($oldProductImage->image)));
					$oldProductImage->delete();
				}
				foreach ($this->images as $key => $image) {
					$fileName = microtime(true) . '.' . $image->extension;
					if ($image->saveAs(\Yii::getAlias('@frontend/web/uploads/product/' . $fileName))) {
						$productImage             = new ProductImage();
						$productImage->product_id = $this->id;
						$productImage->image      = Yii::$app->params['uploadUrl'] . '/uploads/product/' . $fileName;
						$productImage->position   = $key;
						$productImage->save();
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}

	public function isInWishlist() {
		if (!Yii::$app->user->isGuest) {
			return $wishlist = Wishlist::find()->where([
				'product_id' => $this->id,
				'user_id'    => \Yii::$app->user->identity->id,
			])->all();
		}
		return false;
	}
}

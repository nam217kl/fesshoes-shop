<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int         $id
 * @property string      $username
 * @property string      $password_hash
 * @property string|null $password_reset_token
 * @property string      $verification_token
 * @property string      $email
 * @property string      $auth_key
 * @property int         $status
 * @property int         $created_at
 * @property int         $updated_at
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface {

	const STATUS_ACTIVE    = 10;

	const STATUS_NONACTIVE = 0;

	const STATUS           = [
		self::STATUS_ACTIVE    => 'Active',
		self::STATUS_NONACTIVE => 'NonActive',
	];

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'user';
	}

	/**
	 * $username = admin
	 */
	public static function findByUsername($username) {
		return self::findOne(array('username' => $username));
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				'username',
				'unique',
			],
			[
				'email',
				'unique',
			],
			[
				[
					'username',
					'password_hash',
					'email',
					'status',
				],
				'required',
			],
			[
				[
					'status',
					'created_at',
					'updated_at',
				],
				'integer',
			],
			[
				[
					'username',
					'password_hash',
					'password_reset_token',
					'verification_token',
					'email',
					'auth_key',
				],
				'string',
				'max' => 255,
			],
		];
	}

	public function behaviors() {
		return [
			TimestampBehavior::class,
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id'                   => 'ID',
			'username'             => 'Username',
			'password_hash'        => 'Password Hash',
			'password_reset_token' => 'Password Reset Token',
			'verification_token'   => 'Verification Token',
			'email'                => 'Email',
			'auth_key'             => 'Auth Key',
			'status'               => 'Status',
			'created_at'           => 'Created At',
			'updated_at'           => 'Updated At',
		];
	}

	public static function findByPasswordResetToken($token) {
		if (!static::isPasswordResetTokenValid($token)) {
			return null;
		}
		return static::findOne([
			'password_reset_token' => $token,
			'status'               => self::STATUS_ACTIVE,
		]);
	}

	public static function findIdentity($id) {
		return self::findOne($id);
	}

	public static function findIdentityByAccessToken($token, $type = null) {
		return null;
	}

	public static function findByVerificationToken($token) {
		return static::findOne([
			'verification_token' => $token,
			'status'             => self::STATUS_NONACTIVE,
		]);
	}

	public function getId() {
		return $this->id;
	}

	public function getAuthKey() {
		return $this->auth_key;
	}

	public function validateAuthKey($authKey) {
		return $authKey == $this->auth_key;
	}

	public function getWishlists() {
		return $this->hasMany(Wishlist::class, ['id' => 'user_id']);
	}

	/**
	 * @param $password
	 *
	 * @return bool
	 */
	public function validatePassword($password) {
		return Yii::$app->security->validatePassword($password, $this->password_hash);
	}

	public function setPassword($password) {
		$this->password_hash = Yii::$app->security->generatePasswordHash($password);
	}

	/**
	 * @return $this
	 */
	public function generateAuthKey() {
		$length           = 32;
		$characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString     = '';
		for ($i = 0; $i < $length; $i ++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		$this->auth_key = $randomString;
		return $this;
	}

	/**
	 * @return $this
	 */
	public function generateEmailVerificationToken() {
		$length           = 32;
		$characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString     = '';
		for ($i = 0; $i < $length; $i ++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		$this->verification_token = $randomString . '_' . time();
		return $this;
	}

	public static function isPasswordResetTokenValid($token) {
		if (empty($token)) {
			return false;
		}
		$timestamp = (int) substr($token, strrpos($token, '_') + 1);
		$expire    = Yii::$app->params['user.passwordResetTokenExpire'];
		return $timestamp + $expire >= time();
	}

	public function generatePasswordResetToken() {
		$this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
	}

	public function removePasswordResetToken() {
		$this->password_reset_token = null;
	}
}

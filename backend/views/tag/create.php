<?php
/** @var yii\web\View $this */
/** @var common\models\Tag $model */
$this->title                   = 'Create Tag';
$this->params['breadcrumbs'][] = [
	'label' => 'Tags',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $this->title ?></h3>
	</div>
	<div class="card-body pb-0">
		<div class="tag-create">
			<?= $this->render('_form', [
				'model' => $model,
			]) ?>
		</div>
	</div>
</div>

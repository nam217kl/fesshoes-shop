<?php

use common\models\Tag;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var backend\models\search\TagSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Tags';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $this->title ?></h3>
		<div class="card-tools">
			<?= Html::a('Create Tag', ['create'], ['class' => 'btn btn-success']) ?>
		</div>
	</div>
	<div class="card-body pb-0">
		<div class="tag-index">
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel'  => $searchModel,
				'columns'      => [
					[
						'attribute' => 'id',
						'options'   => ['width' => '10%'],
					],
					'name',
					[
						'options' => ['width' => '10%'],
						'label'   => 'Product count',
						'value'   => function(Tag $model) {
							return $model->getProductTags()->count();
						},
					],
					[
						'class'      => ActionColumn::className(),
						'options'    => ['width' => '40px'],
						'template'   => '{update} {delete}',
						'urlCreator' => function($action, Tag $model, $key, $index, $column) {
							return Url::toRoute([
								$action,
								'id' => $model->id,
							]);
						},
					],
				],
			]); ?>

		</div>
	</div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var common\models\Wishlist $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="wishlist-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'product_id')->textInput() ?>

	<?= $form->field($model, 'user_id')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>

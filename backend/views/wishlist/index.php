<?php

use common\models\Wishlist;
use common\models\Product;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\search\WishlistSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Danh sách yêu thích sản phẩm của khách hàng KH' . $_REQUEST['WishlistSearch']['user_id'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $this->title ?></h3>
		<div class="card-tools">
			<!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

			<!--    <p>-->
			<!--        --><? //= Html::a('Create Wishlist', ['create'], ['class' => 'btn btn-success']) ?>
			<!--    </p>-->
			<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	</div>
	<div class="card-body pb-0">
		<div class="tag-index">
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel'  => $searchModel,
				'columns'      => [
					['class' => 'yii\grid\SerialColumn'],
					[
						'attribute' => 'product_name',
						'label'     => 'Tên sản phẩm',
						'value'     => function(Wishlist $data) {
							return $data->product->name;
						},
					],
//					[
//						'attribute' => 'product_price',
//						'label'     => 'Đơn giá',
//						'value'     => function(Wishlist $data) {
//							return $data->product->price;
//						},
//					],
					[
						'attribute' => 'product_short_desc',
						'label'     => 'Mô tả',
						'value'     => function(Wishlist $data) {
							return $data->product->short_desc;
						},
					],
					'created_at:datetime',
				],
			]); ?>

		</div>
	</div>
</div>



<?php

use common\models\Slider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\search\SliderSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Sliders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $this->title ?></h3>
		<div class="card-tools">
			<?= Html::a('Create Slider', ['create'], ['class' => 'btn btn-success']) ?>
		</div>
	</div>
	<div class="card-body pb-0">
		<div class="tag-index">
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel'  => $searchModel,
				'columns'      => [
					['class' => 'yii\grid\SerialColumn'],
					'id',
					'name',
					[
						'attribute' => 'image',
						'label'     => "Ảnh",
						'format'    => 'html',
						'value'     => function($data) {
							return Html::img($data['image'], ['width' => '100px']);
						},
					],
					'description:ntext',
					'url:ntext',
					//'status',
					[
						'class'      => ActionColumn::className(),
						'urlCreator' => function($action, Slider $model, $key, $index, $column) {
							return Url::toRoute([
								$action,
								'id' => $model->id,
							]);
						},
					],
				],
			]); ?>

		</div>
	</div>
</div>

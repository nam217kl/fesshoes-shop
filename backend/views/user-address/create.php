<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var backend\models\UserAddress $model */
$this->title                   = 'Thêm mới địa chỉ khách hàng cho user ' . $model->user_id;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-address-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>

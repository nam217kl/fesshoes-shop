<?php

use backend\models\UserAddress;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\search\UserAddressSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Danh sách địa chỉ khách hàng';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $this->title ?></h3>
		<div class="card-tools">
			<?= Html::a('Create User Address', Url::toRoute([
				'user-address/create',
				'user_id' => $_REQUEST["UserAddressSearch"]["user_id"],
			]), ['class' => 'btn btn-success']) ?>
		</div>
	</div>
	<div class="card-body pb-0">
		<div class="tag-index">
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel'  => $searchModel,
				'columns'      => [
					//            'id',
					//            'user_id',
					'fullname',
					'phone:ntext',
					'address',
					'city',
					'province',
					'country',
					[
						'class'      => ActionColumn::className(),
						'urlCreator' => function($action, UserAddress $model, $key, $index, $column) {
							return Url::toRoute([
								$action,
								'id' => $model->id,
							]);
						},
					],
				],
			]); ?>

		</div>
	</div>
</div>



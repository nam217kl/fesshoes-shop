<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var backend\models\search\UserAddressSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="user-address-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

	<?= $form->field($model, 'id') ?>

	<?= $form->field($model, 'user_id') ?>

	<?= $form->field($model, 'fullname') ?>

	<?= $form->field($model, 'phone') ?>

	<?= $form->field($model, 'address') ?>

	<?php // echo $form->field($model, 'city') ?>

	<?php // echo $form->field($model, 'province') ?>

	<?php // echo $form->field($model, 'country') ?>

	<div class="form-group">
		<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>

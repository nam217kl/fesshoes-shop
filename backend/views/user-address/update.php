<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var backend\models\UserAddress $model */
$this->title                   = 'Cập nhật thông tin khách hàng ' . $model->fullname;
$this->params['breadcrumbs'][] = [
	'label' => 'User Addresses',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = [
	'label' => $model->id,
	'url'   => [
		'view',
		'id' => $model->id,
	],
];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-address-update">

	<!--    <h1>--><? //= Html::encode($this->title) ?><!--</h1>-->

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>

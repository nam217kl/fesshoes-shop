<?php

use common\models\Order;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\search\OrderSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Danh sách đơn hàng';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-primary card-outline">
	<div class="order-index">
		<div class="card-body pb-0">
			<div class="tag-index">
				<?= GridView::widget([
					'dataProvider' => $dataProvider,
					'filterModel'  => $searchModel,
					'columns'      => [
						['class' => 'yii\grid\SerialColumn'],
						//			'id',
						//			'user_address_id',
						[
							'attribute' => 'user_address_name',
							'label'     => 'Tên khách hàng',
							'value'     => function(Order $data) {
								return $data->userAddress->fullname;
							},
						],
						[
							'attribute' => 'user_address_address',
							'label'     => 'Địa chỉ',
							'value'     => function(Order $data) {
								return $data->userAddress->address . ', ' . $data->userAddress->city . ', ' . $data->userAddress->province . ', ' . $data->userAddress->country;
							},
						],
						[
							'attribute' => 'user_address_phone',
							'label'     => 'Số điện thoại',
							'value'     => function(Order $data) {
								return $data->userAddress->phone;
							},
						],
						'total_amount',
						[
							'attribute' => 'status',
							'label' => 'Trạng thái',
							'filter'    => Order::STATUS,
							'value'     => function(Order $data) {
								return Order::STATUS[$data->status];
							},
						],
						[
							'class'      => ActionColumn::className(),
							'template' => '{view}{update}',
							'buttons'    => [
								'view' => function($url, $model, $key) {
									return '<a title="Chi tiết đơn hàng" aria-label="Chi tiết đơn hàng" href="' . Url::toRoute([
											'order-detail/index',
											'OrderDetailSearch[order_id]' => $key,
										]) . '"><i class="fa fa-eye"></i></a>';
								},
							],
							'urlCreator' => function($action, Order $model, $key, $index, $column) {
								return Url::toRoute([
									$action,
									'id' => $model->id,
								]);
							},
						],
					],
				]); ?>

			</div>
		</div>
	</div>
</div>


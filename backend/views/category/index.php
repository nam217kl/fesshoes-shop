<?php

use common\models\Category;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\search\CategorySearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $this->title ?></h3>
		<div class="card-tools">
			<?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
		</div>
	</div>
	<div class="card-body pb-0">
		<div class="tag-index">
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel'  => $searchModel,
				'columns'      => [
					['class' => 'yii\grid\SerialColumn'],
//					'id',
					'name',
					[
						'attribute' => 'parent_name',
						'label'     => "Thư mục",
						'value'              => function($model) {
							$category = Category::findOne($model->parent_id);
							if ($category !== null) {
								return $category->name;
							}
						},
					],
					[
						'class'      => ActionColumn::className(),
						'urlCreator' => function($action, Category $model, $key, $index, $column) {
							return Url::toRoute([
								$action,
								'id' => $model->id,
							]);
						},
					],
				],
			]); ?>

		</div>
	</div>
</div>


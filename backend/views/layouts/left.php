<?php
/** @var View $this */

/** @var string $directoryAsset */

use dmstr\adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\web\View;

?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<?= Html::a('<img class="brand-image img-circle elevation-3" src="' . ($directoryAsset . '/img/AdminLTELogo.png') . '" alt="APP"><span class="brand-text font-weight-light">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'brand-link']) ?>
	<div class="sidebar">

		<nav class="mt-2">
			<?= Menu::widget([
				'options' => [
					'class'       => 'nav nav-pills nav-sidebar flex-column',
					'data-widget' => 'treeview',
				],
				'items'   => [
					[
						'label'  => 'Dashboard',
						'header' => true,
					],
					[
						'icon'  => 'users',
						'label' => 'Quản lý admin',
						'url'   => ['admin/index'],
					],
					[
						'icon'  => 'user',
						'label' => 'Quản lý khách hàng',
						'url'   => ['user/index'],
					],
					[
						'icon'  => 'university',
						'label' => 'Quản lý Brand',
						'url'   => ['branding/index'],
					],
					[
						'icon'  => 'sliders-h',
						'label' => 'Quản lý Slider',
						'url'   => ['slider/index'],
					],
					[
						'icon'  => 'list',
						'label' => 'Quản lý danh mục',
						'url'   => ['category/index'],
					],
					[
						'icon'  => 'shoe-prints',
						'label' => 'Quản lý sản phẩm',
						'url'   => ['product/index'],
					],
					[
						'icon'  => 'tags',
						'label' => 'Quản lý Tag',
						'url'   => ['/tag/index'],
					],
					[
						'icon'  => 'cart-plus',
						'label' => 'Quản lý Order',
						'url'   => ['/order/index'],
					],
				],
			]) ?>
		</nav>

	</div>

</aside>

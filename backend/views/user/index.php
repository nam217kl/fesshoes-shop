<?php

use common\models\User;
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\search\UserSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			//			'id',
			'username',
			//            'password_hash',
			//            'password_reset_token',
			//            'verification_token',
			'email:email',
			//			            'auth_key',
			[
				'attribute' => 'status',
				//truyền format html vào để hiện html
				'format'    => 'html',
				'filter'    => User::STATUS,
				'value'     => function(User $data) {
					return Html::tag('p', User::STATUS[$data->status], ['class' => 'badge badge-' . ($data->status == User::STATUS_ACTIVE ? 'success' : 'danger')]);
				},
			],
			[
				'attribute' => 'created_at',
				'format'    => 'datetime',
				'filter'    => DateRangePicker::widget([
					'name'           => 'SearchCreated',
					'hideInput'      => true,
					'useWithAddon'   => true,
					'convertFormat'  => true,
//					'presetDropdown' => true,
					'startAttribute' => 'UserSearch'.'[created_from_date]',
					'endAttribute'   => 'UserSearch'.'[created_to_date]',
					'pluginOptions'  => [
						'locale'               => ['format' => 'Y-m-d'],
						'showCustomRangeLabel' => true,
						'initRangeExpr'        => true,
						'alwaysShowCalendars'  => true,
					],
				]),
			],
			[
				'attribute' => 'updated_at',
				'format'    => 'datetime',
				'filter'    => DateRangePicker::widget([
					'name'           => 'SearchUpdated',
					'hideInput'      => true,
					'useWithAddon'   => true,
					'convertFormat'  => true,
					'startAttribute' => 'UserSearch'.'[updated_from_date]',
					'endAttribute'   => 'UserSearch'.'[updated_to_date]',
					'pluginOptions'  => [
						'locale'               => ['format' => 'Y-m-d'],
						'showCustomRangeLabel' => true,
						'initRangeExpr'        => true,
						'alwaysShowCalendars'  => true,
					],
				]),
			],
			[
				'class'      => ActionColumn::className(),
				'template'   => '{view}{viewwishlist}{viewcart}{update}{delete}',
				//				'contentOptions' => ['style' => 'padding:500px'],
				'buttons'    => [
					//todo Tiến chỗ này custom lại cái nút view từ icon con mắt sang icon tòa nhà
					'view'         => function($url, $model, $key) {
						return '<a title="Danh sách địa chỉ khách hàng" aria-label="Danh sách địa chỉ khách hàng" href="' . Url::toRoute([
								'user-address/index',
								'UserAddressSearch[user_id]' => $key,
							]) . '"><i class="fa fa-list"></i></a>';
					},
					'viewwishlist' => function($url, $model, $key) {
						return '<a title="Danh sách wishlist" aria-label="Danh sách wishlist" href="' . Url::toRoute([
								'wishlist/index',
								'WishlistSearch[user_id]' => $key,
							]) . '"><i class="fa fa-heart"></i></a>';
					},
					'viewcart'     => function($url, $model, $key) {
						return '<a title="Danh sách wishlist" aria-label="Danh sách wishlist" href="' . Url::toRoute([
								'cart/index',
								'CartSearch[user_id]' => $key,
							]) . '"><i class="fa fa-shopping-cart"></i></a>';
					},
				],
				'urlCreator' => function($action, User $model, $key, $index, $column) {
					return Url::toRoute([
						$action,
						'id' => $model->id,
					]);
				},
			],
		],
	]); ?>

</div>

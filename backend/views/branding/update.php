<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Branding $model */
$this->title                   = 'Update Branding: ' . $model->id;
$this->params['breadcrumbs'][] = [
	'label' => 'Brandings',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = [
	'label' => $model->id,
	'url'   => [
		'view',
		'id' => $model->id,
	],
];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="branding-update">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>

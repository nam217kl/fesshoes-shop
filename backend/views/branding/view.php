<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var common\models\Branding $model */
$this->title                   = $model->id;
$this->params['breadcrumbs'][] = [
	'label' => 'Brandings',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="branding-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Update', [
			'update',
			'id' => $model->id,
		], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Delete', [
			'delete',
			'id' => $model->id,
		], [
			'class' => 'btn btn-danger',
			'data'  => [
				'confirm' => 'Are you sure you want to delete this item?',
				'method'  => 'post',
			],
		]) ?>
	</p>

	<?= DetailView::widget([
		'model'      => $model,
		'attributes' => [
			'id',
			[
				'attribute' => 'image',
				'label'     => "Ảnh",
				'format'    => 'html',
				'value'     => function($data) {
					return Html::img($data['image'], ['width' => '200px']);
				},
			],
			'url:ntext',
		],
	]) ?>

</div>

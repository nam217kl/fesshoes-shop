<?php

use common\models\Branding;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\search\BrandingSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Brandings';
$this->params['breadcrumbs'][] = $this->title;
?>
	<div class="card card-primary card-outline">
		<div class="card-header">
			<h3 class="card-title"><?= $this->title ?></h3>
			<div class="card-tools">
				<?= Html::a('Create Brand', ['create'], ['class' => 'btn btn-success']) ?>
			</div>
		</div>
		<div class="card-body pb-0">
			<div class="tag-index">
				<?= GridView::widget([
					'dataProvider' => $dataProvider,
					'filterModel'  => $searchModel,
					'columns'      => [
						['class' => 'yii\grid\SerialColumn'],
						'id',
						[
							'attribute' => 'image',
							'label'     => "Ảnh",
							'format'    => 'html',
							'value'     => function($data) {
								return Html::img($data['image'], ['width' => '100px']);
							},
						],
						'url:ntext',
						[
							'class'      => ActionColumn::className(),
							'urlCreator' => function($action, Branding $model, $key, $index, $column) {
								return Url::toRoute([
									$action,
									'id' => $model->id,
								]);
							},
						],
					],
				]); ?>

			</div>
		</div>
	</div>

<?php

use common\models\OrderDetail;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\search\OrderDetailSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Chi tiết đơn hàng';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-primary card-outline">
	<div class="order-detail-index">
		<div class="card-body pb-0">
			<div class="tag-index">
				<?= GridView::widget([
					'dataProvider' => $dataProvider,
					'filterModel'  => $searchModel,
					'columns'      => [
						['class' => 'yii\grid\SerialColumn'],
						//						'id',
						//						'order_id',
						[
							'attribute' => 'product_detail_name',
							'label'     => 'Tên sản phẩm',
							'value'     => function(OrderDetail $data) {
								return $data->productDetail->product->name;
							},
						],
						[
							'attribute' => 'product_detail_color',
							'label'     => 'Màu',
							'value'     => function(OrderDetail $data) {
								return $data->productDetail->color;
							},
						],
						[
							'attribute' => 'product_detail_size',
							'label'     => 'Size',
							'value'     => function(OrderDetail $data) {
								return $data->productDetail->size;
							},
						],
						[
							'attribute' => 'product_detail_price',
							'label'     => 'Giá',
							'value'     => function(OrderDetail $data) {
								return $data->productDetail->price;
							},
						],
						//						'product_detail_id',
						'quantity',
						'amount',
						//						[
						//							'class'      => ActionColumn::className(),
						//							'urlCreator' => function($action, OrderDetail $model, $key, $index, $column) {
						//								return Url::toRoute([
						//									$action,
						//									'id' => $model->id,
						//								]);
						//							},
						//						],
					],
				]); ?>
			</div>
		</div>
	</div>

</div>

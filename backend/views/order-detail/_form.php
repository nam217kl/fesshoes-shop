<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var common\models\OrderDetail $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="order-detail-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'order_id')->textInput() ?>

	<?= $form->field($model, 'product_id')->textInput() ?>

	<?= $form->field($model, 'size')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'price')->textInput() ?>

	<?= $form->field($model, 'quantity')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>

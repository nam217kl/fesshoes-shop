<?php

use common\models\Cart;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var backend\models\search\CartSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Giỏ hàng của khách hàng KH' . $_REQUEST["CartSearch"]["user_id"];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cart-index">

	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			//
			//            'id',
			//            'user_id',
			//            'product_detail_id',
			[
				'attribute' => 'product_name',
				'label'     => 'Tên sản phẩm',
				'value'     => function(Cart $data) {
					return $data->productDetail->product->name;
				},
			],
			[
				'attribute' => 'product_color',
				'label'     => 'Màu sản phẩm',
				'value'     => function(Cart $data) {
					return $data->productDetail->color;
				},
			],
			[
				'attribute' => 'product_size',
				'label'     => 'Size',
				'value'     => function(Cart $data) {
					return $data->productDetail->size;
				},
			],
			[
				'attribute' => 'product_price',
				'label'     => 'Đơn giá',
				'value'     => function(Cart $data) {
					return $data->productDetail->price;
				},
			],
			'quantity',
			[
				'attribute' => 'product_total_amount',
				'label'     => 'Thành tiền',
				'value'     => function(Cart $data) {
					return $data->productDetail->price * $data->quantity;
				},
			],
			'created_at:datetime',
			//            [
			//                'class' => ActionColumn::className(),
			//                'urlCreator' => function ($action, Cart $model, $key, $index, $column) {
			//                    return Url::toRoute([$action, 'id' => $model->id]);
			//                 }
			//            ],
		],
	]); ?>

</div>

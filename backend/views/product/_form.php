<?php

use common\models\Category;
use common\models\Product;
use common\models\Tag;
use common\models\ProductImage;
use kartik\depdrop\DepDrop;
use kartik\editors\Summernote;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;

/** @var yii\web\View $this */
/** @var common\models\Product $model */
/** @var common\models\Tag $model_tag */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="product-form">

	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	<?= Html::hiddenInput('continue', 0) ?>
	<div class="row">
		<div class="col-4">
			<?php echo $form->field($model, 'category_id_1')->dropDownList(ArrayHelper::map(Category::find()->andWhere("parent_id IS NULL")->all(), 'id', 'name'), [
				'id'       => 'cat-id',
				'prompt'   => 'Please select',
				'required' => true,
			])->label('Parent category');
			?>
		</div>
		<div class="col-4">
			<?php
			echo $form->field($model, 'category_id_2')->widget(DepDrop::class, [
				'type'           => DepDrop::TYPE_SELECT2,
				'data'           => ArrayHelper::map(Category::find()->all(), 'id', 'name'),
				'options'        => [
					'id'          => 'subcat1-id',
					'placeholder' => 'Select ...',
					'required'    => false,
				],
				'select2Options' => ['pluginOptions' => ['allowClear' => true]],
				'pluginOptions'  => [
					'depends' => ['cat-id'],
					'url'     => Url::to(['/product/subcat1']),
					'params'  => [
						'input-type-1',
						'input-type-2',
					],
				],
			])->label('Sub category');
			?>
		</div>
		<div class="col-4">
			<?php
			$dataTags = Tag::find()->all();
			$data     = ArrayHelper::map($dataTags, 'id', 'name');
			echo $form->field($model, 'tag_name')->widget(Select2::className(), [
				'data'          => $data,
				'options'       => [
					'multiple'    => true,
					'placeholder' => 'Search for a tag...',
				],
				'pluginOptions' => [
					'tags'               => true,
					'allowClear'         => true,
					'minimumInputLength' => 1,
					'language'           => [
						'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
					],
					'ajax'               => [
						'url'      => Url::to(['tag/tag-list']),
						'dataType' => 'json',
						'data'     => new JsExpression('function(params) { return {q:params.term}; }'),
					],
					'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
					'templateResult'     => new JsExpression('function(name) { return name.text; }'),
					'templateSelection'  => new JsExpression('function (name) { return name.text; }'),
				],
			])
			?>
		</div>
	</div>

	<div class="row">
		<div class="col-8">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-2">
			<?= $form->field($model, 'condition')->dropDownList(Product::CONDITION) ?>
		</div>
		<div class="col-2">
			<?= $form->field($model, 'featured')->dropDownList(Product::FEATURED) ?>
		</div>
	</div>

	<?= $form->field($model, 'images')->widget(FileInput::classname(), [
		'options'       => [
			'multiple' => true,
			'accept'   => 'image/*',
		],
		'pluginOptions' => [
			'initialPreview'       => array_values(ArrayHelper::map(ProductImage::find()->andWhere(['product_id' => $model->id])->all(), 'id', 'image')),
			'initialPreviewAsData' => true,
			'browseClass'          => 'btn btn-success',
			'uploadClass'          => 'btn btn-info',
			'showRemove'           => true,
			'showUpload'           => false,
		],
	]); ?>

	<?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

	<?= $form->field($model, 'long_desc')->widget(Summernote::class, [
		'useKrajeePresets' => true,
	]) ?>

	<div class="form-group">
		<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
		<?= Html::button('Save & Add Detail', [
			'class' => 'btn btn-primary btn-save-continue',
		]) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
<script>
	$(document).on('click', '.btn-save-continue', function(e) {
		e.preventDefault();
		let th   = $(this);
		let form = th.closest('form');
		form.find('[name="continue"]').val(1);
		form.submit();
	})
</script>

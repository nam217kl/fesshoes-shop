<?php
/**
 * @var ProductDetail $productDetail
 */

use backend\models\ProductDetail;
use yii\helpers\Html;

?>
<div class="row">
	<div class="col-3">
		<div class="form-group field-product-detail-color">
			<label class="control-label" for="<?= 'product-detail-color-' . $productDetail->id ?>">Color</label>
			<?= Html::activeTextInput($productDetail, 'color', [
				'class'    => 'form-control',
				'readonly' => true,
				'name'     => 'ProductDetail[' . $productDetail->id . '][color]',
				'id'       => 'product-detail-color-' . $productDetail->id,
			]) ?>
		</div>
	</div>
	<div class="col-3">
		<div class="form-group field-product-detail-size">
			<label class="control-label" for="<?= 'product-detail-size-' . $productDetail->id ?>">Size</label>
			<?= Html::activeTextInput($productDetail, 'size', [
				'class'    => 'form-control',
				'readonly' => true,
				'name'     => 'ProductDetail[' . $productDetail->id . '][size]',
				'id'       => 'product-detail-size-' . $productDetail->id,
			]) ?>
		</div>
	</div>
	<div class="col-3">
		<div class="form-group field-product-detail-quantity">
			<label class="control-label" for="<?= 'product-detail-quantity-' . $productDetail->id ?>">Quantity</label>
			<?= Html::activeTextInput($productDetail, 'quantity', [
				'class' => 'form-control',
				'name'  => 'ProductDetail[' . $productDetail->id . '][quantity]',
				'type'  => 'number',
				'id'    => 'product-detail-quantity-' . $productDetail->id,
			]) ?>
		</div>
	</div>
	<div class="col-3">
		<div class="form-group field-product-detail-price">
			<label class="control-label" for="<?= 'product-detail-price-' . $productDetail->id ?>">Price</label>
			<?= Html::activeTextInput($productDetail, 'price', [
				'class' => 'form-control inputmask',
				'name'  => 'ProductDetail[' . $productDetail->id . '][price]',
				'id'    => 'product-detail-price-' . $productDetail->id,
			]) ?>
		</div>
	</div>
</div>


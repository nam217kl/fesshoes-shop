<?php
/**
 * @var View            $this
 * @var Product         $product
 * @var ProductDetail[] $productDetails
 */

use backend\models\Product;
use backend\models\ProductDetail;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInputAsset;

MaskedInputAsset::register($this);
$this->registerJs("$('.inputmask').inputmask({alias: 'currency'});");
$this->title = 'Manage store of ' . $product->name;
?>
<?php $form = ActiveForm::begin(); ?>
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $this->title ?></h3>
	</div>
	<div class="card-body pb-0">
		<div class="row">
			<div class="col-6">
				<?= $form->field($product, 'colors')->widget(Select2::class, [
					'options'       => [
						'multiple' => 'true',
					],
					'pluginOptions' => [
						'tags' => true,
					],
					'pluginEvents'  => [
						'change' => 'function(event) {changeColor(event);}',
					],
				]) ?>
			</div>
			<div class="col-6">
				<?= $form->field($product, 'sizes')->widget(Select2::class, [
					'options'       => [
						'multiple' => 'true',
					],
					'pluginOptions' => [
						'tags' => true,
					],
					'pluginEvents'  => [
						'change' => 'function(event) {changeSize(event);}',
					],
				]) ?>
			</div>
		</div>
		<hr>
	</div>
</div>
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title">Detail of store</h3>
	</div>
	<div class="card-body pb-0">
		<div class="product-detail">
			<?php foreach ($productDetails as $productDetail) : ?>
				<?= $this->render('_detail', ['productDetail' => $productDetail]) ?>
			<?php endforeach; ?>
		</div>
		<div class="form-group mb-4">
			<?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
		</div>
	</div>
</div>
<?php ActiveForm::end(); ?>
<script>
	let colors = <?=Json::encode($product->colors)?>,
		sizes  = <?=Json::encode($product->sizes)?>;

	function changeColor(event) {
		let target = $(event.target);
		colors     = target.val();
		generate(colors, sizes);
	}

	function changeSize(event) {
		let target = $(event.target);
		sizes      = target.val();
		generate(colors, sizes);
	}

	function generate(colors, sizes) {
		if(colors !== null && sizes !== null) {
			$.ajax({
				type   : 'post',
				cache  : false,
				data   : {
					color: colors,
					size : sizes
				},
				url    : '<?=Url::to([
					'/product/detail-update',
					'id' => $product->id,
				])?>',
				success: function(response) {
					$(".product-detail").html(response);
					$(".inputmask").inputmask({alias: 'currency'});
				}
			})
		}
	}

</script>

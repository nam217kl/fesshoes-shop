<?php
/** @var yii\web\View $this */
/** @var common\models\Product $model */
$this->title                   = 'Update Product: ' . $model->name;
$this->params['breadcrumbs'][] = [
	'label' => 'Products',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = [
	'label' => $model->name,
	'url'   => [
		'view',
		'id' => $model->id,
	],
];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $this->title ?></h3>
	</div>
	<div class="card-body pb-0">
		<?= $this->render('_form', [
			'model' => $model,
		]) ?>

	</div>
</div>

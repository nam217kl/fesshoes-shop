<?php

use backend\models\Product;
use common\helpers\StringHelper;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var backend\models\search\ProductSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */
$this->title                   = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card card-primary card-outline">
	<div class="card-header">
		<h3 class="card-title"><?= $this->title ?></h3>
		<div class="card-tools">
			<?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
		</div>
	</div>
	<div class="card-body pb-0">
		<div class="tag-index">
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel'  => $searchModel,
				'columns'      => [
					['class' => 'yii\grid\SerialColumn'],
					'name',
					[
						'attribute' => 'color',
						'format'    => 'raw',
						'value'     => function(Product $data) {
							$response = '';
							foreach ($data->colors as $color) {
								$response .= Html::tag('span', $color, ['class' => 'product-color ' . StringHelper::removeSign($color)]) . '&nbsp;';
							}
							return $response;
						},
					],
					[
						'attribute' => 'size',
						'format'    => 'raw',
						'value'     => function(Product $data) {
							$response = '';
							foreach ($data->sizes as $size) {
								$response .= Html::tag('span', $size, ['class' => 'badge badge-success']) . '&nbsp;';
							}
							return $response;
						},
					],
					[
						'attribute' => 'condition',
						'filter'    => Product::CONDITION,
						'format'    => 'html',
						'value'     => function(Product $data) {
							return Html::tag('p', Product::CONDITION[$data->condition], ['class' => 'badge badge-' . ($data->condition == Product::CONDITION_ACTIVE ? 'success' : 'danger')]);
						},
					],
					[
						'attribute' => 'featured',
						'filter'    => Product::FEATURED,
						'format'    => 'raw',
						'value'     => function(Product $data) {
							return Html::button(Product::FEATURED[$data->featured], [
								'data-id' => $data->id,
								'class'   => 'btn-change-featured btn btn-' . ($data->featured == Product::FEATURED_ACTIVE ? 'primary' : 'warning'),
							]);
						},
					],
					[
						'class'      => ActionColumn::className(),
						'template'   => '{detail} {view} {update} {delete}',
						'buttons'    => [
							'detail' => function($url, $model, $key) {
								return Html::a('<i class="fa fa-list"></i>', $url);
							},
						],
						'urlCreator' => function($action, Product $model, $key, $index, $column) {
							return Url::toRoute([
								$action,
								'id' => $model->id,
							]);
						},
					],
				],
			]); ?>

		</div>
	</div>
</div>
<script>
	$(document).on('click', '.btn-change-featured', function() {
		let th = $(this);
		$.ajax({
			type   : "GET",
			data   : "id=" + th.attr('data-id'),
			cache  : false,
			url    : '<?=Url::to(['/product/change-featured'])?>',
			success: function(response) {
				if(response == 1) {
					if(th.hasClass('btn-primary')) {
						th.removeClass('btn-primary').addClass('btn-warning').html('Nonactive');
					} else {
						th.addClass('btn-primary').removeClass('btn-warning').html('Active');
					}
				}
			}
		})
	});
</script>

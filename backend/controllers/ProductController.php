<?php

namespace backend\controllers;

use backend\models\Product;
use backend\models\ProductDetail;
use backend\models\search\ProductSearch;
use common\models\Category;
use common\models\ProductTag;
use common\models\Tag;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller {

	/**
	 * @inheritDoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'only'  => [
					'create',
					'update',
					'index',
					'view',
					'detail',
					'delete',
					'change-featured',
				],
				'rules' => [
					// allow authenticated users
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * Lists all Product models.
	 *
	 * @return string
	 */
	public function actionIndex() {
		$searchModel  = new ProductSearch();
		$dataProvider = $searchModel->search($this->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Product model.
	 *
	 * @param int $id ID
	 *
	 * @return string
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Product model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return string|Response
	 */
	public function actionCreate() {
		$model = new Product();
		if ($this->request->isPost) {
			if ($model->load($this->request->post()) && $model->save()) {
				foreach ($_POST['Product']['tag_name'] as $tag_name) {
					$tag = Tag::find()->andWhere(['id' => $tag_name])->one();
					if ($tag === null) {
						$tag       = new Tag();
						$tag->name = trim($tag_name);
						$tag->save();
					}
					$productTag             = new ProductTag();
					$productTag->product_id = $model->id;
					$productTag->tag_id     = $tag->id;
					$productTag->save();
				}
				$model->images = UploadedFile::getInstances($model, 'images');
				$model->upload();
				if (isset($_POST['continue']) && trim($_POST['continue']) == 1) {
					return $this->redirect([
						'/product/detail',
						'id' => $model->id,
					]);
				} else {
					return $this->redirect(['index']);
				}
			}
		} else {
			$model->loadDefaultValues();
		}
		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing Product model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id ID
	 *
	 * @return string|Response
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
			ProductTag::deleteAll(['product_id' => $model->id]);
			foreach ($_POST['Product']['tag_name'] as $tag_name) {
				$tag = Tag::find()->andWhere(['id' => $tag_name])->one();
				if ($tag === null) {
					$tag       = new Tag();
					$tag->name = trim($tag_name);
					$tag->save();
				}
				$productTag             = new ProductTag();
				$productTag->product_id = $model->id;
				$productTag->tag_id     = $tag->id;
				$productTag->save();
			}
			$model->images = UploadedFile::getInstances($model, 'images');
			$model->upload();
			if (isset($_POST['continue']) && trim($_POST['continue']) == 1) {
				return $this->redirect([
					'/product/detail',
					'id' => $model->id,
				]);
			} else {
				return $this->redirect(['index']);
			}
		}
		$model->tag_name = ArrayHelper::map($model->productTags, 'id', 'tag_id');
		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing Product model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param int $id ID
	 *
	 * @return Response
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Product model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param int $id ID
	 *
	 * @return Product the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Product::findOne(['id' => $id])) !== null) {
			return $model;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}

	/**
	 * @return array|string[]
	 */
	public function actionSubcat1() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if (isset($_POST['depdrop_parents'])) {
			$parents = $_POST['depdrop_parents'];
			if ($parents != null) {
				$cat_id     = $parents[0];
				$out        = [];
				$categories = Category::find()->andWhere(['parent_id' => $cat_id])->all();
				foreach ($categories as $category) {
					$out[] = [
						'id'   => $category->id,
						'name' => $category->name,
					];
				}
				$selected = $cat_id;
				return [
					'output'   => $out,
					'selected' => $selected,
				];
			}
		}
		return [
			'output'   => '',
			'selected' => '',
		];
	}

	/**
	 * @param $id
	 *
	 * @return string|Response
	 * @throws NotFoundHttpException
	 */
	public function actionDetail($id) {
		$model = $this->findModel($id);
		if ($this->request->isPost && isset($_POST['Product'], $_POST['ProductDetail'])) {
			$colors       = $_POST['Product']['colors'];
			$sizes        = $_POST['Product']['sizes'];
			$model->color = Json::encode($colors);
			$model->size  = Json::encode($sizes);
			foreach ($_POST['ProductDetail'] as $product_detail_id => $product_detail_data) {
				$productDetail             = ProductDetail::findOne($product_detail_id);
				$productDetail->attributes = $product_detail_data;
				$productDetail->save();
			}
			$model->touch('updated_at');
			$model->save();
			ProductDetail::deleteAll("product_id = " . $model->id . " AND id NOT IN (" . implode(',', array_keys($_POST['ProductDetail'])) . ")");
			Yii::$app->session->setFlash('success', 'Update store successfully for ' . $model->name);
			return $this->redirect(['index']);
		}
		return $this->render('detail', [
			'product'        => $model,
			'productDetails' => $model->productDetails,
		]);
	}

	/**
	 * @param $id
	 *
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionDetailUpdate($id) {
		$response = '';
		if (isset($_POST['color'], $_POST['size'])) {
			$productDetails = [];
			$colors         = $_POST['color'];
			$sizes          = $_POST['size'];
			$product        = $this->findModel($id);
			$product->updateAttributes([
				'color' => Json::encode($colors),
				'size'  => Json::encode($sizes),
			]);
			foreach ($colors as $color) {
				foreach ($sizes as $size) {
					$productDetail = $product->getProductDetail()->andWhere([
						'color' => $color,
						'size'  => $size,
					])->one();
					if ($productDetail == null) {
						$productDetail             = new ProductDetail();
						$productDetail->product_id = $product->id;
						$productDetail->size       = $size;
						$productDetail->color      = $color;
						$productDetail->price      = 0;
						$productDetail->save();
					}
					$productDetails[] = $productDetail;
				}
			}
			foreach ($productDetails as $productDetail) {
				$response .= $this->renderPartial('_detail', [
					'productDetail' => $productDetail,
				]);
			}
		}
		return $response;
	}

	/**
	 * @param $id
	 *
	 * @return int
	 * @throws NotFoundHttpException
	 */
	public function actionChangeFeatured($id) {
		$model = $this->findModel($id);
		return $model->updateAttributes(['featured' => !$model->featured]);
	}
}

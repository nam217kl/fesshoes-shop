<?php

namespace backend\controllers;

use backend\models\search\BrandingSearch;
use common\models\Branding;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * BrandingController implements the CRUD actions for Branding model.
 */
class BrandingController extends Controller {

	/**
	 * @inheritDoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'only'  => [
					'create',
					'update',
					'index',
					'view',
					'delete',
				],
				'rules' => [
					// allow authenticated users
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * Lists all Branding models.
	 *
	 * @return string
	 */
	public function actionIndex() {
		$searchModel  = new BrandingSearch();
		$dataProvider = $searchModel->search($this->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Branding model.
	 *
	 * @param int $id ID
	 *
	 * @return string
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Branding model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return string|Response
	 */
	public function actionCreate() {
		$model = new Branding();
		if ($model->load(Yii::$app->request->post())) {
			$model->image = UploadedFile::getInstance($model, 'image');
			$fileName     = time() . '.' . $model->image->extension;
			$model->image->saveAs(Yii::getAlias('@frontend/web/uploads/brand/' . $fileName));
			$model->image = Yii::$app->params['uploadUrl'] . '/uploads/brand/' . $fileName;
			if ($model->save()) {
				return $this->redirect([
					'view',
					'id' => $model->id,
				]);
			}
		} else {
			$model->loadDefaultValues();
		}
		return $this->render('create', [
			'model' => $model,
		]);
	}

	/**
	 * Updates an existing Branding model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param int $id ID
	 *
	 * @return string|Response
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id) {
		$model     = $this->findModel($id);
		$old_image = $model->image;
		if ($model->load(Yii::$app->request->post())) {
			$model->image = UploadedFile::getInstance($model, 'image');
			$fileName     = basename($old_image);
			$model->image->saveAs(Yii::getAlias('@frontend/web/uploads/brand/' . $fileName));
			$model->image = Yii::$app->params['uploadUrl'] . '/uploads/brand/' . $fileName;
			if ($model->save()) {
				return $this->redirect([
					'view',
					'id' => $model->id,
				]);
			}
		}
		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing Branding model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param int $id ID
	 *
	 * @return Response
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Branding model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param int $id ID
	 *
	 * @return Branding the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Branding::findOne(['id' => $id])) !== null) {
			return $model;
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}
}

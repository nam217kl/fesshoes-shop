<?php

namespace backend\models\search;

use common\models\Cart;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CartSearch represents the model behind the search form of `common\models\Cart`.
 */
class CartSearch extends Cart {

	public $product_name  = '';

	public $product_color = '';

	public $product_size  = '';

	public $product_price = '';

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'user_id',
					'product_detail_id',
					'quantity',
					'product_size',
					'product_price',
				],
				'integer',
			],
			[
				[
					'product_name',
					'product_color',
				],
				'string',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Cart::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'                => $this->id,
			'user_id'           => $this->user_id,
			'product_detail_id' => $this->product_detail_id,
			'quantity'          => $this->quantity,
		]);
		if ($this->product_name != '') {
			$query->leftJoin('product_detail', 'cart.product_detail_id = product_detail.id')->leftJoin('product', 'product_detail.product_id = product_id')->andWhere([
				'LIKE',
				'product.name',
				$this->product_name,
			]);
		}
		if ($this->product_price != '') {
			$query->leftJoin('product_detail', 'cart.product_detail_id = product_detail.id')->andWhere([
				'=',
				'product_detail.price',
				$this->product_price,
			]);
		}
		if ($this->product_color != '') {
			$query->leftJoin('product_detail', 'cart.product_detail_id = product_detail.id')->andWhere([
				'LIKE',
				'product_detail.color',
				$this->product_color,
			]);
		}
		if ($this->product_size != '') {
			$query->leftJoin('product_detail', 'cart.product_detail_id = product_detail.id')->andWhere([
				'=',
				'product_detail.size',
				$this->product_size,
			]);
		}
		return $dataProvider;
	}
}

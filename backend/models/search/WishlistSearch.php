<?php

namespace backend\models\search;

use common\models\Wishlist;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * WishlistSearch represents the model behind the search form of `common\models\Wishlist`.
 */
class WishlistSearch extends Wishlist {

	public $product_name       = '';

	public $product_price      = '';

	public $product_short_desc = '';

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'product_id',
					'user_id',
					'product_price',
				],
				'integer',
			],
			[
				[
					'product_name',
					'product_short_desc',
				],
				'string',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Wishlist::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'         => $this->id,
			'product_id' => $this->product_id,
			'user_id'    => $this->user_id,
		]);
		if ($this->product_name != '') {
			$query->leftJoin('product', 'wishlist.product_id = product.id')->andWhere([
				'LIKE',
				'product.name',
				$this->product_name,
			]);
		}
		if ($this->product_price != '') {
			$query->leftJoin('product', 'wishlist.product_id = product.id')->andWhere([
				'=',
				'product.price',
				$this->product_price,
			]);
		}
		if ($this->product_short_desc != '') {
			$query->leftJoin('product', 'wishlist.product_id = product.id')->andWhere([
				'LIKE',
				'product.short_desc',
				$this->product_short_desc,
			]);
		}
		return $dataProvider;
	}
}

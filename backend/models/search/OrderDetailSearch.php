<?php

namespace backend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrderDetail;

/**
 * OrderDetailSearch represents the model behind the search form of `common\models\OrderDetail`.
 */
class OrderDetailSearch extends OrderDetail {

	public $product_detail_name  = '';

	public $product_detail_color = '';

	public $product_detail_size  = '';

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'order_id',
					'product_detail_id',
					'quantity',
					'amount',
				],
				'integer',
			],
			[
				[
					'product_detail_name',
				],
				'string',
			],
			[
				[
					'product_detail_size',
					'product_detail_color',
				],
				'safe',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = OrderDetail::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'                => $this->id,
			'order_id'          => $this->order_id,
			'product_detail_id' => $this->product_detail_id,
			'quantity'          => $this->quantity,
			'amount'            => $this->amount,
		]);
		if ($this->product_detail_name != '') {
			$query->leftJoin('product_detail', 'product_detail.id=order_detail.product_detail_id')->leftJoin('product', 'product_detail.product_id=product.id')->andWhere([
				'like',
				'product.name',
				$this->product_detail_name,
			]);
		}
		if ($this->product_detail_color != '') {
			$query->leftJoin('product_detail', 'product_detail.id=order_detail.product_detail_id')->andWhere([
				'like',
				'product_detail.color',
				$this->product_detail_color,
			]);
		}
		if ($this->product_detail_size != '') {
			$query->leftJoin('product_detail', 'product_detail.id=order_detail.product_detail_id')->andWhere([
				'like',
				'product_detail.size',
				$this->product_detail_size,
			]);
		}
		return $dataProvider;
	}
}

<?php

namespace backend\models\search;

use backend\models\UserAddress;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserAddressSearch represents the model behind the search form of `backend\models\UserAddress`.
 */
class UserAddressSearch extends UserAddress {

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'user_id',
				],
				'integer',
			],
			[
				[
					'fullname',
					'phone',
					'address',
					'city',
					'province',
					'country',
				],
				'safe',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = UserAddress::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'      => $this->id,
			'user_id' => $this->user_id,
		]);
		$query->andFilterWhere([
			'like',
			'fullname',
			$this->fullname,
		])->andFilterWhere([
			'like',
			'phone',
			$this->phone,
		])->andFilterWhere([
			'like',
			'address',
			$this->address,
		])->andFilterWhere([
			'like',
			'city',
			$this->city,
		])->andFilterWhere([
			'like',
			'province',
			$this->province,
		])->andFilterWhere([
			'like',
			'country',
			$this->country,
		]);
		return $dataProvider;
	}
}

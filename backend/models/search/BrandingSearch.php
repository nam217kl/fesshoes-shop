<?php

namespace backend\models\search;

use common\models\Branding;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BrandingSearch represents the model behind the search form of `common\models\Branding`.
 */
class BrandingSearch extends Branding {

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				['id'],
				'integer',
			],
			[
				[
					'image',
					'url',
				],
				'safe',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Branding::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
		]);
		$query->andFilterWhere([
			'like',
			'image',
			$this->image,
		])->andFilterWhere([
			'like',
			'url',
			$this->url,
		]);
		return $dataProvider;
	}
}

<?php

namespace backend\models\search;

use common\models\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form of `common\models\User`.
 */
class UserSearch extends User {

	public $created_from_date = '';

	public $created_to_date   = '';

	public $updated_from_date = '';

	public $updated_to_date   = '';

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'status',
					'created_at',
					'updated_at',
				],
				'integer',
			],
			[
				[
					'created_to_date',
					'created_from_date',
					'updated_to_date',
					'updated_from_date',
				],
				'safe',
			],
			[
				[
					'username',
					'password_hash',
					'password_reset_token',
					'verification_token',
					'email',
					'auth_key',
				],
				'safe',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = User::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'     => $this->id,
			'status' => $this->status,
		]);
		$query->andFilterWhere([
			'like',
			'username',
			$this->username,
		])->andFilterWhere([
			'like',
			'password_hash',
			$this->password_hash,
		])->andFilterWhere([
			'like',
			'password_reset_token',
			$this->password_reset_token,
		])->andFilterWhere([
			'like',
			'verification_token',
			$this->verification_token,
		])->andFilterWhere([
			'like',
			'email',
			$this->email,
		])->andFilterWhere([
			'like',
			'auth_key',
			$this->auth_key,
		]);
		if ($this->created_from_date != '' && $this->created_to_date != '') {
			$this->created_from_date = strtotime($this->created_from_date);
			$this->created_to_date   = strtotime($this->created_to_date) + 86399;
			$query->andFilterWhere([
				'>=',
				'created_at',
				$this->created_from_date,
			]);
			$query->andFilterWhere([
				'<=',
				'created_at',
				$this->created_to_date,
			]);
		}
		if ($this->updated_from_date != '' && $this->updated_to_date != '') {
			$this->updated_from_date = strtotime($this->updated_from_date);
			$this->updated_to_date   = strtotime($this->updated_to_date) + 86399;
			$query->andFilterWhere([
				'>=',
				'updated_at',
				$this->updated_from_date,
			]);
			$query->andFilterWhere([
				'<=',
				'updated_at',
				$this->updated_to_date,
			]);
		}
		return $dataProvider;
	}
}

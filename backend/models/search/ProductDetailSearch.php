<?php

namespace backend\models\search;

use common\models\ProductDetail;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProductDetailSearch represents the model behind the search form of `common\models\ProductDetail`.
 */
class ProductDetailSearch extends ProductDetail {

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'product_id',
					'quantity',
				],
				'integer',
			],
			[
				[
					'size',
					'color',
				],
				'safe',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = ProductDetail::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'         => $this->id,
			'product_id' => $this->product_id,
			'quantity'   => $this->quantity,
		]);
		$query->andFilterWhere([
			'like',
			'size',
			$this->size,
		])->andFilterWhere([
			'like',
			'color',
			$this->color,
		]);
		return $dataProvider;
	}
}

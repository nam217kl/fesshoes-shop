<?php

namespace backend\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form of `common\models\Order`.
 */
class OrderSearch extends Order {

	public $user_address_name    = '';

	public $user_address_address = '';
	public $user_address_phone='';

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'user_address_id',
					'total_amount',
					'status',
				],
				'integer',
			],
			[
				[   'user_address_phone',
					'user_address_name',
					'user_address_address',
				],
				'string',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Order::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'              => $this->id,
			'user_address_id' => $this->user_address_id,
			'total_amount'    => $this->total_amount,
			'status'          => $this->status,
		]);
		if ($this->user_address_name != '') {
			$query->leftJoin('user_address', 'order.user_address_id = user_address.id')->andWhere([
				'LIKE',
				'user_address.fullname',
				$this->user_address_name,
			]);
		}
		if ($this->user_address_phone != '') {
			$query->leftJoin('user_address', 'order.user_address_id = user_address.id')->andWhere([
				'LIKE',
				'user_address.phone',
				$this->user_address_phone,
			]);
		}
		if ($this->user_address_address != '') {
			$query->leftJoin('user_address', 'order.user_address_id = user_address.id')->andWhere([
				'LIKE',
				'user_address.address',
				$this->user_address_address,
			])->orWhere([
				'LIKE',
				'user_address.city',
				$this->user_address_address,
			])->orWhere([
				'LIKE',
				'user_address.province',
				$this->user_address_address,
			])->orWhere([
				'LIKE',
				'user_address.country',
				$this->user_address_address,
			]);
		}
		return $dataProvider;
	}
}

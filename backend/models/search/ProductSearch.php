<?php

namespace backend\models\search;

use frontend\models\Product;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProductSearch represents the model behind the search form of `common\models\Product`.
 */
class ProductSearch extends Product {

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
				],
				'integer',
			],
			[
				[
					'name',
					'condition',
					'short_desc',
					'long_desc',
					'color',
					'size',
					'featured',
				],
				'safe',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Product::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
		]);
		$query->andFilterWhere([
			'like',
			'name',
			$this->name,
		])->andFilterWhere([
			'like',
			'condition',
			$this->condition,
		])->andFilterWhere([
			'like',
			'short_desc',
			$this->short_desc,
		])->andFilterWhere([
			'like',
			'long_desc',
			$this->long_desc,
		])->andFilterWhere([
			'like',
			'color',
			$this->color,
		])->andFilterWhere([
			'like',
			'size',
			$this->size,
		])->andFilterWhere([
			'like',
			'featured',
			$this->featured,
		]);
		return $dataProvider;
	}
}

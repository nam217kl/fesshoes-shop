<?php

namespace backend\models\search;

use common\models\Category;
use PHPUnit\Framework\Constraint\ArrayHasKey;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * CategorySearch represents the model behind the search form of `common\models\Category`.
 */
class CategorySearch extends Category {

	public $parent_name = '';

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'parent_id',
				],
				'integer',
			],
			[
				[
					'name',
					'parent_name',
				],
				'safe',
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Category::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		$this->load($params);
		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
		]);
		$query->andFilterWhere([
			'like',
			'name',
			$this->name,
		]);
		if ($this->parent_name != '') {
			$categories  = Category::find()->andWhere([
				'like',
				'name',
				$this->parent_name,
			])->andWhere("parent_id IS NULL")->all();
			$category_id = ArrayHelper::map($categories, 'id', 'id');
			$query->andFilterWhere([
				'parent_id' => $category_id,
			])->andWhere("parent_id IS NOT NULL");
		}
		return $dataProvider;
	}
}

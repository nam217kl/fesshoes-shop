<?php

namespace backend\models;

use common\models\ProductDetail;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;

/**
 * @property ProductDetail[] $productDetails
 * @property ProductDetail   $productDetail
 */
class Product extends \common\models\Product {

	public $category_id_1 = null;

	public $category_id_2 = null;
	public $tag_name= '';

	/**
	 * @return array
	 */
	public function rules() {
		return [
			[
				[
					'name',
					'condition',
					'short_desc',
					'long_desc',
					'category_id_1',
					'tag_name',
				],
				'required',
			],
			[
				[
					'short_desc',
					'long_desc',
					'size',
					'color',
				],
				'string',
			],
			[
				[
					'category_id_1',
					'category_id_2',
					'category_id',
				],
				'integer',
			],
			[
				[
					'name',
					'condition',
				],
				'string',
				'max' => 255,
			],
			[
				[
					'created_at',
					'updated_at',
					'featured',
				],
				'integer',
			],
			[
				[
					'tag_name'
				],
				'safe'
			],
		];
	}

	public function behaviors() {
		return [
			TimestampBehavior::class,
		];
	}

	/**
	 * @return void
	 */
	public function afterFind() {
		parent::afterFind();
		if (($category = $this->category) !== null) {
			if ($category->parent_id != null) {
				$this->category_id_2 = $this->category_id;
				$this->category_id_1 = $category->parent_id;
			} else {
				$this->category_id_1 = $this->category_id;
			}
		}
	}

	/**
	 * @param $insert
	 *
	 * @return bool
	 */
	public function beforeSave($insert) {
		if ($this->category_id_1 != null) {
			$this->category_id = $this->category_id_2 != null ? $this->category_id_2 : $this->category_id_1;
		}
		return parent::beforeSave($insert);
	}

	/**
	 * Gets query for [[ProductDetails]].
	 *
	 * @return ActiveQuery
	 */
	public function getProductDetails() {
		return $this->hasMany(ProductDetail::class, ['product_id' => 'id']);
	}

	/**
	 * Gets query for [[ProductDetails]].
	 *
	 * @return ActiveQuery
	 */
	public function getProductDetail() {
		return $this->hasOne(ProductDetail::class, ['product_id' => 'id']);
	}
}
